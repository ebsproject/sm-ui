const path = require('path');
const { merge } = require('webpack-merge');
const singleSpaDefaults = require('webpack-config-single-spa-react');
const DotenvWebpackPlugin = require('dotenv-webpack');

module.exports = (webpackConfigEnv, argv) => {
  const defaultConfig = singleSpaDefaults({
    orgName: 'ebs',
    projectName: 'sm',
    webpackConfigEnv,
    argv,
  });

  defaultConfig.externals.push(
    '@ebs/cs',
    '@ebs/styleguide',
    '@ebs/components',
    '@ebs/md',
  );

  return merge(defaultConfig, {
    resolve: {
      alias: {
        assets: path.resolve(__dirname, './src/assets/'),
        components: path.resolve(__dirname, './src/components/'),
        helpers: path.resolve(__dirname, './src/helpers/'),
        pages: path.resolve(__dirname, './src/pages/'),
        stores: path.resolve(__dirname, './src/stores/'),
        utils: path.resolve(__dirname, './src/utils/'),
      },
      extensions: ['.js', '.jsx'],
    },
    plugins: [new DotenvWebpackPlugin()],
  });
};