/** @type {import('jest').Config} */
const config = {
  collectCoverageFrom: ['src/**/*.{js,jsx}', '!**/index.js', '!**/*.test.{js,jsx}'],
  coverageDirectory: 'coverage/',
  coverageReporters: ['json', 'html'],
  moduleDirectories: ['node_modules', 'src'],
  modulePathIgnorePatterns: ['/cache', '/dist'],
  moduleNameMapper: {
    '\\.(css)$': 'identity-obj-proxy',
    'single-spa-react/parcel': 'single-spa-react/lib/cjs/parcel.cjs',
    '@ebs/layout': '<rootDir>/__mocks__/layout.js',
    '@ebs/cs': '<rootDir>/__mocks__/core.js',
    '@ebs/styleguide': '<rootDir>/__mocks__/styleguide.js',
    '@ebs/components': '<rootDir>/__mocks__/components.js',
    '^assets/(.*)$': '<rootDir>/src/assets/$1',
    '^components/(.*)$': '<rootDir>/src/components/$1',
    '^helpers/(.*)$': '<rootDir>/src/helpers/$1',
    '^pages/(.*)$': '<rootDir>/src/pages/$1',
    '^stores/(.*)$': '<rootDir>/src/stores/$1',
    '^utils/(.*)$': '<rootDir>/src/utils/$1',
  },
  testEnvironment: 'jsdom',
  transform: {
    '^.+\\.(j|t)sx?$': 'babel-jest',
    '.+\\.(css|scss|png|jpg|svg)$': 'jest-transform-stub',
  },
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  verbose: true,
};

module.exports = config;
