ARG baseImage=ebsproject/cs-mfe-server-base
ARG baseImageTag=21.08.27
FROM ${baseImage}:${baseImageTag}
LABEL maintainer="Kenichii A. Ana <k.ana@irri.org> & Renee Arianne F. Lat <r.lat@irri.org>"
ARG sourceDir=dist
ARG libVersion=dev
ARG libMainFile=ebs-sm.js
ARG nginxDir=/usr/share/nginx/html
ARG importMapFilename=app.importmap
RUN apt-get update && apt install -y jq
COPY ${sourceDir}/ ${nginxDir}/${libVersion}/
RUN moduleName=${libVersion}; \
    moduleUrl=${libVersion}/${libMainFile}; \
    echo $(cat ${nginxDir}/${importMapFilename} | \
    jq --arg moduleName "$moduleName" \ 
    --arg moduleUrl "$moduleUrl" '.imports[$moduleName] = $moduleUrl') > ${nginxDir}/app.importmap && \
    moduleName=${libVersion}/; \
    moduleUrl=${libVersion}/; \
    echo $(cat ${nginxDir}/${importMapFilename} | \
    jq --arg moduleName "$moduleName" \
    --arg moduleUrl "$moduleUrl" '.imports[$moduleName] = $moduleUrl') > ${nginxDir}/app.importmap