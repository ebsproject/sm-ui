import React from 'react';
import PropTypes from 'prop-types';

import LoadingGear from 'components/atoms/LoadingGear';

function UnderConstructionViewView(props) {
  // Props

  /*
  @prop data-testid: Id to use inside underconstructionview.test.js file.
 */
  return (
    <div>
      <br />
      <LoadingGear message={'This page is under construction at the moment.'} />
    </div>
  );
}
// Type and required properties
UnderConstructionViewView.propTypes = {};

export default UnderConstructionViewView;