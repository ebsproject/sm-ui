import React from 'react'
import ReactTable from "react-table";
import "react-table/react-table.css";

import PropTypes from 'prop-types'
import Pagination from "components/molecules/Pagination/Pagination"
import StatusActionDesign from "components/molecules/StatusActionDesign"

// TODO: to delete
export default function DesignView(props) {
  // Props
  const { property, Function, ...rest } = props

  React.useEffect(()=>{props.getBatchListResult()},[])
  /* This will be rendered in View
  @prop data-testid: Id to use inside design.test.js file.
 */
  return (
    <div   data-testid={'DesignTestId'}> 
    <ReactTable 
        width ="85%"
        filterable
        data={props.data}
        columns={[
          {
            Header: () => <strong>Actions</strong>,
            Cell: row => (
              <div>
                <StatusActionDesign row={row} actionNext={props.nextStageBatch}
                    actionReject={props.rejectedSatge} actionDownload={props.createLaboratoryReport}  
                    actionSaveDesign={props.saveDesignBatch} 
                    />
              </div>
            ),
            style: {
              cursor: "pointer",
              fontSize: 25,
              padding: "0",
              textAlign: "center",
              userSelect: "none"
            },
            minWidth: 100,
                      width: 120,
                      maxWidth: 130,
            accessor: "actions"
          },
          {
            Header: "Batch Name",
            accessor : "Name"
            
          },
          {
            Header: "Description",
            accessor: "Objective"
            
          },
          {
            Header: "Total # Samples",
            accessor: "Total"
            
          }
          ,
          {
            Header: "Total # Plates",
            accessor: "num_containers"
            
          },
          {
            Header: "First Plate Name",
            accessor: "first_Plate"
            
          }
          ,
          {
            Header: "Last Plate Name",
            accessor: "last_plate_number"
            
          }
          ,
          {
            Header: "Status Update Date (Date)",
            accessor: "updateDate"
            
          },
          {
            Header: "Status Update by (Name)",
            accessor: "updateBy"
            
          }
        ]}
        loading={props.isLoading}
        defaultPageSize={5}
        PaginationComponent={Pagination}
        className="-striped -highlight"

      />
    </div>
  )
}
// Type and required properties
DesignView.propTypes = {
  property: PropTypes.string,
  Function: PropTypes.func.isRequired,
}

