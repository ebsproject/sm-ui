import { connect } from 'react-redux'
import DesignView from './designview'
import { getUser } from "redux/modules/UserState";
import {getBatchListResult, createLaboratoryReport, saveDesignBatch} from "redux/modules/Design";

export default connect(
  (state) => ({
    user: state.user.user,
    isLoading: state.design.isLoading,
    data: state.design.data,
    error: state.design.error,
  }),
  //Here goes functions that you want to inyect into container
  { getBatchListResult, createLaboratoryReport, saveDesignBatch },
)(DesignView)
