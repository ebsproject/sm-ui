import React from 'react'
import PropTypes from 'prop-types'
// GLOBALIZATION COMPONENT
import { FormattedMessage } from 'react-intl';
// CORE COMPONENTS
import { Typography } from '@mui/material'

// TODO: to delete
export default function ExampleView({ ...rest }) {

  /*
  @prop data-testid: Id to use inside example.test.js file.
 */
  return (
    <div data-testid={'ExampleTestId'}>
      <Typography variant='h5'>
        <FormattedMessage id='none' defaultMessage='My page' />
      </Typography>
    </div>
  )
}
// Type and required properties
ExampleView.propTypes = {}

