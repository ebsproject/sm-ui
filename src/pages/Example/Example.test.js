  import Example from './Example';
import React from 'react'
import Grid from './Grid'
import { render, cleanup, screen } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
   
afterEach(cleanup)

test('Example is in the DOM', () => {
  render(<Example></Example>)
  expect(screen.getByTestId('ExampleTestId')).toBeInTheDocument();
})
