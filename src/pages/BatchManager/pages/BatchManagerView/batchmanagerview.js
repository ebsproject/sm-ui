import React from 'react';
import PropTypes from 'prop-types';
import BatchStatusGrid from 'components/organisms/BatchStatusGrid';
import AlertNotification from 'components/atoms/AlertNotification';

export default function BatchManagerView(props) {
  return (
    <div data-testid={'BatchManagerTestId'}>
      <AlertNotification />
      <BatchStatusGrid />
    </div>
  );
}

BatchManagerView.propTypes = {};
