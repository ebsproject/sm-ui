import React from 'react';
import PropTypes from 'prop-types';

import BatchManagerForm from 'components/organisms/BatchManagerForm';

export default function CreateBatchView(props) {
  /*
  @prop data-testid: Id to use inside createbatch.test.js file.
 */
  const MODE = 'CREATE';

  return (
    <div data-testid={'CreateBatchTestId'} component='main'>
      <BatchManagerForm mode={MODE} />
    </div>
  );
}
// Type and required properties
CreateBatchView.propTypes = {};

