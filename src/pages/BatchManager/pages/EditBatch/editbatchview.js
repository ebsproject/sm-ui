import React from 'react';
import PropTypes from 'prop-types';

import BatchManagerForm from 'components/organisms/BatchManagerForm';
import { useLocation } from 'react-router-dom';

export default function EditBatchView(props) {
  const location = useLocation();
  const { currentData } = location.state;

  const MODE = 'EDIT';

  return (
    <div data-testid={'EditBatchTestId'} component='main'>
      <BatchManagerForm mode={MODE} toEditBatch={currentData} />
    </div>
  );
}
// Type and required properties
EditBatchView.propTypes = {};

