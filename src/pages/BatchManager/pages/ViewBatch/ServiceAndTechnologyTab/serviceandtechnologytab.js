import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Divider } from '@mui/material';
import { MarkerDBComponents } from '@ebs/md';
import { useTechnologyServiceAssay } from './hooks/use-technologyServiceAssay';
const { MarkerGroupCustomOrganism, WrapperQueryClient } = MarkerDBComponents;
import { Core } from '@ebs/styleguide';
import RequestsSummaryAtom from 'components/atoms/BatchManager/RequestsSummaryAtom';
const { Box, LinearProgress, Grid, Typography } = Core;

const ServiceAndTechnologyTabOrganism = React.forwardRef((props, ref) => {
  const { batchDetails, requests, setError, setMessage } = props;
  const [_requests, setRequests] = useState(requests);
  const [_batchDetails, setBatchDetails] = useState(batchDetails);

  const SELECTED_VENDOR = batchDetails.vendor.name;
  const SELECTED_TECHNOLOGY = batchDetails.technology.name;

  useEffect(() => {
    setRequests(requests);
    setBatchDetails(batchDetails);
  }, []);

  const {
    autoSelectedMarkerGroupId,
    isLoading,
    isMarkerGroup,
    listBatchMarkerGroup,
    markersGroupId,
    serviceProvider,
    technology,
    technologyServiceProvider,
  } = useTechnologyServiceAssay({ requests, batchDetails });

  return (
    <div
      data-testid={'ServiceAndTechnologyTabTestId'}
      ref={ref}
      style={{ padding: '4px 25px' }}
    >
      <Grid container>
        <Grid item xs={12}>
          <Typography variant='h6'>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Service & Technology'}
              />
            </Box>
          </Typography>
        </Grid>
        <br />
        <br />
        <Grid item xs={12} xl={6}>
          <RequestsSummaryAtom
            batchDetails={batchDetails}
            mode={'EDIT'}
            requests={requests}
          />
          {!requests && <p>No requests found</p>}
        </Grid>
        <Grid item xs={12} xl={6}></Grid>
        <Grid item xs={12} xl={6} style={{ marginTop: '40px' }}>
          <Divider />
          <Typography variant='h6' style={{ marginTop: '20px' }}>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage id={'some.id'} defaultMessage={'Technology'} />
            </Box>
          </Typography>
        </Grid>
        <Grid item xs={12} xl={6}></Grid>
        <Grid item xs={12} xl={6} style={{ marginTop: '20px' }}>
          <Grid container>
            <Grid item xs={12} lg={4}>
              <Typography>
                <span style={{ fontWeight: 500 }}>
                  <FormattedMessage
                    id={'some.id'}
                    defaultMessage={'Selected Vendor'}
                  />
                  :
                </span>
                &nbsp;{SELECTED_VENDOR}
              </Typography>
            </Grid>
            <Grid item xs={12} lg={4}>
              <Typography>
                <span style={{ fontWeight: 500 }}>
                  <FormattedMessage
                    id={'some.id'}
                    defaultMessage={'Selected Technology'}
                  />
                  :
                </span>
                &nbsp;{SELECTED_TECHNOLOGY}
              </Typography>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} xl={6}></Grid>
        <Grid item xs={12} xl={6} style={{ marginTop: '40px' }}>
          <Divider />
          <Typography variant='h6' style={{ marginTop: '20px' }}>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage id={'some.id'} defaultMessage={'Marker Groups'} />
            </Box>
          </Typography>
        </Grid>
        <Grid item xs={12} style={{ marginTop: '20px' }}>
          {isLoading && (
            <Box>
              <LinearProgress color='secondary' variant='query' />
            </Box>
          )}
          {!isLoading && isMarkerGroup && (
            <WrapperQueryClient>
              <MarkerGroupCustomOrganism
                autoSelectedMarkerGroup={autoSelectedMarkerGroupId}
                markerGroups={markersGroupId}
                onChange={() => {}}
                serviceProvider={serviceProvider}
                technology={technology}
                technologyServiceProvider={technologyServiceProvider}
                listBatchMarkerGroup={listBatchMarkerGroup}
                isOnlyView={true}
              />
            </WrapperQueryClient>
          )}
          {!isLoading && !isMarkerGroup && <p>No marker group selected.</p>}
        </Grid>
      </Grid>
    </div>
  );
});
// Type and required properties
ServiceAndTechnologyTabOrganism.propTypes = {};

export default ServiceAndTechnologyTabOrganism;
