import { useQuery } from '@tanstack/react-query';
import { useEffect, useState } from 'react';
import { convertFixedMarkers } from 'utils/apollo/apis/serviceTechnologyApi';

const useTechnologyServiceAssay = ({ requests, batchDetails }) => {
  const [markerGroups, setMarkerGroups] = useState([]);
  const [autoSelectedMarkerGroup, setAutoSelectedMarkerGroup] = useState([]);
  const [isLoadingMarkerGroups, setIsLoadingMarkerGroups] = useState(true);

  useEffect(() => {
    if (requests.filter((request) => request.markerPanel).length > 0) {
      const markerGroupList = [];
      const autoSelectedList = [];
      requests
        .filter((request) => request.markerPanel)
        .forEach((item) => {
          item.markerPanel.forEach((markerPanel) => {
            if (!markerGroupList.some((list) => list.id === markerPanel.id)) {
              markerGroupList.push({
                id: markerPanel.id,
                name: markerPanel.name,
              });
            }
            if (!autoSelectedList.some((list) => list.id === markerPanel.id)) {
              autoSelectedList.push({
                id: markerPanel.id,
                name: markerPanel.name,
              });
            }
          });
        });
      requests
        .filter((request) => request.traitCustom)
        .forEach((request) =>
          request.traitCustom
            .filter((trait) => trait.markergroups)
            .forEach((trait) =>
              trait.markergroups.forEach((markerGroup) => {
                if (!markerGroupList.some((list) => list.id === markerGroup.id)) {
                  markerGroupList.push({
                    id: markerGroup.id,
                    name: markerGroup.name,
                  });
                }
                if (!autoSelectedList.some((list) => list.id === markerGroup.id)) {
                  autoSelectedList.push({
                    id: markerGroup.id,
                    name: markerGroup.name,
                  });
                }
              }),
            ),
        );
      setIsLoadingMarkerGroups(false);
      setMarkerGroups(markerGroupList);
      setAutoSelectedMarkerGroup(autoSelectedList);
    }else{
      setIsLoadingMarkerGroups(false);
      setMarkerGroups([]);
      setAutoSelectedMarkerGroup([]);
    }
  }, [requests]);

  const { vendor, technology, technologyServiceProvider, batchMarkerGroups } =
    batchDetails;

  const { data, isLoading, isError, error } = useQuery({
    queryKey: ['batch', technology, vendor, batchMarkerGroups],
    queryFn: async () =>
      await convertFixedMarkers({ technology, vendor, batchMarkerGroups }),
  });

  return {
    autoSelectedMarkerGroupId: autoSelectedMarkerGroup.map((markerGroup) => ({
      id: parseInt(markerGroup.id),
    })),
    isLoading: !isLoading && !isLoadingMarkerGroups ? false : true,
    isMarkerGroup: data?.length > 0,
    listBatchMarkerGroup: data,
    markersGroupId: markerGroups.map((item) => ({ id: parseInt(item.id) })),
    serviceProvider: { id: parseInt(vendor.id), label: vendor.name },
    technology: { id: parseInt(technology.id), label: technology.name },
    technologyServiceProvider,
  };
};

export { useTechnologyServiceAssay };
