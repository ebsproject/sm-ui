import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Box, Grid, Paper } from '@mui/material';
import { ArrowBackIos } from '@mui/icons-material';
import { FormattedMessage } from 'react-intl';
import { Link, useLocation } from 'react-router-dom';
import RequestsTab from './RequestsTab';
import ServiceAndTechnologyTab from './ServiceAndTechnologyTab';
import PlateLayoutTab from './PlateLayoutTab';
import LoadingGear from 'components/atoms/LoadingGear';
import {
  FIND_BATCH,
  FIND_PLATE_LIST,
  FIND_REQUEST,
  FIND_SAMPLE_DETAIL_LIST,
} from 'utils/apollo/gql/BatchManager';
import { SMgraph } from 'utils/apollo/SMclient';
import { SMrest } from 'utils/axios/SMclient';
import {
  WHITE_BUTTON_CLASS,
  PRIMARY_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import { Core } from '@ebs/styleguide';
const { Typography, LinearProgress, Tab, Tabs, Button } = Core;

export default function ViewBatchView(props) {
  const location = useLocation();
  const { currentData } = location.state;
  const [error, setError] = useState(false);
  const [message, setMessage] = useState(false);
  const [batchDetails, setBatchDetails] = useState(null);
  const [requests, setRequests] = useState([]);
  const [value, setValue] = useState(0);
  const [wellsGrid, setWellsGrid] = useState([]);
  const [selectedPlateSize, setSelectedPlateSize] = useState(null);
  const [loading, setLoading] = useState(true);

  async function getRequestsByBatchId(batchId) {
    const getRequestsByBatchIdResponse = await SMrest.get(
      `/batch/list-request-byBatchID?batchId=${batchId}`,
    );
    const requestIds = getRequestsByBatchIdResponse.data.result.data;

    buildRequestList(requestIds);
  }

  async function buildRequestList(requestIds) {
    let requests = [];
    if (requestIds.length) {
      for (let i = 0; i < requestIds.length; i++) {
        const requestBody = await SMgraph.query({
          query: FIND_REQUEST,
          variables: {
            id: requestIds[i].id,
          },
          fetchPolicy: 'no-cache',
        });
        requests.push(requestBody.data.findRequest);
      }
    }
    setRequests(requests);
    setLoading(false);
  }

  useEffect(() => {
    const findBatch = async () => {
      try {
        setLoading(true);
        const response = await SMgraph.query({
          query: FIND_BATCH,
          variables: {
            id: currentData.id,
          },
          fetchPolicy: 'no-cache',
        });
        const plateSize = 96; //TODO: remove the number 96 and put he plateSize
        const plateListResponse = [];
        const plateResponse = await SMgraph.query({
          query: FIND_PLATE_LIST,
          variables: {
            filters: [
              { col: 'batchId', mod: 'EQ', val: response.data.findBatch.id },
            ],
            page: { number: 1, size: 10 },
          },
          fetchPolicy: 'no-cache',
        });
        const plateListByFor = [];
        for (let i = 2; i < plateResponse.data.findPlateList.totalPages; i++) {
          plateListByFor.push(i);
        }
        plateListResponse.push(...plateResponse.data.findPlateList.content);

        for await (const number of plateListByFor) {
          const plateResponsePage = await SMgraph.query({
            query: FIND_PLATE_LIST,
            variables: {
              filters: [
                { col: 'batchId', mod: 'EQ', val: response.data.findBatch.id },
              ],
              page: { number, size: 10 },
            },
            fetchPolicy: 'no-cache',
          });
          plateListResponse.push(...plateResponsePage.data.findPlateList.content);
        }
        const plates = [];
        const sampledetails = [];
        for await (const plate of plateListResponse) {
          const sampleDetailResponse = await SMgraph.query({
            query: FIND_SAMPLE_DETAIL_LIST,
            variables: {
              filters: [
                { col: 'batch.id', mod: 'EQ', val: response.data.findBatch.id },
                { col: 'plateId', mod: 'EQ', val: plate.plateId },
              ],
              page: { number: 1, size: plateSize },
            },
            fetchPolicy: 'no-cache',
          });

          plates.push({
            plateId: plate.plateId,
            sampleDetails: sampleDetailResponse.data.findSampleDetailList.content,
          });
          sampledetails.push(sampleDetailResponse.data.findSampleDetailList.content);
        }
        const findBatch = {
          ...response.data.findBatch,
          plates,
          plateSize,
          sampledetails: sampledetails,
        };

        setBatchDetails(findBatch);
        getRequestsByBatchId(findBatch.id);
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        }
        setError(true);
        throw new Error(error);
      }
    };

    findBatch();
  }, [currentData]);

  function setNumberOfColumns(plateSize) {
    switch (plateSize) {
      case 96:
        return 12;
      case 384:
        return 16;
    }
  }

  function setWellsGridObject(numberOfColumns) {
    const ROWS_OF_8 = {
      A: Array(numberOfColumns).fill(null),
      B: Array(numberOfColumns).fill(null),
      C: Array(numberOfColumns).fill(null),
      D: Array(numberOfColumns).fill(null),
      E: Array(numberOfColumns).fill(null),
      F: Array(numberOfColumns).fill(null),
      G: Array(numberOfColumns).fill(null),
      H: Array(numberOfColumns).fill(null),
    };

    switch (numberOfColumns) {
      case 12:
        return ROWS_OF_8;
    }
  }

  function getVendorControlPositions() {
    const vendorPositions =
      batchDetails.technologyServiceProvider.controlPlate.split(',');
    let convertedVendorPositions = [];
    convertedVendorPositions = vendorPositions.map((item) => {
      return {
        row: item[0],
        column: item.substring(1),
      };
    });
    return convertedVendorPositions;
  }

  const groupBy = (key) => (array) =>
    array.reduce((objectsByKeyValue, obj) => {
      const value = obj[key];
      objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
      return objectsByKeyValue;
    }, {});

  useEffect(() => {
    const buildWellsGridArray = () => {
      if (batchDetails && Object.entries(batchDetails).length > 0) {
        // const groupByPlateNameCall = groupBy('plateId');
        // const groupByPlateName = groupByPlateNameCall(batchDetails.sampledetails);
        // const plateSize = Object.values(groupByPlateName)[0].length;
        setSelectedPlateSize(batchDetails.plateSize);
        const builtWellsGrid = [];
        const numberOfColumns = setNumberOfColumns(batchDetails.plateSize);
        for (const plate of batchDetails.plates) {
          let wellsGridObject = setWellsGridObject(numberOfColumns);
          const vendorControlsPositions = getVendorControlPositions(
            batchDetails.vendor.name,
          );
          plate.sampleDetails.map((cell) => {
            wellsGridObject[cell.row][cell.column - 1] = cell.controltype
              ? cell.controltype.name
              : cell.sampleNumber;
          });
          vendorControlsPositions.map(({ row, column }) => {
            if (row && column)
              wellsGridObject[row][column - 1] = batchDetails.vendor.name;
          });
          const wellsGridRow = {
            plateNumber: batchDetails.name + '_P' + plate.plateId,
            wells: wellsGridObject,
            sortKey: `${plate.plateId}`.slice(1),
          };
          builtWellsGrid.push(wellsGridRow);
        }
        setWellsGrid(builtWellsGrid);
      }
    };

    buildWellsGridArray();
  }, [batchDetails]);

  const tabs = [
    { id: 0, index: 1, name: 'Requests' },
    { id: 1, index: 2, name: 'Service & Technology' },
    { id: 2, index: 3, name: 'Plate layout' },
  ];

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabPanel-${index}`,
    };
  }

  function formattedTab(tabCount, label, id) {
    return (
      <Typography color='primary'>
        <span>{tabCount}</span>
        <FormattedMessage id={id} defaultMessage={label} />
      </Typography>
    );
  }

  function TabPanel(props) {
    const { children, value, index } = props;

    return (
      <div role='tabPanel' hidden={value !== index}>
        {value === index && <>{children}</>}
      </div>
    );
  }

  /*
  @prop data-testid: Id to use inside viewbatch.test.js file.
 */
  return (
    <div data-testid={'ViewBatchTestId'} component='main'>
      {error ? (
        <LoadingGear message={message} />
      ) : (
        <div>
          <Grid container>
            <Grid item container>
              <Button
                startIcon={<ArrowBackIos />}
                component={Link}
                variant='contained'
                color='primary'
                to={'/sm/batchmanager/view'}
              >
                <FormattedMessage
                  id={'some.id'}
                  defaultMessage={'BACK TO BATCH MANAGER'}
                />
              </Button>
            </Grid>
          </Grid>
          <br />
          <Grid item container>
            <Grid item xs={8}>
              <Typography variant='h5'>
                <Box fontWeight={600}>
                  <span>
                    <FormattedMessage id={'some.id'} defaultMessage={'Batch'} />
                  </span>
                  &nbsp;{currentData.name}&nbsp;
                  <span>
                    <FormattedMessage id={'some.id'} defaultMessage={'details'} />
                  </span>
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <div>
                {value !== 0 && (
                  <Button
                    style={{ marginLeft: '15px' }}
                    variant='contained'
                    className={WHITE_BUTTON_CLASS}
                    onClick={() => handleChange(null, value - 1)}
                  >
                    Back
                  </Button>
                )}
                {value !== 2 && (
                  <Button
                    style={{ marginLeft: '15px' }}
                    variant='contained'
                    className={PRIMARY_BUTTON_CLASS}
                    onClick={() => handleChange(null, value + 1)}
                  >
                    Next
                  </Button>
                )}
              </div>
            </Grid>
          </Grid>

          <br />

          <Grid item container>
            {loading ? (
              <LinearProgress style={{ width: '100%' }} />
            ) : (
              <>
                <Grid item xs={12}>
                  <Paper square>
                    <Tabs
                      value={value}
                      onChange={handleChange}
                      indicatorColor='primary'
                    >
                      {tabs &&
                        tabs.map((tabData) => (
                          <Tab
                            key={`view_batchI_view_tab_id_${tabData.id}`}
                            label={formattedTab(
                              tabData.index,
                              tabData.name,
                              'some.id',
                            )}
                            {...a11yProps(tabData.index)}
                          />
                        ))}
                    </Tabs>
                  </Paper>
                  <br />
                </Grid>

                <div>
                  <TabPanel value={value} index={0}>
                    <br />
                    <RequestsTab
                      batchDetails={batchDetails}
                      currentData={currentData}
                      requests={requests}
                    />
                  </TabPanel>
                  <TabPanel value={value} index={1}>
                    <br />
                    <ServiceAndTechnologyTab
                      batchDetails={batchDetails}
                      requests={requests}
                      setError={setError}
                      setMessage={setMessage}
                    />
                  </TabPanel>
                  <TabPanel value={value} index={2}>
                    <br />
                    <PlateLayoutTab
                      batchDetails={batchDetails}
                      requests={requests}
                      wellsGrid={wellsGrid}
                      selectedPlateSize={selectedPlateSize}
                    />
                  </TabPanel>
                </div>
              </>
            )}
          </Grid>
        </div>
      )}
    </div>
  );
}
// Type and required properties
ViewBatchView.propTypes = {};
