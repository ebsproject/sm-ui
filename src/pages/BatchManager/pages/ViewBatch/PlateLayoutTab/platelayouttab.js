import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Box, Divider, Grid, Paper, Tab, Tabs, Typography } from '@mui/material';
import './wells.css';
import WellsCircle from 'components/atoms/WellsCircle';
import RequestsSummaryAtom from 'components/atoms/BatchManager/RequestsSummaryAtom';
import WellsPaginationMolecule from 'components/molecules/v2/wellsPaginationMolecule/WellsPaginationMolecule';
import WellsTableAtom from 'components/atoms/WellsTable';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PlateLayoutTabOrganism = React.forwardRef((props, ref) => {
  const { batchDetails, requests, selectedPlateSize, wellsGrid } = props;
  const [_requests, setRequests] = useState(requests);
  const [_batchDetails, setBatchDetails] = useState(batchDetails);
  const [_wellsGrid, setWellsGrid] = useState(wellsGrid);
  const [tabs, setTabs] = useState([]);
  const [value, setValue] = useState(0);

  const SELECTED_PLATE_SIZE = selectedPlateSize;
  const SELECTED_PLATE_FILL_DIRECTION = batchDetails.loadtype.name;

  useEffect(() => {
    setRequests(requests);
    setBatchDetails(batchDetails);
    setWellsGrid(wellsGrid);
  }, []);

  useEffect(() => {
    if (tabs.length === 0) {
      const platesTabList = [];
      for (let i = 0; i < wellsGrid.length; i++) {
        platesTabList.push({
          id: i,
          batchCode: `${wellsGrid[i].plateNumber}`,
        });
      }
      setTabs(platesTabList);
    }
  }, [wellsGrid]);

  const handleChangePage = (newValue) => {
    if (value !== newValue) setValue(newValue);
  };

  // function a11yProps(index) {
  //   return {
  //     id: `wrapped-tab-${index}`,
  //     'aria-controls': `wrapped-tabpanel-${index}`,
  //   };
  // }

  function TabPanel(props) {
    const { children, value, index } = props;

    return (
      <div role='tabpanel' hidden={value !== index}>
        {value === index && <>{children}</>}
      </div>
    );
  }

  function setColorKey(cell) {
    if (typeof cell === 'number') {
      return 'samplesAssignedCircle';
    } else if (typeof cell === 'string') {
      if (cell === 'Intertek') {
        return 'intertekControlsCircle';
      }
      if (cell === 'DArT') {
        return 'intertekControlsCircle';
      } else if (cell === 'Agriplex') {
        return 'dartControlsCircle';
      } else if (cell === 'Blank') {
        return 'blankCircle';
      } else if (cell === 'Random') {
        return 'randomlyAssignedRandomControlsCircle';
      } else if (cell === 'Positive') {
        return 'userAssignedPositiveControlsCircle';
      }
    } else return 'blankCircle';
  }

  const WellsGrid = ({ content }) => {
    const columns =
      SELECTED_PLATE_SIZE === 96
        ? ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
        : [
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9',
            '10',
            '11',
            '12',
            '13',
            '14',
            '15',
            '16',
          ];

    return (
      <table>
        <thead>
          <tr>
            <th></th>
            {columns.map((header) => (
              <th key={`plateLayoutTab_columns_th_${header}`}>{header}</th>
            ))}
          </tr>
        </thead>
        <tbody>
          {Object.values(content.wells).map((row, index) => {
            return (
              <tr key={`plateLayoutTab_wells_tr_${index}`}>
                <td className='letter-cell'>{String.fromCharCode(65 + index)}</td>
                {row.map((cell, index) => {
                  return (
                    <td
                      style={{ textAlign: 'center' }}
                      key={`plateLayoutTab_td_${index}`}
                    >
                      <div
                        style={{
                          display: 'flex',
                          flexWrap: 'no-wrap',
                          justifyContent: 'center',
                          alignItems: 'center',
                          padding: '3px',
                        }}
                      >
                        <WellsCircle colorKey={setColorKey(cell)} />
                        <span style={{ marginLeft: '5px' }}>
                          {cell ? cell : 'Blank'}
                        </span>
                      </div>
                    </td>
                  );
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

  return (
    /*
     @prop data-testid: Id to use inside platelayouttab.test.js file.
     */
    <div
      data-testid={'PlateLayoutTabTestId'}
      ref={ref}
      style={{ padding: '4px 25px' }}
    >
      <Grid container>
        <Grid item xs={12}>
          <Typography variant='h6'>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage id={'some.id'} defaultMessage={'Plate Layout'} />
            </Box>
          </Typography>
        </Grid>

        <br />
        <br />

        <Grid item xs={12} xl={6}>
          <RequestsSummaryAtom
            batchDetails={batchDetails}
            mode={'EDIT'}
            requests={requests}
          />
          {!requests && <p>No requests found</p>}
        </Grid>

        <Grid item xs={12} xl={6}></Grid>

        <Grid item xs={12} xl={6} style={{ marginTop: '40px' }}>
          <Divider />
          <Typography variant='h6' style={{ marginTop: '20px' }}>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Plate Information'}
              />
            </Box>
          </Typography>
        </Grid>

        <Grid item xs={12} xl={6}></Grid>

        <Grid item xs={12} xl={6} style={{ marginTop: '20px' }}>
          <Grid container>
            <Grid item xs={12} lg={4}>
              <Typography>
                <span style={{ fontWeight: 500 }}>
                  <FormattedMessage
                    id={'some.id'}
                    defaultMessage={'Selected Plate Size'}
                  />
                  :
                </span>
                &nbsp;{SELECTED_PLATE_SIZE}
              </Typography>
            </Grid>
            <Grid item xs={12} lg={4}>
              <Typography>
                <span style={{ fontWeight: 500 }}>
                  <FormattedMessage
                    id={'some.id'}
                    defaultMessage={'Selected Plate Fill Direction'}
                  />
                  :
                </span>
                &nbsp;{SELECTED_PLATE_FILL_DIRECTION}
              </Typography>
            </Grid>

            <br />
            <br />
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <div id='wells-selection-container'>
            <Paper square>
              <WellsPaginationMolecule
                tabs={tabs}
                page={value}
                onChangePage={handleChangePage}
                parentId={'wellsContainer.pagination'}
                numberTabsDisplays={10}
              />
            </Paper>
            {tabs &&
              _wellsGrid.map((content, index) => {
                return (
                  <TabPanel
                    key={`plateLayoutTab_tabPanel_${index}`}
                    value={value}
                    index={index}
                  >
                    <WellsGrid content={content} />
                  </TabPanel>
                );
              })}
          </div>
        </Grid>
      </Grid>
    </div>
  );
});
// Type and required properties
PlateLayoutTabOrganism.propTypes = {};

export default PlateLayoutTabOrganism;
