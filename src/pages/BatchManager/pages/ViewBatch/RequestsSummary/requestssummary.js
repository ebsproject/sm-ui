import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Grid, Typography } from '@mui/material';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RequestsSummaryMolecule = React.forwardRef((props, ref) => {
  const { batchDetails, requests } = props;
  const NUMBER_OF_REQUESTS = requests.length;
  const SERVICE_TYPE = 'Genotyping';
  const SAMPLES = batchDetails.numMembers;
  const PURPOSE = requests[0].purpose.name;
  const MARKERS_PANEL =
    batchDetails.markergroup?.name || 'No marker group selected.';

  return (
    /*
     @prop data-testid: Id to use inside requestssummary.test.js file.
     */
    <Grid container ref={ref} data-testid={'RequestsSummaryTestId'}>
      <Grid item xs={12} sm={10} style={{ background: '#F5F5F5', padding: '7px' }}>
        <Grid container>
          <Grid item xs={12} lg={3}>
            <Typography>
              <span style={{ fontWeight: 500 }}>Requests: </span>&nbsp;
              {NUMBER_OF_REQUESTS}
            </Typography>

            <Typography style={{ marginTop: '15px' }}>
              <span style={{ fontWeight: 500 }}>Service Type: </span>
              &nbsp;{SERVICE_TYPE}
            </Typography>
          </Grid>

          <Grid item xs={12} lg={5}>
            <Typography>
              <span style={{ fontWeight: 500 }}>Samples: </span>&nbsp;
              {SAMPLES}
            </Typography>

            <Typography style={{ marginTop: '15px' }}>
              <span style={{ fontWeight: 500 }}>Purpose: </span>&nbsp;
              {PURPOSE}
            </Typography>
          </Grid>

          <Grid item xs={12} lg={4}>
            <Typography
              align='left'
              style={{ display: 'flex', alignItems: 'baseline' }}
            >
              <span style={{ fontWeight: 500, marginRight: '10px' }}>
                Markers Panel:
              </span>
            </Typography>
            <ul>
              <li>
                <Typography variant='subtitle1'>• {MARKERS_PANEL}</Typography>
              </li>
            </ul>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
});
// Type and required properties
RequestsSummaryMolecule.propTypes = {};

export default RequestsSummaryMolecule;
