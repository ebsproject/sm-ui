import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { FormattedMessage } from 'react-intl';
import { EbsGrid } from '@ebs/components';
import { Link, useLocation } from 'react-router-dom';
import { Box, Grid, Tooltip } from '@mui/material';
import { Visibility } from '@mui/icons-material';
import { filterMembers, setData, sortData } from 'helpers/generalUtils';
import RequestsSummaryAtom from 'components/atoms/BatchManager/RequestsSummaryAtom';
import { Core } from '@ebs/styleguide';
const { IconButton, Typography } = Core;

const RequestsTabOrganism = React.forwardRef(
  ({ batchDetails, currentData, requests, setMessage }, ref) => {
    const [_requests, setRequests] = useState(requests);
    const [_batchDetails, setBatchDetails] = useState(batchDetails);
    const location = useLocation();

    useEffect(() => {
      setRequests(requests);
      setBatchDetails(batchDetails);
    }, []);

    useEffect(() => {
      setRequests(requests);
      return () => {};
    }, [requests]);

    function checkEmptyValue(value) {
      if (!value)
        return (
          <span>
            <FormattedMessage id={'some.id'} defaultMessage={'(Not Set)'} />
          </span>
        );
      return value;
    }

    const columns = [
      {
        Header: 'Id',
        accessor: 'id',
        hidden: true,
        disableGlobalFilter: true,
      },
      {
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.serviceProviderRequestCode'}
            defaultMessage={'Request'}
          />
        ),
        accessor: 'requestCode',
        csvHeader: 'Request Code',
      },
      {
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.program'}
            defaultMessage={'Program'}
          />
        ),
        accessor: 'program.name',
        csvHeader: 'Program',
      },
      {
        Header: (
          <FormattedMessage id={'sm.req.tbl.hdr.crop'} defaultMessage={'Crop'} />
        ),
        accessor: 'crop.name',
        csvHeader: 'Crop',
      },
      {
        Header: 'Service Type',
        accessor: 'servicetype.name',
        csvHeader: 'Service Type',
      },
      {
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.serviceProvider'}
            defaultMessage={'Service Provider'}
          />
        ),
        accessor: 'serviceprovider.name',
        csvHeader: 'Service Provider',
      },
      {
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.purpose'}
            defaultMessage={'Purpose'}
          />
        ),
        accessor: 'purpose.name',
        csvHeader: 'Purpose',
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Marker Group'} />,
        accessor: 'markerGroup',
        csvHeader: 'Marker Group',
        Cell: ({ value }) => {
          return checkEmptyValue(value);
        },
      },
      {
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.quantity'}
            defaultMessage={'Total'}
          />
        ),
        accessor: 'totalEntities',
        csvHeader: 'Total Entities',
      },
    ];

    const rowActions = (rowData, refresh) => {
      return (
        <div>
          <div></div>
          <div>
            <Tooltip title='View'>
              <IconButton
                color='primary'
                component={Link}
                to={`/sm/view-request-service/${rowData.requestCode}`}
                state={{
                  currentData: rowData,
                  prevPath: location.pathname,
                  batchDetails: currentData,
                }}
              >
                <Visibility />
              </IconButton>
            </Tooltip>
          </div>
        </div>
      );
    };

    const fetch = async ({ page, sort, filters }) => {
      return new Promise((resolve, reject) => {
        try {
          let data;
          if (filters.length) {
            const filteredMembers = filterMembers(_requests, filters);
            data = setData(filteredMembers, page);
          } else {
            data = setData(_requests, page);
          }
          const data2 = data.data.map((row) => {
            const markerGroup = row.markerPanel
              ? row.markerPanel
                  .filter((item) => item)
                  .map((item, index) => `(${index + 1}) ${item.name}`)
                  .join('; ')
              : '(Not Set)';
            return {
              ...row,
              markerGroup: markerGroup,
            };
          });
          data.data = data2;
          if (sort.length >= 1) {
            const sortedData = sortData(data, sort);
            resolve(sortedData);
          } else resolve(data);
        } catch (error) {
          const message = error?.response?.data?.metadata.status[0].messageType;
          if (message) {
            setMessage(message);
          }
          setError(true);
          reject(error);
          throw new Error(error);
        }
      });
    };

    return (
      <div
        data-testid={'RequestsTabTestId'}
        ref={ref}
        style={{ padding: '4px 25px' }}
      >
        <Grid container>
          <Grid item xs={12}>
            <Typography variant='h6'>
              <Box sx={{ fontWeight: 500 }}>
                <FormattedMessage
                  id={'some.id'}
                  defaultMessage={'Requests in batch'}
                />
              </Box>
            </Typography>
          </Grid>

          <br />
          <br />
          <Grid item xs={12} xl={6}>
            <RequestsSummaryAtom
              batchDetails={batchDetails}
              mode={'EDIT'}
              requests={requests}
              checkedRequestSelection={requests}
            />
          </Grid>
          <Grid item xs={12}>
            {requests.length > 0 ? (
              <EbsGrid
                columns={columns}
                rowactions={rowActions}
                fetch={fetch}
                height='85vh'
                csvfilename='approved-request-list'
              />
            ) : (
              <p>No requests found.</p>
            )}
          </Grid>
        </Grid>
      </div>
    );
  },
);
// Type and required properties
RequestsTabOrganism.propTypes = {};

export default RequestsTabOrganism;
