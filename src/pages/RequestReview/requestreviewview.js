import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Container from '@mui/material/Container';
import RequestReviewGrid from 'components/organisms/RequestReviewGrid';
import { userContext } from '@ebs/layout';
import { Core } from '@ebs/styleguide';
const { LinearProgress } = Core;

export default function RequestReviewView() {
  const { userProfile } = useContext(userContext);
  return (
    <Container
      data-testid={'RequestReviewTestId'}
      component='main'
      maxWidth='xl'
      disableGutters
      fixed
      style={{ background: '#ffffff', padding: '10px' }}
    >
      {!userProfile ? (
        <LinearProgress />
      ) : (
        <RequestReviewGrid userProfile={userProfile} />
      )}
    </Container>
  );
}
RequestReviewView.propTypes = {};
