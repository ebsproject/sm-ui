import React, { useMemo } from 'react';
import { EbsGrid } from '@ebs/components';
import { GET_SM_URL } from 'utils/config';
import { Core } from '@ebs/styleguide';
import { PurposeDialogOrganism } from 'components/organisms/SettingForm';
import { useSettingPage } from './hooks/useSettingPage';
const { Box, Paper, Typography } = Core;

const PurposePage = (_) => {
  const { handleClose, idEntity, isOpen, mode, rowActions, toolbarActions } =
    useSettingPage({ titleAddButton: 'Add new Purpose', entity: "purpose" });
  const columns = useMemo(
    () => [
      {
        Header: (
          <Typography variant='h5' color='primary'>
            ID
          </Typography>
        ),
        accessor: 'id',
        hidden: true,
        csvHeader: 'ID',
      },
      {
        Header: (
          <Typography variant='h5' color='primary'>
            Code
          </Typography>
        ),
        accessor: 'code',
        width: 200,
        hidden: false,
        csvHeader: 'Code',
      },
      {
        Header: (
          <Typography variant='h5' color='primary'>
            Name
          </Typography>
        ),
        accessor: 'name',
        width: 400,
        hidden: false,
        csvHeader: 'Name',
      },
      {
        Header: (
          <Typography variant='h5' color='primary'>
            Description
          </Typography>
        ),
        accessor: 'description',
        width: 400,
        hidden: false,
        csvHeader: 'Description',
      },
    ],
    [],
  );

  return (
    <Box
      xs={{
        width: 1,
      }}
      component='div'
      id='mainBoxPurpose'
    >
      <Paper id='paperGrid'>
        <EbsGrid
          columns={columns}
          id='PurposeTable'
          toolbar={true}
          title={
            <Typography variant='h4' className='font-ebs text-ebs-green-default'>
              Purpose
            </Typography>
          }
          callstandard='graphql'
          csvfilename='Purpose'
          entity='Purpose'
          height='80vh'
          toolbaractions={toolbarActions}
          uri={`${GET_SM_URL}graphql`.toString()}
          rowactions={rowActions}
          defaultSort={{ col: 'id', mod: 'DES' }}
        />
        <PurposeDialogOrganism
          onClose={handleClose}
          isOpen={isOpen}
          mode={mode}
          idEntity={idEntity}
        />
      </Paper>
    </Box>
  );
};

PurposePage.propTypes = {};

export default PurposePage;
