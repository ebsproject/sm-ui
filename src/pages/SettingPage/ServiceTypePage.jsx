import React, { useMemo } from 'react';
import { EbsGrid } from '@ebs/components';
import { GET_SM_URL } from 'utils/config';
import { ServiceTypeDialogOrganism } from 'components/organisms/SettingForm';
import { Core } from '@ebs/styleguide';
import { useSettingPage } from './hooks/useSettingPage';
const { Box, Paper, Typography } = Core;

const ServiceTypePage = (_) => {
  const { handleClose, idEntity, isOpen, mode, rowActions, toolbarActions } =
    useSettingPage({ titleAddButton: 'Add new Service Type', entity: "serviceType" });
  const columns = useMemo(
    () => [
      {
        Header: (
          <Typography variant='h5' color='primary'>
            ID
          </Typography>
        ),
        accessor: 'id',
        hidden: true,
        csvHeader: 'ID',
      },
      {
        Header: (
          <Typography variant='h5' color='primary'>
            Code
          </Typography>
        ),
        accessor: 'code',
        width: 200,
        hidden: false,
        csvHeader: 'Code',
      },
      {
        Header: (
          <Typography variant='h5' color='primary'>
            Name
          </Typography>
        ),
        accessor: 'name',
        width: 400,
        hidden: false,
        csvHeader: 'Name',
      },
      {
        Header: (
          <Typography variant='h5' color='primary'>
            Description
          </Typography>
        ),
        accessor: 'description',
        width: 1000,
        hidden: false,
        csvHeader: 'Description',
      },
    ],
    [],
  );

  return (
    <Box
      xs={{
        width: 1,
      }}
      component='div'
      id='main-box-serviceType'
    >
      <Paper>
        <EbsGrid
          columns={columns}
          id='ServiceTypeTable'
          toolbar={true}
          title={
            <Typography variant='h4' className='font-ebs text-ebs-green-default'>
              Service Type
            </Typography>
          }
          callstandard='graphql'
          csvfilename='ServiceType'
          entity='ServiceType'
          height='80vh'
          toolbaractions={toolbarActions}
          uri={`${GET_SM_URL}graphql`.toString()}
          rowactions={rowActions}
          defaultSort={{ col: 'id', mod: 'DES' }}
        />
        <ServiceTypeDialogOrganism
          idEntity={idEntity}
          onClose={handleClose}
          mode={mode}
          isOpen={isOpen}
        />
      </Paper>
    </Box>
  );
};

ServiceTypePage.propTypes = {};

export default ServiceTypePage;
