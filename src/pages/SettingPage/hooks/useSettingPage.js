import { forwardRef, useState } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Core, Icons } from '@ebs/styleguide';
import {  useLayoutContext } from '@ebs/layout';
import { deletePurpose, deleteServiceType } from 'utils/apollo/apis';
const {
  Box,
  Button,
  IconButton,
  Tooltip,
  Dialog,
  DialogActions,
  DialogTitle,
  Typography,
} = Core;
const { Edit: EditIcon, Visibility: VisibilityIcon, Delete: DeleteIcon } = Icons;

const useSettingPage = ({ titleAddButton, entity }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [idEntity, setEntity] = useState(0);
  const [mode, setMode] = useState('none');

  const { setFilterGridToApply } = useLayoutContext();

  const handleClose = ({ success }) => {
    if (success) {
      setFilterGridToApply([]);
    }
    setEntity(0);
    setIsOpen(false);
    setMode('none');
  };

  const handleAdd = () => {
    setEntity(0);
    setIsOpen(true);
    setMode('add');
  };

  const handleEdit = (id) => {
    setEntity(parseInt(id));
    setIsOpen(true);
    setMode('edit');
  };

  const handleView = (id) => {
    setEntity(parseInt(id));
    setIsOpen(true);
    setMode('view');
  };

  const toolbarActions = (selectedRows, refresh) => {
    return (
      <Box className='ml-3'>
        <Button
          className='bg-ebs-brand-default hover:bg-ebs-brand-900  text-white'
          onClick={handleAdd}
        >
          <FormattedMessage id={'none'} defaultMessage={titleAddButton} />
        </Button>
      </Box>
    );
  };

  const rowActions = forwardRef(({ rowData: { name, id }, refresh }, ref) => {
    const [open, setOpen] = useState(false);
    const handleClose = () => {
      setOpen(false);
    };

    const handleOpen = () => {
      setOpen(true);
    };

    const handleConfirm = async ({ id }) => {
      setOpen(false);
      if (entity === 'purpose') await deletePurpose({ id });
      else if (entity === 'serviceType') await deleteServiceType({ id });
      refresh();
    };

    return (
      <Box
        className='-ml-2 flex flex-auto'
        component='div'
        id='settingPage.RowAction'
        ref={ref}
      >
        <Tooltip
          arrow
          title={
            <Typography className='font-ebs text-xl'>
              <FormattedMessage
                id={'settingPage.RowAction.icon.edit'}
                defaultMessage={'Edit'}
              />
            </Typography>
          }
        >
          <IconButton size='small' color='primary' onClick={() => handleEdit(id)}>
            <EditIcon />
          </IconButton>
        </Tooltip>
        <Tooltip
          arrow
          title={
            <Typography className='font-ebs text-xl'>
              <FormattedMessage
                id={'settingPage.RowAction.icon.view'}
                defaultMessage={'View'}
              />
            </Typography>
          }
        >
          <IconButton color='primary' size='small' onClick={() => handleView(id)}>
            <VisibilityIcon />
          </IconButton>
        </Tooltip>
        <Tooltip
          arrow
          title={
            <Typography className='font-ebs text-xl'>
              <FormattedMessage
                id={'settingPage.RowAction.icon.view'}
                defaultMessage={'Delete'}
              />
            </Typography>
          }
        >
          <IconButton
            size='small'
            color='primary'
            component='span'
            onClick={handleOpen}
          >
            <DeleteIcon />
          </IconButton>
        </Tooltip>
        <Dialog
          open={open}
          keepMounted
          onClose={handleClose}
          id='settingPage.RowAction.confirm'
        >
          <DialogTitle>{`Confirm to delete the Purpose ${name}?`}</DialogTitle>
          <DialogActions>
            <Button onClick={handleClose}>Cancel</Button>
            <Button onClick={() => handleConfirm({ id })}>Confirm</Button>
          </DialogActions>
        </Dialog>
      </Box>
    );
  });
  return {
    handleClose,
    idEntity,
    isOpen,
    mode,
    rowActions,
    toolbarActions,
  };
};

useSettingPage.propTypes={
  titleAddButton: PropTypes.string.isRequired, 
  entity: PropTypes.oneOf(["purpose", "serviceType"]).isRequired,
}

export { useSettingPage };
