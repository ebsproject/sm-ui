import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';
import TabPanelMolecule from 'components/molecules/tab/TabPanelMolecule';
import PurposePage from './PurposePage';
import ServiceTypePage from './ServiceTypePage';
import { Core } from '@ebs/styleguide';
const { AppBar, Tab, Tabs, Typography } = Core;

const MainSettingPage = (props) => {
  const [value, setValue] = React.useState(0);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box component='div' data-testid='PurposePage_testId' className='bg-white'>
      <Box sx={{ mb: 2 }}>
        <Typography variant='h3'>Settings</Typography>
      </Box>
      <AppBar position='static'>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label='simple tabs example'
          sx={{ '& button.Mui-selected': { color: 'primary.light' } }}
        >
          <Tab label='Purpose' />
          <Tab label='Service Type' />
        </Tabs>
      </AppBar>
      <TabPanelMolecule value={value} index={0}>
        <PurposePage />
      </TabPanelMolecule>
      <TabPanelMolecule value={value} index={1}>
        <ServiceTypePage />
      </TabPanelMolecule>
    </Box>
  );
};

MainSettingPage.propTypes = {};

export default MainSettingPage;
