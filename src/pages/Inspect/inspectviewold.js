import React, {useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import ReactTable from "react-table";
import "react-table/react-table.css";
import GetAppTwoToneIcon from '@mui/icons-material/GetAppTwoTone';
import UnarchiveTwoToneIcon from '@mui/icons-material/UnarchiveTwoTone';
import Pagination from "components/molecules/Pagination/Pagination"
import { green } from '@mui/material/colors';
import DetailBatch from 'components/molecules/DetailBatch'
import { actions } from 'react-table';
import StatusActionbatch from "components/molecules/StatusActionBatch"

// TODO: to delete
export default function InspectView(props) {
  const { property, Function, ...rest } = props

  React.useEffect(()=>{props.getBatchListResult()},[])

  return (
      <div   data-testid={'InspectTestId'}>
      <ReactTable 
          width ="85%"
          filterable
          data={props.data}
          columns={[
            {
              columns: [
                {
                  expander: true,
                  Header: () => <strong></strong>,
                  width: 30,
               
                  Expander: ({ isExpanded }) =>
                    <div>
                      {isExpanded
                        ? <span >
                          
                          <UnarchiveTwoToneIcon style={{ color: green[500] }}/>
                          
                        </span>
                        : <span><GetAppTwoToneIcon style={{ color: green[500] }}/></span>
                      }
                    </div>,
                  style: {
                    cursor: "pointer",
                    fontSize: 25,
                    padding: "0",
                    textAlign: "center",
                    userSelect: "none"
                  }
                }
              ]
            },
            {
              Header: () => <strong>Actions</strong>,
              Cell: row => (
                <div>
                  <StatusActionbatch row={row} actionNext={props.nextStageBatch}
                      actionReject={props.rejectedSatge} />
                </div>
              ),
              style: {
                cursor: "pointer",
                fontSize: 25,
                padding: "0",
                textAlign: "center",
                userSelect: "none"
              },
              minWidth: 120,
                        width: 150,
                        maxWidth: 160,
              accessor: "actions"
            },
            {
              Header: "Batch Name",
              accessor : "Name"
              
            },
            {
              Header: "Description",
              accessor: "Objective"
              
            },
            {
              Header: "Total",
              accessor: "Total"
              
            }
          ]}
          defaultPageSize={5}
          PaginationComponent={Pagination}
          className="-striped -highlight"
          
          SubComponent={(v) =>
            <div style={{ padding: '10px' }}>
            <DetailBatch idBatch={v.original.id} />
            </div>
          }
        />
      </div>
  
  )
}
// Type and required properties
InspectView.propTypes = {
  property: PropTypes.string,
  //Function: PropTypes.func.isRequired,
}


