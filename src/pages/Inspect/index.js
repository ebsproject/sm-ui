import { connect } from 'react-redux'
import InspectView from './inspectview'
import {getBatchListResult, nextStageBatch, rejectedSatge} from "redux/modules/RequestBatch";
export default connect(
  (state) => ({
    user: state.user.user,
    isLoading: state.request_batch.isLoading,
    data: state.request_batch.data,
    error: state.request_batch.error,
  }),
  {getBatchListResult, nextStageBatch, rejectedSatge},
)(InspectView)
