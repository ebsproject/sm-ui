import React from 'react'
import ServiceProviderContent from 'components/organisms/ServiceProviderContents'
import { useLocation } from 'react-router-dom';

const createserviceprovider = (_) => {
    const location = useLocation();
    const { currentData } = location.state
    return (
        <div>
            <ServiceProviderContent currentData={{currentData}} mode="CREATE" />
        </div>
    )
}

export default createserviceprovider
