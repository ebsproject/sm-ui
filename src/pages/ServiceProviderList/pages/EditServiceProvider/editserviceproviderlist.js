import React from 'react'
import ServiceProviderContent from 'components/organisms/ServiceProviderContents'
import { useLocation } from 'react-router-dom';

const EditServiceProviderList = (_) => {
  const location = useLocation();
  const { currentData } = location.state

  /*
  @prop data-testid: Id to use inside editrequestservice.test.js file.
 */
  return (
    <div>
      <ServiceProviderContent currentData={currentData} mode="EDIT" />
    </div>
  )
}

export default EditServiceProviderList
