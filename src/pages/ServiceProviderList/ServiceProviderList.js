import React from 'react'
import Container from '@mui/material/Container'
import ServiceProviderListGrid from 'components/organisms/ServicePrividerListGrid'

const TestGetView = () => {
    return (
        <Container 
            component="div"
            disableGutters
            fixed
            maxWidth="xl"
        >
            <ServiceProviderListGrid />
        </Container>
    )
}

export default TestGetView
