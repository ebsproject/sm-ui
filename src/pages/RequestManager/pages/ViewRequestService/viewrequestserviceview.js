import React, { useState } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ArrowBackIos } from '@mui/icons-material';
import { FormattedMessage } from 'react-intl';
import LoadingGear from 'components/atoms/LoadingGear';
import ViewRequest from 'components/molecules/ViewRequest';
import { Core } from '@ebs/styleguide';
const { Button } = Core;

export default function ViewRequestServiceView(props) {
  // Props
  const location = useLocation();
  

  const { batchDetails, currentData, prevPath } = location.state;
  const [error, setError] = useState(false);
  const [message, setMessage] = useState(false);
  /**
   * TODO: Handle the dyanmic id for the FormattedMessage given
   * the specific label for i18n.
   */
  let buttonLabel = 'Back';
  if (prevPath === '/sm/requestmanager') buttonLabel = 'Back to Request Manager';
  else if (prevPath === '/sm/review')
    buttonLabel = 'Back to Genotyping Service Manager';
  else if (prevPath === '/sm/batchmanager') buttonLabel = 'Back to Batch Manager';
  else if (prevPath.includes('/sm/batchmanager/view'))
    buttonLabel = 'Back to View Batch';
  /*
  @prop data-testid: Id to use inside viewrequestservice.test.js file.
 */
  return (
    <div>
      <br />
      <br />
      <Button
        startIcon={<ArrowBackIos />}
        component={Link}
        variant='contained'
        color='primary'
        to={prevPath}
        state={{
          currentData: batchDetails,
        }}
      >
        <FormattedMessage id={'some.id'} defaultMessage={buttonLabel} />
      </Button>
      <br />
      <br />
      {error ? (
        <>
          <br />
          <LoadingGear message={message} />
        </>
      ) : (
        <ViewRequest
          rowData={currentData}
          setError={setError}
          setMessage={setMessage}
        />
      )}
    </div>
  );
}
// Type and required properties
ViewRequestServiceView.propTypes = {};

