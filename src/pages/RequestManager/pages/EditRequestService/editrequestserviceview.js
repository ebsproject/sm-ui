import React, { useEffect } from 'react';
import PropTypes from 'prop-types';

import { useDispatch } from 'react-redux';

import RequestServiceContent from 'components/organisms/RequestServiceContent';
import { useLocation } from 'react-router-dom';

export default function EditRequestServiceView(props) {
  const location = useLocation();
  const { currentData } = location.state;
  const previousPath = location.state?.previousPath || '/sm/requestmanager';
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({
      type: 'SET_MODE',
      payload: 'EDIT',
    });
  }, []);

  return (
    <div>
      <RequestServiceContent currentData={currentData} previousPath={previousPath} />
    </div>
  );
}
// Type and required properties
EditRequestServiceView.propTypes = {};

