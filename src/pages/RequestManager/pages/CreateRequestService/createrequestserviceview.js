import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import RequestServiceContent from 'components/organisms/RequestServiceContent';
import { useLocation } from 'react-router-dom';

export default function CreateRequestServiceView() {
  const location = useLocation();
  const previousPath = location.state?.previousPath || '/sm/requestmanager';

  const dispatch = useDispatch();

  dispatch({
    type: 'SET_MODE',
    payload: 'CREATE',
  });

  return (
    <div>
      <RequestServiceContent previousPath={previousPath} />
    </div>
  );
}

CreateRequestServiceView.propTypes = {};
