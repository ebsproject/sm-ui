import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import Container from '@mui/material/Container';
import RequestStatusGrid from 'components/organisms/RequestStatusGrid';
import AlertNotification from 'components/atoms/AlertNotification';
import { userContext } from '@ebs/layout';
import { Core } from '@ebs/styleguide';
const { LinearProgress } = Core;

export default function RequestManagerView(props) {
  const { userProfile } = useContext(userContext);

  return (
    <Container
      data-testid={'RequestManagerTestId'}
      component='main'
      maxWidth='xl'
      disableGutters
      fixed
      style={{ background: '#ffffff', padding: '10px' }}
    >
      <AlertNotification />
      {!userProfile ? (
        <LinearProgress />
      ) : (
        <RequestStatusGrid userProfile={userProfile} />
      )}
    </Container>
  );
}

RequestManagerView.propTypes = {};

