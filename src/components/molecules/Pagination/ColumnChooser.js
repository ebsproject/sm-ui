import React from "react";
import Checkbox from "@mui/material/Checkbox";
import Button from '@mui/material/Button';
import Popover from '@mui/material/Popover';
import FormControl from '@mui/material/FormControl';
import FormGroup from '@mui/material/FormGroup';
import FormControlLabel from '@mui/material/FormControlLabel';
import { withStyles } from "@mui/material/styles";
//import 'font-awesome/css/font-awesome.min.css';
import AlarmIcon from '@mui/icons-material/Reorder';
import Tooltip from '@mui/material/Tooltip';
export const popoverStyles = {
    boxElementPad: {
        padding: "16px 24px 16px 24px",
        height:'auto'
    },

    formGroup: {
        marginTop: "8px",
    },
    formControl: {},
    checkbox: {
        width: "12px",
        height: "12px",
    },
    // checkboxColor: {
    //     "&$checked": {
    //         color: "#027cb5",
    //     },
    // },
    checked: {},
    label: {
        fontSize: "15px",
        marginLeft: "5px",
        color: "green",
        fontFamily: "seriff"
    },

};
class ColumnChooser extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            open: false,
        };
    }

    handleClick = (event) => {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            open: true,
            anchorEl: event.currentTarget,
        });
    };

    handleRequestClose = () => {
        this.setState({
            open: false,
        });
    };


    handleColChange = index => {
        this.props.onColumnUpdate(index);
    };

    render() {
        const { classes, columns } = this.props;

        return (
            <div>
              <Tooltip id="tooltip-columns" title="Select Columns">
                <Button size="small" variant="outlined" onClick={this.handleClick} style={{backgroundColor:'white',height:'32px',padding:'2px',marginLeft:'5px'}} >
                    <AlarmIcon className={classes.icon} />
                </Button>
                </Tooltip>
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                    transformOrigin={{horizontal: 'left', vertical: 'top'}}
                    onClose={this.handleRequestClose}
                >
                    <FormControl component={"fieldset"} className={classes.boxElementPad} >
                        <FormGroup className={classes.formGroup}>
                            {columns.map((column, index) => {
                                if(column.id === "_selector") return null
                                return (
                                    <FormControlLabel
                                        key={`columnChooser_formLabel_${index}`}
                                        classes={{
                                            root: classes.formControl,
                                            label: classes.label,
                                        }}
                                        control={
                                            <Checkbox
                                                className={classes.checkbox}

                                                onChange={() => this.handleColChange(index)}
                                                checked={column.show}
                                                value={column.id}
                                            />
                                        }
                                        label={column.Header}
                                    />
                                );
                            })}
                        </FormGroup>
                    </FormControl>
                </Popover>
            </div>
        );
    }
}

export default withStyles(popoverStyles)(ColumnChooser);
