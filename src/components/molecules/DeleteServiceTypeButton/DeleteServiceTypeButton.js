import { Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { Delete, DeleteForever, Send, Warning } from '@mui/icons-material';
import React, { forwardRef } from 'react';
import { FormattedMessage } from 'react-intl';
import { SMgraph } from 'utils/apollo/SMclient';
import { DELETE_SERVICE_TYPE } from 'utils/apollo/gql/RequestManager';
import { useDispatch } from 'react-redux';
import { Core } from '@ebs/styleguide';
const { Typography, Button, IconButton } = Core;

const DeleteServiceTypeButton = forwardRef((props, ref) => {
  const { rowData, refresh, ...rest } = props;
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  async function deleteServiceType() {
    const { data, error, loading } = await SMgraph.mutate({
      mutation: DELETE_SERVICE_TYPE,
      variables: {
        id: rowData.id,
      },
    });
    if (data.deleteServiceType) {
      setOpen(false);
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: `Successfully deleted Service Type: ${rowData.name}`,
          translationId: 'servicetype.deleted',
          severity: 'success',
        },
      });
      refresh();
    }
  }

  return (
    <div ref={ref}>
      <IconButton title='Delete' onClick={handleOpen}>
        <DeleteForever />
      </IconButton>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>
          <Typography>
            <Delete />
            <FormattedMessage
              id={'some.id'}
              defaultMessage={`Delete ${rowData?.name}`}
            />
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Typography>
            <Warning style={{ color: 'orange' }} />
            <FormattedMessage
              id={'some.id'}
              defaultMessage={`You are about to delete
                        ${rowData?.name}. This action will delete the service type in the
                        list of service types. Click confirm to proceed.`}
            />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <FormattedMessage id={'some.id'} defaultMessage={'Cancel'} />
          </Button>
          <Button variant='contained' onClick={deleteServiceType} endIcon={<Send />}>
            <FormattedMessage id={'some.id'} defaultMessage={'CONFIRM'} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});

export default DeleteServiceTypeButton;
