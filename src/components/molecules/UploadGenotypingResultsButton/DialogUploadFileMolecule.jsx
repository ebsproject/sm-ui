import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from '@mui/material';
import UploadMolecule from './UploadMolecule';


const DialogUploadFileMolecule = forwardRef(
  (
    { open, handleClose, fileTypes, handleSelectFile, handleConfirmUpload, idField },
    ref,
  ) => {
    return (
      <Box ref={ref}>
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>Upload results</DialogTitle>
          <DialogContent>
            <UploadMolecule
              label='Upload file result'
              uploadedLabel='Ready to process file'
              handleChange={handleSelectFile}
              name={idField}
              types={fileTypes}
            />
          </DialogContent>
          <DialogActions>
            <Button
              onClick={handleConfirmUpload}
              id='batch.grid.upload.button.upload'
            >
              Upload
            </Button>
          </DialogActions>
        </Dialog>
      </Box>
    );
  },
);

export default DialogUploadFileMolecule;
