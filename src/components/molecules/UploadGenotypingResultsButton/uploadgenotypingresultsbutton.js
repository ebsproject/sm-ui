import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
// CORE COMPONENTS AND ATOMS TO USE
import { SMrest } from 'utils/axios/SMclient';
import {
  Input,
  Dialog,
  DialogContent,
  DialogContentText,
  DialogTitle,
  DialogActions,
  Button,
  IconButton,
  Box,
  CircularProgress,
} from '@mui/material';
import PublishIcon from '@mui/icons-material/Publish';
import useUploadGenotypingResult from './hooks/use-uploadGenotypingResult';
import { DropzoneDialog } from '@ebs/styleguide';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const UploadGenotypingResultsButtonMolecule = React.forwardRef(
  ({ rowData, uploadState, refresh }, ref) => {
    const {
      fileTypes,
      handleCloseUploadFile,
      handleCloseWarning,
      handleOpenUploadFile,
      handleSubmitFile,
      handleSubmitOverFile,
      keyFile,
      messageApi,
      openLoading,
      openUploadFile,
      openWarningMessage,
      title,
    } = useUploadGenotypingResult({
      rowData,
      uploadState,
      refresh,
    });

    return (
      <div ref={ref}>
        <IconButton title={title} onClick={handleOpenUploadFile}>
          <PublishIcon />
        </IconButton>
        <Box>
          <DropzoneDialog
            error={''}
            open={openUploadFile}
            onClose={handleCloseUploadFile}
            onSave={(files) => {
              handleSubmitFile(files);
            }}
            acceptedFiles={fileTypes}
            cancelButtonText={'Cancel'}
            submitButtonText={'OK'}
            maxFileSize={5000000}
            showPreviews={true}
            showFileNamesInPreview={true}
          />
        </Box>

        <Dialog open={openLoading}>
          <DialogContent>
            <CircularProgress />
          </DialogContent>
        </Dialog>

        <Dialog open={openWarningMessage} onClose={handleCloseWarning}>
          <DialogTitle>Upload results</DialogTitle>
          <DialogContent>
            <DialogContentText>
              The file with key: ${keyFile} has validation exceptions: {messageApi}.
              Do you want to save it anyway or discard it? After discarding it, you
              can fix the validation issues and try again or select a different file.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseWarning}>Discard</Button>
            <Button onClick={handleSubmitOverFile}>Import with warnings</Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  },
);

export default UploadGenotypingResultsButtonMolecule;
