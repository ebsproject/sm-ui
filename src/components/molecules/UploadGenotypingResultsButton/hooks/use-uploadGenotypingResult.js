import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { SMrest } from 'utils/axios/SMclient';

const useUploadGenotypingResult = ({ uploadState, rowData, refresh }) => {
  const dispatch = useDispatch();

  const cleanState = () => {
    setOpen(false);
    setDisable({ disableAccept: false, disableDiscard: false });
    refresh();
  };

  const handleClose = () => {
    // if (keyFile !== '') {
    //   setDisable({ disableAccept: true, disableDiscard: true });
    //   handleRemove();
    // }
    setOpen(false);
  };

  const handleOpenDialog = () => {
    setOpen(true);
  };

  const handleClickUploadFile = () => {
    SMrest.put(`batch/upload-results/${keyFile}?accepted=true`)
      .then((response) => {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: `Successfully file with the Key: ${keyFile}`,
            translationId: 'servicetype.upload_success',
            severity: 'success',
          },
        });
        cleanState();
      })
      .catch((error) => {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: `Fail to save file with the Key: ${keyFile}`,
            translationId: 'servicetype.upload_success',
            severity: 'error',
          },
        });
        console.error(
          'Error save file => ',
          error.response?.data.metadata.status[1].message,
        );
      });
  };
  // NEW code

  const fileTypes = ['.csv', '.xls', '.xlsx', '.intertek'];
  const [openUploadFile, setOpenUploadFile] = useState(false);
  const [openWarningMessage, setOpenWarningMessage] = useState(false);
  const [keyFile, setKeyFile] = useState('');
  const [messageApi, setMessageApi] = useState('');
  const [openLoading, setOpenLoading] = useState(false);

  const handleSubmitFile = (file) => {
    if (file && file.length > 0) {
      setOpenLoading(true);
      setOpenUploadFile(false);
      uploadFile(file[0]);
    }
  };

  const handleCloseUploadFile = async () => {
    if (keyFile) {
      await removeFileFromServer();
      setOpenUploadFile(false);
    }
    setOpenUploadFile(false);
  };

  const handleOpenUploadFile = () => {
    setOpenUploadFile(true);
  };

  const handleCloseWarning = () => {
    setOpenWarningMessage(false);
    removeFileFromServer();
  };

  const title =
    uploadState === 'isNotYetUploaded' ? 'Upload Results' : 'Results Uploaded';

  const removeFileFromServer = async () => {
    SMrest.put(`batch/upload-results/${keyFile}?accepted=false`)
      .then((response) => {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: `File with the Key: ${keyFile} was discarded`,
            translationId: 'clean.file.api',
            severity: 'warning',
          },
        });
      })
      .catch((error) => {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: `The file with key ${keyFile},  could not be deleted`,
            translationId: 'servicetype.upload_success',
            severity: 'error',
          },
        });
        console.error(
          'Error save file => ',
          error.response?.data.metadata.status[0].message,
        );
      });
  };

  const uploadFile = (file) => {
    let formData = new FormData();
    formData.append('file', file);
    formData.append('tags', 'gigwa, batchId:' + rowData.id); // gigwa can be connected to multiple db hosts
    let alertPayload;
    if (file.size == 0) {
      alertPayload = {
        message: `Error the file does not have data the file size is:` + file.size,
        translationId: 'servicetype.upload_error',
        severity: 'error',
      };
    } else {
      SMrest.post(`batch/upload-results/1`, formData)
        .then((response) => {
          setOpenLoading(false);
          if (response.data.result.data[0])
            alertPayload = {
              message:
                `Successfully uploaded file with the Key` +
                response.data.result.data[0].key,
              translationId: 'servicetype.upload_success',
              severity: 'success',
            };
          else {
            alertPayload = {
              message:
                `Something went wrong reports this issue to your admin, check the console` +
                response.data.result,
              translationId: 'servicetype.upload_error',
              severity: 'error',
            };
          }
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: alertPayload,
          });
          refresh();
        })
        .catch((err) => {
          setOpenLoading(false);
          if (err.status === 500) {
            const { status } = err.response.data.metadata;
            const message =
              status && status.length > 0
                ? status[0].message
                    .replace(
                      '"{"result":"","metadata":{"pagination":{"pageSize":0,"currentPage":0,"totalCount":0,"totalPages":0},"status":[{"code":1,"message":"',
                      '',
                    )
                    .replace('"}],"dataFiles":[]}}"', '')
                : 'Error';
            alertPayload = {
              message,
              translationId: 'servicetype.upload_error',
              severity: 'error',
            };
            dispatch({
              type: 'ALERT_MESSAGE',
              payload: alertPayload,
            });
          }
          if (err.status === 409) {
            const {
              result: { data: data },
              metadata: { status: status },
            } = err.response.data;
            const keyFile = data && data.length !== 0 ? data[0].key : '';
            if (keyFile !== '') {
              setMessageApi(status[1].message);
              setKeyFile(keyFile);
              setOpenWarningMessage(true);
            } else {
              alertPayload = {
                message: `Not found key: ${status[0].message}`,
                translationId: 'servicetype.upload_error',
                severity: 'error',
              };
            }
          }
        });
    }
  };

  const handleSubmitOverFile = () => {
    setOpenWarningMessage(false);
    SMrest.put(`batch/upload-results/${keyFile}?accepted=true`)
      .then((response) => {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: `Successfully file with the Key: ${keyFile}`,
            translationId: 'servicetype.upload_success',
            severity: 'success',
          },
        });
        refresh();
      })
      .catch((error) => {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: `Fail to save file with the Key: ${keyFile}`,
            translationId: 'servicetype.upload_success',
            severity: 'error',
          },
        });
        console.error(
          'Error save file => ',
          error.response?.data.metadata.status[1].message,
        );
      });
  };

  return {
    fileTypes,
    handleCloseUploadFile,
    handleCloseWarning,
    handleOpenUploadFile,
    handleSubmitFile,
    handleSubmitOverFile,
    keyFile,
    messageApi,
    openLoading,
    openUploadFile,
    openWarningMessage,
    title,
  };
};

export default useUploadGenotypingResult;
