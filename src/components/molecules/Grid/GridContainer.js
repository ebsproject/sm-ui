import React from "react";
// nodejs library to set properties for components
import PropTypes from "prop-types";
// @mui/material components

import Grid from "@mui/material/Grid";



export default function GridContainer(props) {
  const { children, className, ...rest } = props;
  return (
    <Grid container {...rest}>
      {children}
    </Grid>
  );
}

GridContainer.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};
