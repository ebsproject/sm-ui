import React from "react";
import PropTypes from "prop-types";
import Grid from "@mui/material/Grid";


// TODO: to delete
export default function GridItem(props) {
  const { children, className, ...rest } = props;
  return (
    <Grid item {...rest}>
      {children}
    </Grid>
  );
}

GridItem.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};
