import React, { forwardRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Box, Button, IconButton, Stack, Tooltip, Typography } from '@mui/material';
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

const WellsPaginationMolecule = forwardRef(
  ({ tabs, page, onChangePage, numberTabsDisplays, parentId }, ref) => {
    const elementsTabs = tabs ? tabs.length : 0;
    const [{ lowerTab, hightTab }, setDisplayTabs] = useState({
      lowerTab: -1,
      hightTab: numberTabsDisplays,
    });

    const handleClickBack = () => {
      if (lowerTab !== -1) {
        setDisplayTabs({
          lowerTab: lowerTab - numberTabsDisplays,
          hightTab: hightTab - numberTabsDisplays,
        });
        onChangePage(lowerTab - numberTabsDisplays + 1);
      }
    };

    const handleClickNext = () => {
      setDisplayTabs({
        lowerTab: lowerTab + numberTabsDisplays,
        hightTab: hightTab + numberTabsDisplays,
      });
      onChangePage(lowerTab + numberTabsDisplays + 1);
    };

    return (
      <Box ref={ref}>
        {elementsTabs === 0 && <Box>Loading...</Box>}
        {elementsTabs > 0 && (
          <Stack
            direction='row'
            spacing={0}
            data-testid={'batch.wellPagination.molecule'}
          >
            <Box>
              <Tooltip title='BACK'>
                <span>
                  <IconButton
                    data-testid={'button.back'}
                    disabled={lowerTab === -1}
                    id={`${parentId}.buttonBack`}
                    onClick={handleClickBack}
                  >
                    <ArrowBackIosIcon />
                  </IconButton>
                </span>
              </Tooltip>
            </Box>
            {tabs.map(({ id, batchCode }) => {
              if (lowerTab < id && id < hightTab)
                return (
                  <Box key={`batch.Plate.Tab.${id}`}>
                    <Button
                      onClick={() => onChangePage(id)}
                      variant={id !== page ? 'outlined' : 'contained'}
                    >
                      {batchCode}
                    </Button>
                  </Box>
                );
            })}
            <Box>
              <Tooltip title='NEXT'>
                <span>
                  <IconButton
                    data-testid={'button.next'}
                    disabled={hightTab >= elementsTabs}
                    id={`${parentId}.buttonNext`}
                    onClick={handleClickNext}
                  >
                    <ArrowForwardIosIcon />
                  </IconButton>
                </span>
              </Tooltip>
            </Box>
          </Stack>
        )}
      </Box>
    );
  },
);

WellsPaginationMolecule.propTypes = {
  tabs: PropTypes.array,
  page: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  parentId: PropTypes.string.isRequired,
  numberTabsDisplays: PropTypes.number.isRequired,
};

export default WellsPaginationMolecule;
