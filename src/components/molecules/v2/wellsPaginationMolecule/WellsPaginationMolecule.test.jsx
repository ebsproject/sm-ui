import { expect, jest, test, describe } from '@jest/globals';
import { fireEvent, render, screen } from '@testing-library/react';
import WellsPaginationMolecule from './WellsPaginationMolecule';

const testId = 'batch.wellPagination.molecule';
describe('Test for Display or Wells Pagination Molecule => ', () => {
  const props = {
    tabs: [
      { id: 0, batchCode: 'one' },
      { id: 1, batchCode: 'two' },
      { id: 2, batchCode: 'three' },
    ],
    page: 0,
    onChangePage: jest.fn(),
    numberTabsDisplays: 5,
    parentId: 'test',
  };
  test('Render component with 3 tabs ', () => {
    render(
      <WellsPaginationMolecule
        {...props}
        tabs={[
          { id: 0, batchCode: 'one' },
          { id: 1, batchCode: 'one' },
          { id: 2, batchCode: 'one' },
        ]}
      />,
    );

    const component = screen.queryByTestId(testId);
    const list = screen.queryAllByText(/one/);

    expect(component).toBeInTheDocument();
    expect(list).toHaveLength(3);
  });

  test('Select one component', () => {
    render(
      <WellsPaginationMolecule
        {...props}
        tabs={[
          { id: 0, batchCode: 'one' },
          { id: 1, batchCode: 'two' },
          { id: 2, batchCode: 'three' },
        ]}
      />,
    );

    const component = screen.queryByTestId(testId);
    const list = screen.queryByText(/one/);

    expect(component).toBeInTheDocument();
    expect(list).toHaveAttribute('disabled');
  });

  test('Change of page', () => {
    const onChangePage = jest.fn();

    render(<WellsPaginationMolecule {...props} onChangePage={onChangePage} />);

    const tab = screen.queryByText(/two/);
    fireEvent.click(tab);

    expect(tab).toBeInTheDocument();
    expect(onChangePage).toBeCalled();
  });

  test('List first 5 ', () => {
    const onChangePage = jest.fn();

    render(
      <WellsPaginationMolecule
        {...props}
        onChangePage={onChangePage}
        tabs={[
          { id: 0, batchCode: 'one' },
          { id: 1, batchCode: 'one' },
          { id: 2, batchCode: 'one' },
          { id: 3, batchCode: 'one' },
          { id: 4, batchCode: 'one' },
          { id: 5, batchCode: 'one' },
        ]}
      />,
    );

    const tabs = screen.queryAllByText(/one/);

    expect(tabs).toHaveLength(5);
  });

  test('Check disable the tow buttons ', async () => {
    const parentId = 'test';

    render(
      <WellsPaginationMolecule
        {...props}
        parentId={parentId}
        tabs={[
          { id: 0, batchCode: 'one' },
          { id: 1, batchCode: 'one' },
          { id: 2, batchCode: 'one' },
          { id: 3, batchCode: 'one' },
        ]}
      />,
    );

    const back = screen.getByTestId('button.back');
    const next = screen.getByTestId('button.next');

    expect(back).toBeInTheDocument();
    expect(next).toBeInTheDocument();
    expect(back).toHaveAttribute('disabled');
    expect(next).toHaveAttribute('disabled');
  });

  test('Check enable next button ', async () => {
    const parentId = 'test';

    render(
      <WellsPaginationMolecule
        {...props}
        parentId={parentId}
        tabs={[
          { batchCode: 'one', id: 0 },
          { batchCode: 'one', id: 1 },
          { batchCode: 'one', id: 2 },
          { batchCode: 'one', id: 3 },
          { batchCode: 'one', id: 4 },
          { batchCode: 'one', id: 5 },
        ]}
      />,
    );

    const back = screen.getByTestId('button.back');
    const next = screen.getByTestId('button.next');

    expect(back).toBeInTheDocument();
    expect(next).toBeInTheDocument();
    expect(back).toHaveAttribute('disabled');
    expect(next).not.toHaveAttribute('disabled');
  });

  test('List first 5 and check button function ', async () => {
    const parentId = 'test';

    render(
      <WellsPaginationMolecule
        {...props}
        parentId={parentId}
        tabs={[
          { batchCode: 'one', id: 0 },
          { batchCode: 'one', id: 1 },
          { batchCode: 'one', id: 2 },
          { batchCode: 'one', id: 3 },
          { batchCode: 'one', id: 4 },
          { batchCode: 'one', id: 5 },
        ]}
      />,
    );

    const tabs = screen.queryAllByText(/one/);
    const back = screen.getByTestId('button.back');
    const next = screen.getByTestId('button.next');

    fireEvent.click(next);

    const newTabs = screen.queryAllByText(/one/);

    expect(tabs).toHaveLength(5);
    expect(back).toBeInTheDocument();
    expect(next).toBeInTheDocument();
    expect(newTabs).toHaveLength(1);
    expect(next).toHaveAttribute('disabled');
  });

  test('Render with out tabs', () =>{
    render(
      <WellsPaginationMolecule
        {...props}
        tabs={null}
      />,
    );
    const loading = screen.queryByText(/loading/i);

    expect(loading).toBeInTheDocument();
  })
});
