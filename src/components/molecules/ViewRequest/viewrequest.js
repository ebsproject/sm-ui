import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Tabs,
  Tab,
  Paper,
  Card,
  CardContent,
  Table,
  TableCell,
  TableBody,
  TableRow,
  Grid,
} from '@mui/material';
import GridHeaderAtom from 'components/atoms/GridHeader';
import { FormattedMessage } from 'react-intl';
import { orderBy } from 'lodash';
import { getRequesterOwnerInfo } from 'utils/axios/B4Rclient';
import { SMgraph, MarkerDbGraph } from 'utils/apollo/SMclient';
import { FIND_REQUEST_STATUS_LIST } from 'utils/apollo/gql/RequestManager';
import { FIND_MARKER_LIST } from 'utils/apollo/gql/MarkerDb';
import { getUserById } from 'utils/axios/CSclient';
import { Visibility } from '@mui/icons-material';
import EntryListContent from 'components/atoms/EntryListContent';
import MarkerGroupContent from 'components/atoms/MarkerGroupContent';
import StatusBadge from 'components/atoms/StatusBadge';
import { Core } from '@ebs/styleguide';
const { Typography, IconButton } = Core;

const styles = {
  wrapIcon: {
    verticalAlign: 'middle',
    display: 'inline-flex',
  },
  grayBackground: {
    background: '#FAFAFA',
  },
  bold: {
    fontWeight: 600,
    fontSize: '13px',
  },
  marginRight: {
    marginRight: 20,
  },
  statusBadge: {
    background: '#2096F3',
    borderRadius: 3,
    color: 'white',
    fontWeight: 700,
    padding: 5,
    textTransform: 'uppercase',
  },
  table: {
    maxWidth: 500,
  },
  tableCell: {
    paddingRight: 5,
    fontSize: '14px',
  },
};

const notSetLabel = (
  <Typography component='span' color='primary'>
    <Box fontStyle='italic'>(not set)</Box>
  </Typography>
);

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ViewRequestButtonMolecule = React.forwardRef((props, ref) => {
  const { setError, setMessage, refresh } = props;
  const { rowData } = props;
  const [open, setOpen] = useState(false);
  const [selectedMarkerGroup, setSelectedMarkerGroup] = useState(null);
  const [requesterOwnerInfo, setRequesterOwnerInfo] = useState(null);
  const [germplasmOwnerInfo, setGermplasmOwnerInfo] = useState(null);
  const [timestamp, setTimestamp] = useState('');
  const [teamWorkContact, setTeamWorkContact] = useState(null);
  const [traitCustomNames, setTraitCustomNames] = useState('');
  const reArrangedRowData = rearrangeRowData(rowData);
  const [value, setValue] = useState(0);
  const [{ id: markerGroupID, name: markerGroupName }, setMarkerGroupData] =
    useState({ id: 0, name: '' });

  useEffect(() => {
    const setValues = async () => {
      await getRequesterOwnerInfomation();
      // await getTraitCustomNames(rowData);
      await getTeamWorkContactName();
      await getTimestamp(rowData.id);
      if (!germplasmOwnerInfo) await getGermplasmOwnerInfo();
    };
    setValues().catch((error) => {
      throw new Error(error);
    });
  }, []);

  const dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };

  async function getRequesterOwnerInfomation() {
    try {
      const userInfo = await getUserById(rowData.requesterOwnerId);
      setRequesterOwnerInfo({
        requester: userInfo.data ? userInfo.data.full_name : '',
        requesterEmail: userInfo.data ? userInfo.data.account : '',
      });
    } catch (error) {
      throw new Error(error);
    }
  }

  async function getGermplasmOwnerInfo() {
    try {
      const responseGermplasm = await getRequesterOwnerInfo(
        rowData.germplasmOwnerId,
      );

      setGermplasmOwnerInfo({
        personName: responseGermplasm.data.result.data.length
          ? responseGermplasm.data.result.data[0].personName
          : '',
        email: responseGermplasm.data.result.data.length
          ? responseGermplasm.data.result.data[0].email
          : '',
      });
    } catch (error) {
      throw new Error(error);
    }
  }

  async function getTeamWorkContactName() {
    try {
      const userInfo = await getUserById(rowData.adminContactId);
      const personName = userInfo.data.full_name;
      setTeamWorkContact(personName);
    } catch (error) {
      console.error('error ', error);
      const message = error?.response?.data?.metadata.status[0].messageType;
      if (message) {
        setMessage(message);
        setError(true);
        throw new Error(error);
      } else {
        setMessage(
          `There was a problem retrieving the /user/${rowData?.adminContactId} call from the CS-API.`,
        );
      }
    }
  }

  const getTimestamp = async (requestId) => {
    try {
      const response = await SMgraph.query({
        query: FIND_REQUEST_STATUS_LIST,
        variables: {
          sort: { col: 'id', mod: 'DES' },
          filters: [{ col: 'request.id', mod: 'EQ', val: requestId }],
          disjunctionFilters: false,
        },
        fetchPolicy: 'no-cache',
      });

      const requestStatusList = response.data.findRequestStatusList.content;

      /**
       * The index used in setting the timestamp is the second element in the
       * requestStatusList because that is the last element with the updated
       * 'completed' property.
       *
       * If the request is just newly created, obviously there's only a single
       * element in the requestStatusList. To prevent the out of bounce error,
       * hence using 0 as index.
       */
      const REQUEST_WITH_NEW_STATUS = 0;
      const REQUEST_WITH_OTHER_STATUS = 1;
      const index =
        requestStatusList.length > 1
          ? REQUEST_WITH_OTHER_STATUS
          : REQUEST_WITH_NEW_STATUS;

      const timestamp = requestStatusList[index].completed || 'Not Applicable';
      // TODO: Readable Date
      setTimestamp(timestamp);
    } catch (error) {
      throw new Error(error);
    }
  };

  function rearrangeRowData(rowData) {
    //If there are selected traits, get the vendor and markerpanel name to display
    if (!rowData?.traitCustom[0]?.markerpanel) {
      for (let item of rowData.traitCustom) {
        if (item?.markergroups?.length > 0) {
          item.markerpanel =
            item.markergroups.map((mg) => mg.name).join(' , ') || '(no markerpanel)';
          item.vendor = ' ';
        }
      }
    }
    /** SM-667
     * This is just for the ICC value. The label will be changed from
     * 'ICC' to 'Account'. This is a simple implementation to
     * override the EBS Forms from the database.
     */
    for (let value of rowData.values) {
      if (value.field.label === 'ICCs') value.field.label = 'Accounts';
    }

    /**
     * Sorting rowData.values alphabetically. Since JavaScript objects can be
     * randomlly sorted every time.
     */
    rowData.values = orderBy(rowData.values, ['field.label'], ['asc']);

    /**
     * This is used for rearranging the keys of the rowData
     * and prioritize the "Status" key
     */
    if (!rowData) return;
    const reArrangedRowData = {
      crop: {
        label: 'Crop',
        value: rowData.crop.name,
      },
      program: {
        label: 'Program',
        value: rowData.program.name,
      },
      requesterEmail: {
        label: 'Requester Email',
        value: requesterOwnerInfo?.requesterEmail,
      },
      germplasmOwner: {
        label: 'List Creator',
        value: germplasmOwnerInfo?.personName,
      },
      germplasmOwnerEmail: {
        label: 'List Creator Email',
        value: germplasmOwnerInfo?.email,
      },
      adminContact: {
        label: 'Team Work Contact',
        value: teamWorkContact,
      },
      tissueType: {
        label: 'Tissue Type',
        value: rowData?.tissuetype?.name,
      },
      samplesperItem: {
        label: 'Number of samples per item',
        value: Boolean(rowData?.samplesperItem) ? rowData?.samplesperItem : '1',
      },
      totalEntities: {
        label: 'Total Entities',
        value: rowData.totalEntities,
      },
      /*
      service: {
        label: 'Service',
        value: rowData?.service ? rowData.service.name : notSetLabel,
      },*/
      servicetype: {
        label: 'Service Type',
        value: rowData.servicetype.name,
      },
      serviceprovider: {
        label: 'Service Laboratory',
        value: rowData.serviceprovider.name,
      },
      purpose: {
        label: 'Purpose',
        value: rowData.purpose.name,
      },

      selectedTraits: {
        label: 'Selected Traits',
        value: null,
      },
    };
    if (rowData.markerPanel && rowData.markerPanel.length)
      reArrangedRowData.markerPanel = {
        label: 'Marker Group',
        value: Boolean(rowData && rowData.markerPanel && rowData.markerPanel.length),
      };
    if (!rowData.traitCustom || rowData.traitCustom.length === 0)
      delete reArrangedRowData.selectedTraits;
    return reArrangedRowData;
  }

  const REQUEST_DETAILS = {
    requestCode: {
      label: 'Request Code',
      value: rowData.requestCode,
    },
    status: {
      label: 'Status',
      value: rowData.status,
    },
    requester: {
      label: 'Requester',
      value: requesterOwnerInfo?.requester,
    },
    nameOfApproverOrRejecter: {
      label: 'Approver/Rejecter',
      value: '',
    },
    justificationForRejection: {
      label: 'Justification for Rejection',
      value:
        rowData.status.name === 'Rejected'
          ? rowData.requeststatuss[rowData.requeststatuss.length - 1].comments
          : 'Not Applicable',
    },
    timeOfApprovalOrRejection: {
      label: 'Approval/Rejection Timestamp',
      value: timestamp,
    },
    nameOfBatch: {
      label: 'Name of Batch',
      value: rowData.batch === null ? 'Not Applicable' : rowData.batch.name,
    },
    statusOfBatch: {
      label: 'Status of Batch',
      value: rowData.batch === null ? 'Not Applicable' : 'Created',
    },
  };

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

  function TabPanel(props) {
    const { children, value, index } = props;

    return (
      <div role='tabpanel' hidden={value !== index}>
        <br />
        {value === index && (
          <Card>
            <CardContent sx={styles.grayBackground}>{children}</CardContent>
          </Card>
        )}
      </div>
    );
  }

  function tableValue(value) {
    /**
     * This function is to check if the value of the row from the rowData
     * is a string or null. This is because a request can be created
     * in which some of its values can be an empty string or a
     * null value.
     */
    if (value === '' || value === null) return notSetLabel;
    return value;
  }

  const handleClickOpen = async (markerPanel) => {
    try {
      const response = await MarkerDbGraph.query({
        query: FIND_MARKER_LIST,
        variables: {
          filters: [{ col: 'markergroups.id', mod: 'EQ', val: markerPanel.id }],
        },
        fetchPolicy: 'no-cache',
      });
      setSelectedMarkerGroup({
        name: markerPanel.name,
        markers: response.data.findMarkerList.content,
      });
      setMarkerGroupData({ id: parseInt(markerPanel.id), name: markerPanel.name });
      setOpen(true);
    } catch (error) {
      throw new Error(error);
    }
  };

  const handleClose = () => {
    setMarkerGroupData({ id: 0, name: '' });
    setSelectedMarkerGroup(null);
    setOpen(false);
  };

  return (
    /* All ViewButton component displayed in every row (including Dialog component) */
    /*
     @prop data-testid: Id to use inside viewrequestbutton.test.js file.
     */
    <div ref={ref} data-testid={'ViewRequestButtonTestId'}>
      <div>
        <div id='responsive-dialog-title'>
          <Typography component='span' sx={styles.wrapIcon}>
            <GridHeaderAtom
              title={`Viewing ${rowData.requestCode}`}
              description={' '}
            />
          </Typography>
        </div>
        <Box sx={styles.grayBackground} component={'div'}>
          <Paper square>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label='simple tabs example'
            >
              <Tab
                label={<FormattedMessage id={'some.id'} defaultMessage={'BASIC'} />}
                {...a11yProps(0)}
              />
              <Tab
                label={
                  <FormattedMessage id={'some.id'} defaultMessage={'GERMPLASM'} />
                }
                {...a11yProps(1)}
              />
            </Tabs>
          </Paper>
          <TabPanel value={value} index={0}>
            <Typography variant='span' sx={styles.bold}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Basic Information'}
              />
            </Typography>
            <br />
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <Table size='small' sx={styles.table}>
                  <TableBody>
                    {reArrangedRowData &&
                      Object.keys(reArrangedRowData).map((key, index) => {
                        return key !== 'selectedTraits' ? (
                          <TableRow key={`viewRequest_tableRow_${key}`}>
                            <TableCell sx={{ ...styles.tableCell, ...styles.bold }}>
                              <FormattedMessage
                                id={'some.id'}
                                defaultMessage={`${reArrangedRowData[key].label}`}
                              />
                            </TableCell>
                            <TableCell align='left' sx={styles.tableCell}>
                              <div
                                style={{
                                  display: 'flex',
                                  justifyContent: 'space-between',
                                  alignItems: 'center',
                                }}
                              >
                                {key !== 'markerPanel'
                                  ? tableValue(reArrangedRowData[key].value)
                                  : ''}
                                {Boolean(
                                  key === 'markerPanel' &&
                                    rowData.markerPanel !== null,
                                ) && (
                                  <div>
                                    <table>
                                      <tbody>
                                        {rowData.markerPanel.map((item) => {
                                          return (
                                            <tr
                                              key={`viewRequest_markerPanel_tr_${item.id}`}
                                            >
                                              <Box
                                                sx={styles.tableCell}
                                                component={'td'}
                                              >
                                                {item?.name ?? ''}
                                              </Box>
                                              <td>
                                                <IconButton
                                                  color='primary'
                                                  title='View'
                                                  onClick={() =>
                                                    handleClickOpen(item)
                                                  }
                                                >
                                                  <Visibility />
                                                </IconButton>
                                              </td>
                                            </tr>
                                          );
                                        })}
                                      </tbody>
                                    </table>
                                  </div>
                                )}
                              </div>
                            </TableCell>
                          </TableRow>
                        ) : (
                          <TableRow key={key}>
                            {rowData?.traitCustom?.length > 0 ? (
                              <TableCell
                                colSpan={2}
                                style={{ padding: '15px' }}
                                sx={styles.tableCell}
                              >
                                <strong>
                                  <FormattedMessage
                                    id={'some.id'}
                                    defaultMessage={'Selected Traits'}
                                  />
                                </strong>

                                <br />
                                <br />

                                {rowData?.traitCustom?.length > 0 && (
                                  <div>
                                    <table>
                                      <thead>
                                        <tr>
                                          <th>Trait</th>
                                          <th>Category</th>
                                          <th>Marker Group</th>
                                          {/*<th>Vendor</th>*/}
                                        </tr>
                                      </thead>
                                      <tbody>
                                        {rowData.traitCustom.map((item) => {
                                          return (
                                            <tr
                                              key={`viewRequest_traitCustom_tr${item.id}`}
                                            >
                                              <Box
                                                sx={styles.tableCell}
                                                component={'td'}
                                              >
                                                {item?.name ?? ''}
                                              </Box>
                                              <Box
                                                sx={styles.tableCell}
                                                component={'td'}
                                              >
                                                {item?.traitCategory?.name ?? ''}
                                              </Box>
                                              <Box
                                                sx={styles.tableCell}
                                                component={'td'}
                                              >
                                                {item?.markerpanel ?? ''}
                                              </Box>
                                            </tr>
                                          );
                                        })}
                                      </tbody>
                                    </table>
                                  </div>
                                )}
                              </TableCell>
                            ) : (
                              <>
                                <TableCell
                                  sx={{ ...styles.tableCell, ...styles.bold }}
                                >
                                  <FormattedMessage
                                    id={'some.id'}
                                    defaultMessage={'Selected Traits'}
                                  />
                                </TableCell>
                                <TableCell sx={styles.tableCell} align='left'>
                                  {notSetLabel}
                                </TableCell>
                              </>
                            )}
                          </TableRow>
                        );
                      })}
                  </TableBody>
                </Table>
              </Grid>
              <Grid item xs={12} md={6}>
                <Grid item xs={12} md={12}>
                  <Table size='small' sx={styles.table}>
                    <TableBody>
                      {rowData.values &&
                        rowData.values.map((value) => (
                          <TableRow key={`viewRequest_rowData_tableRow_${value.id}`}>
                            <TableCell sx={{ ...styles.tableCell, ...styles.bold }}>
                              {value.field.label}
                            </TableCell>
                            <TableCell align='left' sx={styles.tableCell}>
                              {tableValue(
                                value.field.datatype.name == 'Date'
                                  ? new Date(value?.value)?.toLocaleDateString(
                                      'en-US',
                                      dateOptions,
                                    )
                                  : value.value,
                              )}
                            </TableCell>
                          </TableRow>
                        ))}
                    </TableBody>
                  </Table>
                </Grid>
                <br />
                <br />
              </Grid>
            </Grid>

            <br />
            <Typography variant='span' sx={styles.bold}>
              <FormattedMessage id={'some.id'} defaultMessage={'Request Details'} />
            </Typography>
            <br />
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <Table size='small' sx={styles.table}>
                  <TableBody>
                    {REQUEST_DETAILS &&
                      Object.keys(REQUEST_DETAILS).map((key, index) => (
                        <TableRow key={`viewRequest_requestDetails_tableRow_${key}`}>
                          <TableCell sx={{ ...styles.tableCell, ...styles.bold }}>
                            <FormattedMessage
                              id={'some.id'}
                              defaultMessage={`${REQUEST_DETAILS[key].label}`}
                            />
                          </TableCell>
                          <TableCell sx={styles.tableCell}>
                            {key === 'status' ? (
                              <StatusBadge value={REQUEST_DETAILS[key].value} />
                            ) : (
                              <span>{tableValue(REQUEST_DETAILS[key].value)}</span>
                            )}
                          </TableCell>
                        </TableRow>
                      ))}
                  </TableBody>
                </Table>
              </Grid>
            </Grid>
          </TabPanel>
          <TabPanel value={value} index={1}>
            <EntryListContent
              listId={rowData?.listId}
              setError={setError}
              setMessage={setMessage}
            />
          </TabPanel>
        </Box>
        <MarkerGroupContent
          markerGroup={selectedMarkerGroup}
          markerGroupId={markerGroupID}
          markerGroupName={markerGroupName}
          open={open}
          onClose={handleClose}
        />
      </div>
    </div>
  );
});
// Type and required properties
ViewRequestButtonMolecule.propTypes = {};

export default ViewRequestButtonMolecule;
