import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { Typography, IconButton } from '@mui/material';
import { Button } from 'components/atoms/CustomButtons';
import RejectIcon from '@mui/icons-material/Cancel';
import { FormattedMessage } from 'react-intl';
import ConfirmDialogRaw from 'components/atoms/ConfirmDialogRaw';
import { updateListRequestByBatchByID } from 'utils/apollo/SMclient';

const RejectBatchButtonMolecule = React.forwardRef((props, ref) => {
  const { rowData, refresh, children, idStatus, ...rest } = props;
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const history = useHistory();

  const rejectedRequest = async (query) => {
    updateListRequestByBatchByID(rowData, idStatus, false);
    successReject();
  };

  const warningMessage = () => {
    setOpen(true);
  };

  const successReject = () => {
    //do other stuff
    setOpen(false);
    refresh();
    dispatch({
      type: 'ALERT_MESSAGE',
      payload: {
        message: 'Request rejected',
        translationId: 'request.rejected',
        severity: 'warning',
      },
    });
    history.push('/sm/review');
  };

  const cancelReject = () => {
    //do other stuff
    setOpen(false);
  };

  return (
    /*
     @prop data-testid: Id to use inside rejectbutton.test.js file.
     */
    <div data-testid={'RejectButtonTestId'}>
      <ConfirmDialogRaw
        open={open}
        title={
          <FormattedMessage
            id={'sm.rejectreq.warning.title'}
            defaultMessage={'Are you sure?'}
          />
        }
        onOk={rejectedRequest}
        onCancel={cancelReject}
      >
        <Typography variant='body1'>
          <FormattedMessage
            id={'sm.rejectreq.warning.message'}
            defaultMessage={'Do you want reject this request?'}
          />
        </Typography>
      </ConfirmDialogRaw>
      <IconButton
        // id={`test-reject-${props.row.original.id}`}

        round
        onClick={warningMessage}
        color='primary'
        className='remove'
      >
        <RejectIcon />
      </IconButton>
    </div>
  );
});
// Type and required properties
RejectBatchButtonMolecule.propTypes = {};

export default RejectBatchButtonMolecule;
