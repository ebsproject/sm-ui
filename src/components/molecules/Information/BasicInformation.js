import React from 'react';

import Typography from "@mui/material/Typography";

import GridItem from "components/molecules/Grid/GridItem.js";
import GridContainer from "components/molecules/Grid/GridContainer.js";

import FormLabel from "@mui/material/FormLabel";
import Input from "@mui/material/Input";

//view expansion panel
import ExpansionPanel from '@mui/material/ExpansionPanel';
import ExpansionPanelSummary from '@mui/material/ExpansionPanelSummary';
import ExpansionPanelDetails from '@mui/material/ExpansionPanelDetails';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';

// TODO: to delete
import styles from "assets/jss/material-dashboard-pro-react/views/notificationsStyle.js";



// TODO: to delete
export default function BasicInformation(props) {
  const { row } = props

  return (
          <ExpansionPanel expanded={true}>
            <ExpansionPanelSummary
              expandIcon={<ExpandMoreIcon />}
              aria-controls="panel1a-content"
              id="panel1a-header"
            >
              <Typography >Basic information</Typography>
            </ExpansionPanelSummary>
            <ExpansionPanelDetails>
                <form>
                <GridContainer>
                <GridItem xs={12} sm={12} md={1}>
                  <FormLabel>
                    Service: 
                  </FormLabel>
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <Input
                    id="service"
                    readOnly
                    defaultValue={row.original.service}
                    inputProps={{
                      type: "text"
                    }}
                  />
                </GridItem>

                <GridItem xs={12} sm={12} md={1}>
                  <FormLabel>
                    Requester: 
                  </FormLabel>
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <Input
                    id="requester"
                    defaultValue={row.original.requester}
                    inputProps={{
                      type: "text"
                    }}
                  />
                </GridItem>

                <GridItem xs={12} sm={12} md={1}>
                  <FormLabel>
                    Entities:
                  </FormLabel>
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <Input
                    id="entities"
                    readOnly
                    defaultValue={row.original.entities}
                    inputProps={{
                      type: "text"
                    }}
                  />
                </GridItem>
                </GridContainer>
                {/* Other Ones*/ }
                <GridContainer>
                <GridItem xs={12} sm={12} md={1}>
                  <FormLabel>
                    Crop: 
                  </FormLabel>
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <Input
                    id="crop"
                    defaultValue={row.original.crop}
                    inputProps={{
                      type: "text"
                    }}
                  />
                </GridItem>

                <GridItem xs={12} sm={12} md={1}>
                  <FormLabel>
                    Requester Program: 
                  </FormLabel>
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <Input
                    id="requester_program"
                    readOnly
                    inputProps={{
                      type: "text"
                    }}
                  />
                </GridItem>

                <GridItem xs={12} sm={12} md={1}>
                  <FormLabel>
                    Service Type:
                  </FormLabel>
                </GridItem>
                <GridItem xs={12} sm={12} md={3}>
                  <Input
                    id="service_type"
                    readOnly
                    defaultValue={row.original.sr_type}
                    inputProps={{
                      type: "text"
                    }}
                  />
                </GridItem>
                </GridContainer>
              </form>
            </ExpansionPanelDetails>
          </ExpansionPanel>
  );
}