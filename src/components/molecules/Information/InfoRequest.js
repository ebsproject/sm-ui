/*eslint-disable*/
import React, {useState, useEffect } from "react";
import {instance} from 'utils/axios';
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Slide from "@mui/material/Slide";
import Dialog from "@mui/material/Dialog";
import DialogContent from "@mui/material/DialogContent";
import DialogActions from "@mui/material/DialogActions";
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import Close from "@mui/icons-material/Close";
import InfoIcon from "@mui/icons-material/InfoOutlined";
import InfoOutlined from '@mui/icons-material/InfoOutlined';

import BasicInformation from './BasicInformation'
import AdditionalInformation from './AdditionalInformation'

// core components
import {Button} from "components/atoms/CustomButtons";
import { FormattedMessage } from 'react-intl'
import EntitiesTable from "./EntitiesTable";
import {PoppupMessage} from 'components/molecules/PoppupMessage'



const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

// TODO: to delete
export default function InformationComponent({information, row}) {

  
  const [classicModal, setClassicModal] = useState(false);
  const [data, setData] = useState([]);
  const [list, setList] = useState([]);
  const [value, setValue] = React.useState(0);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
 
  const informateRequest = async() => {
    setLoading(true);
    setError(false);
      try{
      await instance.get(`service-request-detail/${row.original._id}`).then(response => {
        const {data} = response.data.get_service_request_detail.data.service_request_metadata;
        const {list_member} = response.data.get_service_request_detail.data;
        setData(data)
        setList(list_member)
      })
    setClassicModal(true)
      }catch (error){
        setError(true)
      }
      setLoading(false);
  };
  
 return (
    <div>

        <Button
        id = {`test-info-${row.original.sr_id}`}
        justIcon
        round
        simple
        onClick={() => informateRequest(row)}
        color="info"
        >
          <InfoIcon />
        </Button>
        {error && (
          <div style={{ color: `red` }}>
            <PoppupMessage isOpen={true} label="The List Manager Services are not available in this moment" ></PoppupMessage>
          </div>
        )}

        <Dialog
          fullWidth={true}
          maxWidth="lg"
          open={classicModal}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => setClassicModal(false)}
          aria-labelledby="classic-modal-slide-title"
          aria-describedby="classic-modal-slide-description"
        >
        
        {/* Detail title */}
          <AppBar position="static">
            <Toolbar>
              <IconButton
              edge="start"
              key="info"
              color="inherit"
              disabled
              >
                <InfoOutlined />
              </IconButton>

              <Typography variant="h6" >
                  <FormattedMessage id={'sm.inforeq.popup.title'} defaultMessage={"Details Seed Shipment"}/>
              </Typography>

              <IconButton
              edge="end"
              key="close"
              color="inherit"
              onClick={() => setClassicModal(false)}
              >
                <Close/>
              </IconButton>
            </Toolbar>
          </AppBar>

            <AppBar position="static">
              <Tabs value={value} onChange={handleChange} aria-label="simple tabs example">
                <Tab label= {`${<FormattedMessage id={'sm.req.tbl.hdr.srid'} defaultMessage={"SR_ID"}/>}:${row.original.sr_id}`} {...a11yProps(0)} />
                <Tab label={`${<FormattedMessage id={'sm.req.tbl.hdr.entities'} defaultMessage={"Entities"}/>} (${row.original.quantity})`} {...a11yProps(1)} />
              </Tabs>
            </AppBar>

        {/* TAB: Service request */}
        <TabPanel value={value} index={0}>
          <BasicInformation row={row} />
          <AdditionalInformation addInfo={data} />
        </TabPanel> 

            <TabPanel value={value} index={1}>
            <DialogContent
              id="classic-modal-slide-description"
            >
              <EntitiesTable list={list}/>
            </DialogContent>
            </TabPanel>

          <DialogActions>
            <Button
              onClick={() => setClassicModal(false)}
              color="danger"
              simple
            >
              <FormattedMessage id={'sm.inforeq.popup.close'} defaultMessage={"Close"}/>
            </Button>
          </DialogActions>
        </Dialog>
         
    </div>
  );
}