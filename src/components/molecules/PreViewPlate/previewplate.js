import React from 'react'
import PropTypes from 'prop-types'
import { Button } from "components/atoms/CustomButtons";
import VisibilityIcon from '@mui/icons-material/Visibility';
import CreatePlateDesign from "components/molecules/CreatePlateDesign"
const PreViewPlateMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { label, children, ...rest } = props
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div
    data-testid={'PreViewPlateTestId'}>
      
    {/* use this button to add a edit kind of action */}
      <Button
      id = {props.row.original.id}
      justIcon
      round
      simple
     // onClick={handleClickOpen}
      color="success"
      >
          <VisibilityIcon />
      </Button>
      <CreatePlateDesign open={open} handleClose={handleClose} row={props.row}/>
  </div>
  )
})
// Type and required properties
PreViewPlateMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}

export default PreViewPlateMolecule
