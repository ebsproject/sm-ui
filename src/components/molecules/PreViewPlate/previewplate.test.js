import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import PreViewPlate from './previewplate'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

const props = {
  row:{
    original:{
      id:1,
      Total:94,
      Name:'Test',
    },
  },

}
afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<PreViewPlate {...props}></PreViewPlate>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<PreViewPlate {...props}></PreViewPlate>)
  expect(getByTestId('PreViewPlateTestId')).toBeInTheDocument()
})
