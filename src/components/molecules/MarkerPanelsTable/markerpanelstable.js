import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

// CORE COMPONENTS AND ATOMS TO USE
import { Grid, List, ListItem, ListItemText, Tooltip } from '@mui/material';
import { Visibility, DeleteForever } from '@mui/icons-material';
import MarkerGroupContent from 'components/atoms/MarkerGroupContent';
import { FormattedMessage } from 'react-intl';
import MarkerPanelsVendorTable from 'components/molecules/MarkerPanelsVendorTable';
import { FIND_PURPOSE, FIND_MARKER_GROUP } from 'utils/apollo/gql/RequestManager';
import {
  FIND_MARKER_LIST,
  FIND_TRAITCATEGORY_BY_ID,
} from 'utils/apollo/gql/MarkerDb';
import { SMgraph, MarkerDbGraph } from 'utils/apollo/SMclient';
import { useDispatch, useSelector } from 'react-redux';
import { EbsForm } from '@ebs/components';
import { Core } from '@ebs/styleguide';
import { SET_MARKER_PANEL } from 'stores/modules/RequestManager';
const { Checkbox, IconButton, Typography, LinearProgress } = Core;

const MarkerPanelsTableMolecule = React.forwardRef(
  (
    {
      crop,
      selectedPurpose,
      selectedTraitCategory,
      serviceDisabled,
      services,
      setDefaultService,
      setSelectedService,
      setSelectedTraitCategory,
      setSelectedTraitsList,
    },
    ref,
  ) => {
    const [markerPanelVendorList, setMarkerPanelVendorList] = useState([]);
    const [markerPanelsList, setMarkerPanelsList] = useState([]);
    const [traitCategoriesList, setTraitCategoriesList] = useState([]);
    const [open, setOpen] = useState(false);
    const [selectedMarkerGroup, setSelectedMarkerGroup] = useState(null);
    const [{ id: markerGroupId, name: markerGroupName }, setMarkerGroupData] =
      useState({ id: 0, name: '' });

    const [storedSelectedMarkerPanels, setStoredSelectedMarkerPanels] = useState([]);

    const dispatch = useDispatch();
    const { mode, toEditRowData, serviceProviderData } = useSelector(
      ({ RequestManager }) => RequestManager,
    );

    const customStyles = {
      control: (base, state) => ({
        ...base,
        boxShadow: state.isFocused ? 0 : 0,
        borderColor: state.isFocused ? brandColor : base.borderColor,
        color: state.isDisabled ? base.borderColor : brandColor,
        '&:hover': {
          borderColor: state.isFocused ? brandColor : base.borderColor,
        },
        '&:disabled': {
          color: brandColor,
        },
      }),
    };

    useEffect(() => {
      const findTraitCategoryList = async (purposeId) => {
        resetValues();
        setTraitCategoriesList([]);
        setMarkerPanelVendorList([]);

        if (
          mode === 'CREATE' ||
          toEditRowData?.purpose?.name != selectedPurpose?.label
        ) {
          /** If the user is trying to create a request,
           *  check if there is already a previously selected traitcategory or trait in the serviceProviderData
           * (for cases when the user already selected, pressed next then pressed back)
           * If the values exist, set the values accordingly.
           */
          if (serviceProviderData?.traitcategory) {
            setSelectedTraitCategory(serviceProviderData.traitcategory);
          }

          if (serviceProviderData?.selectedTraitsList) {
            setStoredSelectedMarkerPanels(serviceProviderData.selectedTraitsList);
            setSelectedTraitsList(serviceProviderData.selectedTraitsList);
          }
        } else if (mode === 'EDIT') {
          if (toEditRowData?.traitCustom) {
            let toEditTraitsList = toEditRowData.traitCustom;
            for (let item of toEditTraitsList) {
              if (item?.markergroups?.length > 0) {
                item.markerpanel = item?.markergroups[0]
                  ? item?.markergroups[0]
                  : '(no markerpanel)';
                item.vendor = item?.markergroups[0]?.vendor
                  ? item?.markergroups[0]?.vendor
                  : null;
                item.concatenatedname = `${item.name} - ${
                  item?.markerpanel
                    ? item.markergroups.map((mg) => mg.name).join(' , ')
                    : ''
                } `;
              }
            }
            setStoredSelectedMarkerPanels(toEditRowData.traitCustom);
            setSelectedTraitsList(toEditRowData.traitCustom);
          }
        }

        try {
          const response = await SMgraph.query({
            query: FIND_PURPOSE,
            variables: {
              id: purposeId,
            },
            fetchPolicy: 'no-cache',
          });
          const list = response.data.findPurpose.traitCategories
            ? response.data.findPurpose.traitCategories
            : response.data.findPurpose.markerPanels
              ? response.data.findPurpose.markerPanels.filter(
                  (markerPanel) => markerPanel,
                )
              : [];

          if (response?.data?.findPurpose?.traitCategories?.length > 0) {
            setMarkerPanelVendorList([]);
            dispatch({
              type: SET_MARKER_PANEL,
              payload: null,
            });
            if (list.length) {
              const adjustedList = list
                .filter((traitCategory) => traitCategory)
                .map((traitCategory) => ({
                  value: traitCategory.id,
                  label: traitCategory.name,
                }));
              setTraitCategoriesList(adjustedList);
            }
          } else if (response?.data?.findPurpose?.markerPanels?.length > 0) {
            if (list.length > 0) {
              setMarkerPanelVendorList(
                list.filter(
                  (markerPanel) =>
                    markerPanel &&
                    markerPanel.crop &&
                    markerPanel.crop.name.toUpperCase().trim() ===
                      crop.name.toUpperCase().trim(),
                ),
              );
            } else setMarkerPanelVendorList([]);
          } else {
            setSelectedService(null);
            setMarkerPanelVendorList([]);
          }
          if (
            services?.length == 1 &&
            traitCategoriesList?.length === 0 &&
            markerPanelVendorList?.length === 0
          ) {
            setDefaultService(services[0]);
          }
        } catch (error) {
          throw new Error(error);
        }
      };

      if (selectedPurpose.value) findTraitCategoryList(selectedPurpose.value);
    }, [selectedPurpose.value]);

    const handleClickOpen = async (traitItem) => {
      try {
        const response = await MarkerDbGraph.query({
          query: FIND_MARKER_LIST,
          variables: {
            filters: [
              { col: 'markergroups.id', mod: 'EQ', val: traitItem.markerpanel.id },
            ],
          },
          fetchPolicy: 'no-cache',
        });
        setSelectedMarkerGroup({
          name: traitItem.name,
          markers: response.data.findMarkerList.content,
        });
        setMarkerGroupData({
          id: parseInt(traitItem.markerpanel.id),
          name: traitItem.markerpanel.name,
        });
        setOpen(true);
      } catch (error) {
        throw new Error(error);
      }
    };

    const handleClose = () => {
      setSelectedMarkerGroup(null);
      setMarkerGroupData({
        id: 0,
        name: '',
      });
      setOpen(false);
    };

    const updateSelectedService = async (getValues, setValue) => {
      const item = getValues('service');
      setSelectedService(item);
      serviceProviderData.service = item;
      dispatch({
        type: 'SET_MARKER_PANEL_LIST',
        payload: [],
      });
      resetValues();
    };

    const updateSelectedTraitCategory = async (getValues, setValue) => {
      const item = getValues('traitcategory');
      setSelectedTraitCategory(item);
      setValue('trait', null);
      dispatch({
        type: 'SET_MARKER_PANEL_LIST',
        payload: [],
      });
      setMarkerPanelsList([]);

      if (item?.value) {
        try {
          const response = await MarkerDbGraph.query({
            query: FIND_TRAITCATEGORY_BY_ID,
            variables: {
              id: item.value,
            },
            fetchPolicy: 'no-cache',
          });
          const traits = response.data.findTraitCategory.traits || [];
          if (traits.length) {
            let adjustedMarkerPanels = [];
            traits.forEach((trait) => {
              trait.markergroups?.forEach((markerGroup) => {
                if (
                  markerGroup.crop &&
                  parseInt(markerGroup.crop.id) === parseInt(crop.id) &&
                  adjustedMarkerPanels.findIndex(
                    (adjusted) =>
                      adjusted.id === trait.id &&
                      adjusted.idMarkerGroup === markerGroup.id,
                  ) === -1
                ) {
                  adjustedMarkerPanels.push({
                    id: trait.id,
                    name: trait.name,
                    markerpanel: {
                      id: markerGroup.id,
                      name: markerGroup.name,
                    },
                    markers: markerGroup.markers,
                    vendor: markerGroup.vendor,
                    markersname: markerGroup.name,
                    idMarkerGroup: markerGroup.id,
                    trait: trait,
                  });
                }
              });
            });
            setMarkerPanelsList(adjustedMarkerPanels);
          }
        } catch (error) {
          throw new Error(error);
        }
      }
    };

    async function resetValues() {
      setMarkerPanelsList([]);
    }

    const formField = [
      {
        sizes: [12, 12, 12, 12, 12],
        component: 'Select',
        name: 'service',
        options: services,
        inputProps: {
          label: 'Service',
          disabled: serviceDisabled,
          hidden: true,
          styles: customStyles,
        },
        defaultValue:
          mode === 'CREATE'
            ? (serviceProviderData?.service ?? { label: '', value: null })
            : toEditRowData?.service
              ? {
                  value: toEditRowData?.service?.id || toEditRowData?.service?.value,
                  label:
                    toEditRowData?.service?.name || toEditRowData?.service?.label,
                }
              : { label: '', value: null },
        helper: {
          title:
            'Service or product name, corresponding to a specific vendor catalog product or service identifier',
          placement: 'right',
          arrow: true,
        },
      },
    ];

    const handleTogglingSelectedItems = (value) => () => {
      let newSelectedAvailable = [...storedSelectedMarkerPanels];
      if (
        storedSelectedMarkerPanels.filter(function (e) {
          return e.id + e.markerpanel?.id === value.id + value.markerpanel?.id;
        }).length === 0
      ) {
        //let vendor = value?.vendor?.name || '(no vendor name)';
        value.concatenatedname = `${value.name} - ${selectedTraitCategory.label} ${value?.markersname}`;
        newSelectedAvailable.push(value);
      } else {
        newSelectedAvailable = newSelectedAvailable.filter(
          (_value) => _value.id !== value.id,
        );
      }
      setStoredSelectedMarkerPanels(newSelectedAvailable);
      setSelectedTraitsList(newSelectedAvailable);
    };

    const removeSelectedMarkerPanel = (value) => () => {
      let newSelectedAvailable = [...storedSelectedMarkerPanels];
      newSelectedAvailable = newSelectedAvailable.filter((_value) => {
        if (value.markerpanel) {
          return (
            _value.id + _value.markerpanel.id !== value.id + value.markerpanel.id
          );
        } else return _value.id !== value.id;
      });
      setStoredSelectedMarkerPanels(newSelectedAvailable);
      setSelectedTraitsList(newSelectedAvailable);
    };

    const formFieldDefinition = (props) => {
      const { getValues, setValue } = props;

      if (typeof setValue === 'function') {
        formField.forEach((fc) => {
          for (let key in fc) {
            if (fc[key] === 'service') {
              fc['onChange'] = (element) =>
                updateSelectedService(getValues, setValue);
            }
          }
        });
      }

      return {
        name: 'serviceFieldDefinition',
        components: formField,
      };
    };

    const traitField = [
      {
        sizes: [12, 12, 12, 12, 12],
        component: 'Select',
        name: 'traitcategory',
        options: traitCategoriesList,
        inputProps: {
          label: 'Trait Category',
        },
        defaultValue:
          mode === 'CREATE'
            ? (serviceProviderData?.traitcategory ?? { label: '', value: null })
            : toEditRowData?.service
              ? {
                  value:
                    (toEditRowData?.traitcategory?.id ||
                      toEditRowData?.traitcategory?.value) ??
                    '',
                  label:
                    (toEditRowData?.traitcategory?.name ||
                      toEditRowData?.traitcategory?.label) ??
                    '',
                }
              : {
                  value: '',
                  label: '',
                },
        helper: {
          title: 'Please select a Trait Category',
          placement: 'right',
          arrow: true,
        },
      },
    ];

    const traitFieldDefinition = (props) => {
      const { getValues, setValue } = props;

      if (typeof setValue === 'function') {
        traitField.forEach((fc) => {
          for (let key in fc) {
            if (fc[key] === 'traitcategory') {
              fc['onChange'] = () =>
                updateSelectedTraitCategory(getValues, setValue);
            }
          }
        });
      }

      return {
        name: 'traitFieldDefinition',
        components: traitField,
      };
    };

    const [{ isLoading, isError }, setLoading] = useState({
      isLoading: true,
      isError: false,
    });

    useEffect(() => {
      if (
        markerPanelVendorList.length > 0 ||
        markerPanelsList.length > 0 ||
        traitCategoriesList.length > 0
      ) {
        setLoading((prev) => ({ ...prev, isLoading: false }));
      }
      return () => {};
    }, [markerPanelVendorList, markerPanelsList, traitCategoriesList]);

    useEffect(() => {
      setLoading((prev) => ({ ...prev, isError: false, isLoading: true }));
      const timer = setTimeout(() => {
        setLoading((prev) => ({ ...prev, isLoading: false }));
      }, 5000);
      return () => clearTimeout(timer);
    }, [selectedPurpose]);

    useEffect(() => {
      if (!isLoading) {
        if (
          markerPanelVendorList.length === 0 &&
          markerPanelsList.length === 0 &&
          traitCategoriesList.length === 0
        ) {
          setLoading((prev) => ({ ...prev, isError: true }));
        }
      }
      return () => {};
    }, [isLoading, markerPanelVendorList, markerPanelsList, traitCategoriesList]);

    return (
      <div data-testid={'MarkerPanelsTableTestId'}>
        {isLoading && <LinearProgress color='secondary' />}
        {isError && (
          <span>
            <FormattedMessage
              id={'markerPanelTable.emptyMarker'}
              defaultMessage={
                'Purpose does not have marker group information data or these marker groups do not have the same crop selected.'
              }
            />
          </span>
        )}
        {traitCategoriesList.length === 0 ? (
          <div>
            {markerPanelVendorList.length > 0 ? (
              <MarkerPanelsVendorTable
                selectedPurpose={selectedPurpose}
                markerPanelVendorlist={markerPanelVendorList}
              />
            ) : (
              <EbsForm definition={formFieldDefinition}></EbsForm>
            )}
          </div>
        ) : (
          <div>
            <Grid container spacing={3}>
              <Grid item xs={6}>
                <EbsForm definition={traitFieldDefinition}></EbsForm>
                {markerPanelsList.length > 0 ? (
                  <div>
                    <p />
                    <table>
                      <thead>
                        <tr>
                          <th></th>
                          <th>Trait</th>
                          <th>Marker Group</th>
                        </tr>
                      </thead>
                      <tbody>
                        {markerPanelsList.map((item) => {
                          return (
                            <tr
                              key={`markerPanelStable_tr_${item.id + item.markerpanel?.id}`}
                            >
                              <td>
                                <Checkbox
                                  onClick={handleTogglingSelectedItems(item)}
                                  edge='start'
                                  checked={
                                    storedSelectedMarkerPanels.filter(function (e) {
                                      return (
                                        e.id + e.markerpanel?.id ===
                                        item.id + item.markerpanel?.id
                                      );
                                    }).length > 0
                                  }
                                  tabIndex={-1}
                                  disableRipple
                                />
                                <IconButton
                                  color='primary'
                                  title='View'
                                  onClick={() => handleClickOpen(item)}
                                >
                                  <Visibility />
                                </IconButton>
                              </td>
                              <td>{item?.name ?? ''}</td>
                              <td>{item?.markersname ?? ''}</td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                ) : (
                  <div>
                    {storedSelectedMarkerPanels.length == 0 && (
                      <p> No Marker Groups </p>
                    )}
                  </div>
                )}
              </Grid>
              <Grid item xs={6}>
                <Typography variant='body2'>
                  <span>
                    <FormattedMessage
                      id={'some.id'}
                      defaultMessage={'Selected Traits:'}
                    />
                  </span>
                </Typography>
                <List>
                  {storedSelectedMarkerPanels.map((value) => {
                    return (
                      <div
                        key={`markerPanelStable_div_${value.id + value.markerpanel?.id}`}
                        style={{
                          display: 'flex',
                          justifyContent: 'flex-start',
                          paddingLeft: '11.5px',
                        }}
                      >
                        <ListItem key={value.id} dense>
                          <Tooltip title='View'>
                            <IconButton
                              color='primary'
                              title='Delete'
                              onClick={removeSelectedMarkerPanel(value)}
                            >
                              <DeleteForever />
                            </IconButton>
                          </Tooltip>
                          <ListItemText
                            id={value.id}
                            primary={value.concatenatedname}
                          />
                        </ListItem>
                      </div>
                    );
                  })}
                </List>
              </Grid>
            </Grid>
          </div>
        )}
        <MarkerGroupContent
          markerGroup={selectedMarkerGroup}
          markerGroupId={markerGroupId}
          markerGroupName={markerGroupName}
          open={open}
          onClose={handleClose}
        />
      </div>
    );
  },
);
// Type and required properties
MarkerPanelsTableMolecule.propTypes = {};

export default MarkerPanelsTableMolecule;
