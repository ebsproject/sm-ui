import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  IconButton,
  Paper,
  Tab,
  TableBody,
  TableCell,
  TableRow,
  Tabs,
  Typography,
} from '@mui/material';
import { Info, Visibility } from '@mui/icons-material';
import React, { forwardRef, useState } from 'react';
import { FormattedMessage } from 'react-intl';

const rearrangeRowData = (rowData) => {
  if (!rowData) return;
  const reArrangedRowData = {
    name: {
      label: 'Name',
      value: rowData.name,
    },
    code: {
      label: 'Code',
      value: rowData.code,
    },
    tenant: {
      label: 'Tenant',
      value: rowData.tenant,
    },
    description: {
      label: 'Description',
      value: rowData.description,
    },
  };
  return reArrangedRowData;
};
const ViewServiceTypeButton = forwardRef((props, ref) => {
  const { rowData, refresh, ...rest } = props;
  const reArrangedRowData = rearrangeRowData(rowData);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(0);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  function TabPanel(props) {
    const { children } = props;

    return (
      <div role='tabpanel'>
        <br />
        <Card>
          <CardHeader
            title={
              <Typography variant='body2'>
                <FormattedMessage
                  id={'some.id'}
                  defaultMessage={'Basic Information'}
                />
              </Typography>
            }
          />
          <CardContent>{children}</CardContent>
        </Card>
      </div>
    );
  }

  function tableValue(value) {
    if (value === '' || value === null) return notSetLabel;
    return value;
  }
  const notSetLabel = (
    <Typography component='span' color='primary'>
      <Box fontStyle='italic'>(not set)</Box>
    </Typography>
  );

  return (
    <div ref={ref}>
      <IconButton
        title='View'
        color='primary'
        onClick={() => {
          handleOpen();
        }}
      >
        <Visibility />
      </IconButton>
      <Dialog fullWidth={true} maxWidth='lg' open={open} onClose={handleClose}>
        <DialogTitle id='responsive-dialog-title'>
          <Typography component='span'>
            <Info /> {reArrangedRowData['name'].value}
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Paper square>
            <Tabs value={value} aria-label='simple tabs example'>
              <Tab
                label={
                  <FormattedMessage id={'some.id'} defaultMessage={'INFORMATION'} />
                }
              />
            </Tabs>
          </Paper>
          <TabPanel>
            <Container maxWidth='md'>
              <TableBody value={value} index={0}>
                {reArrangedRowData &&
                  Object.keys(reArrangedRowData).map((key, index) => (
                    <TableRow
                      key={`viewServiceTypeButton_arrangeRowData_tableRow_${key}`}
                    >
                      <TableCell align='right'>
                        <FormattedMessage
                          id={'some.id'}
                          defaultMessage={`${reArrangedRowData[key].label}`}
                        />
                      </TableCell>
                      <TableCell align='left'>
                        <span>{tableValue(reArrangedRowData[key].value)}</span>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Container>
          </TabPanel>
        </DialogContent>
        <DialogActions>
          <Button
            variant='contained'
            color='primary'
            autoFocus
            onClick={handleClose}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'OK'} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});

export default ViewServiceTypeButton;
