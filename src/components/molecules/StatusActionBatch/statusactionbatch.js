import React from 'react'
import PropTypes from 'prop-types'
import AcceptButton from "components/atoms/AcceptButton"
import RejectButton from "components/atoms/RejectButton"

const StatusActionBatchMolecule = React.forwardRef((props, ref) => {

  
  return (
    <div
    data-testid={'StatusActionBatchTestId'}>
    <table align={'center'} className='test-table'>
    <tbody>
        <tr>
        <td>
             <AcceptButton row={props.row} action={props.actionNext} /> 
        </td>
        <td>
             <RejectButton row={props.row} action={props.actionReject}/>
        </td>

        </tr>
    </tbody>
  </table>
</div>
  )
})
// Type and required properties
StatusActionBatchMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}

export default StatusActionBatchMolecule
