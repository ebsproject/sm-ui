import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import {
  Dialog,
  DialogActions,
  DialogTitle,
  DialogContent,
  Typography,
} from '@mui/material';
import {
  PRIMARY_BUTTON_CLASS,
  WHITE_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import { Core } from '@ebs/styleguide';
const { Button, IconButton } = Core;
import { Delete, Send, Warning } from '@mui/icons-material';
import { DeleteForever } from '@mui/icons-material';
import { FormattedMessage } from 'react-intl';
import { useDispatch } from 'react-redux';
import { DELETE_REQUEST } from 'utils/apollo/gql/RequestManager';
import { SMgraph } from 'utils/apollo/SMclient';


//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DeleteRequestButtonMolecule = React.forwardRef((props, ref) => {
  const { rowData, refresh, ...rest } = props;
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  async function deleteRequest() {
    const { data, error, loading } = await SMgraph.mutate({
      mutation: DELETE_REQUEST,
      variables: {
        id: rowData.id,
      },
    });

    if (data.deleteRequest) {
      setOpen(false);

      /**
       * This is for not using the cached data but instead the fetch
       * function in the Request Manager to get the updated
       * changes in the Request List.
       */
      dispatch({
        type: 'SET_IS_DATABASE_UPDATED',
        payload: true,
      });

      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: `Successfully deleted Request: ${rowData.requestCode}`,
          translationId: 'request.deleted',
          severity: 'success',
        },
      });

      refresh();
    }
  }

  return (
    /*
     @prop data-testid: Id to use inside deleterequestbutton.test.js file.
     */
    <div ref={ref} data-testid={'DeleteRequestButtonTestId'}>
      <IconButton
        title='Delete'
        onClick={handleOpen}
      >
        <DeleteForever />
      </IconButton>
      <Dialog
        open={open}
        onClose={handleClose}
      >
        <DialogTitle >
          <Typography >
            <Delete />
            <FormattedMessage
              id={'some.id'}
              defaultMessage={`Delete ${rowData?.requestCode}`}
            />
          </Typography>
        </DialogTitle>
        <DialogContent >
          <Typography >
            <Warning style={{ color: 'orange' }} />
            <FormattedMessage
              id={'some.id'}
              defaultMessage={`You are about to delete
            ${rowData?.requestCode}. This action will delete the request in the
            list of requests. Click confirm to proceed.`}
            />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} className={WHITE_BUTTON_CLASS}>
            <FormattedMessage id={'some.id'} defaultMessage={'Cancel'} />
          </Button>
          <Button
          
            className={PRIMARY_BUTTON_CLASS}
            variant='contained'
            onClick={deleteRequest}
            endIcon={<Send />}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'CONFIRM'} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
DeleteRequestButtonMolecule.propTypes = {};

export default DeleteRequestButtonMolecule;
