import React from 'react'
import PropTypes from 'prop-types'
import ButtonDownload from 'components/atoms/ButtonDownload'

import ConfigurableBatch from 'components/molecules/ConfigureBatch'
import PreViewPlate from 'components/molecules/PreViewPlate'
const StatusActionDesignMolecule = React.forwardRef((props, ref) => {
  return (
    <div data-testid={'StatusActionDesignTestId'}>
      <table align={'center'} className="test-table">
        <tbody>
          <tr>
            <td>
              {props.row.num_containers === 0 ? (
                <ConfigurableBatch
                  row={props.row}
                  action={props.actionReject}
                  actionSaveDesign={props.actionSaveDesign}
                  refresh={props.refresh}
                />
              ) : (
                ''
              )}
            </td>
            <td>
              {props.row.num_containers > 0 ? (
                <ButtonDownload row={props.row} action={props.actionDownload} />
              ) : (
                ''
              )}
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  )
})
// Type and required properties
StatusActionDesignMolecule.propTypes = {}

export default StatusActionDesignMolecule
