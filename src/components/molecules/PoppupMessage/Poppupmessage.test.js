import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import PoppupMessage from './Poppupmessage'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

const props = {
  label:'test',

}
afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<PoppupMessage {...props}></PoppupMessage>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<PoppupMessage {...props}></PoppupMessage>)
  expect(getByTestId('PoppupMessageTestId')).toBeInTheDocument()
})
