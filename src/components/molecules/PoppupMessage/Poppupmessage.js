import React from 'react'
import PropTypes from 'prop-types'
// CORE COMPONENTS AND ATOMS TO USE
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const PoppupMessageMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { label, children } = props
  const [open, setOpen] = React.useState(props.isOpen);
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <div
    data-testid={'PoppupMessageTestId'}
    >
 
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        PaperProps={{
          style: {
              backgroundColor: "#fff",
          },
       }}
      >
        <DialogTitle id="alert-dialog-title">
          <span style={{color: 'green'}}>
          {"Sytem information warnign"}
          </span>
          </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.label}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} variant="contained" color="primary">
            Accept
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  
  )
})
// Type and required properties
PoppupMessageMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}

export default PoppupMessageMolecule
