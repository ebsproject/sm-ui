import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import {
  Tooltip,
  DialogTitle,
  DialogContent,
  DialogActions,
  Dialog,
  Slide,
  Box,
} from '@mui/material';
import RejectIcon from '@mui/icons-material/Cancel';
import { FormattedMessage } from 'react-intl';
import { SMgraph } from 'utils/apollo/SMclient';
import {
  CREATE_REQUEST_STATUS,
  MODIFY_REQUEST,
} from 'utils/apollo/gql/RequestManager';
import { updatePreviousRequestStatus } from 'helpers/graphQLUtils';
import { useWorkflow } from '@ebs/components';
import {
  WHITE_BUTTON_CLASS,
  PRIMARY_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import { Core } from '@ebs/styleguide';
const { TextField, IconButton, Typography, Button } = Core;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const RejectButtonMolecule = React.forwardRef((props, ref) => {
  const { rowData, refresh } = props;
  const [justification, setJustification] = useState(null);
  const [open, setOpen] = React.useState(false);
  const dispatch = useDispatch();
  const { sendNotification } = useWorkflow();

  const handleChange = (e) => {
    setJustification(e.target.value);
  };

  const rejectedRequest = async () => {
    try {
      const STATUS_ID_REJECTED = 3;
      await SMgraph.mutate({
        mutation: MODIFY_REQUEST,
        variables: {
          RequestTo: {
            id: rowData.id,
            status: { id: STATUS_ID_REJECTED },
          },
        },
      });

      if (updatePreviousRequestStatus(rowData.id)) {
        await SMgraph.mutate({
          mutation: CREATE_REQUEST_STATUS,
          variables: {
            RequestStatusTo: {
              id: 1,
              tenant: 1,
              comments: justification,
              request: {
                id: rowData.id,
              },
              status: {
                id: STATUS_ID_REJECTED,
              },
            },
          },
        });
      }

      sendNotification({
        recordId: parseInt(rowData.id),
        jobWorkflowId: 6,
        otherParameters: {},
      });

      successReject();
    } catch (error) {
      rollback();
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: 'Rejecting a Request failed!',
          translationId: 'some.id',
          severity: 'error',
        },
      });
      setOpen(false);
      refresh();
      throw new Error(error);
    }
  };

  const warningMessage = () => {
    setOpen(true);
  };

  const successReject = () => {
    setOpen(false);
    refresh();
    dispatch({
      type: 'ALERT_MESSAGE',
      payload: {
        message: 'Request rejected',
        translationId: 'request.rejected',
        severity: 'warning',
      },
    });
  };

  const cancelReject = () => {
    setJustification(null);
    setOpen(false);
  };

  const rollback = async () => {
    try {
      setJustification(null);

      const STATUS_ID_NEW = 1;
      await SMgraph.mutate({
        mutation: MODIFY_REQUEST,
        variables: {
          RequestTo: {
            id: rowData.id,
            status: { id: STATUS_ID_NEW },
          },
        },
      });
    } catch (error) {
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: 'Something went wrong',
          translationId: 'some.id',
          severity: 'error',
        },
      });
      setOpen(false);
      refresh();
      throw new Error(error);
    }
  };

  return (
    <Box data-testid={'RejectButtonTestId'} ref={ref}>
      <Dialog
        disableEscapeKeyDown
        maxWidth='sm'
        fullWidth
        TransitionComponent={Transition}
        open={open}
        data-testid={'ConfirmDialogRawTestId'}
      >
        <DialogTitle id='confirmation-dialog-title'>
          <Typography align='left' variant='h4'>
            <FormattedMessage
              id={'some.id'}
              defaultMessage={`Reject Request ${rowData.requestCode}?`}
            />
          </Typography>
        </DialogTitle>
        <DialogContent dividers>
          <Typography variant='body1'>
            <FormattedMessage
              id={'some.id'}
              defaultMessage={
                'Please justify why do you want to reject this request.'
              }
            />
          </Typography>

          <br />

          <TextField
            onChange={handleChange}
            fullWidth
            id='outlined-multiline-static'
            multiline
            variant='outlined'
          />
        </DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={cancelReject}
            className={WHITE_BUTTON_CLASS}
            variant='contained'
          >
            <FormattedMessage id='cs.confirmDialog.cancel' defaultMessage='Cancel' />
          </Button>
          <Button
            onClick={rejectedRequest}
            disabled={!Boolean(justification)}
            className={PRIMARY_BUTTON_CLASS}
            variant='contained'
          >
            <FormattedMessage
              id='cs.confirmDialog.confirm'
              defaultMessage='Confirm'
            />
          </Button>
        </DialogActions>
      </Dialog>

      <Tooltip title='Reject' arrow>
        <IconButton
          round='true'
          onClick={warningMessage}
          color='primary'
          className='remove'
        >
          <RejectIcon />
        </IconButton>
      </Tooltip>
    </Box>
  );
});
RejectButtonMolecule.propTypes = {};

export default RejectButtonMolecule;
