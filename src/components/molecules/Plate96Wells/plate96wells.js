import React, {useState }  from 'react'
import PropTypes from 'prop-types'
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import ValidateIcon from "@mui/icons-material/CheckCircleOutline";
import TableRow from '@mui/material/TableRow';
import { green, blue, grey } from '@mui/material/colors';

import {columns, createDataBaseColumns} from "utils/other/UtilsDesign"

const Plate96WellsMolecule = React.forwardRef((props, ref) => {
  const { label, children, ...rest } = props
  const [rows, setRows] = useState(props.rows ?props.rows:[]);
    React.useEffect(()=>{setRows(createDataBaseColumns(props.plateName, 
      props.idPlate, props.idDesnsity, props.startIndex
      , props.quantitySample))
    },[])

    const classesName = {
      root: {
        width : '100%'
      },
      container : {
        maxHeight: 440,
      }
    }

    function setColorIcon (id, value){
      if (id=== 'col0')
      return (
        <div>
          {value}
        </div>
      )
      if (id!== 'col0' && value==='Vendor' )
      return (
        <div>
          <ValidateIcon fontSize='small' style={{ color: grey[900] }}/>
        </div>
      )
      if (id!== 'col0' && value!== '' && value!=='Vendor' )
      return (
        <div>
        <ValidateIcon fontSize='small' style={{ color: green[500] }}/>
        {value}
        </div>
      )


    }
    
  return (
    <Paper 
    sx={classesName.root}
    data-testid={'Plate96WellsTestId'}
    >
      <TableContainer sx={classesName.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={`plate96Well_cell_${column.id}`}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={`plate96Well_tableRow${row.code}`}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={`plate96Well_tableCell_${column.id}`} align={column.align}>
                        {setColorIcon(column.id, value)}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>

    </Paper>
  )
})
// Type and required properties
Plate96WellsMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
}

export default Plate96WellsMolecule
