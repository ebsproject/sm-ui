import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Typography } from '@mui/material';
import { Button } from 'components/atoms/CustomButtons';
import ValidateIcon from '@mui/icons-material/CheckCircleOutline';
import { FormattedMessage } from 'react-intl';
import ConfirmDialogRaw from 'components/atoms/ConfirmDialogRaw';
import { useDispatch } from 'react-redux';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AcceptRequestMolecule = React.forwardRef((props, ref) => {
  const [open, setOpen] = React.useState(false);
  const { refresh, rowData, children, ...rest } = props;
  const dispatch = useDispatch();

  const warningMessage = () => {
    setOpen(true);
  };

  const cancelValidate = () => {
    //do other stuff
    setOpen(false);
  };

  const successValidate = () => {
    //do other stuff
    setOpen(false);
    refresh();
    dispatch({
      type: 'ALERT_MESSAGE',
      payload: {
        message: 'Request validated',
        translationId: 'request.validated',
        severity: 'success',
      },
    });
  };
  const validateRequest = async (query) => {
    //props.action(props.row.original);

    successValidate();
  };

  return (
    <div data-testid={'AcceptRequstTestId'}>
      <ConfirmDialogRaw
        open={open}
        title={
          <FormattedMessage
            id={'sm.validatereq.warning.title'}
            defaultMessage={'Are you sure?'}
          />
        }
        onOk={validateRequest}
        onCancel={cancelValidate}
      >
        <Typography variant='body1'>
          <FormattedMessage
            id={'sm.validatereq.warning.message'}
            defaultMessage={'Do you want ' + props.text + ' confirm?'}
          />
        </Typography>
      </ConfirmDialogRaw>
      {/* use this button to add a edit kind of action */}
      <Button
        // id = {`test-validate-${props.row.original.sr_id}`}
        round='true'
        onClick={warningMessage}
        color='success'
      >
        <ValidateIcon />
      </Button>
    </div>
  );
});
// Type and required properties
AcceptRequestMolecule.propTypes = {};

export default AcceptRequestMolecule;
