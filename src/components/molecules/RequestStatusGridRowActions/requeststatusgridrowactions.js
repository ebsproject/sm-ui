import React from "react";
import PropTypes from "prop-types";
import AcceptButton from "components/atoms/AcceptButton";
import RejectButton from "components/atoms/RejectButton";
import ValidateButton from "components/atoms/ValidateButton";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RequestStatusGridRowActionsMolecule = React.forwardRef((props, ref) => {
  return (
    /* 
     @prop data-testid: Id to use inside requeststatusgridrowactions.test.js file.
     */
    <div
      data-testid={"RequestStatusGridRowActionsTestId"}
    >
      <AcceptButton {...props} />
      <RejectButton {...props} />
      <ValidateButton {...props} />
    </div>
  );
});
// Type and required properties
RequestStatusGridRowActionsMolecule.propTypes = {
  rowData: PropTypes.object,
  refresh: PropTypes.func,
};

export default RequestStatusGridRowActionsMolecule;
