import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Visibility } from '@mui/icons-material';
import MarkerGroupContent from 'components/atoms/MarkerGroupContent';
import { MarkerDbGraph } from 'utils/apollo/SMclient';
import { useDispatch, useSelector } from 'react-redux';
import { FIND_MARKER_LIST } from 'utils/apollo/gql/MarkerDb';
import { FormControlLabel } from '@mui/material';
import { Core } from '@ebs/styleguide';
const { Checkbox, IconButton } = Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/

const MarkerPanelsVendorTableMolecule = React.forwardRef(
  ({ markerPanelVendorlist, selectedPurpose }, ref) => {
    const [selectedMarkerPanelsList, setSelectedMarkerPanelsList] = useState([]);
    const [open, setOpen] = useState(false);
    const [selectedMarkerGroup, setSelectedMarkerGroup] = useState(null);
    const [{ id: markerGroupID, name: markerGroupName }, setMarkerGroupData] =
      useState({ id: 0, name: '' });
    const dispatch = useDispatch();
    const { toEditRowData, markerPanelList } = useSelector(
      ({ RequestManager }) => RequestManager,
    );

    useEffect(() => {
      if (
        toEditRowData &&
        toEditRowData.markerPanel &&
        markerPanelList.length === 0
      ) {
        const newMarkerPanelList = markerPanelVendorlist
          .sort(function (a, b) {
            return a.id - b.id;
          })
          .map((markerPanel) => {
            const checked = toEditRowData.markerPanel.find(
              (panel) => panel.id === markerPanel.id,
            )
              ? true
              : false;
            return {
              ...markerPanel,
              checked,
            };
          });
        dispatch({
          type: 'SET_MARKER_PANEL_LIST',
          payload: newMarkerPanelList,
        });
        setSelectedMarkerPanelsList(newMarkerPanelList);
      } else if (markerPanelVendorlist.length > 0) {
        const newMarkerPanelList = markerPanelVendorlist
          .sort(function (a, b) {
            return a.id - b.id;
          })
          .map((markerPanel) => {
            const checked = markerPanelList
              .filter((panel) => panel.checked)
              .find((panel) => panel.id === markerPanel.id)
              ? true
              : false;
            return {
              ...markerPanel,
              checked,
            };
          });
        dispatch({
          type: 'SET_MARKER_PANEL_LIST',
          payload: newMarkerPanelList,
        });
        setSelectedMarkerPanelsList(newMarkerPanelList);
      }
    }, [selectedPurpose.value]);

    const onOptionChange = ({ idMarkerGroup, value }) => {
      const newMarkerPanelsList = selectedMarkerPanelsList.map((markerPanelS) => {
        return {
          ...markerPanelS,
          checked: markerPanelS.id === idMarkerGroup ? !value : markerPanelS.checked,
        };
      });
      setSelectedMarkerPanelsList(newMarkerPanelsList);
      dispatch({
        type: 'SET_MARKER_PANEL_LIST',
        payload: newMarkerPanelsList,
      });
      // Need to JSON.parse to actually get object value
      // The JSON.stringify is to just prevent the selected item to be [object Object
      // setMarkerpanel(e.target.value);
      // dispatch({
      //   type: 'SET_MARKER_PANEL',
      //   payload: JSON.parse(e.target.value),
      // });
    };

    const handleClickOpen = async (markerPanel) => {
      try {
        const response = await MarkerDbGraph.query({
          query: FIND_MARKER_LIST,
          variables: {
            id: markerPanel.id,
          },
          fetchPolicy: 'no-cache',
        });
        setMarkerGroupData({ id: parseInt(markerPanel.id), name: markerPanel.name });
        setSelectedMarkerGroup(response.data.findMarkerList.content);
        setOpen(true);
      } catch (error) {
        throw new Error(error);
      }
    };

    const handleClose = () => {
      setSelectedMarkerGroup(null);
      setOpen(false);
      setMarkerGroupData({ id: 0, name: '' });
    };

    return (
      <div data-testid={'MarkerPanelsVendorTableTestId'} ref={ref}>
        {selectedMarkerPanelsList.length === 0 ? (
          <p>No marker Groups</p>
        ) : (
          <table>
            <thead>
              <tr>
                <th></th>
                <th>Marker Group</th>
              </tr>
            </thead>
            <tbody>
              {selectedMarkerPanelsList.map((item) => {
                return (
                  <tr key={`markerPanelVendor_tr_${item.id}`}>
                    <td>
                      <FormControlLabel
                        control={
                          <Checkbox
                            checked={item.checked}
                            onChange={() =>
                              onOptionChange({
                                idMarkerGroup: item.id,
                                value: item.checked,
                              })
                            }
                          />
                        }
                        label=''
                      />
                      <IconButton
                        color='primary'
                        title='View'
                        onClick={() => handleClickOpen(item)}
                      >
                        <Visibility />
                      </IconButton>
                    </td>
                    <td>{item.name}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        )}
        <MarkerGroupContent
          markerGroup={selectedMarkerGroup}
          open={open}
          markerGroupId={markerGroupID}
          markerGroupName={markerGroupName}
          onClose={handleClose}
        />
      </div>
    );
  },
);
// Type and required properties
MarkerPanelsVendorTableMolecule.propTypes = {};

export default MarkerPanelsVendorTableMolecule;
