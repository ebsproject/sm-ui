import React from 'react';
import PropTypes from 'prop-types';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';

import Divider from '@mui/material/Divider';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import CloseIcon from '@mui/icons-material/Close';
import Slide from '@mui/material/Slide';
import Grid from '@mui/material/Grid';
import TextField from 'components/atoms/TexField';
import FormControl from 'components/atoms/FormControl';
import TabPlate from 'components/molecules/TabPlate';
import { calculatePlates } from 'utils/other/UtilsDesign';
import { getbatchCodeById } from 'utils/apollo/SMclient';
import FormControlPlateDirecction from 'components/atoms/FormControlPlateDirection';


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

const CreatePlateDesignMolecule = React.forwardRef((props, ref) => {
  const { label, children, ...rest } = props;
  const [open, setOpen] = React.useState(null);
  const [idPlate, setIdPlate] = React.useState(1);
  const [idDesnsity, setIdDensity] = React.useState(1);
  const [result, setResult] = React.useState(null);
  const [batchCode, setBatchCode] = React.useState(null);
  React.useEffect(() => {
    getbatchCodeById(1).then((response) => {
      setBatchCode(response);
    });
  }, []);

  const openTabPlates = () => {
    let result = calculatePlates(
      props.row.Total,
      96,
      props.row.Name,
      batchCode.lastPlateNumber,
      batchCode.lastSampleNumber,
      idDesnsity,
    );
    setOpen(
      <TabPlate
        dataTab={result}
        lastSample={batchCode.lastSampleNumber}
        idPlate={idPlate}
        idDesnsity={idDesnsity}
      ></TabPlate>,
    );
  };

  const closeWindow = () => {
    setOpen(null);
    props.handleClose();
    props.refresh();
  };

  const saveDesign = () => {
    props.actionSaveDesign(props.row.id, idPlate, idDesnsity, 1).then((reponse) => {
      props.handleClose();
      props.refresh();
    });
  };
  return (
    <div data-testid={'CreatePlateDesignTestId'}>
      <Dialog
        fullScreen
        open={props.open}
        onClose={closeWindow}
        TransitionComponent={Transition}
      >
        <AppBar>
          <Toolbar>
            <IconButton
              edge='start'
              color='inherit'
              onClick={closeWindow}
              aria-label='close'
            >
              <CloseIcon />
            </IconButton>
            <Typography variant='h6'>
              Create Plate
            </Typography>
            <Button autoFocus color='inherit' onClick={saveDesign}>
              Generate Plate
            </Button>
          </Toolbar>
        </AppBar>
        <Divider component='li' />
        <li>
          <Typography
            color='textSecondary'
            display='block'
            variant='caption'
          ></Typography>
        </li>
        <div >
          <Grid container spacing={2}>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={4}>
                <TextField
                  label='Total Entities'
                  value={props.row.Total}
                  disable={'true'}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField
                  label='Plate Name Prefix'
                  value={props.row.Name + '-P###'}
                  disable={'true'}
                />
              </Grid>
              <Grid item xs={4}>
                <TextField label='Tissue Type' value='Leaf' disable={'true'} />
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={4}>
                <FormControl label='Select Control' handleChange={setIdDensity} />
              </Grid>
              <Grid item xs={4}>
                <FormControlPlateDirecction
                  label='Plate Fill  Direction'
                  setIdPlate={setIdPlate}
                />
              </Grid>
              <Grid item xs={4}>
                <Button variant='contained' color='primary' onClick={openTabPlates}>
                  Plate Preview
                </Button>
              </Grid>
            </Grid>
          </Grid>
          {open}
        </div>
      </Dialog>
    </div>
  );
});
// Type and required properties
CreatePlateDesignMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
};

export default CreatePlateDesignMolecule;
