import { Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import { Delete, DeleteForever, Send, Warning } from '@mui/icons-material';
import React, { forwardRef } from 'react';
import { FormattedMessage } from 'react-intl';
import { DELETE_SERVICE_PROVIDER } from 'utils/apollo/gql/RequestManager';
import { useDispatch } from 'react-redux';
import { SMgraph } from 'utils/apollo/SMclient';
import { Core } from '@ebs/styleguide';
const { Typography, Button, IconButton } = Core;

const deleteserviceprovider = forwardRef((props, ref) => {
  const { rowData, refresh, ...rest } = props;
  const dispatch = useDispatch();
  const [open, setOpen] = React.useState(false);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  async function deleteServiceProvider() {
    const { data, error, loading } = await SMgraph.mutate({
      mutation: DELETE_SERVICE_PROVIDER,
      variables: {
        id: rowData.id,
      },
    });
    if (data.deleteRequest) {
      setOpen(false);
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: `Successfully deleted Request: ${rowData.name}`,
          translationId: 'request.deleted',
          severity: 'success',
        },
      });
      refresh();
    }
    setOpen(false);
    window.location.reload();
  }

  return (
    <div ref={ref}>
      <IconButton title='Delete' onClick={handleOpen}>
        <DeleteForever />
      </IconButton>
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>
          <Typography>
            <Delete />
            <FormattedMessage
              id={'some.id'}
              defaultMessage={`Delete ${rowData?.name}`}
            />
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Typography>
            <Warning style={{ color: 'orange' }} />
            <FormattedMessage
              id={'some.id'}
              defaultMessage={`You are about to delete
                        ${rowData?.name}. This action will delete the service provider in the
                        list of service providers. Click confirm to proceed.`}
            />
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>
            <FormattedMessage id={'some.id'} defaultMessage={'Cancel'} />
          </Button>
          <Button
            variant='contained'
            onClick={deleteServiceProvider}
            endIcon={<Send />}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'CONFIRM'} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});

export default deleteserviceprovider;
