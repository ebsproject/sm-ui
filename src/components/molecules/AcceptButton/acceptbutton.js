import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Box, Tooltip } from '@mui/material';
import ValidateIcon from '@mui/icons-material/CheckCircle';
import { FormattedMessage } from 'react-intl';
import ConfirmDialogRaw from 'components/atoms/ConfirmDialogRaw';
import { useDispatch } from 'react-redux';
import { SMgraph } from 'utils/apollo/SMclient';
import { updatePreviousRequestStatus } from 'helpers/graphQLUtils';
import {
  CREATE_REQUEST_STATUS,
  MODIFY_REQUEST,
} from 'utils/apollo/gql/RequestManager';
import { useWorkflow } from '@ebs/components';
import { Core } from '@ebs/styleguide';
const { Typography, IconButton } = Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AcceptButtonMolecule = React.forwardRef((props, ref) => {
  const [open, setOpen] = React.useState(false);
  const { refresh, rowData, children, idStatus, ...rest } = props;
  const dispatch = useDispatch();
  const { sendNotification } = useWorkflow();

  const validateRequest = async () => {
    try {
      const STATUS_ID_APPROVED = idStatus;
      await SMgraph.mutate({
        mutation: MODIFY_REQUEST,
        variables: {
          RequestTo: {
            id: rowData.id,
            status: { id: STATUS_ID_APPROVED },
          },
        },
      });
      if (updatePreviousRequestStatus(rowData.id)) {
        await SMgraph.mutate({
          mutation: CREATE_REQUEST_STATUS,
          variables: {
            RequestStatusTo: {
              id: 1,
              tenant: 1,
              request: {
                id: rowData.id,
              },
              status: {
                id: STATUS_ID_APPROVED,
              },
            },
          },
        });
      }
      sendNotification({
        recordId: parseInt(rowData.id),
        jobWorkflowId: 5,
        otherParameters: {},
      });
      successValidate();
    } catch (error) {
      rollback();
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: 'Accepting a Request failed!',
          translationId: 'some.id',
          severity: 'error',
        },
      });
      setOpen(false);
      refresh();
      throw new Error(error);
    }
  };

  const warningMessage = () => {
    setOpen(true);
  };

  const successValidate = () => {
    setOpen(false);
    refresh();
    dispatch({
      type: 'ALERT_MESSAGE',
      payload: {
        message: 'Request validated',
        translationId: 'request.validated',
        severity: 'success',
      },
    });
  };

  const cancelValidate = () => {
    setOpen(false);
  };

  const rollback = async () => {
    try {
      const STATUS_ID_NEW = 1;
      await SMgraph.mutate({
        mutation: MODIFY_REQUEST,
        variables: {
          RequestTo: {
            id: rowData.id,
            status: { id: STATUS_ID_NEW },
          },
        },
      });
    } catch (error) {
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: 'Something went wrong',
          translationId: 'some.id',
          severity: 'error',
        },
      });
      setOpen(false);
      refresh();
      throw new Error(error);
    }
  };

  return (
    <Box ref={ref} data-testid={'AcceptButtonTestId'}>
      <ConfirmDialogRaw
        open={open}
        title={
          <FormattedMessage
            id={'sm.validateRequest.warning.title'}
            defaultMessage={'Are you sure?'}
          />
        }
        onOk={validateRequest}
        onCancel={cancelValidate}
      >
        <Typography variant='body1'>
          <FormattedMessage
            id={'sm.validateRequest.warning.message'}
            defaultMessage={'Do you want to confirm?'}
          />
        </Typography>
      </ConfirmDialogRaw>
      <Tooltip title='Accept' arrow>
        <IconButton
          onClick={warningMessage}
          color='primary'
        >
          <ValidateIcon />
        </IconButton>
      </Tooltip>
    </Box>
  );
});
// Type and required properties
AcceptButtonMolecule.propTypes = {};

export default AcceptButtonMolecule;
