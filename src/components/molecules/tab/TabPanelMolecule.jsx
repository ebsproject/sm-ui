import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';

const TabPanelMolecule = forwardRef(({ children, value, index, ...other }, ref) => {
  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      ref={ref}
      {...other}
    >
      {value === index && <Box>{children}</Box>}
    </div>
  );
});

TabPanelMolecule.propTypes = {
  value: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  children: PropTypes.element.isRequired,
};

export default TabPanelMolecule;
