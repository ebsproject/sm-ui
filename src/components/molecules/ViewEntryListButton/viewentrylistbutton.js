import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import EntryListContent from 'components/atoms/EntryListContent';
import { PRIMARY_BUTTON_CLASS } from 'utils/other/tailwindCSSClasses';
import {} from '@mui/material';
import { Core } from '@ebs/styleguide';
const { Button, Dialog, DialogActions, DialogContent, IconButton } = Core;
import { Visibility } from '@mui/icons-material';

const ViewEntryListButtonMolecule = React.forwardRef((props, ref) => {
  const { rowData, setError, setMessage, ...rest } = props;
  const [open, setOpen] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    /*
     @prop data-testid: Id to use inside viewentrylistbutton.test.js file.
     */
    <div ref={ref} data-testid={'ViewEntryListButtonTestId'}>
      <IconButton title='View' color='primary' onClick={handleOpen}>
        <Visibility />
      </IconButton>
      <Dialog fullWidth={true} maxWidth='lg' open={open} onClose={handleClose}>
        <DialogContent>
          <EntryListContent
            listId={rowData.listDbId}
            setError={setError}
            setMessage={setMessage}
          />
        </DialogContent>
        <DialogActions>
          <Button
            variant='contained'
            className={PRIMARY_BUTTON_CLASS}
            autoFocus
            onClick={handleClose}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'Close'} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});
// Type and required properties
ViewEntryListButtonMolecule.propTypes = {
  rowData: PropTypes.object,
};

export default ViewEntryListButtonMolecule;
