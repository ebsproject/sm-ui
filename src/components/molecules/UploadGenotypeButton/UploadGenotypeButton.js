import { forwardRef } from 'react';
import { useDispatch } from 'react-redux';
import {  Input } from '@mui/material';
import { CloudUpload } from '@mui/icons-material';
import { getProgress, getToken, GigwaRest } from 'utils/axios/GigwaClient';
import { Core } from '@ebs/styleguide';
const { IconButton } = Core;

const UploadGenotypeButton = forwardRef((props, ref) => {
  const { rowData, refresh, ...rest } = props;

  const dispatch = useDispatch();

  /**
   * Sets parameters and uploads file.
   * Checks the progress immediately and hows a popup with the result.
   * @param {File} file
   * @param {string} batchName Used as the `project` name in Gigwa
   */
  const uploadFile = (file, batchName) => {
    let formData = new FormData();
    formData.append('file[0]', file);
    formData.append('host', 'defaultMongoHost'); // gigwa can be connected to multiple db hosts
    formData.append('module', 'Intertek_Auto'); // "database" name in gigwa
    formData.append('project', batchName); // required
    formData.append(
      'run',
      file.name.substring(0, file.name.lastIndexOf('.')) || file.name,
    ); // required
    formData.append('skipMonomorphic', 'on');

    getToken().then((token) => {
      GigwaRest(token)
        .post(`/rest/gigwa/genotypeImport`, formData, {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        })
        .then((response) => {
          getProgress(response.data).then((resp) => {
            let alertPayload;
            if (resp.error) {
              alertPayload = {
                message: `Upload failed! -- ${resp.error}`,
                translationId: 'upload.failed',
                severity: 'error',
              };
            } else {
              alertPayload = {
                message: `Successfully uploaded file`,
                translationId: 'servicetype.upload_success',
                severity: 'success',
              };
            }
            dispatch({
              type: 'ALERT_MESSAGE',
              payload: alertPayload,
            });
          });
        })
        .catch((err) => {
          console.error(err.message);
          throw new Error(err.message);
        });
    });
  };

  return (
    <div ref={ref}>
      <IconButton
        color='primary'
        title='Upload genotype'
        aria-label='upload genotype'
        component='label'
      >
        <Input
          id='file'
          inputProps={{
            type: 'file',
            hidden: true,
            style: { display: 'none' },
          }}
          onChange={(e) => uploadFile(e.target.files[0], rowData.batchName)}
          onClick={(e) => (e.target.value = null)}
        />
        <CloudUpload />
      </IconButton>
    </div>
  );
});

export default UploadGenotypeButton;
