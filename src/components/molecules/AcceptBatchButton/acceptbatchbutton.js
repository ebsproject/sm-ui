import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS AND ATOMS TO USE
import { Typography, IconButton } from '@mui/material';
import { Button } from 'components/atoms/CustomButtons';
import ValidateIcon from '@mui/icons-material/CheckCircle';
import { FormattedMessage } from 'react-intl';
import ConfirmDialogRaw from 'components/atoms/ConfirmDialogRaw';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { updateListRequestByBatchByID } from 'utils/apollo/SMclient';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AcceptBatchButtonMolecule = React.forwardRef((props, ref) => {
  const [open, setOpen] = React.useState(false);
  const { refresh, rowData, children, idStatus, ...rest } = props;
  const dispatch = useDispatch();
  const history = useHistory();

  const validateRequest = async (query) => {
    updateListRequestByBatchByID(rowData, idStatus, true);
    successValidate();
  };

  const warningMessage = () => {
    setOpen(true);
  };

  const successValidate = () => {
    //do other stuff
    refresh();
    setOpen(false);
    dispatch({
      type: 'ALERT_MESSAGE',
      payload: {
        message: 'Request validated',
        translationId: 'request.validated',
        severity: 'success',
      },
    });

    history.push('/sm/design');
  };

  const cancelValidate = () => {
    //do other stuff
    setOpen(false);
  };

  return (
    /* 
     @prop data-testid: Id to use inside acceptbutton.test.js file.
     */
    <div data-testid={'AcceptButtonTestId'}>
      <ConfirmDialogRaw
        open={open}
        title={
          <FormattedMessage
            id={'sm.validatereq.warning.title'}
            defaultMessage={'Are you sure?'}
          />
        }
        onOk={validateRequest}
        onCancel={cancelValidate}
      >
        <Typography variant='body1'>
          <FormattedMessage
            id={'sm.validatereq.warning.message'}
            defaultMessage={'Do you want to confirm?'}
          />
        </Typography>
      </ConfirmDialogRaw>
      {/* use this button to add a edit kind of action */}
      <IconButton
        // id = {`test-validate-${props.row.original.sr_id}`}
        onClick={warningMessage}
        color='primary'
      >
        <ValidateIcon />
      </IconButton>
    </div>
  );
});
// Type and required properties
AcceptBatchButtonMolecule.propTypes = {};

export default AcceptBatchButtonMolecule;
