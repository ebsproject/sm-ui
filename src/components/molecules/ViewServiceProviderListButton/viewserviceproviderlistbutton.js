import {
  Box,
  Card,
  CardContent,
  CardHeader,
  Container,
  Dialog,
  DialogContent,
  DialogTitle,
  TableBody,
  TableCell,
  TableRow,
  DialogActions,
} from '@mui/material';
import { Info, Visibility } from '@mui/icons-material';
import React, { forwardRef, useState } from 'react';
import { FormattedMessage } from 'react-intl';

import { Core } from '@ebs/styleguide';
const { Typography, Button, IconButton, Tab, Tabs, Paper } = Core;

const rearrangeRowData = (rowData) => {
  if (!rowData) return;
  const reArrangedRowData = {
    name: {
      label: 'Name',
      value: rowData.name,
    },
    code: {
      label: 'Code',
      value: rowData.code,
    },
    tenant: {
      label: 'Tenant',
      value: rowData.tenant,
    },
    servicetypes: {
      label: 'Service Types',
      value: rowData.servicetypes,
    },
    // purposes: {
    //   label: "Purposes",
    //   value: rowData.purposes,
    // },
    // services: {
    //   label: "Services",
    //   value: rowData.services,
    // },
    // requests: {
    //   label: "Requests",
    //   value: rowData.requests,
    // },
    program: {
      label: 'Program',
      value: rowData.program,
    },
    crop: {
      label: 'Crop',
      value: rowData.crop,
    },
  };

  return reArrangedRowData;
};
const notSetLabel = (
  <Typography component='span' color='primary'>
    <Box fontStyle='italic'>(not set)</Box>
  </Typography>
);
const viewserviceproviderlistbutton = forwardRef((props, ref) => {
  const { rowData, refresh, ...rest } = props;

  const reArrangedRowData = rearrangeRowData(rowData);
  const [open, setOpen] = useState(false);
  const [value, setValue] = useState(0);

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function TabPanel(props) {
    const { children, value, index } = props;

    return (
      <div role='tabpanel' hidden={value !== index}>
        <br />
        {value === index && (
          <Card>
            {index === 0 && (
              <CardHeader
                title={
                  <Typography variant='body2'>
                    <FormattedMessage
                      id={'some.id'}
                      defaultMessage={'Basic Information'}
                    />
                  </Typography>
                }
              />
            )}
            <CardContent>{children}</CardContent>
          </Card>
        )}
      </div>
    );
  }

  function tableValue(value) {
    if (value === '' || value === null) return notSetLabel;
    return value;
  }

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      'aria-controls': `simple-tabpanel-${index}`,
    };
  }

  return (
    <div ref={ref}>
      <IconButton
        title='View'
        color='primary'
        onClick={() => {
          handleOpen();
        }}
      >
        <Visibility />
      </IconButton>
      <Dialog fullWidth={true} maxWidth='lg' open={open} onClose={handleClose}>
        <DialogTitle id='responsive-dialog-title'>
          <Typography component='span'>
            <Info /> {reArrangedRowData['name'].value}
          </Typography>
        </DialogTitle>
        <DialogContent>
          <Paper square>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label='simple tabs example'
            >
              <Tab
                label={
                  <FormattedMessage id={'some.id'} defaultMessage={'BASIC INFO'} />
                }
                {...a11yProps(0)}
              />
            </Tabs>
          </Paper>
          <TabPanel value={value} index={0}>
            <Container maxWidth='md'>
              <TableBody>
                {reArrangedRowData &&
                  Object.keys(reArrangedRowData).map((key, index) => (
                    <TableRow
                      key={`viewServiceProviderListButton_arrangedRowData_tableRow_${key}`}
                    >
                      <TableCell align='right'>
                        <FormattedMessage
                          id={'some.id'}
                          defaultMessage={`${reArrangedRowData[key].label}`}
                        />
                      </TableCell>
                      <TableCell align='left'>
                        <span>{tableValue(reArrangedRowData[key].value)}</span>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Container>
          </TabPanel>
        </DialogContent>
        <DialogActions>
          <Button
            variant='contained'
            color='primary'
            autoFocus
            onClick={handleClose}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'OK'} />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
});

export default viewserviceproviderlistbutton;
