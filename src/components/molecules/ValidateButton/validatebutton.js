import React from "react";
import PropTypes from "prop-types";
import { IconButton } from "@mui/material";
import ErrorOutlineOutlined from "@mui/icons-material/ErrorOutlineOutlined";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ValidateButtonMolecule = React.forwardRef((props, ref) => {
  const { rowData, refresh, ...rest } = props;

  return (
    /* 
     @prop data-testid: Id to use inside validatebutton.test.js file.
     */
    <IconButton
      ref={ref}
      data-testid={"ValidateButtonTestId"}
      title="Validate"
      onClick={() => {
        alert(JSON.stringify(rowData));
        refresh();
      }}
    >
      <ErrorOutlineOutlined/>
    </IconButton>
  );
});
// Type and required properties
ValidateButtonMolecule.propTypes = {};

export default ValidateButtonMolecule;
