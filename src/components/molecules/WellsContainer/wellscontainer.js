import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';
import WellsTable from 'components/atoms/WellsTable';
import { Box, Pagination, PaginationItem, Tab, Tabs } from '@mui/material';
import TabPanelAtom from 'components/atoms/forms/tabs/TabPanelAtom';
import WellsPaginationMolecule from '../v2/wellsPaginationMolecule/WellsPaginationMolecule';

const WellsContainerMolecule = React.forwardRef(
  (
    {
      cells,
      cellsLabel,
      cellsColorKey,
      cellsLabelWithDirection,
      controlType,
      mode,
      tabs,
      updatePlate,
      wellsGrid,
      wellsId,
    },
    ref,
  ) => {
    const { storedRandomIndexes, selectedPlateSize, currentPlateIndex } =
      useSelector(({ request_batch }) => request_batch);
    const [value, setValue] = useState(currentPlateIndex);
    const [plateSize, setPlateSize] = useState(selectedPlateSize);
    const [randomIndexes, setRandomIndexes] = useState(storedRandomIndexes);

    const isGridReady =
      plateSize && cells.length && cellsLabel.length && cellsColorKey.length;

    useEffect(() => {
      setPlateSize(selectedPlateSize);
    }, [selectedPlateSize]);

    useEffect(() => {
      setValue(currentPlateIndex);
    }, [currentPlateIndex]);

    useEffect(() => {
      setRandomIndexes(storedRandomIndexes);
    }, [storedRandomIndexes]);

    const handleChangePage = (newValue) => {
      if (newValue !== value) setValue(newValue);
    };

    function a11yProps(index) {
      return {
        id: `wrapped-tab-${index}`,
        'aria-controls': `wrapped-tabpanel-${index}`,
      };
    }

    return (
      <div
        ref={ref}
        data-testid={'WellsContainerTestId'}
        style={{ width: '1500px' }}
      >
        {mode === 'CREATE' ? (
          <>
            {isGridReady ? (
              <div>
                <Box sx={{ width: '100%' }}>
                  <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <WellsPaginationMolecule
                      tabs={tabs}
                      page={value}
                      onChangePage={handleChangePage}
                      parentId={'wellsContainer.pagination'}
                      numberTabsDisplays={10}
                    />
                  </Box>
                  <WellsTable
                    cells={cells[value]}
                    cellsLabel={
                      cellsLabelWithDirection?.length > 0
                        ? cellsLabelWithDirection[value]
                        : cellsLabel[value]
                    }
                    cellsColorKey={cellsColorKey[value]}
                    controlType={controlType}
                    index={value}
                    randomIndexes={randomIndexes[value]}
                    updatePlate={updatePlate}
                  />
                </Box>
              </div>
            ) : (
              <div></div>
            )}
          </>
        ) : (
          cells.length && (
            <div>
              <Box sx={{ width: '100%' }}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                  <WellsPaginationMolecule
                    tabs={tabs}
                    page={value}
                    onChangePage={handleChangePage}
                    parentId={'wellsContainer.pagination'}
                    numberTabsDisplays={10}
                  />
                </Box>
                <WellsTable
                  plateNumber={wellsGrid[value].plateNumber}
                  mode={mode}
                  cells={cells[value]}
                  cellsLabel={
                    cellsLabelWithDirection?.length > 0
                      ? cellsLabelWithDirection[value]
                      : cellsLabel[value]
                  }
                  cellsColorKey={cellsColorKey[value]}
                  controlType={controlType}
                  index={value}
                  updatePlate={updatePlate}
                  wellsId={wellsId[value]}
                />
              </Box>
            </div>
          )
        )}
      </div>
    );
  },
);
// Type and required properties
WellsContainerMolecule.propTypes = {};

export default WellsContainerMolecule;
