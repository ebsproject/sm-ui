import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import ConfigureBatch from './configurebatch'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

// Props to send component to be rendered
const props = {
  row:{
    original:{
      id:1,
    },
  },

}

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<ConfigureBatch {...props}></ConfigureBatch>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<ConfigureBatch {...props} ></ConfigureBatch>)
  expect(getByTestId('ConfigureBatchTestId')).toBeInTheDocument()
})
