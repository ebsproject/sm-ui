import React, { forwardRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useQuery } from '@tanstack/react-query';
import { fetchAllMarkerGroup } from 'utils/apollo/apis';
import SelectMolecule from './SelectMolecule';

const SelectMarkerGroupMolecule = forwardRef(
  (
    {
      control,
      disabled= false,
      errorData,
      id,
      idCrop= '0',
      isDebug= false,
      isRequired= false,
      label= 'label',
      multiple= false,
      name,
    },
    ref,
  ) => {
    const {
      data,
      error: errorFetch,
      isLoading,
    } = useQuery({
      queryKey: ['markerPanelAll', idCrop],
      queryFn: async () => await fetchAllMarkerGroup({ idCrop }),
    });

    return (
      <SelectMolecule
        control={control}
        options={data}
        errorData={errorData}
        errorFetch={errorFetch}
        id={id}
        isDebug={isDebug}
        isLoading={isLoading}
        isRequired={isRequired}
        label={label}
        multiple={multiple}
        name={name}
        ref={ref}
        disabled={disabled}
      />
    );
  },
);

SelectMarkerGroupMolecule.propTypes = {
  control: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  id: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  label: PropTypes.string,
  multiple: PropTypes.bool,
  name: PropTypes.string,
};

export default SelectMarkerGroupMolecule;
