import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { useQuery } from '@tanstack/react-query';
import { fetchAllTraitCategory } from 'utils/apollo/apis';
import SelectMolecule from './SelectMolecule';

const SelectTraitCategoryMolecule = forwardRef(
  (
    {
      control,
      disabled = false,
      errorData,
      id,
      isDebug = false,
      isRequired = false,
      label = 'label',
      multiple = false,
      name,
    },
    ref,
  ) => {
    const {
      data,
      error: errorFetch,
      isLoading,
    } = useQuery({
      queryKey: ['traitCategoryAll'],
      queryFn: fetchAllTraitCategory,
    });

    return (
      <SelectMolecule
        control={control}
        disabled={disabled}
        errorData={errorData}
        errorFetch={errorFetch}
        id={id}
        isDebug={isDebug}
        isLoading={isLoading}
        isRequired={isRequired}
        label={label}
        multiple={multiple}
        name={name}
        options={data}
        ref={ref}
      />
    );
  },
);

SelectTraitCategoryMolecule.propTypes = {
  control: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  id: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  label: PropTypes.string,
  multiple: PropTypes.bool,
  name: PropTypes.string,
};

export default SelectTraitCategoryMolecule;
