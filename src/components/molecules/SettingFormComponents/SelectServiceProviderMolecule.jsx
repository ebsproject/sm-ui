import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
// import { Core } from '@ebs/styleguide';
import { useQuery } from '@tanstack/react-query';
import { fetchAllServiceProvider } from 'utils/apollo/apis';
import SelectMolecule from './SelectMolecule';

const SelectServiceProviderMolecule = forwardRef(
  (
    {
      control,
      disabled = false,
      errorData,
      id,
      isDebug = false,
      isRequired = false,
      label = 'label',
      multiple = false,
      name,
    },
    ref,
  ) => {
    const {
      data,
      error: errorFetch,
      isLoading,
    } = useQuery({
      queryKey: ['serviceProviderAll'],
      queryFn: fetchAllServiceProvider,
    });

    return (
      <SelectMolecule
        control={control}
        options={data}
        errorData={errorData}
        errorFetch={errorFetch}
        id={id}
        isDebug={isDebug}
        isLoading={isLoading}
        isRequired={isRequired}
        label={label}
        multiple={multiple}
        name={name}
        ref={ref}
        disabled={disabled}
      />
    );
  },
);

SelectServiceProviderMolecule.propTypes = {
  control: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
  multiple: PropTypes.bool,
  disabled: PropTypes.bool,
};

export default SelectServiceProviderMolecule;
