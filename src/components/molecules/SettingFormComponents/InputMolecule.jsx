import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { Core } from '@ebs/styleguide';
const { TextField } = Core;

const InputMolecule = forwardRef(
  ({ control, id, label, name, type, isRequired = false, ...rest }, ref) => {
    const nameComponent = name ? name : id;
    return (
      <Controller
        control={control}
        id={id}
        name={nameComponent}
        render={({ field, fieldState: { error } }) => {
          return (
            <TextField
              {...field}
              error={!!error}
              fullWidth
              helperText={error?.message}
              label={label}
              required={isRequired}
              type={type}
              variant='standard'
              {...rest}
            />
          );
        }}
      />
    );
  },
);

InputMolecule.propTypes = {
  control: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  label: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.oneOf(['text', 'number']).isRequired,
  isRequired: PropTypes.bool,
};

export default InputMolecule;
