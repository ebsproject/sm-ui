import React, { forwardRef, useState } from 'react';
import PropTypes from 'prop-types';
import { Controller } from 'react-hook-form';
import { Core, Lab } from '@ebs/styleguide';
import { TextField } from '@mui/material';
import { Autocomplete } from '@mui/material';

const {
  Alert,
  CircularProgress,
  // TextField,
} = Core;
// const { Autocomplete } = Lab;

const SelectMolecule = forwardRef(
  (
    {
      callbackChange,
      control,
      disabled = false,
      errorData,
      errorFetch,
      id,
      isDebug = false,
      isLoading,
      isRequired = false,
      label = 'label',
      multiple = false,
      name,
      options,
    },
    ref,
  ) => {
    const nameComponent = name ? name : id;
    const errorFetchComponent = isDebug ? errorFetch : errorData;
    const [inputValue, setInputValue] = useState('');

    if (isLoading) {
      return <CircularProgress color='secondary' ref={ref} />;
    } else if (errorFetch) {
      return (
        <Alert severity='error' ref={ref}>
          {errorFetchComponent}
        </Alert>
      );
    } else {
      return (
        <Controller
          id={id}
          name={nameComponent}
          control={control}
          ref={ref}
          render={({ field: { onChange, ...field }, fieldState: { error } }) => {
            return (
              <Autocomplete
                {...field}
                inputValue={inputValue}
                isOptionEqualToValue={(option, value) => option.id === value.id}
                multiple={multiple}
                options={options}
                getOptionLabel={(option) => option.label || ''}
                disabled={disabled}
                onChange={(_, data) => {
                  onChange(data);
                  if (callbackChange) callbackChange(data);
                }}
                onInputChange={(_, newInputValue) => {
                  setInputValue(newInputValue);
                }}
                renderInput={(params) => {
                  return (
                    <TextField
                      {...params}
                      label={`${label} ${isRequired ? '*' : ''}`}
                      error={!!error}
                      helperText={error?.message}
                    />
                  );
                }}
              />
            );
          }}
        />
      );
    }
  },
);

SelectMolecule.propTypes = {
  control: PropTypes.object.isRequired,
  id: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  label: PropTypes.string,
  multiple: PropTypes.bool,
  name: PropTypes.string,
  disabled: PropTypes.bool,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      label: PropTypes.string,
    }),
  ),
};

export default SelectMolecule;
