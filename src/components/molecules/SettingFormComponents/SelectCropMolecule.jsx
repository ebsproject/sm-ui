import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { useQuery } from '@tanstack/react-query';
import { fetchCropToSelect } from 'utils/apollo/apis';
import SelectMolecule from './SelectMolecule';

const SelectCropMolecule = forwardRef(
  (
    {
      callbackChange,
      control,
      disabled= false,
      errorData,
      id,
      isDebug= false,
      isRequired= false,
      label= 'label',
      multiple= false,
      name,
    },
    ref,
  ) => {
    const {
      data,
      error: errorFetch,
      isLoading,
    } = useQuery({
      queryKey: ['cropAllWithAll'],
      queryFn: fetchCropToSelect,
    });

    return (
      <SelectMolecule
        control={control}
        options={data}
        errorData={errorData}
        errorFetch={errorFetch}
        id={id}
        isDebug={isDebug}
        isLoading={isLoading}
        isRequired={isRequired}
        label={label}
        multiple={multiple}
        name={name}
        ref={ref}
        disabled={disabled}
        callbackChange={callbackChange}
      />
    );
  },
);

SelectCropMolecule.propTypes = {
  control: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  id: PropTypes.string.isRequired,
  isRequired: PropTypes.bool,
  label: PropTypes.string,
  multiple: PropTypes.bool,
  name: PropTypes.string,
};

export default SelectCropMolecule;
