import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { useQuery } from '@tanstack/react-query';
import { fetchAllServiceType } from 'utils/apollo/apis';
import SelectMolecule from './SelectMolecule';

const SelectServiceTypeMolecule = forwardRef(
  (
    {
      control,
      disabled = false,
      errorData,
      id,
      isDebug = false,
      isRequired = false,
      label = 'label',
      name,
    },
    ref,
  ) => {
    const {
      data,
      error: errorFetch,
      isLoading,
    } = useQuery({
      queryKey: ['serviceTypeAll'],
      queryFn: fetchAllServiceType,
    });

    return (
      <SelectMolecule
        control={control}
        disabled={disabled}
        errorData={errorData}
        errorFetch={errorFetch}
        id={id}
        isDebug={isDebug}
        isLoading={isLoading}
        isRequired={isRequired}
        label={label}
        name={name}
        options={data}
        ref={ref}
      />
    );
  },
);

SelectServiceTypeMolecule.propTypes = {
  control: PropTypes.object.isRequired,
  disabled: PropTypes.bool,
  id: PropTypes.string.isRequired,
  isDebug: PropTypes.bool,
  isRequired: PropTypes.bool,
  isRequired: PropTypes.bool,
  label: PropTypes.string,
  name: PropTypes.string,
};

export default SelectServiceTypeMolecule;
