import React from 'react';
import SweetAlert from 'react-bootstrap-sweetalert';

import { Button } from 'components/atoms/CustomButtons';
import RejectIcon from '@mui/icons-material/HighlightOffOutlined';
import { FormattedMessage } from 'react-intl';
import styles from 'assets/jss/material-dashboard-pro-react/views/sweetAlertStyle.js';

// TODO: to delete
export default function RejectRequest({ rejected, row }) {
  const [alert, setAlert] = React.useState(null);

  const hideAlert = () => {
    setAlert(null);
  };

  const rejectedRequest = () => {
    rejected(row.original);
    successReject();
  };

  const warningMessage = () => {
    setAlert(
      <SweetAlert
        warning
        // style={{ display: "block", marginTop: "-100px" }}
        title={
          <FormattedMessage
            id={'sm.rejectreq.warning.title'}
            defaultMessage={'Are you sure?'}
          />
        }
        onConfirm={rejectedRequest}
        onCancel={() => cancelReject()}
        confirmBtnText={
          <FormattedMessage
            id={'sm.rejectreq.warning.confirm'}
            defaultMessage={'Yes, reject it!'}
          />
        }
        cancelBtnText={
          <FormattedMessage
            id={'sm.rejectreq.warning.cancel'}
            defaultMessage={'Cancel'}
          />
        }
        showCancel
      >
        <FormattedMessage
          id={'sm.rejectreq.warning.message'}
          defaultMessage={'You want reject this request'}
        />
      </SweetAlert>,
    );
  };

  const successReject = () => {
    //do other stuff
    setAlert(
      <SweetAlert
        success
        // style={{ display: "block", marginTop: "-100px" }}
        title={
          <FormattedMessage
            id={'sm.rejectreq.success.title'}
            defaultMessage={"It's reject"}
          />
        }
        onConfirm={() => hideAlert()}
        onCancel={() => hideAlert()}
      >
        <FormattedMessage
          id={'sm.rejectreq.success.message'}
          defaultMessage={'This request has been reject it!'}
        />
      </SweetAlert>,
    );
  };

  const cancelReject = () => {
    //do other stuff
    setAlert(
      <SweetAlert
        danger
        // style={{ display: "block", marginTop: "-100px" }}
        title={
          <FormattedMessage
            id={'sm.rejectreq.cancel.title'}
            defaultMessage={'Cancelled'}
          />
        }
        onConfirm={() => hideAlert()}
        onCancel={() => hideAlert()}
      >
        <FormattedMessage
          id={'sm.rejectreq.cancel.message'}
          defaultMessage={'This request was rejected!'}
        />
      </SweetAlert>,
    );
  };

  return (
    <div>
      {alert}
      {/* use this button to add a edit kind of action */}
      <Button
        id={`test-reject-${row.original.sr_id}`}
        round='true'
        onClick={warningMessage}
        color='danger'
        className='remove'
      >
        <RejectIcon />
      </Button>
    </div>
  );
}
