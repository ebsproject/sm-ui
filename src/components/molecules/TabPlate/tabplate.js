import React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import SwipeableViews from 'react-swipeable-views';
import Plate96Wells from 'components/molecules/Plate96Wells';

const styles = {
  root: {
    flexGrow: 1,
    backgroundColor: 'palette.background.paper',
  },
};

function TabPanel(props) {
  const {
    children,
    value,
    index,
    plateName,
    idPlate,
    idDesnsity,
    startIndex,
    quantitySample,
  } = props;

  return (
    <Box
      role='tabpanel'
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      sx={styles.root}
      component={'div'}
    >
      {value === index && (
        <Plate96Wells
          plateName={plateName}
          idPlate={idPlate}
          idDesnsity={idDesnsity}
          startIndex={startIndex}
          quantitySample={quantitySample}
        ></Plate96Wells>
      )}
    </Box>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const TabPlateMolecule = React.forwardRef((props, ref) => {
  // Properties of the molecule
  const { label, children, ...rest } = props;

  const [value, setValue] = React.useState(0);
  const theme = useTheme();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleChangeIndex = (index) => {
    setValue(index);
  };
  return (
    <div data-testid={'TabPlateTestId'}>
      <AppBar position='static' color='primary'>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label='simple tabs example'
          indicatorColor='primary'
          variant='fullWidth'
        >
          {props.dataTab.map((element) => (
            <Tab
              label={element.plate}
              key={`tabPlate_tab_${element.id}`}
              {...a11yProps(element.id)}
            />
          ))}
        </Tabs>
      </AppBar>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
      >
        {props.dataTab.map((element) => (
          <TabPanel
            key={`tabPlate_tabPanel_${element.id}`}
            value={value}
            index={element.id}
            dir={theme.direction}
            plateName={element.plate}
            idPlate={props.idPlate}
            idDesnsity={props.idDesnsity}
            startIndex={element.startIndex}
            quantitySample={element.sampleQuantity}
          >
            {element.plate}
          </TabPanel>
        ))}
      </SwipeableViews>
    </div>
  );
});
// Type and required properties
TabPlateMolecule.propTypes = {
  label: PropTypes.String,
  children: PropTypes.node,
};

export default TabPlateMolecule;
