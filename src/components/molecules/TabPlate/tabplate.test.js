import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import TabPlate from './tabplate'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

const props = {
  dataTab: [],
}


afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TabPlate {...props}></TabPlate>, div)
})
// Props to send component to be rende

test('Render correctly', () => {
  const { getByTestId } = render(<TabPlate {...props}></TabPlate>)
  expect(getByTestId('TabPlateTestId')).toBeInTheDocument()
})
