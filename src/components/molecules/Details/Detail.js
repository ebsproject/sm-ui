/*eslint-disable*/
import React, { useState, useEffect } from 'react';
// @mui/material components
import Slide from '@mui/material/Slide';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
// react component for creating dynamic tables
import ReactTable from 'react-table';
// @mui/icons-material
import Close from '@mui/icons-material/Close';
import InfoIcon from '@mui/icons-material/InfoOutlined';

// core components
import Button from 'components/atoms/CustomButtons/CustomButton';
import Pagination from 'components/molecules/Pagination/Pagination';

//import {getSampleToSeedHelathByBatchID} from '../../client/sample/sampleClient'
//import { useFetchInformation } from 'components/Information/FetchInformation'

import styles from 'assets/jss/material-dashboard-pro-react/views/notificationsStyle.js';
// import catalogs
//import entityType from '../../components/catalogs/entityType'

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='down' ref={ref} {...props} />;
});

// TODO: to delete
export default function InformationComponent(props) {
  const [classicModal, setClassicModal] = useState(false);
  const columns = getColumnsName();
  const data = [];
  const [state, setState] = useState({ dataSample: [] });
  const onClickEvent = () => {
    setClassicModal(true);
    getSampleDataFromService();
  };

  function getCatalogs(rowData) {
    const dataActions = rowData.map((item) => {
      Object.keys(item).forEach((key) => {
        if (key === 'entity_type') {
          //item.entity_type = entityType.get(item.entity_type);
        }
      });
      return item;
    });
    return dataActions;
  }

  async function getSampleDataFromService() {
    try {
      setState({
        //dataSample :  getCatalogs(await getSampleToSeedHelathByBatchID(props))
      });
    } catch (error) {
      console.error(error);
    }
  }

  function getColumnsName() {
    const columns = [];
    const headers = ['code', 'type', 'designation', 'parentage'];
    headers.map((element) => {
      let confColumns = {};
      confColumns = {
        minWidth: 40,
        width: 150,
        maxWidth: 100,
        style: {
          textAlign: 'left',
        },
      };

      let header = '';
      switch (element) {
        case 'code':
          header = 'Code';
          break;
        case 'type':
          header = 'Type';
          break;
        case 'designation':
          header = 'Designation';
          break;
        case 'parentage':
          header = 'Parentage';
          break;
        default:
          break;
      }

      columns.push({
        accessor: element,
        Header: header || element,
        ...confColumns,
      });
    });
    return columns;
  }
  return (
    <div>
      <Button justIcon round simple onClick={onClickEvent} color='info'>
        <InfoIcon />
      </Button>

      <Dialog
        open={classicModal}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => setClassicModal(false)}
        aria-labelledby='classic-modal-slide-title'
        aria-describedby='classic-modal-slide-description'
        fullWidth={true}
        maxWidth='lg'
      >
        <DialogTitle id='classic-modal-slide-title'>
          <Button
            justIcon
            key='close'
            aria-label='Close'
            color='transparent'
            onClick={() => setClassicModal(false)}
          >
            <Close />
          </Button>
          <h4>Sample Details</h4>
        </DialogTitle>
        <DialogContent id='classic-modal-slide-description'>
          <ReactTable
            noDataText='text'
            //ref={r => (this.checkboxTable = r)}
            //filterable
            data={state.dataSample}
            columns={columns}
            //loading={loading}
            //{...checkboxProps}
            PaginationComponent={Pagination}
            defaultPageSize={15}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setClassicModal(false)} color='danger' simple>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
