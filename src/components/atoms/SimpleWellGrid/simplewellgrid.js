import React, { useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Checkbox } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import './wells.css';
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
// TODO: to fix with style
//   alignItemsToTheRight: {
//     display: 'flex',
//     justifyContent: 'flex-end',
//   },
//   label: {
//     fontWeight: 600,
//   },
//   colorKey: {
//     textTransform: 'none',
//     color: 'black',
//   },
//   dialog: {
//     position: 'absolute',
//     right: 0,
//     top: 140,
//   },
//   formControl: {
//     minWidth: 180,
//   },
//   samplesAssignedCircle: {
//     background: '#F2C113',
//     width: '20px',
//     height: '20px',
//     borderRadius: '50%',
//   },
//   dartControlsCircle: {
//     background: '#A0A0A0',
//     width: '20px',
//     height: '20px',
//     borderRadius: '50%',
//   },
//   intertekControlsCircle: {
//     background: '#A0A0A0',
//     width: '20px',
//     height: '20px',
//     borderRadius: '50%',
//   },
//   textTransformNone: {
//     textTransform: 'none',
//     fontSize: 12,
//   },
//   userAssignedBlanksCircle: {
//     background: '#1279F2',
//     width: '20px',
//     height: '20px',
//     borderRadius: '50%',
//   },
//   userAssignedPositiveControlsCircle: {
//     background: '#FF2500',
//     width: '20px',
//     height: '20px',
//     borderRadius: '50%',
//   },
//   problemWithSampleCircle: {
//     background: '#F2C113',
//     width: '20px',
//     height: '20px',
//     borderRadius: '50%',
//     border: 'solid 4px #FF2500',
//   },
//   emptyOrNotDefinedCircle: {
//     background: '#FFFFFF',
//     width: '20px',
//     height: '20px',
//     borderRadius: '50%',
//     border: 'solid 2px #B1B1B1',
//   },
// }));

const SimpleWellGridAtom = React.forwardRef((props, ref) => {
  let {
    passedPlateSize,
    plateFillDirection,
    index,
    generateSamplePlate,
    lastSampleNumber,
  } = props;
  const dispatch = useDispatch();
  const divRef = useRef(null);

  const {
    cells,
    cellsLabel,
    cellsColorKey,
    selectedBlanks,
    selectedPositiveControls,
    selectedVendor,
    cellsLabelWithDirection,
    selectedPlateFillDirection,
    totalPlatesInBatch,
    totalSamplesInBatch,
  } = useSelector(({ request_batch }) => request_batch);

  useEffect(() => {
    // divRef.current.scrollIntoView({ behavior: 'auto' });

    if (generateSamplePlate) {
      generateSamplePlates();
    }
  }, [generateSamplePlate]);

  function createCells() {
    const data =
      passedPlateSize.value === '96'
        ? Array(96).fill(false)
        : Array(384).fill(false);

    return data;
  }

  function createCellsLabel() {
    const data =
      passedPlateSize.value === '96'
        ? Array(96).fill('Approved')
        : Array(384).fill('Approved');

    if (selectedVendor.code === 'DArT') {
      data[83] = 'DArT';
      data[95] = 'DArT';
    } else if (selectedVendor.code === 'Agriplex') {
      data[83] = 'Agriplex';
      data[95] = 'Agriplex';
    } else if (selectedVendor.code === 'Intertek') {
      if (passedPlateSize.value === '96') {
        data[94] = 'Intertek';
        data[95] = 'Intertek';
      } else {
        data[382] = 'Intertek';
        data[383] = 'Intertek';
      }
    }

    return data;
  }

  function createCellsColorKey() {
    let data =
      passedPlateSize.value === '96'
        ? Array(96).fill('samplesAssignedCircle')
        : Array(384).fill('samplesAssignedCircle');

    if (selectedVendor.code === 'DArT' || selectedVendor.code === 'Agriplex') {
      data[83] = 'dartControlsCircle';
      data[95] = 'dartControlsCircle';
    } else if (selectedVendor.code === 'Intertek') {
      if (passedPlateSize.value === '96') {
        data[94] = 'intertekControlsCircle';
        data[95] = 'intertekControlsCircle';
      } else {
        data[382] = 'intertekControlsCircle';
        data[383] = 'intertekControlsCircle';
      }
    }

    return data;
  }

  if (!cells) {
    let cellsContainer = [];
    for (let i = 0; i < totalPlatesInBatch; i++) {
      const data = createCells();
      cellsContainer.push(data);
    }

    dispatch({
      type: 'SET_CELLS',
      payload: cellsContainer,
    });
  }

  if (!cellsLabel) {
    let cellsLabelContainer = [];
    for (let i = 0; i < totalPlatesInBatch; i++) {
      const data = createCellsLabel();
      cellsLabelContainer.push(data);
    }

    dispatch({
      type: 'SET_CELLS_LABEL',
      payload: cellsLabelContainer,
    });
    dispatch({
      type: 'SET_CELLS_LABEL_WITH_DIRECTION',
      payload: cellsLabelContainer,
    });
  }

  if (!cellsColorKey) {
    let cellsColorKeyContainer = [];
    for (let i = 0; i < totalPlatesInBatch; i++) {
      const data = createCellsColorKey();

      cellsColorKeyContainer.push(data);
    }

    dispatch({
      type: 'SET_CELLS_COLORKEY',
      payload: cellsColorKeyContainer,
    });
  }

  function getZeroes(length) {
    switch (length) {
      case 1:
        return '00000';
      case 2:
        return '0000';
      case 3:
        return '000';
      case 4:
        return '00';
      case 5:
        return '0';
      default:
        return '';
    }
  }

  function getSampleName(counter) {
    const zeroes = getZeroes(counter.toString().length);
    const sampleName = `${zeroes}${counter + 1}`;
    return sampleName;
  }

  function generateSamplePlates() {
    const size = passedPlateSize.value === '96' ? 96 : 384;
    const N = size === 96 ? 8 : 16;
    const M = size === 96 ? 12 : 24;

    let cellsLabelContainer = [];
    let counter = lastSampleNumber;
    let maxCount = counter + totalSamplesInBatch;

    for (let i = 0; i < totalPlatesInBatch; i++) {
      let data = [];
      data = [...cellsLabel[i]];

      if (plateFillDirection.name === 'Rows') {
        for (let i = 0; i < size; i++) {
          if (data[i] === 'Approved') {
            if (counter !== maxCount) {
              data[i] = getSampleName(counter);
              counter++;
            }
          } else continue;
        }
      } else {
        for (let i = 0; i < M; i++) {
          let downCounter = 0;
          for (let j = 0; j < N; j++) {
            if (data[i + downCounter] === 'Approved') {
              if (counter !== maxCount) {
                data[i + downCounter] = getSampleName(counter);
                downCounter += M;
                counter++;
              }
            } else {
              downCounter += M;
              continue;
            }
          }
        }
      }

      cellsLabelContainer.push(data);
    }

    dispatch({
      type: 'SET_CELLS_LABEL_WITH_DIRECTION',
      payload: cellsLabelContainer,
    });

    dispatch({
      type: 'SET_SELECTED_PLATE_FILL_DIRECTION',
      payload: plateFillDirection,
    });

    dispatch({
      type: 'SET_CELLS',
      payload: null,
    });
    dispatch({
      type: 'SET_SELECTED_BLANKS',
      payload: null,
    });

    dispatch({
      type: 'SET_SELECTED_POSITIVE_CONTROLS',
      payload: null,
    });
  }

  const handleClick = (num, isChecked, label, colorKey) => {
    return;
    /**
     * This will be disabled at the moment since the Add Blanks and Add Positive
     * Controls will be disabled. This will be further discussed in
     * Milestone 6.
     */
    if (!isChecked) return;
    if (selectedPlateFillDirection) return;

    if (selectedVendor.code === 'DArT') {
      if (num === 83 || num === 95) return;
    } else if (selectedVendor.code === 'Intertek') {
      if (passedPlateSize.value === '96') {
        if (num === 94 || num === 95) return;
      } else if (num === 382 || num === 383) return;
    }

    // divRef.current.scrollIntoView({ behavior: 'auto' });

    let squares = [...cells[index]];
    let squaresLabel = [...cellsLabel[index]];
    let squaresColorkey = [...cellsColorKey[index]];
    squares[num] = isChecked;
    squaresLabel[num] = label;
    squaresColorkey[num] = colorKey;

    let cellsContainer = [...cells];
    let cellsLabelContainer = [...cellsLabel];
    let cellsColorKeyContainer = [...cellsColorKey];
    cellsContainer[index] = squares;
    cellsLabelContainer[index] = squaresLabel;
    cellsColorKeyContainer[index] = squaresColorkey;

    dispatch({
      type: 'SET_CELLS',
      payload: cellsContainer,
    });

    dispatch({
      type: 'SET_CELLS_LABEL',
      payload: cellsLabelContainer,
    });

    dispatch({
      type: 'SET_CELLS_LABEL_WITH_DIRECTION',
      payload: cellsLabelContainer,
    });

    dispatch({
      type: 'SET_CELLS_COLORKEY',
      payload: cellsColorKeyContainer,
    });

    dispatch({
      type: 'SET_TAB_INDEX',
      payload: index,
    });
  };

  const CustomCircle = ({ colorKey }) => {
    return <div></div>;
  };

  const Cell = ({ num }) => {
    let flag = '';
    let colorKey = '';
    if (selectedBlanks?.value) {
      flag = selectedBlanks.value;
      colorKey = 'userAssignedBlanksCircle';
    } else if (selectedPositiveControls?.value) {
      flag = selectedPositiveControls.value;
      colorKey = 'userAssignedPositiveControlsCircle';
    }

    return (
      <td>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            minWidth: '80px',
            paddingRight: '7px',
          }}
        >
          {
            <>
              <Checkbox
                checked={cells[index][num]}
                type='checkbox'
                onClick={(event) =>
                  handleClick(num, event.target.checked, flag, colorKey)
                }
              />
              <CustomCircle colorKey={cellsColorKey[index][num]} />
            </>
          }
          <p style={{ marginLeft: '5px' }}>{cellsLabelWithDirection[index][num]}</p>
        </div>
      </td>
    );
  };

  function createTableBody(size) {
    const N = size === '96' ? 8 : 16;
    const M = size === '96' ? 12 : 24;
    let rows = [];
    let columns = [];

    let counter = 0;
    for (let i = 0; i < N; i++) {
      const letterized = String.fromCharCode(97 + i).toUpperCase();
      rows.push(letterized);

      let columnsArray = [];
      for (let j = 0; j < M; j++) {
        columnsArray.push(j + counter);
      }

      columns.push(columnsArray);

      counter += M;
    }

    return (
      <tbody>
        {rows.map((row, index) => {
          return (
            <tr key={`tr_simpleWell_${index}`}>
              <td class='letter-cell'>{row}</td>
              {columns[index].map((index) => {
                return <Cell key={`td_simpleWell_${index}`} num={index} />;
              })}
            </tr>
          );
        })}
      </tbody>
    );
  }

  function createTable(size) {
    const N = size === '96' ? 8 : 16;
    const M = size === '96' ? 12 : 24;

    const tableBody = createTableBody(size);

    const table = (
      <table>
        <thead>
          <tr>
            <th></th>
            {[...Array(M).keys()].map((header) => (
              <th key={`th_simpleWell_${header + 1}`}>{header + 1}</th>
            ))}
          </tr>
        </thead>
        {tableBody}
      </table>
    );

    return table;
  }

  return (
    /*
     @prop data-testid: Id to use inside simplewellgrid.test.js file.
     */
    <div
      id='plate-layout-grid-container'
      data-testid={'SimpleWellGridTestId'}
      ref={divRef}
    >
      {cells !== null && cells[index]?.length && (
        <div>{createTable(passedPlateSize.value)}</div>
      )}
    </div>
  );
});
// Type and required properties
SimpleWellGridAtom.propTypes = {};

export default SimpleWellGridAtom;
