import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { Container } from '@mui/material';
import DraggableListItem from 'components/atoms/DraggableListItem';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DraggableListAtom = React.forwardRef((props, ref) => {
  const { items, setItems } = props;

  const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
  };

  const onDragEnd = ({ destination, source, draggableId }) => {
    // Condition: dropped outside the list/UI
    if (!destination) return;

    // Condition: dropped in the same list in the same position (nothing happened)
    if (
      destination.droppableId === source.droppableId &&
      destination.index === source.index
    ) {
      return;
    }

    const start = items.columns[source.droppableId];
    const finish = items.columns[destination.droppableId];

    // Condition: switching within the same column
    if (start === finish) {
      // create a shallow clone for manipulation
      const newItems = Array.from(start.items);

      // get the selected item to be dragged
      const removed = newItems.splice(source.index, 1);

      // update the same list that has been 'removed'
      newItems.splice(destination.index, 0, removed[0]);

      // update the same list that has been added with the removed item
      const newItemState = {
        ...start,
        items: newItems,
      };

      // update the parent data
      const newGlobalStateItem = {
        ...items,
        columns: {
          ...items.columns,
          [newItemState.id]: newItemState,
        },
      };

      setItems(newGlobalStateItem);
      return;
    }

    // Condition: dropped in a different list

    // create a shallow clone for manipulation
    const startItems = Array.from(start.items);

    // get the selected item to be dragged
    const removed = startItems.splice(source.index, 1);

    const newStart = {
      ...start,
      items: startItems,
    };

    // update the list that has been added with the removed item
    const finishItems = Array.from(finish.items);
    finishItems.splice(destination.index, 0, removed[0]);
    const newFinish = {
      ...finish,
      items: finishItems,
    };

    // update the parent data
    const newGlobalStateItem = {
      ...items,
      columns: {
        ...items.columns,
        [newStart.id]: newStart,
        [newFinish.id]: newFinish,
      },
    };
    setItems(newGlobalStateItem);
  };

  const ContainerList = ({ column, markerPanels }) => {
    return (
      <Container>
        <p>{column.title}</p>
        <Droppable droppableId={column.id}>
          {(provided) => (
            <div ref={provided.innerRef} {...provided.droppableProps}>
              {markerPanels.map((markerPanel, index) => (
                <DraggableListItem
                  item={markerPanel}
                  index={index}
                  key={`draggable_list_id_${markerPanel.id}`}
                />
              ))}
              {provided.placeholder}
            </div>
          )}
        </Droppable>
      </Container>
    );
  };

  return (
    /*
     @prop data-testid: Id to use inside draggablelist.test.js file.
     */

    <DragDropContext
      onDragEnd={onDragEnd}
      data-testid={'DraggableListTestId'}
      ref={ref}
    >
      <Container>
        {items.columnOrder.map((columnId) => {
          const column = items.columns[columnId];
          const markerPanels = column.items;

          return (
            <ContainerList
              key={`draggable_list_column_id_${column.id}`}
              column={column}
              markerPanels={markerPanels}
            />
          );
        })}
      </Container>
    </DragDropContext>
  );
});
// Type and required properties
DraggableListAtom.propTypes = {};

export default DraggableListAtom;
