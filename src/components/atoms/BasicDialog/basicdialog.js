import React from 'react';
import PropTypes from 'prop-types';
import { DialogTitle, DialogContent, DialogActions, Dialog } from '@mui/material';
import { Core } from '@ebs/styleguide';
const { Typography, Button } = Core;
import { FormattedMessage } from 'react-intl';
import { PRIMARY_BUTTON_CLASS } from 'utils/other/tailwindCSSClasses';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const BasicDialogAtom = React.forwardRef((props, ref) => {
  const {
    children,
    confirmationButtonLabel,
    dialogTitle,
    onCancel,
    onConfirm,
    open,
  } = props;

  return (
    /*
     @prop data-testid: Id to use inside basicdialog.test.js file.
     */
    <Dialog
      data-testid={'BasicDialogTestId'}
      ref={ref}
      disableEscapeKeyDown
      maxWidth='sm'
      fullWidth
      open={open}
    >
      <DialogTitle id='confirmation-dialog-title'>
        <Typography align='left' variant='h5'>
          {dialogTitle}
        </Typography>
      </DialogTitle>
      <DialogContent>{children}</DialogContent>
      <DialogActions>
        <Button style={{ textTransform: 'none' }} onClick={onCancel}>
          <FormattedMessage id='cs.confirmdialog.cancel' defaultMessage='Cancel' />
        </Button>
        <Button
          style={{ textTransform: 'none' }}
          className={PRIMARY_BUTTON_CLASS}
          variant='contained'
          onClick={onConfirm}
        >
          <FormattedMessage
            id='cs.confirmdialog.cancel'
            defaultMessage={confirmationButtonLabel}
          />
        </Button>
      </DialogActions>
    </Dialog>
  );
});
// Type and required properties
BasicDialogAtom.propTypes = {};

export default BasicDialogAtom;
