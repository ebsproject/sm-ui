import React from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';

const styles ={
  dartControlsCircle: {
    background: '#A0A0A0',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  intertekControlsCircle: {
    background: '#A0A0A0',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  samplesAssignedCircle: {
    background: '#F2C113',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  userAssignedPositiveControlsCircle: {
    background: '#38893B',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  randomlyAssignedRandomControlsCircle: {
    background: '#8c0a17',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  blankCircle: {
    background: '#4473E9',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
}

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const WellsCircleAtom = React.forwardRef((props, ref) => {
  const { colorKey } = props;


  return (
    /*
     @prop data-testid: Id to use inside wellscircle.test.js file.
     */
    <div data-testid={'WellsCircleTestId'} ref={ref}>
      <Box sx={styles[colorKey]}></Box>
    </div>
  );
});
// Type and required properties
WellsCircleAtom.propTypes = {};

export default WellsCircleAtom;
