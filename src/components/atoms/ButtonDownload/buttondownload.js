import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import Button from '@mui/material/Button';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ButtonDownloadAtom = React.forwardRef((_, ref) => {
  const createReport = () => {
    props.action(props.row);
  };

  return (
    /*
     @prop data-testid: Id to use inside buttondownload.test.js file.
     */
    <Button
      data-testid={'ButtonDownloadTestId'}
      //id = {`test-validate-${props.row.original.id}`}

      round='true'
      onClick={createReport}
      color='primary'
    >
      <CloudDownloadIcon />
    </Button>
  );
});
// Type and required properties
ButtonDownloadAtom.propTypes = {
  color: PropTypes.oneOf([
    'primary',
    'info',
    'success',
    'warning',
    'danger',
    'transparent',
  ]),
  children: PropTypes.node.isRequired,
};

export default ButtonDownloadAtom;
