import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Draggable } from 'react-beautiful-dnd';
import { Divider, ListItem, ListItemText  } from '@mui/material';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const DraggableListItemAtom = React.forwardRef((props, ref) => {
  const { item, index } = props;
  const classes = useStyles();

  return (
    /*
     @prop data-testid: Id to use inside draggablelistitem.test.js file.
     */

    <Draggable
      draggableId={item.id}
      index={index}
      data-testid={'DraggableListItemTestId'}
      ref={ref}
    >
      {(provided, snapshot) => (
        <div
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
        >
          {item.primary}
        </div>
      )}
    </Draggable>
  );
});
// Type and required properties
DraggableListItemAtom.propTypes = {};

export default DraggableListItemAtom;
