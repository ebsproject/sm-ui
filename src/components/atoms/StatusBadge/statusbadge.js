import React from 'react';
import PropTypes from 'prop-types';
import { IconButton, Tooltip } from '@mui/material';
import HelpIcon from '@mui/icons-material/Help';
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const StatusBadgeAtom = React.forwardRef(({ value }, ref) => {
  return value ? (
    <>
      <span
        data-testid={'StatusBadgeTestId'}
        ref={ref}
        style={{
          borderRadius: '5px',
          color: 'white',
          padding: '2px 4px',
          backgroundColor: value.color,
        }}
      >
        {value.name}
      </span>
      {value.messageFileProcessError && (
        <Tooltip title={value.messageFileProcessError}>
          <IconButton>
            <HelpIcon />
          </IconButton>
        </Tooltip>
      )}
    </>
  ) : (
    <span />
  );
});
// Type and required properties
StatusBadgeAtom.propTypes = {};

export default StatusBadgeAtom;
