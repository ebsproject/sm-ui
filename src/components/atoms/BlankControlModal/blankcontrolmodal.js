import React, { useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  DialogTitle,
  DialogContent,
  DialogActions,
  Dialog,
  FormControl,
  FormControlLabel,
  Grid,
} from '@mui/material';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import { Core } from '@ebs/styleguide';
const { Typography, Button, TextField, RadioGroup, Radio } = Core;
import {
  PRIMARY_BUTTON_CLASS,
  WHITE_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const BlankControlModalAtom = React.forwardRef((props, ref) => {
  const {
    applyManualBlankControl,
    handleGenerateBlankRandomControl,
    handleRandomBlankCells,
    loading,
    numberOfRandomBlankControlsPerPlate,
    onCancel,
  } = props;

  const [blankOption, setBlankOption] = useState('');

  const { openBlankControlTypeModal } = useSelector(({ request_batch }) => {
    return request_batch;
  });

  const handleBlankOptionChange = (event) => {
    setBlankOption(event.target.value);
    handleRandomBlankCells(1); // TODO: change later to an actual input
  };

  return (
    /*
     @prop data-testid: Id to use inside blankcontrolmodal.test.js file.
     */
    <Dialog
      data-testid={'BlankControlModalTestId'}
      ref={ref}
      disableEscapeKeyDown
      maxWidth='sm'
      fullWidth
      open={openBlankControlTypeModal}
    >
      <DialogTitle id='confirmation-dialog-title'>
        <Typography align='left' variant="bold">
          Select a Blank option:
        </Typography>
      </DialogTitle>

      <DialogContent>
        <FormControl component='fieldset'>
          <RadioGroup value={blankOption} onChange={handleBlankOptionChange}>
            <FormControlLabel
              value='Manual'
              control={<Radio />}
              label='Manual (Select cells manually)'
            />
            <FormControlLabel
              value='Random'
              control={<Radio />}
              label='Random (Allow system to choose randomly)'
            />
          </RadioGroup>
        </FormControl>

        {blankOption === 'Random' && (
          <Grid
            container
            direction='row'
            alignItems='center'
            style={{
              display: 'flex',
              justifyContent: 'flex-start',
              alignContent: 'center',
            }}
          >
            <Typography variant='body1'>
              <FormattedMessage
                id='some.id'
                defaultMessage='Number of Blank Controls per Plate'
              />
              :
            </Typography>
            <TextField
              disabled
              style={{ width: '75px', marginLeft: '5px' }}
              id='outlined-basic'
              size='small'
              type='number'
              variant='outlined'
              onChange={handleRandomBlankCells}
              value={numberOfRandomBlankControlsPerPlate}
            />
          </Grid>
        )}
      </DialogContent>

      <DialogActions>
        <Button
          // disabled={loading}
          style={{ textTransform: 'none' }}
          onClick={onCancel}
          autoFocus
        >
          <FormattedMessage id='cs.confirmDialog.cancel' defaultMessage='Cancel' />
        </Button>
        {blankOption !== '' && (
          <Button
            style={{ textTransform: 'none', marginLeft: '5px' }}
            className={PRIMARY_BUTTON_CLASS}
            variant='contained'
            onClick={
              blankOption === 'Manual'
                ? applyManualBlankControl
                : handleGenerateBlankRandomControl
            }
          >
            {blankOption === 'Manual' ? (
              <FormattedMessage
                id='cs.confirmDialog.confirm'
                defaultMessage='Use Manual'
              />
            ) : (
              <FormattedMessage
                id='cs.confirmDialog.confirm'
                defaultMessage='Use Random'
              />
            )}
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
});
// Type and required properties
BlankControlModalAtom.propTypes = {};

export default BlankControlModalAtom;
