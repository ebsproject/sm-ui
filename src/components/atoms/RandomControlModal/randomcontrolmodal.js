import React, { useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  DialogTitle,
  DialogContent,
  DialogActions,
  Dialog,
  Grid,
} from '@mui/material';
import { FormattedMessage } from 'react-intl';
import { useSelector } from 'react-redux';
import {
  PRIMARY_BUTTON_CLASS,
  WHITE_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import { Core } from '@ebs/styleguide';
const { Typography, Button, TextField, CircularProgress } = Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RandomControlModalAtom = React.forwardRef((props, ref) => {
  const {
    handleAddRandomControls,
    loading,
    numberOfRandomControlsPerPlate,
    onCancel,
    onConfirm,
    validationError,
  } = props;

  const { openRandomControlTypeModal } = useSelector(({ request_batch }) => {
    return request_batch;
  });

  return (
    /*
     @prop data-testid: Id to use inside randomcontrolmodal.test.js file.
     */

    <Dialog
      data-testid={'RandomControlModalTestId'}
      ref={ref}
      disableEscapeKeyDown
      maxWidth='sm'
      fullWidth
      open={openRandomControlTypeModal}
    >
      <DialogTitle id='confirmation-dialog-title'>
        <Typography align='left' variant='h5'>
          Setup Random Controls
        </Typography>
      </DialogTitle>
      <DialogContent dividers>
        <Grid>
          <Grid container direction='row' alignItems='center'>
            <Grid item xs={12} sm={6}>
              <FormattedMessage
                id='some.id'
                defaultMessage='Number of Random Controls per Plate'
              />
              :
            </Grid>
            <Grid item xs={12} sm={2}>
              <TextField
                disabled
                id='outlined-basic'
                size='small'
                type='number'
                variant='outlined'
                onChange={handleAddRandomControls}
                value={numberOfRandomControlsPerPlate}
              />
            </Grid>
          </Grid>

          <br />

          <Grid container direction='row' alignItems='center'>
            <Grid item xs={12} sm={2}>
              <FormattedMessage id='some.id' defaultMessage='Error Log' />:
            </Grid>
            <Grid item xs={12} sm={10}>
              <p style={{ color: 'red' }}>
                <strong>{validationError || ''}</strong>
              </p>
            </Grid>
          </Grid>
        </Grid>
      </DialogContent>
      <DialogActions>
        <Button
          // disabled={loading}
          style={{ textTransform: 'none' }}
          onClick={onCancel}
          autoFocus
          className={WHITE_BUTTON_CLASS}
          variant='contained'
        >
          <FormattedMessage id='cs.confirmdialog.cancel' defaultMessage='Cancel' />
        </Button>
        {loading ? (
          <Button disabled={loading}>
            <CircularProgress size={24} color='grey' />
          </Button>
        ) : (
          <Button
            style={{ textTransform: 'none' }}
            disabled={!numberOfRandomControlsPerPlate}
            onClick={onConfirm}
            className={PRIMARY_BUTTON_CLASS}
            variant='contained'
          >
            <FormattedMessage
              id='cs.confirmdialog.confirm'
              defaultMessage='Add Random Controls'
            />
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
});
// Type and required properties
RandomControlModalAtom.propTypes = {};

export default RandomControlModalAtom;
