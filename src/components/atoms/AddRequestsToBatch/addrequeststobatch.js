import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  Box,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  Grid,
  Typography,
} from '@mui/material';
import { Core } from '@ebs/styleguide';
const { Button } = Core;

import { PRIMARY_BUTTON_CLASS } from 'utils/other/tailwindCSSClasses';

import { FormattedMessage } from 'react-intl';
import { EbsGrid } from '@ebs/components';
import { useDispatch, useSelector } from 'react-redux';
import { filterMembers, setData, sortData } from 'helpers/generalUtils';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AddRequestsToBatchAtom = React.forwardRef((props, ref) => {
  const { open, handleClose, setError, setMessage } = props;
  const dispatch = useDispatch();
  const [remainingApprovedRequests, setRemainingApprovedRequests] = useState(null);
  const [ableToAdd, setAbleToAdd] = useState(false);
  let toBeAddedRequests = [];

  const { approvedRequests, checkedRequestSelection } = useSelector(
    ({ request_batch }) => request_batch,
  );

  const PURPOSE = checkedRequestSelection[0]?.purpose.name || null;

  useEffect(() => {
    if (remainingApprovedRequests === null) {
      /**
       * Comparing two arrays and retaining the different ones using
       * an object property.
       *
       * From: https://stackoverflow.com/a/55316303
       */
      const remaining = approvedRequests.filter(
        ({ id: id1 }) => !checkedRequestSelection.some(({ id: id2 }) => id2 === id1),
      );

      // Filtering only Requests with the same Purpose
      //  const remainingWithSamePurpose = remaining.filter(
      //  (request) => request.purpose.name === PURPOSE,
      //);
      setRemainingApprovedRequests(remaining);
    }
  }, []);

  function checkEmptyValue(value) {
    if (!value)
      return (
        <span>
          <FormattedMessage id={'some.id'} defaultMessage={'(Not Set)'} />
        </span>
      );
    return value;
  }

  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.srid'} defaultMessage={'Request'} />
      ),
      accessor: 'requestCode',
      csvHeader: 'Request Code',
    },
    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.requester'}
          defaultMessage={'Requester'}
        />
      ),
      accessor: 'requester',
      csvHeader: 'Requester',
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.crop'} defaultMessage={'Crop'} />
      ),
      accessor: 'cropName',
      csvHeader: 'Crop',
    },
    {
      Header: 'Service Type',
      accessor: 'servicetype.name',
      csvHeader: 'Service Type',
    },
    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.servprov'}
          defaultMessage={'Service Provider'}
        />
      ),
      accessor: 'serviceprovider.name',
      csvHeader: 'Service Provider',
    },
    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.service'}
          defaultMessage={'Marker Group'}
        />
      ),
      accessor: 'markerPanel.name',
      csvHeader: 'Marker Group',
      Cell: ({ value }) => {
        return checkEmptyValue(value);
      },
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.program'} defaultMessage={'Program'} />
      ),
      accessor: 'programName',
      csvHeader: 'Program',
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.purpose'} defaultMessage={'Purpose'} />
      ),
      accessor: 'purpose.name',
      csvHeader: 'Purpose',
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.quantity'} defaultMessage={'Total'} />
      ),
      accessor: 'totalEntities',
      csvHeader: 'Total Entities',
    },
  ];

  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        let data;
        if (filters.length) {
          const filteredMembers = filterMembers(remainingApprovedRequests, filters);
          data = setData(filteredMembers, page);
        } else {
          data = setData(remainingApprovedRequests, page);
        }

        if (sort.length >= 1) {
          const sortedData = sortData(data, sort);
          resolve(sortedData);
        } else resolve(data);
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        }
        setError(true);
        reject(error);
        throw new Error(error);
      }
    });
  };

  const toolbarActions = (rowData) => {
    toBeAddedRequests = [...rowData];

    if (rowData.length === 0) setAbleToAdd(false);
    else setAbleToAdd(true);

    return (
      <>
        <Grid />
      </>
    );
  };

  const updateSelectedApprovedRequests = () => {
    if (toBeAddedRequests.length > 0) {
      const newCheckedRequestSelection = [
        ...checkedRequestSelection,
        ...toBeAddedRequests,
      ];

      dispatch({
        type: 'SET_CHECKED_REQUEST_SELECTION',
        payload: newCheckedRequestSelection,
      });

      handleClose();
    }
  };

  return (
    /*
     @prop data-testid: Id to use inside addrequeststobatch.test.js file.
     */

    <Container data-testid={'AddRequestsToBatchTestId'} ref={ref}>
      <Dialog open={open} onClose={handleClose} maxWidth='md'>
        <DialogTitle>
          <Typography variant='body1' component={'span'}>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Add Request(s) to Batch'}
              />
            </Box>
          </Typography>
        </DialogTitle>

        <DialogContent>
          <Grid container>
            <Grid
              item
              xs={12}
              lg={7}
              style={{ background: '#F5F5F5', padding: '7px' }}
            >
              <span>
                <span style={{ fontWeight: 500 }}>Service Type: </span>
                &nbsp; Genotyping &nbsp;
                <span style={{ fontWeight: 500 }}>Purpose: </span>&nbsp;
                {PURPOSE} &nbsp;
                <span style={{ fontWeight: 500 }}>Marker Group: </span>&nbsp;
              </span>
            </Grid>

            <Grid item xs={12} style={{ marginTop: '20px' }}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={
                  'This browser shows genotyping requests with the same Purpose and Service as the Batch and Status "Approved"'
                }
              />
            </Grid>

            <Grid item xs={12}>
              {remainingApprovedRequests && (
                <EbsGrid
                  id='AddRequestToBatch'
                  toolbaractions={toolbarActions}
                  columns={columns}
                  fetch={fetch}
                  height='50vh'
                  select='multi'
                />
              )}
            </Grid>
          </Grid>
        </DialogContent>

        <DialogActions>
          <Button onClick={handleClose} color='primary'>
            Cancel
          </Button>
          <Button
            onClick={updateSelectedApprovedRequests}
            variant='contained'
            className={PRIMARY_BUTTON_CLASS}
            autoFocus
            disabled={!ableToAdd}
          >
            Add
          </Button>
        </DialogActions>
      </Dialog>
    </Container>
  );
});
// Type and required properties
AddRequestsToBatchAtom.propTypes = {};

export default AddRequestsToBatchAtom;
