import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import CircleCheckedFilled from '@mui/icons-material/CheckCircle';
import CircleUnchecked from '@mui/icons-material/RadioButtonUnchecked';
import { Core } from '@ebs/styleguide';
const { Checkbox } = Core;




const SelectListRowAtom = React.forwardRef((props, ref) => {
  const { rowData } = props;

  const dispatch = useDispatch();

  const [checked, setChecked] = useState(false);

  const { mode, selectedEntryList } = useSelector(
    ({ RequestManager }) => RequestManager,
  );

  useEffect(() => {
    if (
      selectedEntryList?.requestCode === rowData.requestCode &&
      selectedEntryList?.listDbId === rowData.listDbId
    ) {
      setChecked(true);
    } else setChecked(false);
  }, [selectedEntryList]);

  const handleChange = (event) => {
    if (event.target.checked) {
      dispatch({
        type: 'SET_SELECTED_ENTRY_LIST',
        payload: rowData,
      });
    }
  };

  return (
    /*
     @prop data-testid: Id to use inside selectlistrow.test.js file.
     */
    <div>
     {mode === 'CREATE' ? ( 
        <Checkbox
          checked={checked}
          onChange={handleChange}
          color='primary'
          icon={<CircleUnchecked />}
          checkedIcon={<CircleCheckedFilled />}
        />
     ): (
       <></>
     )
     }
    </div>


  );
});
// Type and required properties
SelectListRowAtom.propTypes = {};

export default SelectListRowAtom;
