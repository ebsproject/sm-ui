import React from 'react'
import PropTypes from 'prop-types'
import TextField from '@mui/material/TextField';



// TODO: to delete
const TexFieldAtom = React.forwardRef((props, ref) => {
  // Properties of the atom
  const { color, children, ...rest } = props
  return (
    <form noValidate autoComplete="off">
      <div
      data-testid={'TexFieldTestId'}
      >
      <TextField
              id="standard-read-only-input"
              label={props.label}
              defaultValue={props.value}
              InputProps={{
                readOnly: props.disable,
              }}
              variant="outlined"  
            />
            
              </div>
    </form>
  )
})
// Type and required properties
TexFieldAtom.propTypes = {
  color: PropTypes.oneOf([
    'primary',
    'info',
    'success',
    'warning',
    'danger',
    'transparent',
  ]),
  children: PropTypes.node.isRequired,
}

export default TexFieldAtom
