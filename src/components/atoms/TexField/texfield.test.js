import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import TexField from './texfield'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

const props = {
  label:'test',
  value:'value',
  disable:false,
}

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<TexField {...props}></TexField>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<TexField {...props}></TexField>)
  expect(getByTestId('TexFieldTestId')).toBeInTheDocument()
})
