import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { DialogTitle, DialogContent, DialogActions, Dialog } from '@mui/material';
import { Core } from '@ebs/styleguide';
const { Typography, Button, CircularProgress } = Core;

import { FormattedMessage } from 'react-intl';

import {
  PRIMARY_BUTTON_CLASS,
  WHITE_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ConfirmationModalAtom = React.forwardRef((props, ref) => {
  const { children, loading, onCancel, onDelete, open, title } = props;

  return (
    /*
     @prop data-testid: Id to use inside confirmationmodal.test.js file.
     */
    <Dialog
      data-testid={'ConfirmationModalTestId'}
      ref={ref}
      disableBackdropClick
      disableEscapeKeyDown
      maxWidth='sm'
      fullWidth
      open={open}
    >
      <DialogTitle id='confirmation-dialog-title'>
        <Typography align='left' variant='h5'>
          {title}
        </Typography>
      </DialogTitle>
      <DialogContent dividers>{children}</DialogContent>
      <DialogActions>
        <Button
          disabled={loading}
          onClick={onCancel}
          autoFocus
          className={WHITE_BUTTON_CLASS}
          variant='contained'
        >
          <FormattedMessage id='cs.confirmdialog.cancel' defaultMessage='Cancel' />
        </Button>
        {loading ? (
          <Button disabled={loading}>
            <CircularProgress size={24} color='grey' />
          </Button>
        ) : (
          <Button
            onClick={onDelete}
            className={PRIMARY_BUTTON_CLASS}
            variant='contained'
          >
            <FormattedMessage
              id='cs.confirmdialog.confirm'
              defaultMessage='Confirm'
            />
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
});
// Type and required properties
ConfirmationModalAtom.propTypes = {};

export default ConfirmationModalAtom;
