import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { DialogActions } from '@mui/material';
import { EbsForm } from '@ebs/components';
import { FormattedMessage } from 'react-intl';
import { Core } from '@ebs/styleguide';
const { Button } = Core;
//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const EntryListformAtom = React.forwardRef((props, ref) => {
  const { value, handleChange, onSubmit, ...rest } = props;

  const entryListDefinition = (props) => {
    const { getValues, setValue, reset } = props;
    return {
      name: 'entryList',
      components: [
        {
          sizes: [12, 6, 6, 6, 6],
          component: 'Select',
          name: 'listId',
          options: [
            { label: 'List 1', value: 1 },
            { label: 'List 2', value: 2 },
          ],
          inputProps: {
            placeholder: 'Member List',
          },
          onChange: (e) => {
            setValue('totalEntities', '100');
            setValue('hiddenList', '100');
          },
          helper: {
            title: 'Members of the list to be analyzed or shipped.',
            placement: 'right',
            arrow: true,
          },
          rules: { required: 'Member List is required. Please select one.' },
        },
        {
          sizes: [12, 6, 4, 4, 4],
          component: 'TextField',
          name: 'totalEntities',
          inputProps: {
            color: 'primary',
            disabled: true,
            fullWidth: true,
            label: 'Member List',
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'hiddenList',
          inputProps: {
            type: 'hidden',
            color: 'primary',
            fullWidth: true,
          },
        },
      ],
    };
  };
  return (
    <div data-testid={'EntryListFormTestId'} ref={ref}>
      <EbsForm
        definition={entryListDefinition}
        onSubmit={onSubmit}
        actionPlacement='top'
      >
        <DialogActions>
          <Button
            onClick={(e) => handleChange(value - 1)}
            variant='contained'
            color='primary'
          >
            <FormattedMessage id='ms.next' defaultMessage='Prev' />
          </Button>
          <Button type='submit' color='primary' variant='contained'>
            Save
          </Button>
        </DialogActions>
      </EbsForm>
    </div>
  );
});
// Type and required properties
EntryListformAtom.propTypes = {};

export default EntryListformAtom;
