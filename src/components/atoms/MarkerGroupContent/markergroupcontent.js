import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Dialog, DialogTitle, DialogContent, Grid, Box } from '@mui/material';
import { EbsGrid } from '@ebs/components';
import { FormattedMessage } from 'react-intl';
import { findMarkerByMarkerGroup } from 'utils/apollo/apis';
import CloseIcon from '@mui/icons-material/Close';
import { Core } from '@ebs/styleguide';
const { IconButton } = Core;

const MarkerGroupContentAtom = React.forwardRef(
  (
    {
      onClose,
      markerGroup,
      markerGroupId = 0,
      markerGroupName = '',
      open = false,
    },
    ref,
  ) => {
    const handleClose = () => {
      onClose();
    };

    const MARKER_GROUP_COLUMNS = [
      {
        Header: 'Id',
        accessor: 'id',
        hidden: true,
      },
      {
        Header: (
          <FormattedMessage
            id={'some.id'}
            defaultMessage={'Allele Definitions (Name, Comments)'}
          />
        ),
        accessor: 'alleleDefinitions',
        csvHeader: 'Allele Definitions',
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Assays'} />,
        accessor: 'assays',
        csvHeader: 'Assays',
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Name'} />,
        accessor: 'name',
        csvHeader: 'Name',
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Marker Groups'} />,
        accessor: 'category',
        csvHeader: 'Category',
      },
      {
        Header: (
          <FormattedMessage id={'some.id'} defaultMessage={'Marker Positions'} />
        ),
        accessor: 'markerPositions',
        csvHeader: 'Marker Positions',
      },
      {
        Header: (
          <FormattedMessage id={'some.id'} defaultMessage={'Reference Genome'} />
        ),
        accessor: 'referenceGenome',
        csvHeader: 'Reference Genome',
      },
      {
        Header: (
          <FormattedMessage id={'some.id'} defaultMessage={'Chromosome Name'} />
        ),
        accessor: 'chromosome',
        csvHeader: 'Chromosome Name',
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Source'} />,
        accessor: 'source',
        csvHeader: 'Source',
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Target'} />,
        accessor: 'target',
        csvHeader: 'Target',
      },
    ];

    const fetch = async ({ page, sort, filters }) => {
      return new Promise((resolve, reject) => {
        try {
          let data = [];
          findMarkerByMarkerGroup({ page, sort, filters, markerGroupId })
            .then((response) => {
              resolve({ ...response });
            })
            .catch((error) => {
              reject(error);
              throw new Error(error);
            });
        } catch (error) {
          reject(error);
          throw new Error(error);
        }
      });
    };
    if (markerGroupId === 0) return null;
    else
      return (
        <Dialog
          id={`MarkerGroupContentTestId_${markerGroupId}`}
          data-testid={'MarkerGroupContentTestId'}
          aria-labelledby='simple-dialog-title'
          open={open}
          onClose={handleClose}
          ref={ref}
          maxWidth='lg'
        >
          <DialogTitle>
            <Grid
              container
              direction='row'
              justifyContent='space-around'
              alignItems='flex-start'
            >
              <Grid item xs={6}>
                <Box sx={{ display: 'flex', mt: 2 }}>
                  Marker Details of: {markerGroupName}
                </Box>
              </Grid>
              <Grid item xs={6}>
                <Box
                  sx={{
                    display: 'flex',
                    flexDirection: 'row-reverse',
                  }}
                >
                  <IconButton
                    color='primary'
                    aria-label='close'
                    component='span'
                    onClick={handleClose}
                  >
                    <CloseIcon />
                  </IconButton>
                </Box>
              </Grid>
            </Grid>
          </DialogTitle>
          <DialogContent>
            <Grid container>
              <Grid item xs={12}>
                <EbsGrid
                  id='MarkerGroupContent'
                  toolbar={true}
                  columns={MARKER_GROUP_COLUMNS}
                  fetch={fetch}
                  csvfilename='MarkerGroupList'
                  height='85vh'
                  indexing
                />
              </Grid>
            </Grid>
          </DialogContent>
        </Dialog>
      );
  },
);
// Type and required properties
MarkerGroupContentAtom.propTypes = {
  markerGroupId: PropTypes.number.isRequired,
  markerGroupName: PropTypes.string.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default MarkerGroupContentAtom;
