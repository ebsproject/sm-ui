import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useDispatch, useSelector } from 'react-redux';
import Checkbox from '@mui/material/Checkbox';
import { Box } from '@mui/material';

const styles = {
  alignItemsToTheRight: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  label: {
    fontWeight: 600,
  },
  colorKey: {
    textTransform: 'none',
    color: 'black',
  },
  dialog: {
    position: 'absolute',
    right: 0,
    top: 140,
  },
  formControl: {
    minWidth: 180,
  },
  samplesAssignedCircle: {
    background: '#F2C113',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  dartControlsCircle: {
    background: '#A0A0A0',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  intertekControlsCircle: {
    background: '#A0A0A0',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  textTransformNone: {
    textTransform: 'none',
    fontSize: 12,
  },
  userAssignedBlanksCircle: {
    background: '#1279F2',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  userAssignedPositiveControlsCircle: {
    background: '#38893B',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  randomlyAssignedRandomControlsCircle: {
    background: '#8c0a17',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  emptyOrNotDefinedCircle: {
    background: '#FFFFFF',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
    border: 'solid 2px #B1B1B1',
  },
};

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const WellsTableAtom = React.forwardRef((props, ref) => {
  const {
    cells,
    cellsLabel,
    cellsColorKey,
    controlType,
    index,
    mode,
    plateNumber,
    updatePlate,
    wellsId,
  } = props;
  const dispatch = useDispatch();

  const { selectedPlateSize, selectedVendor } = useSelector(
    ({ request_batch }) => request_batch,
  );
  const [plateSize, setPlateSize] = useState(selectedPlateSize);

  // Local state is needed for clicking, then to be passed upwards
  const [cellsOfPlate, setCellsOfPlate] = useState(cells.length === 0 ? [] : cells);
  const [cellsLabelOfPlate, setCellsLabelOfPlate] = useState(
    cellsLabel.length === 0 ? [] : cellsLabel,
  );
  const [cellsColorKeyOfPlate, setCellsColorKeyOfPlate] = useState(
    cellsColorKey.length === 0 ? [] : cellsColorKey,
  );

  useEffect(() => {
    setPlateSize(selectedPlateSize);
  }, [selectedPlateSize]);

  useEffect(() => {
    setCellsOfPlate(cells);
  }, [cells]);

  useEffect(() => {
    setCellsLabelOfPlate(cellsLabel);
  }, [cellsLabel]);

  useEffect(() => {
    setCellsColorKeyOfPlate(cellsColorKey);
  }, [cellsColorKey]);

  function createTableBody(size) {
    const N = size === '96' ? 8 : 16;
    const M = size === '96' ? 12 : 24;
    let rows = [];
    let columns = [];

    let counter = 0;
    for (let i = 0; i < N; i++) {
      const letterized = String.fromCharCode(97 + i).toUpperCase();
      rows.push(letterized);

      let columnsArray = [];
      for (let j = 0; j < M; j++) {
        columnsArray.push(j + counter);
      }

      columns.push(columnsArray);

      counter += M;
    }

    return (
      <tbody>
        {rows.map((row, index) => {
          return (
            <tr key={`wellStable_tr_${index}`}>
              <td
                style={{
                  width: '20px',
                  left: '0px',
                  position: '-webkit - sticky',
                  position: 'sticky',
                  background: 'white',
                  zIndex: '20',
                }}
              >
                {row}
              </td>
              {columns[index].map((index) => {
                return <Cell key={`wellStable_cell_${index}`} num={index} />;
              })}
            </tr>
          );
        })}
      </tbody>
    );
  }

  function createTable() {
    const M = plateSize === '96' ? 12 : 24;

    const tableBody = createTableBody(plateSize);

    const table = (
      <div style={{ position: 'relative', overflow: 'auto' }}>
        <table>
          <thead>
            <tr>
              <th
                style={{
                  width: '20px',
                  left: '0px',
                  position: '-webkit - sticky',
                  position: 'sticky',
                  zIndex: '20',
                }}
              ></th>
              {[...Array(M).keys()].map((header) => (
                <th key={`wellStable_th_${header + 1}`}>{header + 1}</th>
              ))}
            </tr>
          </thead>
          {tableBody}
        </table>
      </div>
    );

    return table;
  }

  const handleClick = (num, isChecked, label, colorKey) => {
    if (
      mode === 'EDIT' &&
      typeof cellsLabelOfPlate[num] !== 'number' &&
      controlType !== 'Delete'
    ) {
      return;
    }

    if (!label) return;

    // This is for ignoring the click if to Delete a Blank cell already
    if (controlType === 'Delete' && cellsLabelOfPlate[num] === null) return;
    /*
    if (selectedVendor.code === 'DArT') {
      if (num === 83 || num === 95) return;
    } else if (selectedVendor.code === 'Intertek') {
      if (plateSize === '96') {
        if (num === 94 || num === 95) return;
      } else if (num === 382 || num === 383) return;
    }*/
    plateSize === '96';

    // Updating Grid
    let _cellsOfPlate = [...cellsOfPlate];
    let _cellsLabelOfPlate = [...cellsLabelOfPlate];
    let _cellsColorKeyOfPlate = [...cellsColorKeyOfPlate];

    _cellsOfPlate[num] = isChecked;
    setCellsOfPlate(_cellsOfPlate);

    _cellsLabelOfPlate[num] = label;
    setCellsLabelOfPlate(_cellsLabelOfPlate);

    _cellsColorKeyOfPlate[num] = colorKey;
    setCellsColorKeyOfPlate(_cellsColorKeyOfPlate);

    let sampleDetails = {};
    if (mode === 'EDIT') {
      sampleDetails = {
        id: wellsId[num],
        plateName: plateNumber,
        sampleNumber: controlType === 'Delete' ? num : cellsLabelOfPlate[num],
      };
    }

    dispatch({
      type: 'SET_CURRENT_PLATE_INDEX',
      payload: index,
    });

    // bring this up in the parent to update the array of grids
    updatePlate(
      _cellsOfPlate,
      _cellsLabelOfPlate,
      _cellsColorKeyOfPlate,
      index,
      sampleDetails,
    );
  };

  const CustomCircle = ({ colorKey }) => {
    return <Box sx={styles[colorKey]}></Box>;
  };

  const Cell = ({ num }) => {
    let flag = '';
    let colorKey = '';
    if (controlType === 'Positive') {
      flag = 'Positive';
      colorKey = 'userAssignedPositiveControlsCircle';
    } else if (controlType === 'Blank' || controlType === 'Delete') {
      flag = 'Blank';
      colorKey = 'userAssignedBlanksCircle';
    } else if (controlType === 'Random') {
      colorKey = 'randomlyAssignedRandomControlsCircle';
    }

    return (
      <td>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'flex-start',
            minWidth: '80px',
            paddingRight: '7px',
          }}
        >
          {
            <>
              <Checkbox
                checked={cellsOfPlate[num]}
                type='checkbox'
                onClick={(event) =>
                  handleClick(num, event.target.checked, flag, colorKey)
                }
              />
              <CustomCircle colorKey={cellsColorKeyOfPlate[num]} />
            </>
          }
          <p style={{ marginLeft: '5px' }}>
            {cellsLabelOfPlate[num] ? cellsLabelOfPlate[num] : 'Blank'}
          </p>
        </div>
      </td>
    );
  };

  return (
    /*
     @prop data-testid: Id to use inside wellstable.test.js file.
     */
    <div data-testid={'WellsTableTestId'} ref={ref}>
      <div>{cells.length > 0 && createTable()}</div>
    </div>
  );
});
// Type and required properties
WellsTableAtom.propTypes = {};

export default WellsTableAtom;
