import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Avatar, Chip, Grid } from '@mui/material';
import { VERSION } from 'utils/config';

const VersionAtom = forwardRef((_, ref) => {
  return (
    <Grid container spacing={2} direction={'row-reverse'}>
      <Grid
        item
        xs={4}
        container
        ref={ref}
        data-testid={'FooterTestId'}
        justifyContent='flex-end'
        direction='row'
        alignItems='center'
      >
        <Chip avatar={<Avatar>V</Avatar>} label={VERSION} />
      </Grid>
    </Grid>
  );
});

VersionAtom.propTypes = {};

export default VersionAtom;
