import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import FormControlPlateDirection from './formcontrolplatedirection'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'
const props = {
  label:'test',

}
afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FormControlPlateDirection {...props}></FormControlPlateDirection>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<FormControlPlateDirection {...props}></FormControlPlateDirection>)
  expect(getByTestId('FormControlPlateDirectionTestId')).toBeInTheDocument()
})
