import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

// TODO: to delete
const FormControlPlateDirectionAtom = React.forwardRef((_, ref) => {
  const [valueId, setvalueId] = React.useState(1);
  const handleChange = (event) => {
    setvalueId(event.target.value);
    props.setIdPlate(event.target.value);
  };
  return (
    <div data-testid={'FormControlPlateDirectionTestId'} ref={ref}>
      <FormControl variant='outlined'>
        <InputLabel htmlFor='outlined-age-native-simple'>{props.label}</InputLabel>
        <Select
          native
          value={valueId}
          onChange={handleChange}
          label={props.label}
          inputProps={{
            name: 'FormControl',
            id: 'outlined-age-native-simple',
          }}
        >
          <option value={1}>Column</option>
          <option value={2}>Row</option>
        </Select>
      </FormControl>
    </div>
  );
});
// Type and required properties
FormControlPlateDirectionAtom.propTypes = {};

export default FormControlPlateDirectionAtom;
