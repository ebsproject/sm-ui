import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  Box,
  Container,
  Dialog,
  DialogContent,
  DialogTitle,
  List,
  ListItem,
  ListItemText,
  Typography,
} from '@mui/material';

import { PRIMARY_BUTTON_CLASS } from 'utils/other/tailwindCSSClasses';

import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { userContext } from '@ebs/layout';


//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
// TODO: to delete
const RadioButtonDialogAtom = React.forwardRef((props, ref) => {
  const { open, setOpen, setEBSForm, setError, setMessage } = props;
  const [serviceProviders, setServiceProviders] = useState(null);
  const dispatch = useDispatch();
  const { userProfile } = useContext(userContext);

  useEffect(() => {
    if (serviceProviders === null && userProfile) {
      let user_profile = userProfile;
      setServiceProviders(user_profile.service_providers);
    }
  }, [userProfile]);

  const handleListItemClick = (selectedValue) => {
    //setServiceProviderhere
    dispatch({
      type: 'SET_SERVICE_LABORATORY_ID',
      payload: selectedValue.id, //Default
    });
    setEBSForm(selectedValue.id);
    setOpen(false);
  };

  return (
    /*
     @prop data-testid: Id to use inside radiobuttondialog.test.js file.
     */

    <Container data-testid={'RadioButtonDialogTestId'}>
      <Dialog open={open} maxWidth='md'>
        <DialogTitle>
          <Typography variant='h6'>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Select which Service Laboratory to use:'}
              />
            </Box>
          </Typography>
        </DialogTitle>

        <DialogContent>
          {serviceProviders != null && (
            <List sx={{ pt: 0 }}>
              {serviceProviders.map((sp) => (
                <ListItem button onClick={() => handleListItemClick(sp)} key={sp.id}>
                  <ListItemText primary={sp.name} />
                </ListItem>
              ))}
            </List>
          )}
        </DialogContent>
      </Dialog>
    </Container>
  );
});
// Type and required properties
RadioButtonDialogAtom.propTypes = {};

export default RadioButtonDialogAtom;
