import React, { forwardRef } from "react";
import ServiceTypeForm from "../ServiceTypeForm";

const ServiceProviderInfoTab = forwardRef((props, ref) => {
  const { currentData, mode, value, onSubmit, handleClose, ...rest } = props;

  return (
    <div>
      <ServiceTypeForm
        currentData={currentData}
        mode={mode}
        onSubmit={onSubmit}
        value={value}
        handleClose={handleClose}
      />
    </div>
  );
});

export default ServiceProviderInfoTab;
