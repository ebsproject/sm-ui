import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Box, IconButton } from '@mui/material';
import { RBAC } from '@ebs/layout';
import DeleteIcon from '@mui/icons-material/Delete';

const DeleteRowButton = forwardRef(
  ({ rowData, refresh, deleteBatch, userProfile }, ref) => {
    const isAllowToDelete =
      rowData.requests &&
      rowData.requests.filter((item) => item.status.id === '5').length ===
        rowData.requests.length;

    const isOwnToDelete =
      rowData.requests &&
      rowData.requests.filter((item) => item.status.id === '5').length ===
        rowData.requests.length &&
      rowData.createBy.id === userProfile.dbId;
    return (
      <Box ref={ref}>
        {isOwnToDelete && (
          <IconButton
            title='Delete'
            onClick={() => {
              deleteBatch(rowData, refresh);
            }}
            color='primary'
          >
            <DeleteIcon />
          </IconButton>
        )}
        {!isOwnToDelete && isAllowToDelete && (
          <Box>
            <RBAC allowedAction={'Delete'} product={'Batch Manager'}>
              <IconButton
                title='Delete'
                onClick={() => {
                  deleteBatch(rowData, refresh);
                }}
                color='primary'
              >
                <DeleteIcon />
              </IconButton>
            </RBAC>
          </Box>
        )}
      </Box>
    );
  },
);

export default DeleteRowButton;
