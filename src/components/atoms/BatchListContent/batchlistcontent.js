import React, { useEffect, useState, useContext } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { useLocation, Link } from 'react-router-dom';
import { EbsGrid } from '@ebs/components';
import { RBAC, userContext } from '@ebs/layout';
import { FormattedMessage } from 'react-intl';
import { SMgraph } from 'utils/apollo/SMclient';
import ConfirmationModal from 'components/atoms/ConfirmationModal';
import Typography from '@mui/material/Typography';
import { FIND_BATCH_LIST, DELETE_BATCH } from 'utils/apollo/gql/BatchManager';
import UploadGenotypingResultsButton from 'components/molecules/UploadGenotypingResultsButton';
import { Icons } from '@ebs/styleguide';
import { FileRest } from 'utils/axios/Fileclient';
import { GETCONTEXT } from 'utils/config';
const { Edit, GetApp, Visibility } = Icons;
import { FIND_BATCH_KEYFILE } from 'utils/apollo/gql/BatchManager';
import { Stack } from '@mui/material';
import { getSortOrDefault, SelectColumnFilter } from 'utils/other/generalFunctions';
import { Core } from '@ebs/styleguide';
const { IconButton, Box } = Core;
import StatusBadge from 'components/atoms/StatusBadge';
import DownloadRowButton from './DownloadRowButton';
import DeleteRowButton from './DeleteRowButton';

const BatchListContent = React.forwardRef(
  ({ setError, setMessage, view, setLoading }, ref) => {
    const dispatch = useDispatch();
    const location = useLocation();
    const [openConfirmation, setOpenConfirmation] = useState(false);
    const [batchToDelete, setBatchToDelete] = useState(null);
    const [refreshGrid, setRefreshGrid] = useState(() => () => {});
    const [loadingButton, setLoadingButton] = useState(false);
    const { userProfile } = useContext(userContext);

    const styles = {
      rowActionStyle: {
        display: 'flex',
        alignItems: 'stretch',
      },
    };

    const columns = [
      {
        Header: 'Id',
        accessor: 'id',
        hidden: true,
        disableGlobalFilter: true,
      },
      {
        width: 150,
        Header: (
          <FormattedMessage id={'sm.req.tbl.hdr.status'} defaultMessage={'Status'} />
        ),
        accessor: 'requests',
        Cell: ({ value }) => {
          return <StatusBadge value={value ? value[0].status : null} />;
        },
        Filter: SelectColumnFilter,
        csvHeader: 'Status',
        sticky: 'left',
      },
      {
        width: 210,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.sr.id'}
            defaultMessage={'Batch Name'}
          />
        ),
        accessor: 'name',
        csvHeader: 'Batch Name',
        sticky: 'left',
      },
      {
        width: 210,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.batcher'}
            defaultMessage={'Description'}
          />
        ),
        accessor: 'objective',
        csvHeader: 'Description',
      },
      {
        width: 210,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.program'}
            defaultMessage={'Total of Samples'}
          />
        ),
        accessor: 'numMembers',
        csvHeader: 'Total Samples',
      },
      {
        width: 210,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.crop'}
            defaultMessage={'Total # of plates'}
          />
        ),
        accessor: 'numContainers',
        csvHeader: 'Num of plates',
      },
      {
        width: 210,
        Header: 'First Plate Name',
        accessor: 'firstPlateName',
        csvHeader: 'First Plate Name',
      },
      {
        width: 210,
        Header: 'Last Plate Name',
        accessor: 'lastPlateName',
        csvHeader: 'Last Plate Name',
      },
      {
        width: 310,
        Header: 'Service Provider',
        accessor: 'serviceprovider',
        csvHeader: 'Service Provider',
      },
      {
        width: 310,
        Header: 'Vendor',
        accessor: 'vendor',
        csvHeader: 'Vendor',
      },
      {
        width: 310,
        Header: 'Service Type',
        accessor: 'servicetypes',
        csvHeader: 'Service Type',
      },
      {
        width: 310,
        Header: 'Creator',
        accessor: 'createByUser',
        csvHeader: 'Creator',
      },
      {
        width: 310,
        Header: 'Modifier',
        accessor: 'updatedByUser',
        csvHeader: 'Modifier',
      },

      {
        width: 210,
        Header: 'Creation Timestamp',
        accessor: 'createdOn',
        csvHeader: 'Creation Timestamp',
      },
      {
        width: 210,
        Header: 'Modification Timestamp',
        accessor: 'updatedOn',
        csvHeader: 'Modification Timestamp',
      },
    ];

    // useEffect(async () => {}, [isDatabaseUpdated]);
    const cancelDeleteBatch = () => {
      setOpenConfirmation(false);
      setBatchToDelete(null);
      setRefreshGrid(() => () => {});
    };

    const confirmedDeleteBatch = () => {
      setLoadingButton(true);
      SMgraph.mutate({
        mutation: DELETE_BATCH,
        variables: {
          id: batchToDelete.id,
        },
      })
        .then(() => {
          setTimeout(() => {
            setOpenConfirmation(false);
            setLoadingButton(false);
            refreshGrid();
          }, 2000);
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: `Successfully deleted ${batchToDelete?.name}.`,
              translationId: 'some.id',
              severity: 'success',
            },
          });
        })
        .catch((error) => {
          const message = error?.response?.data?.metadata.status[0].messageType;
          if (message) {
            setMessage(message);
          }
          setError(true);
          console.error(error);
        });
    };

    const deleteBatch = (batch, refresh) => {
      setOpenConfirmation(true);
      setBatchToDelete(batch);
      setRefreshGrid(() => refresh);
    };

    const rowActions = (rowData, refresh) => {
      return (
        <Stack
          direction='row'
          spacing={0}
          sx={styles.rowActionStyle}
          component={'div'}
        >
          {rowData.numContainers > 0 && (
            <>
              <Box>
                <RBAC allowedAction={'View'} product={'Batch Manager'}>
                  <IconButton
                    color='primary'
                    title='View'
                    component={Link}
                    to={`/sm/batchmanager/view/${rowData.name}`}
                    state={{
                      currentData: rowData,
                      prevPath: location.pathname,
                    }}
                  >
                    <Visibility />
                  </IconButton>
                </RBAC>
              </Box>
              <Box>
                <DownloadRowButton userProfile={userProfile} rowData={rowData} />
              </Box>
              {rowData.requests &&
                rowData.requests.filter((item) => item.status.id === '5').length ===
                  rowData.requests.length && (
                  <Box>
                    <RBAC allowedAction={'Modify'} product={'Batch Manager'}>
                      <IconButton
                        color='primary'
                        title='Edit'
                        component={Link}
                        to={`/sm/batchmanager/edit-batch/${rowData.name}`}
                        state={{
                          currentData: rowData,
                          prevPath: location.pathname,
                        }}
                      >
                        <Edit />
                      </IconButton>
                    </RBAC>
                  </Box>
                )}
              <Box>
                <RBAC allowedAction={'Upload Result File'} product={'Batch Manager'}>
                  <UploadGenotypingResultsButton
                    uploadState={
                      rowData.resultFileId === null ? 'isNotYetUploaded' : 'uploaded'
                    }
                    refresh={refresh}
                    rowData={rowData}
                  />
                </RBAC>
              </Box>
              <Box>
                <RBAC
                  allowedAction={'Download Genotyping File'}
                  product={'Batch Manager'}
                >
                  <IconButton
                    color='primary'
                    title='Download Results'
                    onClick={() => downloadFile(rowData)}
                  >
                    <GetApp />
                  </IconButton>
                </RBAC>
              </Box>
            </>
          )}
          <Box>
            <DeleteRowButton
              refresh={refresh}
              rowData={rowData}
              userProfile={userProfile}
              deleteBatch={deleteBatch}
            />
          </Box>
        </Stack>
      );
    };

    function adjustFilters(filters) {
      /**
       * This is to update the proper filter object before the query.
       */

      const adjustedFilters = filters
        .filter((item) => !['requests', 'vendor'].includes(item.col))
        .map((filter) => {
          const adjustedFilter = { ...filter };

          // The keys for filtering: col, mod, val
          adjustedFilter['col'] = filter.col;
          adjustedFilter['mod'] = 'LK';
          adjustedFilter['val'] = filter.val;

          // Removing unimportant keys
          delete adjustedFilter.id;
          delete adjustedFilter.value;

          return adjustedFilter;
        });
      return adjustedFilters;
    }

    async function setBatches(content) {
      const newData = [];
      if (content.length) {
        for (var i = 0; i < content.length; i++) {
          let copyOfData = {
            id: content[i].id,
            objective: content[i].objective,
            numMembers: content[i].numMembers,
            lastPlateName: content[i].lastPlate,
            firstPlateName: content[i].fistPlate,
            name: content[i].name,
            vendorId: content[i].vendorId,
            resultFileId: content[i].resultFileId,
            numContainers: content[i].numContainers,
            createByUser: content[i].createByUser
              ? content[i].createByUser.contact.person.fullName
              : '',
            createBy: content[i].createByUser,
            updatedByUser: content[i].updatedByUser
              ? content[i].updatedByUser.contact.person.fullName
              : '',
            createdOn: content[i].createdOn ? content[i].createdOn : '',
            updatedOn: content[i].updatedOn ? content[i].updatedOn : '',
            serviceprovider: content[i].serviceprovider
              ? content[i].serviceprovider.name
              : '',
            servicetypes: content[i].serviceprovider
              ? content[i].serviceprovider.servicetypes[0].name
              : '',
            requests: content[i].requests,
            vendor: content[i].vendor ? content[i].vendor.name : '',
          };

          newData.push(copyOfData);
        }
      }
      return newData;
    }

    const fetch = async ({ page, sort, filters }) => {
      const adjustedFilters = adjustFilters(filters);
      const adjustedSort = getSortOrDefault(sort);

      return new Promise((resolve, reject) => {
        try {
          SMgraph.query({
            query: FIND_BATCH_LIST,
            variables: {
              page: page,
              sort: adjustedSort,
              filters: [...adjustedFilters],
            },
            fetchPolicy: 'no-cache',
          }).then(async ({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              const { content } = data.findBatchList;
              const { totalElements } = data.findBatchList;
              const numberOfBatches = totalElements;
              const batchs = await setBatches(content);
              const pagesBasedOnNumberOfbatchs = Math.ceil(
                numberOfBatches / page.size,
              );
              dispatch({
                type: 'ALERT_MESSAGE',
                payload: {
                  open: false,
                  message: null,
                  translationId: null,
                  severity: null,
                  strongMessage: null,
                },
              });
              setLoading(false);
              resolve({
                pages:
                  view === 'batch-manager'
                    ? pagesBasedOnNumberOfbatchs
                    : Math.ceil(batchs.length / page.size),
                elements: view === 'batch-manager' ? numberOfBatches : batchs.length,
                data: batchs,
              });
            }
          });
        } catch (error) {
          const message = error?.response?.data?.metadata.status[0].messageType;
          if (message) {
            setMessage(message);
          }
          setError(true);
          console.error(error);
        }
      });
    };

    const downloadFile = (row) => {
      let alertPayload;
      SMgraph.query({
        query: FIND_BATCH_KEYFILE,
        variables: {
          id: row.id,
        },
        fetchPolicy: 'no-cache',
      })
        .then(({ data }) => {
          if (data.findBatch.resultFileId) {
            FileRest({
              url: `${GETCONTEXT().tenantId}/objects/${
                data.findBatch.resultFileId
              }/download`,
              method: 'GET',
              responseType: 'blob',
            })
              .then((response) => {
                const type = response.headers['content-type'];
                const blob = new Blob([response.data], {
                  type: type,
                  encoding: 'UTF-8',
                });
                const fileName = response.headers['content-disposition']
                  .match(/".*"/)[0]
                  .replace(/"/g, '');
                const link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = fileName;
                link.click();
              })
              .catch((err) => {
                alertPayload = {
                  message: err.message,
                  translationId: 'servicetype.upload_error',
                  severity: 'error',
                };
                dispatch({
                  type: 'ALERT_MESSAGE',
                  payload: alertPayload,
                });
              });
          } else {
            dispatch({
              type: 'ALERT_MESSAGE',
              payload: {
                message: `This Batch does not have file results uploaded yet`,
                translationId: 'servicetype.upload_warning',
                severity: 'warning',
              },
            });
          }
        })
        .catch((error) => {});
    };

    return (
      <div ref={ref}>
        <EbsGrid
          id='BatchListContent'
          columns={columns}
          rowactions={rowActions}
          fetch={fetch}
          height='85vh'
          raWidth={300}
          csvfilename='batch-list'
          indexing={true}
          select='multi'
        />
        <ConfirmationModal
          open={openConfirmation}
          title={`Are you sure you want to delete ${batchToDelete?.name}?`}
          onCancel={cancelDeleteBatch}
          onDelete={confirmedDeleteBatch}
          loading={loadingButton}
        >
          <Typography variant='subtitle1'>
            Once confirmed, the deleted batch will no longer be in the batch list.
          </Typography>
        </ConfirmationModal>
      </div>
    );
  },
);

BatchListContent.propTypes = {
  setError: PropTypes.func.isRequired,
  setMessage: PropTypes.func.isRequired,
};

export default BatchListContent;
