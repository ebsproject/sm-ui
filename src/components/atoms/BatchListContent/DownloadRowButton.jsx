import React, { forwardRef, useEffect, useState } from 'react';
import {
  Box,
  IconButton,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
} from '@mui/material';
import CloudDownloadIcon from '@mui/icons-material/CloudDownload';
import { downloadBatch } from 'utils/other/SampleManager';
import SaveIcon from '@mui/icons-material/Save';
import { RBAC } from '@ebs/layout';
import { printoutUri, token } from 'utils/config';
import { saveAs } from 'file-saver';

const DownloadRowButton = forwardRef(({ userProfile, rowData }, ref) => {
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const id = `batch.button.row.${rowData.id}`;

  const downloadScriptoria = async () => {
    const uriDownload = `${printoutUri}/api/report/export?id=${rowData.id}&format=csv&name=sys_scriptoria_genotyping`;
    try {
      handleClose();
      const response = await fetch(uriDownload, {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (!response.ok) {
        throw new Error(`Error to try to get file: ${response.status}`);
      }
      const blob = await response.blob();
      saveAs(blob, `${rowData.name}.csv`);
    } catch (error) {
      console.error(error);
    }
  };

  const downloadVendor = () => {
    downloadBatch(userProfile, rowData);
    handleClose();
  };

  const actions = [
    {
      value: 'vendorFile',
      label: 'Vendor File',
      icon: <SaveIcon />,
      onClick: downloadVendor,
    },
    {
      value: 'scriptoriaFile',
      label: 'CGIAR Service Portal Scriptoria File',
      icon: <SaveIcon />,
      onClick: downloadScriptoria,
    },
  ];

  return (
    <Box ref={ref} data-testid='downloadRowButton' id={id}>
      <RBAC allowedAction={'Download Vendor Report'} product={'Batch Manager'}>
        <IconButton color='primary' title='Download' onClick={handleClick}>
          <CloudDownloadIcon />
        </IconButton>
        <Menu
          id='basic-menu'
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          MenuListProps={{
            'aria-labelledby': 'basic-button',
          }}
        >
          {actions.map((icon) => (
            <MenuItem value={icon.value} key={icon.value} onClick={icon.onClick}>
              <ListItemIcon>{icon.icon}</ListItemIcon>
              <ListItemText primary={icon.label} />
            </MenuItem>
          ))}
        </Menu>
      </RBAC>
    </Box>
  );
});

export default DownloadRowButton;
