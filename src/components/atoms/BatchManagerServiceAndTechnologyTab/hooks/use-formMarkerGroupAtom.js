import { SET_STEP_TWO_TECHNOLOGY } from 'stores/modules/RequestBatch';

const { useEffect, useState } = require('react');

const useFormMarkerGroupAtom = ({
  mode,
  dispatch,
  checkedRequestSelection,
  stepTwoTechnology,
}) => {
  const [markerGroups, setMarkerGroups] = useState([]);
  const [autoSelectedMarkerGroup, setAutoSelectedMarkerGroup] = useState([]);
  const [isLoadingMarkerGroups, setIsLoadingMarkerGroups] = useState(true);

  useEffect(() => {
    if (mode === 'EDIT') {
      setIsLoadingMarkerGroups(false);
      setMarkerGroups(
        stepTwoTechnology.listBatchMarkerGroup.map((listBatch) => ({
          id: listBatch.markerGroupID,
        })),
      );
      setAutoSelectedMarkerGroup(
        stepTwoTechnology.listBatchMarkerGroup.map((listBatch) => ({
          id: listBatch.markerGroupID,
        })),
      );
    } else {
      if (checkedRequestSelection.length > 0) {
        const markerGroupList = [];
        const autoSelectedList = [];
        checkedRequestSelection.forEach((item) => {
          if (item.service === null) {
            return;
          } else {
            if (item.markerPanel) {
              item.markerPanel.forEach((markerPanel) => {
                if (!markerGroupList.some((list) => list.id === markerPanel.id)) {
                  markerGroupList.push({
                    id: markerPanel.id,
                    name: markerPanel.name,
                  });
                }
                if (!autoSelectedList.some((list) => list.id === markerPanel.id)) {
                  autoSelectedList.push({
                    id: markerPanel.id,
                    name: markerPanel.name,
                  });
                }
              });
            }
            if (item.purpose.markerPanels) {
              item.purpose.markerPanels
                .filter((markerPanel) => markerPanel)
                .forEach((markerPanel) => {
                  if (!markerGroupList.some((list) => list.id === markerPanel.id)) {
                    markerGroupList.push({
                      id: markerPanel.id,
                      name: markerPanel.name,
                    });
                  }
                });
            }
            if (item.traitCustom) {
              item.traitCustom.forEach((trait) => {
                if (trait.markergroups) {
                  trait.markergroups
                    .filter((markerGroup) => markerGroup)
                    .forEach((markerGroup) => {
                      if (
                        !markerGroupList.some((list) => list.id === markerGroup.id)
                      ) {
                        markerGroupList.push({
                          id: markerGroup.id,
                          name: markerGroup.name,
                        });
                      }
                      if (
                        !autoSelectedList.some((list) => list.id === markerGroup.id)
                      ) {
                        autoSelectedList.push({
                          id: markerGroup.id,
                          name: markerGroup.name,
                        });
                      }
                    });
                }
              });
            }
          }
        });
        setIsLoadingMarkerGroups(false);
        setMarkerGroups(markerGroupList);
        setAutoSelectedMarkerGroup(autoSelectedList);
      }
    }
    return () => {};
  }, [checkedRequestSelection, mode]);

  const handleChangeTechnologyServiceMarkerGroup = ({
    listBatchMarkerGroup,
    serviceProvider,
    technology,
    technologyServiceProvider,
  }) => {
    dispatch({
      type: SET_STEP_TWO_TECHNOLOGY,
      payload: {
        listBatchMarkerGroup,
        serviceProvider,
        technology,
        technologyServiceProvider,
        isViewOnly: false,
      },
    });
  };

  return {
    autoSelectedMarkerGroupId: autoSelectedMarkerGroup.map((markerGroup) => ({
      id: parseInt(markerGroup.id),
    })),
    handleChangeTechnologyServiceMarkerGroup,
    isLoadingMarkerGroups,
    markerGroups,
    markersGroupId: markerGroups.map((item) => ({ id: parseInt(item.id) })),
    serviceProvider: stepTwoTechnology.serviceProvider,
    technology: stepTwoTechnology.technology,
    technologyServiceProvider: stepTwoTechnology.technologyServiceProvider,
    listBatchMarkerGroup: stepTwoTechnology.listBatchMarkerGroup,
    isViewOnly: stepTwoTechnology.isViewOnly ? stepTwoTechnology.isViewOnly : false,
  };
};

export { useFormMarkerGroupAtom };
