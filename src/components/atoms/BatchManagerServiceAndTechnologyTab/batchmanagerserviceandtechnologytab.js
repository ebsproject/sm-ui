import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import { Box, Divider, Grid } from '@mui/material';
import { MarkerDBComponents } from '@ebs/md';
import RequestsSummaryAtom from '../BatchManager/RequestsSummaryAtom';
import { useFormMarkerGroupAtom } from './hooks/use-formMarkerGroupAtom';
const { MarkerGroupCustomOrganism, WrapperQueryClient } = MarkerDBComponents;
import { Core } from '@ebs/styleguide';
const { Typography, LinearProgress } = Core;

const BatchManagerServiceAndTechnologyTabAtom = React.forwardRef(
  ({ batchDetails, mode, requests }, ref) => {
    const dispatch = useDispatch();
    const { checkedRequestSelection, totalSamplesInBatch, stepTwoTechnology } =
      useSelector(({ request_batch }) => request_batch);

    const {
      autoSelectedMarkerGroupId,
      handleChangeTechnologyServiceMarkerGroup,
      isLoadingMarkerGroups,
      markersGroupId,
      serviceProvider,
      technology,
      technologyServiceProvider,
      listBatchMarkerGroup,
      isViewOnly,
    } = useFormMarkerGroupAtom({
      mode,
      dispatch,
      checkedRequestSelection,
      stepTwoTechnology,
    });

    return (
      <div
        data-testid={'BatchManagerServiceAndTechnologyTabTestId'}
        ref={ref}
        style={{ padding: '4px 25px' }}
      >
        <Grid>
          <Grid item xs={12}>
            <Typography variant='h6'>
              <Box sx={{ fontWeight: 500 }}>
                <FormattedMessage
                  id={'some.id'}
                  defaultMessage={'Service & Technology'}
                />
              </Box>
            </Typography>
          </Grid>
          <br />
          <Grid item xs={12} xl={6}>
            <RequestsSummaryAtom
              batchDetails={batchDetails}
              checkedRequestSelection={checkedRequestSelection}
              mode={mode}
              requests={requests}
              totalSamplesInBatch={totalSamplesInBatch}
            />
          </Grid>
          <Grid item xs={12}>
            <Grid item xs={12} style={{ marginTop: '40px' }}>
              <Divider />
              <Typography variant='h6' style={{ marginTop: '20px' }}>
                <Box sx={{ fontWeight: 500 }}>
                  <FormattedMessage id={'some.id'} defaultMessage={'Technology'} />
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={12} sm={12} style={{ padding: '7px' }}>
              <WrapperQueryClient>
                {isLoadingMarkerGroups && <LinearProgress />}
                {!isLoadingMarkerGroups && (
                  <MarkerGroupCustomOrganism
                    autoSelectedMarkerGroup={autoSelectedMarkerGroupId}
                    markerGroups={markersGroupId}
                    onChange={(result) =>
                      handleChangeTechnologyServiceMarkerGroup(result)
                    }
                    serviceProvider={serviceProvider}
                    technology={technology}
                    technologyServiceProvider={technologyServiceProvider}
                    listBatchMarkerGroup={listBatchMarkerGroup}
                    isOnlyView={isViewOnly}
                  />
                )}
              </WrapperQueryClient>
            </Grid>
          </Grid>
        </Grid>
      </div>
    );
  },
);
// Type and required properties
BatchManagerServiceAndTechnologyTabAtom.propTypes = {};

export default BatchManagerServiceAndTechnologyTabAtom;
