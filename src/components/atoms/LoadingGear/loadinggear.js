import React from 'react'
import PropTypes from 'prop-types'
import { Typography } from '@mui/material'
import { FormattedMessage } from 'react-intl'
// CORE COMPONENTS
import './loading-gear.css'

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const LoadingGearAtom = React.forwardRef((props, ref) => {
  const { message } = props
  return (
    /*
     @prop data-testid: Id to use inside loadinggear.test.js file.
     */
    <>
      <div className="loader">
        <div className="loader_cogs">
          <div className="COGfirst">
            <div className="firstPart"></div>
            <div className="firstPart"></div>
            <div className="firstPart"></div>
            <div className="firstHole"></div>
          </div>
          <div className="COGsecond">
            <div className="secondPart"></div>
            <div className="secondPart"></div>
            <div className="secondPart"></div>
            <div className="secondHole"></div>
          </div>
          {/* <div className="COGthird">
          <div className="thirdPart"></div>
          <div className="thirdPart"></div>
          <div className="thirdPart"></div>
          <div className="thirdHole"></div>
        </div> */}
        </div>
      </div>
      <br />
      <br />
      <div className="error_text">
        <Typography variant="h5">
          <FormattedMessage
            id={'some.id'}
            defaultMessage={
              message ? message : 'An unexpected error has occured.'
            }
          />
        </Typography>
      </div>
      <div className="error_text">
        <Typography variant="body1">
          <FormattedMessage
            id={'some.id'}
            defaultMessage={'  '}
          />
        </Typography>
      </div>
    </>
  )
})
// Type and required properties
LoadingGearAtom.propTypes = {}

export default LoadingGearAtom
