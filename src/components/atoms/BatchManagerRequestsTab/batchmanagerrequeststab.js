import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Link, useLocation } from 'react-router-dom';
import { Box, Grid, Tooltip } from '@mui/material';
import { DeleteForever, Visibility } from '@mui/icons-material';
import { EbsGrid } from '@ebs/components';
import { useDispatch, useSelector } from 'react-redux';
import { filterMembers, setData, sortData } from 'helpers/generalUtils';
import LoadingGear from 'components/atoms/LoadingGear';
import AddRequestsToBatch from 'components/atoms/AddRequestsToBatch';
import { PRIMARY_DENSE_BUTTON_CLASS } from 'utils/other/tailwindCSSClasses';
import RequestsSummaryAtom from '../BatchManager/RequestsSummaryAtom';
import { Core } from '@ebs/styleguide';

const { Button, IconButton, Typography } = Core;

const styles = {
  errorThemedIconButton: {
    color: 'palette.error.dark',
  },
  loadingGearContainer: {
    height: '100vh',
  },
  notSetLabel: {
    color: 'palette.primary.dark',
    fontStyle: 'italic',
  },
  rowActionStyle: {
    display: 'flex',
    alignItems: 'stretch',
  },
};

const BatchManagerRequestsTabAtom = React.forwardRef((props, ref) => {
  const dispatch = useDispatch();
  const location = useLocation();
  const [open, setOpen] = useState(false);
  const { checkedRequestSelection, totalPlatesInBatch, totalSamplesInBatch } =
    useSelector(({ request_batch }) => request_batch);

  const [error, setError] = useState(false);
  const [message, setMessage] = useState(false);

  function checkEmptyValue(value) {
    if (!value)
      return (
        <Box sx={styles.notSetLabel} component={'span'}>
          <FormattedMessage id={'some.id'} defaultMessage={'(Not Set)'} />
        </Box>
      );
    return value;
  }

  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      hidden: true,
      disableGlobalFilter: true,
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.srid'} defaultMessage={'Request'} />
      ),
      accessor: 'requestCode',
      csvHeader: 'Request Code',
    },
    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.requester'}
          defaultMessage={'Requester'}
        />
      ),
      accessor: 'requester',
      csvHeader: 'Requester',
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.program'} defaultMessage={'Program'} />
      ),
      accessor: 'programName',
      csvHeader: 'Program',
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.crop'} defaultMessage={'Crop'} />
      ),
      accessor: 'cropName',
      csvHeader: 'Crop',
    },
    {
      Header: 'Service Type',
      accessor: 'servicetype.name',
      csvHeader: 'Service Type',
    },
    {
      Header: (
        <FormattedMessage
          id={'sm.req.tbl.hdr.servprov'}
          defaultMessage={'Service Provider'}
        />
      ),
      accessor: 'serviceprovider.name',
      csvHeader: 'Service Provider',
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.purpose'} defaultMessage={'Purpose'} />
      ),
      accessor: 'purpose.name',
      csvHeader: 'Purpose',
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Marker Group'} />,
      accessor: 'markerGroup',
      csvHeader: 'Marker Group',
      Cell: ({ value }) => {
        return checkEmptyValue(value);
      },
    },
    {
      Header: (
        <FormattedMessage id={'sm.req.tbl.hdr.quantity'} defaultMessage={'Total'} />
      ),
      accessor: 'totalEntities',
      csvHeader: 'Total Entities',
    },
  ];

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const removeSelectedApprovedRequest = (rowData) => {
    const newCheckedRequestSelection = checkedRequestSelection.filter(
      (request) => request.id !== rowData.id,
    );

    dispatch({
      type: 'SET_CHECKED_REQUEST_SELECTION',
      payload: newCheckedRequestSelection,
    });
  };

  /*
    @dataSelection: Array object with data rows selected.
    @refresh: Function to refresh Grid data.
    */
  const toolbarActions = () => {
    return (
      <>
        <Button
          variant='contained'
          className={PRIMARY_DENSE_BUTTON_CLASS}
          onClick={handleClickOpen}
        >
          <FormattedMessage
            id={'some.id'}
            defaultMessage={'Add Request(s) to batch'}
          />
        </Button>
      </>
    );
  };

  /*
    @rowData: Object with row data.
    @refresh: Function to refresh Grid data.
    */
  const rowActions = (rowData, refresh) => {
    return (
      <div>
        <div></div>
        <Box sx={styles.rowActionStyle} component={'div'}>
          <Tooltip title='View'>
            <>
              <IconButton
                component={Link}
                to={`/sm/view-request-service/${rowData.requestCode}`}
                state={{
                  currentData: rowData,
                  prevPath: location.pathname,
                }}
              >
                <Visibility />
              </IconButton>
            </>
          </Tooltip>
          <Tooltip title='Remove Request'>
            <>
              <IconButton
                sx={styles.errorThemedIconButton}
                title='Delete'
                onClick={() => removeSelectedApprovedRequest(rowData)}
              >
                <DeleteForever />
              </IconButton>
            </>
          </Tooltip>
        </Box>
      </div>
    );
  };

  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        let data;
        if (filters.length) {
          const filteredMembers = filterMembers(checkedRequestSelection, filters);
          data = setData(filteredMembers, page);
        } else {
          data = setData(checkedRequestSelection, page);
        }
        const data2 = data.data.map((row) => {
          const markerGroup = row.markerPanel
            ? row.markerPanel
                .filter((item) => item)
                .map((item, index) => `(${index + 1}) ${item.name}`)
                .join('; ')
            : '(Not Set)';
          return {
            ...row,
            markerGroup: markerGroup,
          };
        });
        data.data = data2;
        if (sort.length >= 1) {
          const sortedData = sortData(data, sort);
          resolve(sortedData);
        } else resolve(data);
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        }
        setError(true);
        reject(error);
        throw new Error(error);
      }
    });
  };

  return (
    <div
      data-testid={'BatchManagerRequestsTabTestId'}
      ref={ref}
      style={{ padding: '4px 25px' }}
    >
      {error ? (
        <Box sx={styles.loadingGearContainer} component={'div'}>
          <LoadingGear message={message} />
        </Box>
      ) : (
        <>
          <Grid container>
            <Grid item xs={12}>
              <Typography variant='h6'>
                <Box sx={{ fontWeight: 500 }}>
                  <FormattedMessage
                    id={'some.id'}
                    defaultMessage={'Requests in batch'}
                  />
                </Box>
              </Typography>
            </Grid>
            <br />
            <br />
            <Grid item xs={12} xl={6}>
              <RequestsSummaryAtom
                checkedRequestSelection={checkedRequestSelection}
                mode={'CREATE'}
                totalSamplesInBatch={totalSamplesInBatch}
              />
            </Grid>
            <Grid item xs={12} style={{ marginTop: '50px' }}>
              <Typography variant='subtitle1'>
                <Box sx={{ fontWeight: 500 }}>
                  <FormattedMessage
                    id={'some.id'}
                    defaultMessage={
                      'This browser shows the list of genotyping requests in your batch.'
                    }
                  />
                </Box>
              </Typography>
            </Grid>
            <Grid item xs={12} style={{ marginTop: '20px' }}>
              <AddRequestsToBatch
                open={open}
                handleClose={handleClose}
                setError={setError}
                setMessage={setMessage}
              />
              {!open && (
                <EbsGrid
                  id='BatchManagerRequestTab'
                  columns={columns}
                  toolbaractions={toolbarActions}
                  rowactions={rowActions}
                  fetch={fetch}
                  height='85vh'
                  csvfilename='approved-request-list'
                  select='multi'
                />
              )}
            </Grid>
          </Grid>
        </>
      )}
    </div>
  );
});
// Type and required properties
BatchManagerRequestsTabAtom.propTypes = {};

export default BatchManagerRequestsTabAtom;
