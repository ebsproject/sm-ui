import React from "react";
import PropTypes from "prop-types";
// CORE COMPONENTS
import { FormattedMessage } from "react-intl";
import { Typography } from "@mui/material";

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const GridHeaderAtom = React.forwardRef((props, ref) => {
  const { title, description, ...rest } = props;

  return (
    /*
     @prop data-testid: Id to use inside gridheader.test.js file.
     */
    <div data-testid={"GridHeaderTestId"} ref={ref}>
      <Typography style={{ fontSize: '24px' }}>
        <FormattedMessage id={"some.id"} defaultMessage={props.title} />
      </Typography>
      <br />
      <Typography style={{ fontSize: '12px' }}>
        <FormattedMessage id={"some.id"} defaultMessage={props.description} />
      </Typography>
      <br />
    </div>
  );
});
// Type and required properties
GridHeaderAtom.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default GridHeaderAtom;
