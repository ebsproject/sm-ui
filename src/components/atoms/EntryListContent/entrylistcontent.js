import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { EbsGrid } from '@ebs/components';
import { B4Rrest } from 'utils/axios/B4Rclient';
import { filterMembers, setData, sortData } from 'helpers/generalUtils';
import { FormattedMessage } from 'react-intl';
import { Core } from '@ebs/styleguide';
const { LinearProgress } = Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const EntryListContentAtom = React.forwardRef((props, ref) => {
  const { listId, setError, setMessage } = props;

  const [entryListName, setEntryListName] = useState('');
  const [type, setType] = useState(null);
  const [loading, setLoading] = useState(false);

  const GERMPLASM_TYPE_COLUMNS = [
    {
      Header: 'Id',
      accessor: 'germplasmDbId',
      hidden: true,
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Display Value'} />,
      accessor: 'displayValue',
      csvHeader: 'Display Value',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      width: 250,
      Header: (
        <FormattedMessage
          id={'some.id'}
          defaultMessage={'Germplasm Normalized Name'}
        />
      ),
      accessor: 'germplasmNormalizedName',
      csvHeader: 'Germplasm Normalized Name',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      width: 200,
      Header: (
        <FormattedMessage id={'some.id'} defaultMessage={'Germplasm Name Type'} />
      ),
      accessor: 'germplasmNameType',
      csvHeader: 'Germplasm Name Type',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Germplasm State'} />,
      accessor: 'germplasmState',
      csvHeader: 'Germplasm State',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      width: 250,
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Parentage'} />,
      accessor: 'parentage',
      csvHeader: 'Parentage',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Designation'} />,
      accessor: 'designation',
      csvHeader: 'Designation',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Generation'} />,
      accessor: 'generation',
      csvHeader: 'Generation',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Order Number'} />,
      accessor: 'orderNumber',
      csvHeader: 'Order Number',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Other Names'} />,
      accessor: 'otherNames',
      csvHeader: 'Other Names',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Remarks'} />,
      accessor: 'remarks',
      csvHeader: 'Remarks',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
  ];

  const PACKAGE_TYPE_COLUMNS = [
    {
      Header: 'Id',
      accessor: 'packageDbId',
      hidden: true,
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Seed Code'} />,
      accessor: 'GID',
      csvHeader: 'Seed Code',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Designation'} />,
      accessor: 'designation',
      csvHeader: 'Designation',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Package Label'} />,
      accessor: 'label',
      csvHeader: 'Package Label',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Display Value'} />,
      accessor: 'displayValue',
      csvHeader: 'Display Value',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Order Number'} />,
      accessor: 'orderNumber',
      csvHeader: 'Order Number',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Package Code'} />,
      accessor: 'packageCode',
      csvHeader: 'Package Code',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Parentage'} />,
      accessor: 'parentage',
      csvHeader: 'Parentage',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Package Manager'} />,
      accessor: 'seedManager',
      csvHeader: 'Package Manager',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Seed Name'} />,
      accessor: 'seedName',
      csvHeader: 'Seed Name',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
  ];

  const PLOT_TYPE_COLUMNS = [
    {
      Header: 'Id',
      accessor: 'plotDbId',
      hidden: true,
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Block Number'} />,
      accessor: 'blockNumber',
      csvHeader: 'Block Number',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Crop Code'} />,
      accessor: 'cropCode',
      csvHeader: 'Crop Code',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Design X'} />,
      accessor: 'designX',
      csvHeader: 'Design X',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Design Y'} />,
      accessor: 'designY',
      csvHeader: 'Design Y',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Designation'} />,
      accessor: 'designation',
      csvHeader: 'Designation',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Entry Name'} />,
      accessor: 'entryName',
      csvHeader: 'Entry Name',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Entry Number'} />,
      accessor: 'entryNumber',
      csvHeader: 'Entry Number',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Sample ID'} />,
      accessor: 'listMemberDbId',
      csvHeader: 'Sample ID',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Harvest Status'} />,
      accessor: 'harvestStatus',
      csvHeader: 'Harvest Status',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Occurence Name'} />,
      accessor: 'occurrenceName',
      csvHeader: 'Occurence Number',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Parentage'} />,
      accessor: 'parentage',
      csvHeader: 'Parentage',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Plot Number'} />,
      accessor: 'plotNumber',
      csvHeader: 'Plot Number',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Plot Type'} />,
      accessor: 'plotType',
      csvHeader: 'Plot Type',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
    {
      Header: <FormattedMessage id={'some.id'} defaultMessage={'Rep'} />,
      accessor: 'rep',
      csvHeader: 'Rep',
      Cell: (row) => {
        return checkEmptyValue(row.value);
      },
    },
  ];

  useEffect(() => {
    listName();
    if (!type) getType();
  }, []);

  function checkEmptyValue(value) {
    if (!value)
      return (
        <span>
          <FormattedMessage id={'some.id'} defaultMessage={'(Not Set)'} />
        </span>
      );
    return value;
  }

  async function getType() {
    try {
      setLoading(true);
      const response = await B4Rrest.get(`/lists/${listId}/members`);
      setType(response.data.result.data[0].type);
      setLoading(false);
    } catch (error) {
      setLoading(false);
      const message = error?.response?.data?.metadata.status[0].messageType;
      if (message) {
        setMessage(message);
      } else {
        setMessage(
          `There was a problem retrieving the /lists/${listId}/members call from the CB-API.`,
        );
      }
      setError(true);
      console.error(error);
    }
  }

  async function listName() {
    try {
      const ids = listId;
      const getList = await B4Rrest.get(`/lists/${ids}`);
      const getListData = getList.data.result.data;

      getListData?.map((list) => {
        setEntryListName(list.displayName);
      });
    } catch (error) {
      const message = error?.response?.data?.metadata.status[0];
      if (message) {
        setMessage(message.messageType + ' for call: ' + message.message);
      } else {
        setMessage(
          `There was a problem retrieving the /lists/${ids} call from the CB-API.`,
        );
      }
      setError(true);
      console.error(error);
    }
  }

  function distributeDataPerPage(data, page) {
    /**
     * Manual pagination since this is an array of objects.
     * The API returns pagination data but not about the
     * members per list.
     */
    let distributedData = [];
    const { number, size } = page;
    let i = 0;
    let dataSized = [];

    /**
     * Given page number and size selected per page, distribute the array of objects
     * into sets of objects, separated by each page. For example:
     *
     * [obj0, obj1, obj2, obj3, obj4, obj5, obj6]
     * size 3 per page, given an array with length of 7
     *
     * [
     *    [obj0, obj1, obj2], // page 1
     *    [obj3, obj4, obj5], // page 2
     *    [obj6,]             // page 3
     * ]
     *
     */
    if (!data.length) return [];
    while (i < data.length) {
      if (dataSized.length < size) {
        dataSized.push(data[i]);

        if (dataSized.length === size) {
          distributedData.push(dataSized);
          dataSized = [];
        } else if (i === data.length - 1) {
          distributedData.push(dataSized);
        }

        i++;
      }
    }

    return distributedData[number - 1];
  }

  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        const id = listId;
        B4Rrest.get(`/lists/${id}/members`).then((response) => {
          const members = response.data.result.data[0].members;
          let data;
          if (filters.length) {
            const filteredMembers = filterMembers(members, filters);
            data = setData(filteredMembers, page);
          } else {
            data = setData(members, page);
          }

          if (sort.length >= 1) {
            const sortedData = sortData(data, sort);
            resolve(sortedData);
          } else resolve(data);
        });
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        } else {
          setMessage(
            `There was a problem retrieving the /lists/${id}/members call from the CB-API.`,
          );
        }
        setError(true);
        reject(error);
      }
    });
  };

  function setGridType(type) {
    switch (type) {
      case 'germplasm':
        return GERMPLASM_TYPE_COLUMNS;
      case 'package':
        return PACKAGE_TYPE_COLUMNS;
      case 'plot':
        return PLOT_TYPE_COLUMNS;
      default:
        return;
    }
  }

  function setCSVFilename(type) {
    switch (type) {
      case 'germplasm':
        return 'germplasm-list';
      case 'package':
        return 'package-list';
      case 'plot':
        return 'plot-list';
      default:
        return;
    }
  }

  return (
    /*
     @prop data-testid: Id to use inside entrylistcontent.test.js file.
     */
    <div>
      <h2>{entryListName}</h2>
      {loading && <LinearProgress color='primary' />}
      {type && (
        <EbsGrid
          id='EntryListContent'
          toolbar={true}
          title={entryListName}
          columns={setGridType(type)}
          fetch={fetch}
          csvfilename={setCSVFilename(type)}
          height='85vh'
          callstandard='brapi'
        />
      )}
    </div>
  );
});
// Type and required properties
EntryListContentAtom.propTypes = {};

export default EntryListContentAtom;
