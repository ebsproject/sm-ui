import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import {
  DialogTitle,
  DialogContent,
  DialogActions,
  Dialog,
  Slide,
} from '@mui/material';
import { FormattedMessage } from 'react-intl';
import {
  WHITE_BUTTON_CLASS,
  PRIMARY_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import { Core } from '@ebs/styleguide';
const { Button, Typography } = Core;

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='up' ref={ref} {...props} />;
});

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ConfirmDialogRawAtom = React.forwardRef(
  ({ open = false, onCancel, children, title, onOk, ...other }, ref) => {
    return (
      <Dialog
        data-testid={'ConfirmDialogRawTestId'}
        disableBackdropClick
        disableEscapeKeyDown
        fullWidth
        id='confirmation-dialog-title'
        maxWidth='sm'
        open={open}
        ref={ref}
        TransitionComponent={Transition}
        {...other}
      >
        <DialogTitle id='confirmation-dialog-title'>
          <Typography align='left' variant='h4'>
            {title}
          </Typography>
        </DialogTitle>
        <DialogContent dividers>{children}</DialogContent>
        <DialogActions>
          <Button
            autoFocus
            onClick={onCancel}
            className={WHITE_BUTTON_CLASS}
            variant='contained'
          >
            <FormattedMessage id='cs.confirmdialog.cancel' defaultMessage='Cancel' />
          </Button>
          <Button
            onClick={onOk}
            className={PRIMARY_BUTTON_CLASS}
            variant='contained'
          >
            <FormattedMessage
              id='cs.confirmdialog.confirm'
              defaultMessage='Confirm'
            />
          </Button>
        </DialogActions>
      </Dialog>
    );
  },
);
// Type and required properties
ConfirmDialogRawAtom.propTypes = {
  title: PropTypes.node,
  open: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onOk: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default ConfirmDialogRawAtom;
