import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Grid } from '@mui/material';
import { EbsForm } from '@ebs/components';
import { FormattedMessage } from 'react-intl';
import { useQuery } from '@apollo/client';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import {
  PRIMARY_DENSE_BUTTON_CLASS,
  WHITE_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import {
  FIND_CROP_LIST,
  FIND_TYSSUE_TYPE_LIST,
  FIND_CUSTOM_FIELDS_BY_EBSFORM_ID,
} from 'utils/apollo/gql/RequestManager';
import { getAdminContactsByTeam } from 'helpers/requestManagerUtils';
import { SMgraph } from 'utils/apollo/SMclient';
import { Core } from '@ebs/styleguide';
import {
  SET_CUSTOM_FIELDS,
  SET_TO_EDIT_ROW_DATA_REFORMATED,
} from 'stores/modules/RequestManager';
const { Button, Typography, DialogActions, LinearProgress } = Core;

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const GeneralFormAtom = React.forwardRef((props, ref) => {
  const { value, onSubmitCimmyt, handleChange, setError, setMessage, goBackPath } =
    props;

  const {
    mode,
    basicTabData,
    selectedEntryList,
    toEditRowData,
    programId,
    programName,
    entryUserName,
    entryUserEmail,
    germplasmOwner,
    germplasmOwnerEmail,
    programList,
    tissueTypeId,
    tissueTypeName,
    customFields,
    ebsFormId,
    numberOfSamplesPerItem,
    cropName,
    cropId,
  } = useSelector(({ RequestManager }) => RequestManager);

  const MAX_SAMPLES = 50000;
  const dispatch = useDispatch();
  const brandColor = '#808080';
  const customStyles = {
    control: (base, state) => ({
      ...base,
      boxShadow: state.isFocused ? 0 : 0,
      borderColor: state.isFocused ? brandColor : base.borderColor,
      color: state.isDisabled ? base.borderColor : brandColor,
      '&:hover': {
        borderColor: state.isFocused ? brandColor : base.borderColor,
      },
      '&:disabled': {
        color: brandColor,
      },
    }),
  };
  const [crops, setCrops] = useState([]);
  const [tissueTypes, setTissueTypes] = useState(null);
  const [formComponents, setFormComponents] = useState([]);
  const [adminContacts, setAdminContacts] = useState(null);
  const [adminContact, setAdminContact] = useState(null);
  const [adminContactEmail, setAdminContactEmail] = useState('asdsadasd');
  const [programComponent, setProgramComponent] = useState('TextField');
  const [isLoading, setIsLoading] = useState(true);
  const [programDefaultValue, setProgramDefaultValue] = useState({
    value: programId,
    label: programName,
  });
  const [programInputProps, setProgramInputProps] = useState({
    size: 'small',
    color: 'primary',
    label: 'Program',
    disabled: true,
  });
  const [_numberOfSamplesPerItem, setNumberOfSamplesPerItem] = useState(
    numberOfSamplesPerItem,
  );
  
  const handleSaveButton = (data, _numberOfSamplesPerItem) => {
      onSubmitCimmyt(data, _numberOfSamplesPerItem);
  };

  useEffect(() => {
    const setRunningSegmentCodeBlock = async () => {
      if (entryUserEmail !== null || entryUserName !== null) {
        /**
         *
         * The two if's are for preventing the functions to be infinitely
         * called. The setAdminContactsByTeam is for getting the list
         * of admins and after getting the list of admins, use it
         * to build the rest of the components.
         */

        if (!adminContacts || (mode === 'CREATE' && !adminContacts)) {
          setAdminContactsByTeam(programId);
          setAdminContactEmail('');
        }
        if (adminContacts || (mode === 'CREATE' && adminContacts)) {
          buildFormComponents(ebsFormId, adminContacts).then((result) => {
            setIsLoading(false);
          });
        }
      }

      if (programList.length > 1) {
        setProgramComponent('Select');
        let props = {
          placeholder: 'Service Provider',
          label: 'Program',
          styles: customStyles,
        };
        setProgramInputProps(props);
      } else {
        setProgramDefaultValue(programName);
        setValue('crop');
      }

      if (mode === 'CREATE' && basicTabData) {
        const reformattedArray = customFields.map((obj) => {
          const objNew = { ...obj };
          Object.entries(basicTabData).forEach(([key, value]) => {
            if (obj.component.name === key) {
              objNew.value = value;
            }
          });
          return objNew;
        });
        dispatch({
          type: 'SET_CUSTOM_FIELDS',
          payload: reformattedArray,
        });
      }
      if (mode === 'EDIT') {
        let customFieldsEdit = [];
        let customValueFromToEdit = [...toEditRowData.values];

        customValueFromToEdit.forEach((customValue) => {
          let field = {};
          for (let key in customValue) {
            if (key === 'field') {
              if (customValue[key].name === 'idNotes') field['name'] = 'description';
              else if (customValue[key].name === 'idContactPerson')
                field['name'] = 'contactPerson';
              else if (customValue[key].name === 'idOcsNumber')
                field['name'] = 'ocsNumber';
              else field['name'] = customValue[key].name;

              field['id'] = customValue[key].id;
            } else if (key === 'value') {
              field[key] = customValue[key];
            }
          }
          customFieldsEdit.push(field);
        });
        dispatch({
          type: SET_TO_EDIT_ROW_DATA_REFORMATED,
          payload: customFieldsEdit,
        });

        // toEditRowData.reformatedCustomFields = customFieldsEdit;
      }
    };
    setRunningSegmentCodeBlock().catch((error) => {
      throw new Error(error);
    });
  }, [adminContact, adminContacts, adminContactEmail]);

  const cropsData = useQuery(FIND_CROP_LIST);
  const tissueTypeData = useQuery(FIND_TYSSUE_TYPE_LIST);

  if (!crops && cropsData.data) {
    let crops = [];
    cropsData.data.findCropList.content.map((crop) => {
      crops.push({ value: crop.id, label: crop.cropName });
    });
    setCrops(crops);
  }
  if (!tissueTypes && tissueTypeData.data) {
    const tissues = tissueTypeData.data.findTissueTypeList.content.map((tissue) => ({
      value: tissue.id,
      label: tissue.name,
    }));
    setTissueTypes(tissues);
  }

  //to build the custom form components (left form of Basic Tab in RM)
  async function buildFormComponents(ebsFormId, teamMembersWithAdminAsRoleCode) {
    const components = [];
    const customFieldsEdit = [];
    SMgraph.query({
      query: FIND_CUSTOM_FIELDS_BY_EBSFORM_ID,
      variables: {
        id: ebsFormId,
      },
      fetchPolicy: 'no-cache',
    })
      .then(({ data }) => {
        // Populating the custom form component
        data.findFieldList.content.map((componentData) => {
          if (componentData.name != 'idEstimatedBy') {
            if (componentData.name === 'idAdminContact') {
              componentData.isRequired = false;
              componentData.component.isRequired = false;
            }
            if (componentData.name === 'idTeamWorkContactEmail') {
              componentData.isRequired = false;
              componentData.component.isRequired = false;
            }

            //the call that builds the component to the format that we usually set it to.
            let component = createComponent(componentData);

            if (componentData.name === 'idAdminContact') {
              component.options = teamMembersWithAdminAsRoleCode;
              /**
               * This is to set the value of the Admin Contact on Edit mode,
               * since the selected Request to be edited has
               * an adminContactId.
               */
              if (mode === 'CREATE' && basicTabData) {
                /**
                 * This is for showing the previously selected Admin Contact/Researcher
                 * when Tab switching.
                 */
                component['defaultValue'] = basicTabData.adminContact;
              }
            }

            if (!componentData.isBase) {
              //not base, meaning custom
              let field = componentData;

              if (!componentData.defaultValue) {
                field['value'] = component.defaultValue;
              }
              customFieldsEdit.push(field);
            }
            components.push(component);
          }
        });
        dispatch({
          type: SET_CUSTOM_FIELDS,
          payload: customFieldsEdit,
        });
        setFormComponents(components);
        return components;
      })
      .catch((error) => {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        } else {
          setMessage(
            `There was a problem retrieving the findFieldList by ID: ${ebsFormId} call from the SM-API.`,
          );
        }
        setError(true);
      });
  }

  const generalDefinition = (props, data) => {
    const datatoValidate = [...data];
    const { getValues, setValue } = props;
    cimmytDefinition(props, datatoValidate);
    return {
      name: 'generalForm',
      components: [
        {
          sizes: [6, 6, 6, 6, 6],
          component: programComponent,
          name: 'program',
          options: programList,
          onChange: (e, option, setValue, getValues) =>
            updateCrop(e, option, setCrops, setValue),
          rules: { required: 'Required ..' },
          inputProps: programInputProps,
          sort: 1,
          //defaultValue: programList.length > 1 ? programDefaultValue : programName,
          defaultValue: programDefaultValue ? programDefaultValue :  programList.length > 1 ? { value: cropId,
            label: cropName}: null,
          helper: {
            title: 'Program',
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [6, 6, 6, 6, 6],
          component: 'select',
          name: 'crop',
          options: crops,
          rules: { required: 'Required ..' },
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Crop',
            placeholder: ' ',
            disabled: crops.length <= 1 ? true : false,
          },

          sort: 3,
          defaultValue: cropId
            ? {
                value: cropId,
                label: cropName,
              }
            : programList.length > 1
              ? {
                  value: programList[0].crops[0].id,
                  label: programList[0].crops[0].name,
                }
              : null,
          helper: {
            title: 'Crop',
            placement: 'right',
            arrow: true,
          },
        },

        {
          sizes: [6, 6, 6, 6, 6],
          component: 'TextField',
          name: 'requester',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Requester',
            placeholder: entryUserName,
            disabled: true,
          },
          sort: 5,
          defaultValue: entryUserName,
          helper: {
            title: 'The person who is making the request',
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [6, 6, 6, 6, 6],
          component: 'TextField',
          name: 'requesterEmail',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: "Requester's Email",
            placeholder: entryUserEmail,
            disabled: true,
          },
          sort: 7,
          defaultValue: entryUserEmail,
          helper: {
            title: "Requester's email address",
            placement: 'right',
            arrow: true,
          },
        },
        {
          //sizes: datatoValidate.length > 3 ?[12, 12, 12, 12, 12]:[6, 6, 6, 6, 6],
          sizes:
            datatoValidate.length === 3 ? [12, 12, 12, 12, 12] : [6, 6, 6, 6, 6],
          //sizes: [12, 12, 12, 12, 12],
          component: 'TextField',
          name: 'germplasmOwner',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'List Creator',
            placeholder: germplasmOwner,
            disabled: true,
            sx: { width: datatoValidate.length === 3 ? '50%' : '100%' },
          },
          sort: 9,
          defaultValue: germplasmOwner,
          helper: {
            title: 'The creator of the germplasm list',
            placement: 'right',
            arrow: true,
          },
        },
        {
          //sizes: [6, 6, 6, 6, 6],
          sizes:
            datatoValidate.length === 3 ? [12, 12, 12, 12, 12] : [6, 6, 6, 6, 6],
          component: 'TextField',
          name: 'germplasmOwnerEmail',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'List Creator email',
            placeholder: germplasmOwner,
            disabled: true,
            sx: { width: datatoValidate.length === 3 ? '50%' : '100%' },
          },
          sort: 11,
          defaultValue: germplasmOwnerEmail,
          helper: {
            title: 'List Creator email',
            placement: 'right',
            arrow: true,
          },
        },
        {
          sizes: [12, 12, 12, 12, 12],
          component: 'Select',
          name: 'tissuetype',
          options: tissueTypes,
          onChange: (e, option) => updateTissue(e, option),
          rules: { required: 'Required ..' },
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Tissue Type',
            placeholder: 'Tissue Type',
            disabled: false,
            sx: { width: '10%' },
          },
          defaultValue: {
            value: tissueTypeId,
            label: tissueTypeName,
          },
          sort: 13,
          helper: {
            title: 'Tissue Type',
            placement: 'right',
            arrow: true,
          },
          rules: {
            required: 'Please select a Tissue type',
          },
        },
        {
          sizes: [6, 6, 6, 6, 6],
          component: 'TextField',
          name: 'numberSamplesPerItem',
          inputProps: {
            size: 'small',
            color: 'primary',
            label: 'Number of samples per item',
            placeholder: numberOfSamplesPerItem,
            disabled: mode === 'CREATE' ? false : true,
            type: 'number',
            variant: 'outlined',
            sx: { width: '30%' },
            min: 1,
          },
          sort: 14,
          validationMessageError: `Max sample size is 50,000.`,
          rules: {
            validate: (e) => {
              return handleNumberOfSamplesPerItemChange(e);
            },
            required: 'required',
          },
          defaultValue: numberOfSamplesPerItem ? numberOfSamplesPerItem : 1,
          helper: {
            title: 'Number of samples per item',
            placement: 'right',
            arrow: true,
          },
        },
        ...datatoValidate,
      ].sort((a, b) => a.sort - b.sort),
    };
  };

  const setService = async (getValues, setValue) => {
    const item = getValues('adminContact');
    setValue('adminContactEmail', item.email);
  };

  function createComponent(componentData) {
    let inputProps = getInputProps(componentData);
    let componentRules = getRules(componentData);
    let helpers = componentData.component.helpers;
    let component = {
      name: componentData.component.name,
      component: componentData.component.componenttype.name,
      sizes: [12, 12, 12, 12, 12],
      inputProps: inputProps,
      rules: componentRules,
    };
    var date = new Date();
    if (componentData.name === 'idEstimatedBy') {
      const numToAdd = 15;

      for (let i = 1; i <= numToAdd; i++) {
        date.setDate(date.getDate() + 1);
        if (date.getDay() === 6) {
          date.setDate(date.getDate() + 2);
        } else if (date.getDay() === 0) {
          date.setDate(date.getDate() + 1);
        }
      }
      date.setDate(date.getDate());
      component['defaultValue'] = date;
    }

    /**
     * This is for showing the previously selected Admin Contact/Researcher/values
     * when Tab switching.
     */

    if (helpers.length > 0) {
      component.helper = {
        title: helpers[0].title,
        placement: helpers[0].placement,
        arrow: helpers[0].arrow,
      };
    }

    /**
     * This is needed so that the value of the Select for Researcher will appear in the UI
     */
    if (componentData.component.label === 'Researcher') {
      component.helper = {
        title: 'The name of the Researcher',
        placement: 'right',
        arrow: true,
      };
    }
    return component;
  }

  function getInputProps(componentData) {
    let inputProps = {};

    for (let i = 0; i < componentData.component.checkprops.length; i++) {
      let property = componentData.component.checkprops[i];
      if (
        componentData.name === 'idAdminContact' ||
        componentData.name === 'idNotes'
      ) {
        if (property.key === 'placeholder') {
          inputProps['label'] = evaluate(property.value);
        } else if (property.key === 'styles') {
          inputProps['styles'] = customStyles;
        }
      } else if (property.key === 'variant') {
      } else inputProps[property.key] = evaluate(property.value);
    }

    inputProps['size'] = 'small';
    return inputProps;
  }

  function evaluate(value) {
    try {
      return eval(value);
    } catch (err) {
      return value;
    }
  }

  function getRules(componentData) {
    let rules = {};
    if (componentData.isRequired) {
      if (componentData.name === 'idContactPerson') {
        rules = {
          required:
            'Contact Person is required. Please provide the name of person to contact.',
        };
      } else {
        rules = {
          required: componentData.component.rule,
        };
      }
    }
    return rules;
  }

  async function setAdminContactsByTeam(id) {
    try {
      const teamMembersWithAdminAsRoleCode = await getAdminContactsByTeam(
        id,
        setMessage,
      );
      const sortAdmins = teamMembersWithAdminAsRoleCode.sort((a, b) =>
        a.lastName.localeCompare(b.lastName),
      );
      setAdminContacts(sortAdmins);
      if (mode === 'EDIT') {
        const adminContact = teamMembersWithAdminAsRoleCode.find(
          (admin) => admin.value == toEditRowData.adminContactId,
        );
        if (adminContact) setAdminContact(adminContact);
      }
      dispatch({
        type: 'SET_REQUESTER',
        payload: entryUserName,
      });
    } catch (error) {
      const message = error?.response?.data?.metadata.status[0].messageType;
      console.error(error);
      if (message) {
        setMessage(message);
      } else {
        setMessage(
          `There was a problem retrieving the /persons/${id}/teams call from the CS-API.`,
        );
      }
      setError(true);
    }
  }

  const cimmytDefinition = (props, datatoValidate) => {
    //just formFieldDefinition
    const { getValues, setValue } = props;
    if (typeof setValue === 'function') {
      var indexInitSort = 2;
      datatoValidate.forEach((fc) => {
        fc['sort'] = indexInitSort;
        fc['sizes'] = [6, 6, 6, 6, 6];
        for (let key in fc) {
          if (key === 'inputProps' && fc[key].label === 'Team Work Contact') {
            fc['onChange'] = (element) => setService(getValues, setValue);
          } else if (fc[key] === 'submitionDate') {
            setValue(fc[key], new Date());
          }
          if ((mode === 'CREATE' || basicTabData?.submitted) && key === 'name') {
            if (basicTabData?.submitted) {
              const componentKey = Object.keys(basicTabData).find(
                (keyBasicTabData) => keyBasicTabData === fc[key],
              );
              if (componentKey) setValue(fc[key], basicTabData[componentKey]);
            }
          } else if (mode === 'EDIT' && key === 'name') {
            if (fc[key] === 'adminContact' && adminContact) {
              const contact = {
                value: adminContact.value,
                label: adminContact.label,
              };
              setValue(fc[key], contact);
            }
            if (fc[key] === 'adminContactEmail' && adminContact) {
              setValue(fc[key], adminContact.email);
            }
            if (fc[key] === 'submitionDate') {
              setValue(fc[key], new Date());
            } else {
              const componentKey = Object.keys(toEditRowData).find(
                (KeyToEditRowData) => KeyToEditRowData === fc[key],
              );
              if (componentKey) setValue(fc[key], toEditRowData[componentKey]);
              else if (toEditRowData.reformatedCustomFields) {
                var customComponent = toEditRowData.reformatedCustomFields.find(
                  (item) => item.name === fc[key],
                );
                if (customComponent) setValue(fc[key], customComponent.value);
              }
            }
          }
        }
        indexInitSort = indexInitSort + 2;
      });
    }
    //return datatoValidate;
  };

  async function updateTissue(e, option) {
    // const item = getValues('tissuetype');
    dispatch({
      type: 'SET_TISSUE_TYPE_ID',
      payload: option ? option.value : null,
    });
    dispatch({
      type: 'SET_TISSUE_TYPE_NAME',
      payload: option ? option.label : '',
    });
  }

  async function updateCrop(e, option, setCrops, setValue) {
    // const item = getValues('program');
    if (option) {
      dispatch({
        type: 'SET_PROGRAM_ID',
        payload: option.value,
      });
      dispatch({
        type: 'SET_PROGRAM_NAME',
        payload: option.label,
      });

      setAdminContactsByTeam(option.value);
      setAdminContactEmail('');

      if (option.crops.length > 0) {
        dispatch({
          type: 'SET_CROP_ID',
          payload: option.crops[0].id,
        });

        dispatch({
          type: 'SET_CROP_NAME',
          payload: option.crops[0].name,
        });
        setCrops(
          option.crops.map((crop) => {
            return { value: crop.id, label: crop.name };
          }),
        );
        setValue('crop', { value: option.crops[0].id, label: option.crops[0].name });
      }
    }
  }

  async function handleNumberOfSamplesPerItemChange(e) {
    const value = Number(e);
    if (value <= 0) {
      e.target.value = 1;
      setNumberOfSamplesPerItem(e);
      return;
    }
    if (value * selectedEntryList.memberCount > MAX_SAMPLES) {
      return false;
    }
    setNumberOfSamplesPerItem(value);
    return true;
  }

  return (
    <div>
      <Grid container spacing={3} ref={ref}>
        {isLoading && <LinearProgress />}
        {!isLoading && (
          <Grid item xs={12}>
            <div>
              <EbsForm
                definition={(props) => generalDefinition(props, formComponents)}
                onSubmit={(data) => handleSaveButton(data, _numberOfSamplesPerItem)}
                actionPlacement='top'
              >
                <DialogActions>
                  <Button
                    onClick={() => goBackPath()}
                    className={WHITE_BUTTON_CLASS}
                  >
                    <Typography variant='body1'>
                      <FormattedMessage id={'some.id'} defaultMessage={'CANCEL'} />
                    </Typography>
                  </Button>
                  <Button
                    className={PRIMARY_DENSE_BUTTON_CLASS}
                    variant='contained'
                    onClick={() => handleChange(null, value - 1)}
                  >
                    <Typography variant='body1'>
                      <FormattedMessage
                        id={'some.id'}
                        defaultMessage={'BACK'}
                        className={PRIMARY_DENSE_BUTTON_CLASS}
                      />
                    </Typography>
                  </Button>
                  <Button
                    disabled={
                      formComponents.length > 0 && tissueTypeId ? false : true
                    }
                    type='submit'
                    className={PRIMARY_DENSE_BUTTON_CLASS}
                  >
                    <Typography variant='body1'>
                      <FormattedMessage id='something.id' defaultMessage='NEXT' />
                    </Typography>
                  </Button>
                </DialogActions>
              </EbsForm>
            </div>
          </Grid>
        )}
      </Grid>
    </div>
  );
});
// Type and required properties
GeneralFormAtom.propTypes = {};

export default GeneralFormAtom;
