import React from 'react'
import ReactDOM from 'react-dom'
// Component to be Test
import FormControl from './formcontrol'
// Test Library
import { render, cleanup } from '@testing-library/react'
import '@testing-library/dom'
import '@testing-library/jest-dom/extend-expect'

const props = {
  label:'test',

}

afterEach(cleanup)
test('Report name', () => {
  const div = document.createElement('div')
  ReactDOM.render(<FormControl {...props}></FormControl>, div)
})

test('Render correctly', () => {
  const { getByTestId } = render(<FormControl {...props}></FormControl>)
  expect(getByTestId('FormControlTestId')).toBeInTheDocument()
})
