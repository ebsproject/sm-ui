import React from 'react';
import PropTypes from 'prop-types';
import InputLabel from '@mui/material/InputLabel';

import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

const FormControlAtom = React.forwardRef((_, ref) => {
  // Properties of the atom
  const [valueId, setvalueId] = React.useState(1);
  const handleChange = (event) => {
    setvalueId(event.target.value);
    props.handleChange(event.target.value);
  };

  return (
    <div data-testid={'FormControlTestId'} ref={ref}>
      <FormControl variant='outlined'>
        <InputLabel htmlFor='outlined-age-native-simple'>{props.label}</InputLabel>
        <Select
          native
          value={valueId}
          onChange={handleChange}
          label={props.label}
          inputProps={{
            name: 'FormControl',
            id: 'outlined-age-native-simple',
          }}
        >
          <option value={1}>InterTek low density blanks (H11, H12)</option>
          <option value={2}>InterTek mid density blanks (G12, H12)</option>
          <option value={3}>DArT blanks (G12, H12)</option>
          <option value={4}>User-defined controls</option>
        </Select>
      </FormControl>
    </div>
  );
});
// Type and required properties
FormControlAtom.propTypes = {};

export default FormControlAtom;
