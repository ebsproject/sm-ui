import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Avatar, Box, Stack } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import { Core } from '@ebs/styleguide';
const { Typography, Tab } = Core;

const TabTitleAtom = forwardRef(
  ({ id, tabCount, label, idFormatted = 'some.id' }, ref) => {
    return (
      <Tab
        id={`tabTile.${id}.${tabCount}`}
        label={
          <Stack direction={'row'} spacing={1} ref={ref}>
            <Box alignContent={'center'}>
              {/* TODO pallete primary main*/}
              <Avatar sx={{ width: 22, height: 22, backgroundColor: 'palette.primary.main' } }>
                <Typography variant='body1'>{tabCount}</Typography>
              </Avatar>
            </Box>
            <Box alignContent={'center'}>
              <Typography variant='h6'>
                <strong>
                  <FormattedMessage id={idFormatted} defaultMessage={label} />
                </strong>
              </Typography>
            </Box>
          </Stack>
        }
      />
    );
  },
);

TabTitleAtom.propTypes = {};

export default TabTitleAtom;
