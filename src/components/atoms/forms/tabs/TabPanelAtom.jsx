import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';

const TabPanelAtom = forwardRef(({ id, children, value, index }, ref) => {
  return (
    <Box id={`tabPane.${id}.value${value}`} ref={ref}>
      {value === index && <>{children}</>}
    </Box>
  );
});

TabPanelAtom.propTypes = {
  id: PropTypes.string.isRequired,
  value: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  children: PropTypes.element,
};

export default TabPanelAtom;
