import React, { useEffect, useMemo } from 'react';
import PropTypes from 'prop-types';
import { EbsGrid } from '@ebs/components';
import { SMgraph } from 'utils/apollo/SMclient';
import { FIND_REQUEST_LIST } from 'utils/apollo/gql/RequestManager';
import { GETUSERPROFILE } from 'utils/config';
import { FormattedMessage } from 'react-intl';
import { getSortOrDefault, SelectColumnFilter } from 'utils/other/generalFunctions';
import StatusBadge from 'components/atoms/StatusBadge';
import { useDispatch, useSelector } from 'react-redux';
import { getPersonID } from 'utils/axios/B4Rclient';
import { Core } from '@ebs/styleguide';
const { Box, Typography } = Core;

const notSetLabel = (
  <Typography component='span' color='primary'>
    <Box fontStyle='italic'> (Missing value) </Box>{' '}
  </Typography>
);
const ebsGridFontStyles = {
  columnStyle: {
    fontSize: 16,
  },
  rowStyle: {
    fontSize: 14,
  },
};

// TODO: Remove the batch code is one table, exist one call to api and need to remove it
// TODO: fetch "batchCode" replace by 0 sampleId, and plateId

const RequestListContent = React.forwardRef(
  ({ toolbarActions, rowActions, setError, setMessage, view, userProfile }, ref) => {
    const USER_PROFILE = userProfile;

    const dispatch = useDispatch();

    const { isDatabaseUpdated, numberOfSamplesPerItem } = useSelector(
      ({ RequestManager }) => RequestManager,
    );

    useEffect(() => {
      if (numberOfSamplesPerItem !== null) {
        dispatch({
          type: 'SET_NUMBER_OF_SAMPLES_PER_ITEM',
          payload: null,
        });
      }
    }, []);
    // Columns
    const columns = useMemo(() => [
      {
        Header: 'Id',
        accessor: 'id',
        hidden: true,
        disableGlobalFilter: true,
      },
      {
        width: 150,
        Header: (
          <FormattedMessage id={'sm.req.tbl.hdr.status'} defaultMessage={'Status'} />
        ),
        accessor: 'status',
        Cell: ({ value }) => {
          return <StatusBadge value={value} />;
        },
        Filter: SelectColumnFilter,
        csvHeader: 'Status',
        sticky: 'left',
      },
      {
        width: 170,
        Header: (
          <FormattedMessage id={'sm.req.tbl.hdr.srId'} defaultMessage={'Request'} />
        ),
        accessor: 'requestCode',
        csvHeader: 'Request Code',
        sticky: 'left',
      },
      {
        width: 170,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.requester'}
            defaultMessage={'Requester'}
          />
        ),
        accessor: 'requester',
        csvHeader: 'Requester',
      },
      {
        width: 170,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.program'}
            defaultMessage={'Program'}
          />
        ),
        accessor: 'programName',
        csvHeader: 'Program',
      },
      {
        width: 100,
        Header: (
          <FormattedMessage id={'sm.req.tbl.hdr.crop'} defaultMessage={'Crop'} />
        ),
        accessor: 'cropName',
        csvHeader: 'Crop',
      },
      {
        width: 170,
        Header: 'Service Type',
        accessor: 'servicetype.name',
        csvHeader: 'Service Type',
      },
      {
        width: 170,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.servprov'}
            defaultMessage={'Service Laboratory'}
          />
        ),
        accessor: 'serviceprovider.name',
        csvHeader: 'Service Laboratory',
      },
      {
        width: 170,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.purpose'}
            defaultMessage={'Purpose'}
          />
        ),
        accessor: 'purpose.name',
        csvHeader: 'Purpose',
      },
      {
        width: 170,
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Trait (s)'} />,
        accessor: 'traitCustomString',
        csvHeader: 'Trait Custom',
        Cell: ({ value }) => {
          return checkEmptyValue(value);
        },
      },
      {
        width: 170,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.service'}
            defaultMessage={'Marker Group'}
          />
        ),
        accessor: 'markerGroup',
        csvHeader: 'markerGroup',
        Cell: ({ value }) => {
          return checkEmptyValue(value);
        },
      },
      {
        width: 75,
        Header: (
          <FormattedMessage
            id={'sm.req.tbl.hdr.quantity'}
            defaultMessage={'Total'}
          />
        ),
        accessor: 'totalEntities',
        csvHeader: 'Total Entities',
      },
      {
        width: 310,
        Header: 'Modifier',
        accessor: 'updatedByUser',
        csvHeader: 'Modifier',
      },
      {
        width: 210,
        Header: 'Modified Timestamp',
        accessor: 'updatedOn',
        csvHeader: 'Modified timestamp',
      },
      {
        width: 210,
        Header: 'Creation Timestamp',
        accessor: 'createdOn',
        csvHeader: 'Creation timestamp',
      },
    ]);

    useEffect(() => {
      const setAdminData = async () => {
        const data = await setAdmin();
        resetRequestDetails();
      };
      setAdminData().catch((error) => {
        throw new Error(error);
      });
    }, [isDatabaseUpdated]);

    function resetRequestDetails() {
      /**
       * This dispatch is for clearing the selected Entry List in the store
       * for Edit Mode. This clearing is needed so when the user cancels
       * the Edit mode of a Request, there will be a reset of what was
       * actually stored in the store.
       */

      dispatch({
        type: 'SET_REQUEST_VALUES',
        payload: null,
      });
    }

    async function setAdmin() {
      const email = USER_PROFILE.account;

      try {
        const response = await getPersonID(email);
        if (response?.data?.result?.data[0]?.personDbId) {
          dispatch({
            type: 'SET_ADMIN_CONTACT_ID',
            payload: response.data.result.data[0].personDbId,
          });
        } else {
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: `The Core Breeding call /persons-search did not return a personDbId for your profile.`,
              translationId: 'some.id',
              severity: 'warning',
            },
          });
        }
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        } else {
          setMessage(
            `There was a problem retrieving the /persons-search call with email as parameter from the CB-API.`,
          );
        }
        throw new Error(error);
      }
    }

    function setupRequests(content) {
      let requests = [];
      for (let request of content) {
        /**
         * Add properties such as requester, cropName, programName, and filter
         * depending on what view is this component used for.
         */

        //     const { givenName, familyName } = request.user.contact.person;

        // This is to handle the case of empty Service in CSV download
        if (request.service === null) request.service = { name: '' };
        request['requester'] = request.user.contact.person.fullName || notSetLabel;
        request['cropName'] = request.crop?.name || notSetLabel;
        request['programName'] = request.program?.name || notSetLabel;
        request['updatedByUser'] = request.updatedByUser
          ? request.updatedByUser.contact.person.fullName
          : '';
        request['updatedOn'] = request.updatedOn ? request.updatedOn : '';
        requests.push(request);
      }
      return requests;
    }

    function setApprovedRequests(requests) {
      const approvedRequests = requests.filter(
        (request) => request.status.name === 'Approved',
      );
      dispatch({
        type: 'SET_APPROVED_REQUESTS',
        payload: approvedRequests,
      });
    }

    function adjustFilters(filters, userId) {
      /**
       * This is to update the proper filter object before the query.
       */
      const adjustedFilters = filters.map((filter) => {
        const adjustedFilter = { ...filter };

        // The keys for filtering: col, mod, val
        adjustedFilter['col'] = filter.col;
        adjustedFilter['mod'] = 'LK';
        adjustedFilter['val'] = filter.val;
        if (filter.col === 'status') {
          adjustedFilter['col'] = 'status.id';
          adjustedFilter['mod'] = 'EQ';
        }

        if (filter.col === 'totalEntities') {
          adjustedFilter['col'] = 'totalEntities';
          adjustedFilter['mod'] = 'EQ';
        }

        // Removing unimportant keys
        delete adjustedFilter.id;
        delete adjustedFilter.value;

        return adjustedFilter;
      });

      if (view === 'request-manager') {
        const arrayPrograms = GETUSERPROFILE.permissions.memberOf.units
          .filter((unit) => unit.type === 'Program')
          .map((program) => program.id);

        const filterProgram = {
          col: 'idProgram',
          mod: 'IN',
          vals: arrayPrograms,
        };
        adjustedFilters.push(filterProgram);
      }
      if (view === 'genotyping-service-manager') {
        const arraySP = GETUSERPROFILE.service_providers.map((sp) => sp.id);
        const filterArrSP = {
          col: 'serviceprovider.id',
          mod: 'IN',
          vals: arraySP,
        };
        adjustedFilters.push(filterArrSP);
      }

      return adjustedFilters;
    }

    function checkEmptyValue(value) {
      if (!value)
        return (
          <span>
            <FormattedMessage id={'some.id'} defaultMessage={'(Not Set)'} />
          </span>
        );
      return value;
    }

    const fetch = async ({ page, sort, filters }) => {
      const id = GETUSERPROFILE.dbId;
      const adjustedFilters = adjustFilters(filters, id);
      const adjustedSort = getSortOrDefault(sort);

      return new Promise((resolve, reject) => {
        try {
          SMgraph.query({
            query: FIND_REQUEST_LIST,
            variables: {
              sort: adjustedSort,
              page: page,
              filters: [...adjustedFilters],
              disjunctionFilters: false,
            },
            fetchPolicy: 'no-cache',
          }).then(async ({ data, errors }) => {
            if (errors) {
              reject(errors);
            } else {
              const { content } = data.findRequestList;
              const squashedData = content.map((row) => {
                let traitCustom = '';
                row.traitCustom.forEach((item, index) => {
                  if (item?.name) {
                    traitCustom = traitCustom.concat(`(${index + 1}) `);
                    traitCustom = traitCustom.concat(item.name);
                    if (index !== row.traitCustom.length - 1)
                      traitCustom = traitCustom.concat('; ');
                  }
                });
                const markerGroup = row.markerPanel
                  ? row.markerPanel
                      .filter((item) => item)
                      .map((item, index) => `(${index + 1}) ${item.name}`)
                      .join('; ')
                  : '(Not Set)';

                const regex = new RegExp('failed', 'i');
                const isFileProcessError = regex.test(row.status.name);
                let messageFileProcessError = '';
                if (
                  isFileProcessError &&
                  row.requeststatuss &&
                  row.requeststatuss.length > 0
                ) {
                  const requeststatuss = row.requeststatuss.sort(
                    (a, b) => b.id - a.id,
                  );
                  messageFileProcessError = requeststatuss[0].comments;
                }

                return {
                  ...row,
                  markerGroup: markerGroup,
                  traitCustomString: traitCustom,
                  status: { ...row.status, messageFileProcessError },
                };
              });

              const requests = setupRequests(squashedData);

              const numberOfRequests = data.findRequestList.totalElements;

              if (view === 'genotyping-service-manager')
                setApprovedRequests(requests);
              const pagesBasedOnNumberOfrequests = Math.ceil(
                numberOfRequests / page.size,
              );

              dispatch({
                type: 'SET_REQUEST_LIST',
                payload: requests,
              });

              /**
               * This is to reset the Alert data so when the user goes back to the
               * Request Grid this will not reappear.
               */
              dispatch({
                type: 'ALERT_MESSAGE',
                payload: {
                  open: false,
                  message: null,
                  translationId: null,
                  severity: null,
                  strongMessage: null,
                },
              });

              const d = {
                pages: pagesBasedOnNumberOfrequests,
                elements: numberOfRequests,
                data: requests,
              };
              resolve(d);
            }
          });
        } catch (error) {
          const message = error?.response?.data?.metadata.status[0].messageType;
          if (message) {
            setMessage(message);
          } else {
            setMessage(
              `There was a problem retrieving the list of requests from the SM-API.`,
            );
          }
          setError(true);
          console.error(error);
        }
      });
    };

    return (
      <EbsGrid
        id='RequestListContent'
        columns={columns}
        toolbaractions={toolbarActions}
        rowactions={rowActions}
        fetch={fetch}
        height='85vh'
        raWidth={300}
        csvfilename='request-list'
        styleStatic={ebsGridFontStyles}
        ref={ref}
      />
    );
  },
);
RequestListContent.propTypes = {
  view: PropTypes.string.isRequired,
  toolbarActions: PropTypes.func.isRequired,
  rowActions: PropTypes.elementType,
  setError: PropTypes.func.isRequired,
  setMessage: PropTypes.func.isRequired,
  userProfile: PropTypes.object.isRequired,
};

export default RequestListContent;
