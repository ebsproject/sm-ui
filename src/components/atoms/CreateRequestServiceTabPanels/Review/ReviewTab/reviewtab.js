import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS

import { useSelector } from 'react-redux';

import {
  Divider,
  Grid,
  Table,
  TableCell,
  TableBody,
  TableRow,
  Typography,
  List,
  ListItem,
  ListItemText,
  Box,
} from '@mui/material';
import { FormattedMessage } from 'react-intl';

const styles = {
bold: {
  fontWeight: 600,
  fontSize: '13px',
},
inlineStyle: {
  display: 'inline-block',
},
labelStyle: {
  fontWeight: 600,
  marginRight: '30px',
},
mainLabel: {
  marginRight: '5px',
  fontWeight: 600,
  fontSize: '14px',
  color: 'palette.primary.dark',
},
mainValues: {
  fontSize: '14px',
},
notSetLabel: {
  color: 'palette.primary.dark',
  fontStyle: 'italic',
},
table1: {
  maxWidth: 500,
},
table2: {
  maxWidth: 500,
},
values: {
  fontSize: '13px',
},
tableItem: {
  textAlign: 'center',
  fontSize: '14px',
},
}

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const ReviewTabAtom = React.forwardRef((props, ref) => {
  const {
    mode,
    selectedEntryList,
    toEditRowData,
    requestCode,
    programName,
    cropName,
    basicTabData,
    serviceProviderData,
    ebsFormId,
    requester,
    requesterEmail,
    germplasmOwner,
    germplasmOwnerEmail,
    tissueTypeName,
    numberOfSamplesPerItem,
    markerPanelList,
  } = useSelector(({ RequestManager }) => RequestManager);

  const { serviceprovider, servicetype, purpose, service, selectedTraitsList } =
    serviceProviderData;

  const dateOptions = {
    weekday: 'long',
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  };

  const SERVICE_ORDER_FORM_VALUES = [
    {
      id: 'some.id',
      defaultMessage: 'Selected Service Type',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(servicetype?.label)
          : checkEmptyValue(toEditRowData?.servicetype.label),
    },
    {
      id: 'some.id',
      defaultMessage: 'Selected Service Laboratory',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(serviceprovider?.label)
          : checkEmptyValue(toEditRowData?.serviceprovider.label),
    },
    {
      id: 'some.id',
      defaultMessage: 'Selected Purpose',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(purpose?.label)
          : checkEmptyValue(toEditRowData?.purpose.label),
    },
  ];

  const CIMMYT_SERVICE_INFORMATION = [
    {
      id: 'some.id',
      defaultMessage: 'Program',
      value: checkEmptyValue(programName),
    },
    {
      id: 'some.id',
      defaultMessage: 'Crop',
      value: checkEmptyValue(cropName),
    },
    {
      id: 'some.id',
      defaultMessage: 'Requester',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(basicTabData?.requester)
          : checkEmptyValue(requester),
    },
    {
      id: 'some.id',
      defaultMessage: 'Requester Email',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(basicTabData?.requesterEmail)
          : checkEmptyValue(requesterEmail),
    },
    {
      id: 'some.id',
      defaultMessage: 'List Creator',
      value: checkEmptyValue(germplasmOwner),
    },
    {
      id: 'some.id',
      defaultMessage: 'List Creator Email',
      value: checkEmptyValue(germplasmOwnerEmail),
    },
    {
      id: 'some.id',
      defaultMessage: 'Tissue Type',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(tissueTypeName)
          : checkEmptyValue(tissueTypeName), // tissue prop DNE
    },
    {
      id: 'some.id',
      defaultMessage: 'Total Entities',
      value:
        mode === 'CREATE'
          ? Boolean(+numberOfSamplesPerItem)
            ? +numberOfSamplesPerItem * +selectedEntryList.memberCount
            : selectedEntryList.memberCount
          : toEditRowData.totalEntities,
    },
    {
      id: 'some.id',
      defaultMessage: 'Number of samples per item',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(numberOfSamplesPerItem)
          : checkEmptyValue(numberOfSamplesPerItem),
    },
    {
      id: 'some.id',
      defaultMessage: 'Team Work Contact',
      value: checkEmptyValue(
        basicTabData?.adminContact?.label || basicTabData?.adminContact?.personName,
      ),
    },
    {
      id: 'some.id',
      defaultMessage: 'Team Work Contact Email',
      value: checkEmptyValue(basicTabData?.adminContactEmail),
    },
    {
      id: 'some.id',
      defaultMessage: 'Account',
      value: checkEmptyValue(basicTabData?.account || basicTabData?.idICC),
    },

    {
      id: 'some.id',
      defaultMessage: 'Notes',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(basicTabData?.description)
          : checkEmptyValue(basicTabData?.idNotes || basicTabData?.description),
    },
  ];

  const IRRI_SERVICE_INFORMATION = [
    {
      id: 'some.id',
      defaultMessage: 'Researcher',
      value: checkEmptyValue(
        basicTabData?.adminContact?.label || basicTabData?.adminContact?.personName,
      ),
    },
    {
      id: 'some.id',
      defaultMessage: 'Requester',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(basicTabData?.requester)
          : checkEmptyValue(requester),
    },
    {
      id: 'some.id',
      defaultMessage: 'Requester Email',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(basicTabData?.requesterEmail)
          : checkEmptyValue(requesterEmail),
    },
    {
      id: 'some.id',
      defaultMessage: 'List Creator',
      value: checkEmptyValue(germplasmOwner),
    },
    {
      id: 'some.id',
      defaultMessage: 'List Creator Email',
      value: checkEmptyValue(germplasmOwnerEmail),
    },
    {
      id: 'some.id',
      defaultMessage: 'Program',
      value: checkEmptyValue(programName),
    },
    {
      id: 'some.id',
      defaultMessage: 'Crop',
      value: checkEmptyValue(cropName),
    },
    {
      id: 'some.id',
      defaultMessage: 'OCS Number',
      value: checkEmptyValue(basicTabData?.ocsNumber || basicTabData?.idOcsNumber),
    },

    {
      id: 'some.id',
      defaultMessage: 'Contact Person',
      value: checkEmptyValue(
        basicTabData?.contactPerson || basicTabData?.idContactPerson,
      ),
    },
    {
      id: 'some.id',
      defaultMessage: 'Tissue Type',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(tissueTypeName)
          : checkEmptyValue(tissueTypeName), // tissue prop DNE
    },
    {
      id: 'some.id',
      defaultMessage: 'Total Entities',
      value:
        mode === 'CREATE'
          ? Boolean(+numberOfSamplesPerItem)
            ? +numberOfSamplesPerItem * +selectedEntryList.memberCount
            : selectedEntryList.memberCount
          : toEditRowData.totalEntities,
    },
    {
      id: 'some.id',
      defaultMessage: 'Number of samples per item',
      value:
        mode === 'CREATE'
          ? checkEmptyValue(numberOfSamplesPerItem)
          : checkEmptyValue(numberOfSamplesPerItem),
    },
  ];

  function checkEmptyValue(value) {
    if (!value)
      return (
        <Box
        sx={styles.notSetLabel}
        component={'span'}
        >
          <FormattedMessage id={'some.id'} defaultMessage={'(Not Set)'} />
        </Box>
      );
    return value;
  }

  return (
    <Box id={`reviewTab.main`} ref={ref}>
      <Box sx={{ mt: 2 }}>
        <Typography>
          <FormattedMessage
            id={'some.id'}
            defaultMessage={
              'Please review the following information before submitting the request.'
            }
          />
        </Typography>
      </Box>
      <Divider />
      <Box sx={{ mt: 2 }}>
        <Typography>
          <Box
          sx={styles.mainLabel}
          component={'span'}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'Service Order:'} />
          </Box>
          <Box
          sx={styles.mainValues}
          component={'span'}
          >
            {mode === 'CREATE'
              ? checkEmptyValue(requestCode)
              : checkEmptyValue(selectedEntryList.requestCode)}
          </Box>
        </Typography>

        <Typography>
          <Box
          sx={styles.mainLabel}
          component={'span'}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'Selected List:'} />
          </Box>
          <Box sx={styles.mainValues} component={'span'}>{selectedEntryList.name}</Box>
        </Typography>
      </Box>
      <Grid container spacing={3} sx={{ mt: 2 }}>
        <Grid item xs={12} md={6}>
          <Typography>
            <Box sx={styles.mainLabel} component={'span'}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Selected Basic Information'}
              />
            </Box>
          </Typography>

          <Table size='small' sx={styles.table2}>
            {ebsFormId == 1 ? (
              <TableBody>
                {CIMMYT_SERVICE_INFORMATION.map((row) => (
                  <TableRow key={row.defaultMessage}>
                    <TableCell sx={styles.bold}>
                      <FormattedMessage
                        id={row.id}
                        defaultMessage={row.defaultMessage}
                      />
                    </TableCell>
                    <TableCell sx={styles.values}>{row.value}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            ) : (
              <TableBody>
                {IRRI_SERVICE_INFORMATION.map((row) => (
                  <TableRow key={row.defaultMessage}>
                    <TableCell sx={styles.bold}>
                      <FormattedMessage
                        id={row.id}
                        defaultMessage={row.defaultMessage}
                      />
                    </TableCell>
                    <TableCell sx={styles.values}>{row.value}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            )}
          </Table>
        </Grid>

        <Grid item xs={12} md={6}>
          <Typography>
            <Box sx={styles.mainLabel} component={'span'} >
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Selected Service Information'}
              />
            </Box>
          </Typography>

          <Table size='small' sx={styles.table1} >
            <TableBody>
              {SERVICE_ORDER_FORM_VALUES.map((row) => (
                <TableRow key={row.defaultMessage}>
                  <TableCell sx={styles.bold}>
                    <FormattedMessage
                      id={row.id}
                      defaultMessage={row.defaultMessage}
                    />
                  </TableCell>
                  <TableCell sx={styles.values}>{row.value}</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>

          <br />
          {markerPanelList?.length > 0 ? (
            <div>
              <Box sx={styles.mainLabel} component={'span'} >
                <FormattedMessage
                  id={'some.id'}
                  defaultMessage={'Selected Marker Group:'}
                />
              </Box>
              <List>
                {markerPanelList
                  .filter((markerPanel) => markerPanel.checked)
                  .map((markerPanel) => (
                    <ListItem key={`markerPanel_${markerPanel.id}`}>
                      <ListItemText>{markerPanel.name}</ListItemText>
                    </ListItem>
                  ))}
              </List>
            </div>
          ) : (
            <div>
              {selectedTraitsList?.length > 0 ? (
                <div>
                  <Typography>
                    <Box sx={styles.mainLabel} >
                      <FormattedMessage
                        id={'some.id'}
                        defaultMessage={'Selected Traits:'}
                      />
                    </Box>
                    <List>
                      {selectedTraitsList.map((value) => {
                        return (
                          <div
                            key={`review_tab_trait_list_${value.id}`}
                            style={{
                              display: 'flex',
                              justifyContent: 'flex-start',
                              paddingLeft: '11.5px',
                            }}
                          >
                            <ListItem key={value.id}>
                              <ListItemText
                                id={value.id}
                                primary={value.concatenatedname}
                              />
                            </ListItem>
                          </div>
                        );
                      })}
                    </List>
                  </Typography>
                </div>
              ) : (
                <div>
                  <Typography>
                    <Box sx={styles.mainLabel}  component={'span'}>
                      <FormattedMessage
                        id={'some.id'}
                        defaultMessage={'Selected Service:'}
                      />
                    </Box>
                    <Box sx={styles.mainValues} component={'span'} >
                      {mode === 'CREATE'
                        ? checkEmptyValue(service?.label)
                        : checkEmptyValue(toEditRowData?.service?.label)}
                    </Box>
                  </Typography>
                        
                  <Typography>
                    <Box sx={styles.mainLabel} component={'span'} >
                      <FormattedMessage
                        id={'some.id'}
                        defaultMessage={'(No available Marker Group options)'}
                      />
                    </Box>
                  </Typography>
                </div>
              )}
            </div>
          )}
        </Grid>
      </Grid>
    </Box>
  );
});
ReviewTabAtom.propTypes = {};
export default ReviewTabAtom;
