import React, { useState, useEffect } from 'react';
// import EbsGrid from 'ebs-grid-lib';
import { EbsGrid } from '@ebs/components';
import { RBAC } from '@ebs/layout';

// CORE COMPONENTS
import SelectListRow from 'components/atoms/SelectListRow';
import ViewEntryList from 'components/molecules/ViewEntryListButton';
import AlertNotification from 'components/atoms/AlertNotification';
import { Box, Stack, Typography } from '@mui/material';
import { B4Rrest } from 'utils/axios/B4Rclient';
import { FormattedMessage } from 'react-intl';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAllEntryByEntryListId, fetchEntryList } from 'utils/apollo/apis';
import StatusBadgeAtom from 'components/atoms/StatusBadge/statusbadge';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/

const columns = [
  {
    Header: 'Id',
    accessor: 'listDbId',
    hidden: true,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Abbrev'} />,
    accessor: 'abbrev',
    csvHeader: 'Abbreviation',
    width: 400,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Name'} />,
    accessor: 'name',
    csvHeader: 'Name',
    width: 400,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Display Name'} />,
    accessor: 'displayName',
    csvHeader: 'DisplayName',
    width: 200,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Type'} />,
    accessor: 'type',
    csvHeader: 'Type',
    width: 100,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Member Count'} />,
    accessor: 'memberCount',
    csvHeader: 'MemberCount',
    width: 200,
  },
  {
    Header: <FormattedMessage id={'some.id'} defaultMessage={'Description'} />,
    accessor: 'description',
    csvHeader: 'Description',
    width: 300,
  },
];

const EntryListTabAtom = React.forwardRef((props, ref) => {
  const { setError, setMessage, currentData } = props;

  const dispatch = useDispatch();

  const { mode, selectedEntryList } = useSelector(
    ({ RequestManager }) => RequestManager,
  );

  async function getAllLists(pageNum) {
    try {
      const res = B4Rrest.get(`/lists?limit=100&page=${pageNum}`);
      return res;
    } catch (error) {
      const message = error?.res?.data?.metadata.status[0].messageType;
      if (message) {
        setMessage(message);
      }
      setError(true);
      throw new Error(error);
    }
  }

  const fetch = async ({ page, sort, filters }) => {
    return new Promise((resolve, reject) => {
      try {
        fetchEntryList({
          page,
          sort,
          filters,
        }).then(async ({ data, errors }) => {
          if (errors) {
            reject(errors);
          } else {
            dispatch({
              type: 'ALERT_MESSAGE',
              payload: {
                open: false,
                message: null,
                translationId: null,
                severity: null,
                strongMessage: null,
              },
            });
            dispatch({
              type: 'SET_ENTRY_LIST_TAB_DATA',
              payload: data.findListList.content,
            });
            resolve({
              data: data.findListList.content,
              elements: data.findListList.totalElements,
              pages: data.findListList.totalPages,
            });
          }
        });
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        } else {
          setMessage(
            'There was a problem retrieving the findListList graphQL call from the SM-API.',
          );
        }
        setError(true);
        reject(error);
      }
    });
  };

  const rowActions = (rowData, refresh) => {
    return (
      <Stack direction={'row'} spacing={1}>
        {mode === 'CREATE' ? (
          <>
            <RBAC allowedAction={'Create'}>
              <Box>
                <SelectListRow rowData={rowData} />
              </Box>
              <Box>
                <ViewEntryList
                  setError={setError}
                  setMessage={setMessage}
                  rowData={rowData}
                />
              </Box>
            </RBAC>
          </>
        ) : (
          <>
            <Box>
              <RBAC allowedAction={'Modify'}>
                <SelectListRow rowData={rowData} />
              </RBAC>
            </Box>
            <Box>
              <RBAC allowedAction={'Modify'}>
                <ViewEntryList
                  setError={setError}
                  setMessage={setMessage}
                  rowData={rowData}
                />
              </RBAC>
            </Box>
          </>
        )}
      </Stack>
    );
  };

  const Detail = ({ rowData }) => {
    const columns = [
      {
        Header: 'Id',
        accessor: 'listDbId',
        hidden: true,
      },
      {
        Header: (
          <FormattedMessage id={'sm.req.tbl.hdr.status'} defaultMessage={'Status'} />
        ),
        accessor: 'status',
        Cell: (row) => {
          return <StatusBadgeAtom value={row.value.name} />;
        },
        csvHeader: 'Status',
        width: 150,
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Request'} />,
        accessor: 'requestCode',
        csvHeader: 'Request',
        width: 250,
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Requester'} />,
        accessor: 'user',
        csvHeader: 'Rquester',
        width: 250,
        Cell: (row) => {
          return `${row.value.contact.person.familyName} ${row.value.contact.person.givenName}`;
        },
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Program'} />,
        accessor: 'program',
        csvHeader: 'Program',
        width: 250,
        Cell: (row) => {
          return `${row.value.name}`;
        },
      },
      {
        Header: (
          <FormattedMessage id={'some.id'} defaultMessage={'Service Provider'} />
        ),
        accessor: 'serviceprovider',
        csvHeader: 'serviceProvider',
        width: 400,
        Cell: (row) => {
          return `${row.value.name}`;
        },
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Service Type'} />,
        accessor: 'servicetype',
        csvHeader: 'servicerType',
        width: 250,
        Cell: (row) => {
          return `${row.value.name}`;
        },
      },
      {
        Header: <FormattedMessage id={'some.id'} defaultMessage={'Purpose'} />,
        accessor: 'purpose',
        csvHeader: 'purpose',
        width: 250,
        Cell: (row) => {
          return `${row.value.name}`;
        },
      },
      {
        Header: (
          <FormattedMessage id={'some.id'} defaultMessage={'Total Entities'} />
        ),
        accessor: 'totalEntities',
        csvHeader: 'totalEntities',
        width: 100,
      },
    ];

    const fetch = async ({ page, sort, filters }) => {
      return new Promise((resolve, reject) => {
        fetchAllEntryByEntryListId({ page, sort, filters, listId: rowData.listDbId })
          .then(({ data }) => {
            resolve({
              data: data.findRequestList.content,
              elements: data.findRequestList.totalElements,
              pages: data.findRequestList.totalPages,
            });
          })
          .catch((error) => {
            reject(error);
          });
      });
    };
    return (
      <EbsGrid
        id='detailEntryList'
        columns={columns}
        fetch={fetch}
        toolbar={false}
        height='20vh'
      />
    );
  };

  return (
    /*
     @prop data-testid: Id to use inside entrylisttab.test.js file.
     */
    <Box ref={ref}>
      <AlertNotification />
      <Typography>
        <FormattedMessage
          id={'some.id'}
          defaultMessage={'This is the browser for plot and package lists.'}
        />
        <span>
          <FormattedMessage
            id={'some.id'}
            defaultMessage={
              mode === 'CREATE'
                ? '*Note: Please select a list down below first.'
                : '*Note: Cannot edit the selected Entry List.'
            }
          />
        </span>
      </Typography>
      <br />
      <EbsGrid
        id='EntryListTab'
        toolbar={true}
        columns={columns}
        rowactions={rowActions}
        fetch={fetch}
        csvfilename='entry-list'
        height='85vh'
        detailcomponent={Detail}
      />
    </Box>
  );
});
// Type and required properties
EntryListTabAtom.propTypes = {};

export default EntryListTabAtom;
