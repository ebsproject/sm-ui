import React from 'react';
import PropTypes from 'prop-types';

import GeneralForm from 'components/atoms/GeneralForm';
// CORE COMPONENTS

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const BasicTabAtom = React.forwardRef((props, ref) => {
  const { setError, setMessage, value, handleChange, onSubmitCimmyt, goBackPath } = props;

  return (
    /*
     @prop data-testid: Id to use inside basictab.test.js file.
     */
    <div ref={ref}>
      <GeneralForm
        setError={setError}
        setMessage={setMessage}
        value={value}
        onSubmitCimmyt={onSubmitCimmyt}
        handleChange={handleChange}
        goBackPath= {goBackPath}
      />
    </div>
  );
});
// Type and required properties
BasicTabAtom.propTypes = {};

export default BasicTabAtom;
