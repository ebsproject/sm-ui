import React from 'react';
import PropTypes from 'prop-types';

import ServiceProviderForm from 'components/atoms/ServiceProviderForm';

const ServiceProviderTabAtom = React.forwardRef(
  ({ setError, setMessage, value, handleChange, onSubmitCimmyt, crop, goBackPath }, ref) => {
    return (
      /*
     @prop data-testid: Id to use inside ServiceProviderTab.test.js file.
     */
      <div ref={ref}>
        <ServiceProviderForm
          setError={setError}
          setMessage={setMessage}
          value={value}
          handleChange={handleChange}
          onSubmitCimmyt={onSubmitCimmyt}
          crop={crop}
          goBackPath= {goBackPath}
        />
      </div>
    );
  },
);
// Type and required properties
ServiceProviderTabAtom.propTypes = {};
export default ServiceProviderTabAtom;
