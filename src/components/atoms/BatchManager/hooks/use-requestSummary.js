const useRequestSummary = ({
  batchDetails,
  checkedRequestSelection,
  mode,
  requests,
  totalSamplesInBatch,
}) => {
  const markersPanelListWithNoDuplicates = [];
  const traitsListWithNoDuplicates = [];

  if (checkedRequestSelection && checkedRequestSelection.length > 0) {
    checkedRequestSelection.forEach((item) => {
      if (item.service === null) {
        return;
      }
      if (item.markerPanel) {
        item.markerPanel
          .filter((markerPanel) => markerPanel)
          .forEach((markerPanel) => {
            if (!markersPanelListWithNoDuplicates.includes(markerPanel.name)) {
              markersPanelListWithNoDuplicates.push(markerPanel.name);
            }
          });
      }
      if (item?.traitCustom[0]?.name) {
        item.traitCustom
          .filter((trait) => trait)
          .forEach((trait) => {
            if (!markersPanelListWithNoDuplicates.includes(trait.name)) {
              traitsListWithNoDuplicates.push(trait.name);
            }
          });
      }
    });
  }

  if (requests && requests.length > 0) {
    requests.forEach((request) => {
      request.traitCustom.map((traitCustomIem) => {
        if (!traitsListWithNoDuplicates.includes(traitCustomIem.name))
          traitsListWithNoDuplicates.push(traitCustomIem.name);
      });
      if (request.markerPanel)
        request.markerPanel.forEach((markerPanel) => {
          if (!markersPanelListWithNoDuplicates.includes(markerPanel.name))
            markersPanelListWithNoDuplicates.push(markerPanel.name);
        });
    });
  }

  return {
    requestLength: checkedRequestSelection ? checkedRequestSelection.length : 0,
    samples:
      mode === 'CREATE'
        ? totalSamplesInBatch
          ? totalSamplesInBatch
          : ''
        : batchDetails
          ? batchDetails.numMembers
          : '',
    markerPanels: markersPanelListWithNoDuplicates,
    serviceType: mode === 'CREATE' ? 'Genotyping' : 'Genotyping',
    purpose:
      mode === 'CREATE'
        ? checkedRequestSelection
          ? checkedRequestSelection[0].purpose.name
          : ''
        : requests
          ? requests[0].purpose.name
          : '',
    traits: traitsListWithNoDuplicates,
  };
};

export default useRequestSummary;
