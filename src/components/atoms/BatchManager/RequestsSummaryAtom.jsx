import React from 'react';
import PropTypes from 'prop-types';
import { forwardRef } from 'react';
import { Grid } from '@mui/material';
import useRequestSummary from './hooks/use-requestSummary';

const RequestsSummaryAtom = forwardRef(
  (
    { batchDetails, checkedRequestSelection, requests, mode, totalSamplesInBatch },
    ref,
  ) => {
    const { requestLength, samples, markerPanels, serviceType, purpose, traits } =
      useRequestSummary({
        batchDetails,
        checkedRequestSelection,
        mode,
        requests,
        totalSamplesInBatch,
      });

    return (
      <Grid
        data-testid={'RequestsSummaryAtomTestId'}
        ref={ref}
        container
        style={{ background: '#F5F5F5', padding: '3px' }}
      >
        <Grid item xs={12}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={3}>
              <span style={{ fontWeight: 500 }}>Requests:</span>
              &nbsp; {requestLength}
            </Grid>
            <Grid item xs={12} sm={4}>
              <span style={{ fontWeight: 500 }}>Samples:</span>
              &nbsp; {samples}
            </Grid>
            <Grid item xs={12} sm={4}>
              <span style={{ fontWeight: 500 }}>Marker Group(s):</span>
              <div style={{ height: '50px', overflowY: 'auto', marginTop: '2px' }}>
                {markerPanels && markerPanels.length > 0 ? (
                  <ul style={{ paddingLeft: '10px' }}>
                    {markerPanels.map((item, index) => {
                      return <li key={`${item}_request_summary_li`}>• {item}</li>;
                    })}
                  </ul>
                ) : (
                  <span style={{ fontStyle: 'italic' }}>None</span>
                )}
              </div>
            </Grid>
          </Grid>
        </Grid>

        <Grid item xs={12}>
          <Grid container spacing={1}>
            <Grid item xs={12} sm={3}>
              <span style={{ fontWeight: 500 }}>Service Type:</span>
              &nbsp; {serviceType}
            </Grid>
            <Grid item xs={12} sm={4}>
              <span style={{ fontWeight: 500 }}>Purpose:</span>
              &nbsp; {purpose}
            </Grid>
            <Grid item xs={12} sm={4}>
              <span style={{ fontWeight: 500 }}>Traits(s):</span>
              <div style={{ height: '70px', overflowY: 'auto', marginTop: '2px' }}>
                {traits?.length > 0 ? (
                  <ul style={{ paddingLeft: '10px' }}>
                    {traits.map((item) => {
                      return <li key={`trait_id_${item}`}>• {item}</li>;
                    })}
                  </ul>
                ) : (
                  <span style={{ fontStyle: 'italic' }}>None</span>
                )}
              </div>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  },
);

RequestsSummaryAtom.propTypes = {};

export default RequestsSummaryAtom;
