import React, { useState } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Checkbox, FormControl } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { SET_CHECKED_REQUEST_SELECTION } from 'stores/modules/RequestBatch';
import { ALERT_MESSAGE } from 'stores/modules/General';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const SelectRequestRowAtom = React.forwardRef(({ rowData }, ref) => {
  const dispatch = useDispatch();
  const { checkedRequestSelection } = useSelector(
    ({ request_batch }) => request_batch,
  );
  const [checked, setChecked] = useState(false);
  const [disabled, setDisabled] = useState(false);

  const validateCheck = () => {
    const { totalEntities } = rowData;
    const subtotalEntities =
      checkedRequestSelection && checkedRequestSelection.length > 0
        ? checkedRequestSelection
            .map((request) => request.totalEntities)
            .reduce((accumulator, current) => accumulator + current)
        : 0;
    if (totalEntities > 50000 || subtotalEntities + totalEntities > 50000) {
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: `You will only be able to select requests with a total of "50,000" or less entities.`,
          translationId: 'some.id',
          severity: 'warning',
        },
      });
      return false;
    }
    switch (rowData.status.id) {
      case '2':
        break;
      default:
        dispatch({
          type: ALERT_MESSAGE,
          payload: {
            message: `You can only select request with "Approved" status to Create Batch`,
            translationId: 'some.id',
            severity: 'warning',
          },
        });
        return false;
    }
    return true;
  };

  const handleChange = (event) => {
    if (event.target.checked) {
      if (validateCheck()) {
        setChecked(event.target.checked);
        dispatch({
          type: SET_CHECKED_REQUEST_SELECTION,
          payload: [...checkedRequestSelection, rowData],
        });
      }
    } else {
      setChecked(event.target.checked);
      dispatch({
        type: SET_CHECKED_REQUEST_SELECTION,
        payload: [
          ...checkedRequestSelection.filter((request) => request.id !== rowData.id),
        ],
      });
    }
  };

  return (
    <FormControl>
      <Checkbox
        checked={checked}
        onChange={handleChange}
        color='primary'
        ref={ref}
        disabled={disabled}
      />
    </FormControl>
  );
});
// Type and required properties
SelectRequestRowAtom.propTypes = {};

export default SelectRequestRowAtom;
