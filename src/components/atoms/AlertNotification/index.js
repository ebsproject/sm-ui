import { connect } from "react-redux";
import AlertNotification from "./alertnotification";
/*
Here you send the status that the container will handle.
*/
export default connect(
  (state) => ({
    open: state.general.open,
    message: state.general.message,
    strongMessage: state.general.strongMessage,
    severity: state.general.severity,
    translationId: state.general.translationId,
  })
  //{ Funcion },
)(AlertNotification);
