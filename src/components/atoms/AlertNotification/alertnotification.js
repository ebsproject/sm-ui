import React from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Snackbar, Slide, Alert, AlertTitle } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import CloseIcon from '@mui/icons-material/Close';
import { useDispatch } from 'react-redux';
import { Core } from '@ebs/styleguide';
const { Typography, IconButton } = Core;

const CustomAlert = React.forwardRef((props, ref) => {
  return <Alert ref={ref} elevation={6} variant='filled' {...props} />;
});

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const AlertNotificationAtom = React.forwardRef((props, ref) => {
  const { open, severity, message, strongMessage, translationId, ...rest } = props;
  const dispatch = useDispatch();

  const Transition = React.forwardRef((props, ref) => (
    <Slide direction='down' ref={ref} {...props} />
  ));

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    dispatch({
      type: 'CLOSE_ALERT',
    });
  };

  if (severity && message) {
    return (
      /*
     @prop data-testid: Id to use inside alertnotification.test.js file.
     */
      <div data-testid={'AlertNotificationTestId'} ref={ref}>
        <Snackbar
          open={open}
          autoHideDuration={3000}
          onClose={handleClose}
          TransitionComponent={Transition}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
          action={
            <IconButton
              aria-label='close'
              color='inherit'
              size='small'
              onClick={handleClose}
            >
              <CloseIcon fontSize='inherit' />
            </IconButton>
          }
        >
          <CustomAlert onClose={handleClose} severity={severity}>
            <AlertTitle>
              <Typography variant='body1'>
                <FormattedMessage
                  id={`core.alert.${severity}`}
                  defaultMessage={severity.toUpperCase()}
                />
              </Typography>
            </AlertTitle>
            <FormattedMessage id={translationId} defaultMessage={message} />
            {strongMessage && (
              <strong>
                —{' '}
                <FormattedMessage
                  id={`${translationId}.strong`}
                  defaultMessage={strongMessage}
                />
              </strong>
            )}
          </CustomAlert>
        </Snackbar>
      </div>
    );
  } else {
    return <></>;
  }
});
// Type and required properties
AlertNotificationAtom.propTypes = {
  severity: PropTypes.string,
  message: PropTypes.string,
  strongMessage: PropTypes.string,
};

export default AlertNotificationAtom;
