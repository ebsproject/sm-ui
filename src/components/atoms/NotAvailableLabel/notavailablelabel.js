import React from 'react';
// CORE COMPONENTS
import { FormattedMessage } from 'react-intl';

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
// TODO: to delete
const NotAvailableLabelAtom = React.forwardRef((props, ref) => {
  return (
    /*
     @prop data-testid: Id to use inside notavailablelabel.test.js file.
     */
    <span>
      <FormattedMessage id={'some.id'} defaultMessage={'(Not Available)'} />
    </span>
  );
});
// Type and required properties
NotAvailableLabelAtom.propTypes = {};

export default NotAvailableLabelAtom;
