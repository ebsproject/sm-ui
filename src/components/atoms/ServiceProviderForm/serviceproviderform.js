import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
// CORE COMPONENTS
import { Grid, DialogActions, Box } from '@mui/material';
import {
  PRIMARY_DENSE_BUTTON_CLASS,
  WHITE_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import { EbsForm } from '@ebs/components';
import { FormattedMessage } from 'react-intl';
import { SMgraph } from 'utils/apollo/SMclient';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { GETUSERPROFILE } from 'utils/config';
import {
  FIND_SERVICE_TYPE_LIST,
  FIND_SERVICE_TYPE_BY_SERVICE_PROVIDER_ID,
  FIND_SERVICE_PROVIDER_LIST_BY_SERVICE_TYPE_ID,
  FIND_PURPOSES_BY_SERVICE_TYPE_ID,
  FIND_SERVICE_BY_PURPOSE_ID,
} from 'utils/apollo/gql/RequestManager';
import { goToRequestManagerGrid } from 'helpers/requestManagerUtils';
import MarkerPanelsTable from 'components/molecules/MarkerPanelsTable';
import { Core } from '@ebs/styleguide';
import { SET_TO_EDIT_ROW_DATA } from 'stores/modules/RequestManager';
const { Button, Typography } = Core;

const ServiceProviderFormAtom = React.forwardRef(
  ({ setError, setMessage, handleChange, value, crop, goBackPath }, ref) => {
    const navigate = useNavigate();

    const dispatch = useDispatch();
    const { serviceProviderData, mode, toEditRowData, serviceProviderId } =
      useSelector(({ RequestManager }) => RequestManager);

    const [serviceTypes, setServiceTypes] = useState([]);
    const [serviceProviders, setServiceProviders] = useState([]);
    const [userProfileServiceProviders, setUserProfileServiceProviders] = useState(
      [],
    );
    const [purposes, setPurposes] = useState([]);
    const [services, setServices] = useState([]);
    const [selectedService, setSelectedService] = useState([]);
    const [selectedTraitCategory, setSelectedTraitCategory] = useState(null);
    const [selectedTrait, setSelectedTrait] = useState(null);
    const [selectedTraitsList, setSelectedTraitsList] = useState([]);
    const [selectedPurpose, setSelectedPurpose] = useState(null);
    const [defaultService, setDefaultService] = useState(null);
    const [serviceTypeDisabled, setServiceTypeDisabled] = useState(false);
    const [serviceProviderDisabled, setServiceProviderDisabled] = useState(true);
    const [purposesDisabled, setPurposesDisabled] = useState(true);
    const [serviceDisabled, setServiceDisabled] = useState(true);

    const [nextDisabled, setNextDisabled] = useState(
      !Boolean(Object.keys(serviceProviderData).length),
    );

    const brandColor = '#808080';

    const customStyles = {
      control: (base, state) => ({
        ...base,
        boxShadow: state.isFocused ? 0 : 0,
        borderColor: state.isFocused ? brandColor : base.borderColor,
        color: state.isDisabled ? base.borderColor : brandColor,
        '&:hover': {
          borderColor: state.isFocused ? brandColor : base.borderColor,
        },
        '&:disabled': {
          color: brandColor,
        },
      }),
    };

    useEffect(() => {
      const callEffect = async () => {
        let userProfile = [];
        if (GETUSERPROFILE.service_providers.length > 0) {
          if (mode === 'EDIT') {
            //if editing data autopopulate
            let serviceTypeId = 0;
            if (toEditRowData?.servicetype?.id) {
              serviceTypeId = toEditRowData.servicetype.id;
            } else if (toEditRowData?.servicetype?.value) {
              serviceTypeId = toEditRowData.servicetype.value;
            }
            if (serviceTypeId > 0) {
              const purposesByServiceType =
                await getPurposesByServiceTypeId(serviceTypeId);
              if (purposesByServiceType?.length > 0) {
                setPurposes(purposesByServiceType);
                setServiceDisabled(false);
              }
            }
          } else if (serviceProviderId != null) {
            let selected = GETUSERPROFILE.service_providers.find(
              (o) => o.id === serviceProviderId,
            );
            userProfile.push(selected);
          } else userProfile = GETUSERPROFILE.service_providers;
          setUserProfileServiceProviders(userProfile);
          const serviceLabs = userProfile.map((sl) => {
            return {
              value: sl.id,
              label: sl.name,
            };
          });
          setServiceProviders(serviceLabs);
          setServiceProviderDisabled(false);
          if (userProfile.length > 1 && !serviceProviderData?.serviceprovider?.value)
            setServiceTypeDisabled(true);
        } else if (!serviceTypes.length) {
          //only pupulate service types if there is no CS user profile provided service provider
          const serviceTypeList = await getServiceTypeList();
          setServiceTypes(serviceTypeList);
          return;
        }

        if (toEditRowData?.purpose) {
          let purpose = {
            value: toEditRowData.purpose.id || toEditRowData.purpose.value,
            label: toEditRowData.purpose.name || toEditRowData.purpose.label,
          };

          setSelectedPurpose(purpose);
        }
      };
      callEffect();
    }, []);

    // Use effect populate purpose on load
    useEffect(() => {
      const fetch = async () => {
        const serviceTypeId = toEditRowData?.servicetype?.id
          ? toEditRowData.servicetype.id
          : toEditRowData?.servicetype?.value
            ? toEditRowData.servicetype.value
            : 0;
        if (purposes.length === 0 && serviceTypeId !== 0) {
          const resultPurposes = await getPurposesByServiceTypeId(serviceTypeId);
          if (resultPurposes?.length > 0) {
            setPurposes(resultPurposes);
          }
        }
      };
      fetch().catch((error) => console.error('Error to get purpose -> ', error));
      return () => {};
    }, [serviceProviders]);

    // Use effect to populate service type
    // TODO: convert on api function to use this function and remove on other component to only have one call to api
    useEffect(() => {
      const fetch = async () => {
        if (serviceProviders.length === 0 && serviceProviderId) {
          const response = await SMgraph.query({
            query: FIND_SERVICE_TYPE_BY_SERVICE_PROVIDER_ID,
            variables: {
              id: serviceProviderId,
            },
            fetchPolicy: 'no-cache',
          });
          if (response.data.findServiceProvider.servicetypes.length) {
            const adjustedServiceTypes =
              response.data.findServiceProvider.servicetypes.map((servicetype) => {
                return {
                  value: servicetype.id,
                  label: servicetype.name,
                };
              });
            setServiceTypes(adjustedServiceTypes);
          }
        }
      };
      fetch().catch((error) =>
        console.error('Error to get service types -> ', error),
      );
      return () => {};
    }, [serviceProviderId]);

    async function getServiceTypeList() {
      try {
        const response = await SMgraph.query({
          query: FIND_SERVICE_TYPE_LIST,
          fetchPolicy: 'no-cache',
        });

        const { content } = response.data.findServiceTypeList;
        if (content.length) {
          const serviceTypeList = content
            .map((servicetype) => {
              return {
                value: servicetype.id,
                label: servicetype.name,
              };
            })
            .sort((a, b) => (a.label > b.label ? 1 : b.label > a.label ? -1 : 0));
          return serviceTypeList;
        }
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        } else {
          setMessage(
            `There was a problem retrieving the list of serviceTypes from the SM-API.`,
          );
        }
        setError(true);
        throw new Error(error);
      }
    }

    async function getPurposesByServiceTypeId(serviceTypeId) {
      try {
        const response = await SMgraph.query({
          query: FIND_PURPOSES_BY_SERVICE_TYPE_ID,
          variables: {
            filters: [{ col: 'servicetype.id', val: serviceTypeId, mod: 'EQ' }],
            sort: { col: 'name', mod: 'ASC' },
          },
          fetchPolicy: 'no-cache',
        });
        const purposes = response.data.findPurposeList.content;
        if (purposes.length) {
          const adjustedPurposes = purposes.map((purpose) => ({
            value: purpose.id,
            label: purpose.name,
          }));
          setPurposes(adjustedPurposes);
          return adjustedPurposes;
        }
        return [];
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        } else {
          setMessage(
            `There was a problem retrieving the list of purposes by servicetype ID from the SM-API.`,
          );
        }
        setError(true);
        throw new Error(error);
      }
    }

    const updateServiceTypesByValue = async (getValues, setValue) => {
      const item = getValues('serviceprovider');

      updateServiceTypes(item, setValue);
    };

    const updateServiceTypes = async (item, setValue) => {
      // Clear values of the rest since they're all related

      setServiceTypeDisabled(false);
      if (mode === 'CREATE') setPurposesDisabled(true);

      if (!userProfileServiceProviders.length) {
        //if userProfiles are empty, disable everything
        setPurposesDisabled(true);
        setServiceDisabled(true);
      }
      dispatch({
        type: 'SET_SERVICE_PROVIDER_DATA',
        payload: { purpose: null, service: null },
      });
      setValue('purpose', null);
      setValue('servicetype', null);
      setDefaultService(null);

      try {
        const response = await SMgraph.query({
          query: FIND_SERVICE_TYPE_BY_SERVICE_PROVIDER_ID,
          variables: {
            id: item.value, //service provider id
          },
          fetchPolicy: 'no-cache',
        });

        const { servicetypes } = response.data.findServiceProvider;

        if (servicetypes.length) {
          //if there are service types
          const adjustedServiceTypes = servicetypes.map((servicetype) => {
            return {
              value: servicetype.id,
              label: servicetype.name,
            };
          });
          setServiceTypes(adjustedServiceTypes);

          // Auto Select of Service Type dropdown if the list is of length 1.
          if (adjustedServiceTypes.length === 1) {
            setValue('servicetype', adjustedServiceTypes[0]);
            dispatch({
              type: 'SET_SERVICE_PROVIDER_DATA',
              payload: {
                ...serviceProviderData,
                servicetype: adjustedServiceTypes[0],
              },
            });
            // Call this because the auto-selection will not trigger the onChange()
            updatePurposesByServiceType(adjustedServiceTypes[0], setValue);
          } else setValue('servicetype', null);
        } else setServiceProviders([]);
      } catch (error) {
        throw new Error(error);
      }
    };

    const updateServiceProviders = async (item, setValue) => {
      dispatch({
        type: 'SET_SERVICE_PROVIDER_DATA',
        payload: {
          ...serviceProviderData,
          servicetype: item,
          serviceprovider: null,
          purpose: null,
          service: null,
        },
      });

      setValue('serviceprovider', null);
      setValue('purpose', null);
      setDefaultService(null);

      // TODO: disable logic here
      setServiceProviderDisabled(false);

      // Find Service Provider List under the selected Service Type
      try {
        const response = await SMgraph.query({
          query: FIND_SERVICE_PROVIDER_LIST_BY_SERVICE_TYPE_ID,
          variables: {
            id: item.value,
          },
          fetchPolicy: 'no-cache',
        });

        const { serviceproviders } = response.data.findServiceType;
        if (serviceproviders.length) {
          const adjustedServiceProviders = serviceproviders.map(
            (serviceprovider) => {
              return {
                value: serviceprovider.id,
                label: serviceprovider.name,
              };
            },
          );

          setServiceProviders(adjustedServiceProviders);

          // Auto Select of Service Provider dropdown if the list is of length 1.
          if (adjustedServiceProviders.length === 1) {
            setValue('serviceprovider', adjustedServiceProviders[0]);

            // Call this because the auto-selection will not trigger the onChange()
            updatePurposesBySP(adjustedServiceProviders[0], setValue);
          } else setValue('serviceprovider', []);
        } else setServiceProviders([]);
      } catch (error) {
        throw new Error(error);
      }
    };

    const setServiceProvidersValue = async (item, setValue) => {
      setValue('serviceprovider', serviceProviders[0]);
    };

    const updateServiceProvidersByValue = async (getValues, setValue) => {
      const item = getValues('servicetype');

      updateServiceProviders(item, setValue);
    };

    const resetServiceByValue = async (getValues, setValue) => {
      const item = getValues('purpose');
      resetService(item, setValue);
    };

    const getMarkerPanelsByPurpose = (getValues) => {
      const purpose = getValues('purpose');
      setSelectedPurpose(purpose);
      dispatch({
        type: 'SET_MARKER_PANEL_LIST',
        payload: [],
      });
    };

    // Find Purpose List under the selected Service Type
    const updatePurposesByValue = async (getValues, setValue) => {
      const item = getValues('serviceprovider');
      updatePurposesByServiceType(item.servicetype, setValue);
    };

    // Find Purpose List under the selected Service Type
    const updatePurposesBySP = async (item, setValue) => {
      dispatch({
        type: 'SET_SERVICE_PROVIDER_DATA',
        payload: { ...serviceProviderData, serviceprovider: item },
      });

      updatePurposesByServiceType(item.servicetype, setValue);
    };

    const updatePurposesByServiceType = async (item, setValue) => {
      // Clear values of the rest since they're all related

      setPurposesDisabled(false);

      //get the purposes associated to the selected servicetype
      if (item?.value) {
        const adjustedPurposes = await getPurposesByServiceTypeId(item.value);

        if (adjustedPurposes.length) {
          const adjustedPurposes = purposes
            .map((purpose) => {
              return {
                value: purpose.id,
                label: purpose.name,
              };
            })
            .sort((a, b) => (a.label > b.label ? 1 : b.label > a.label ? -1 : 0));

          // Auto Select of Purposes dropdown if the list is of length 1.
          if (adjustedPurposes.length === 1) {
            setValue('purpose', adjustedPurposes[0]);
            // Call this because the auto-selection will not trigger the onChange()
            resetService(adjustedPurposes[0], setValue);
            dispatch({
              type: 'SET_SERVICE_PROVIDER_DATA',
              payload: { serviceProviderData, service: null },
            });

            setDefaultService([]);
            setSelectedPurpose(adjustedPurposes[0]);
          } else if (
            serviceProviderData?.purpose?.value &&
            item.value === serviceProviderData?.serviceprovider?.value
          ) {
            setValue('purpose', serviceProviderData?.purpose);
            setSelectedPurpose(serviceProviderData?.purpose);
            updateServices(serviceProviderData?.purpose, setValue);
          } else {
            dispatch({
              type: 'SET_SERVICE_PROVIDER_DATA',
              payload: { ...serviceProviderData, service: null },
            });
            setDefaultService([]);
          }
        } else setPurposes([]);
      } else setPurposes([]);
    };

    // Find Service List under the selected Purpose
    const updateServices = async (item, setValue) => {
      dispatch({
        type: 'SET_SERVICE_PROVIDER_DATA',
        payload: { ...serviceProviderData, purpose: item },
      });
      setServiceDisabled(false);
      /**
       * Remove disable attr of NEXT button since only Service Type, Service
       * Provider, and Purpose is required for Services tab.
       */
      setNextDisabled(false);

      const response = await SMgraph.query({
        query: FIND_SERVICE_BY_PURPOSE_ID,
        variables: {
          id: item.value,
        },
        fetchPolicy: 'no-cache',
      });

      const { services } = response.data.findPurpose;
      if (services.length) {
        const adjustedServices = services
          .map((service) => {
            return {
              value: service.id,
              label: service.name,
            };
          })
          .sort((a, b) => (a.label > b.label ? 1 : b.label > a.label ? -1 : 0));

        setServices(adjustedServices);

        if (adjustedServices.length === 1) {
          setDefaultService(adjustedServices[0]);
        }
      } else if (
        serviceProviderData?.service?.value &&
        item.value === serviceProviderData?.purpose?.value
      ) {
        setDefaultService(serviceProviderData?.service);
      } else {
        setServices([]);
        setDefaultService([]);
        dispatch({
          type: 'SET_SERVICE_PROVIDER_DATA',
          payload: { ...serviceProviderData, service: null },
        });
      }
    };

    function resetService(item, setValue) {
      setDefaultService([]);
      dispatch({
        type: 'SET_SERVICE_PROVIDER_DATA',
        payload: { ...serviceProviderData, service: null },
      });
      updateServices(item, setValue);
    }

    async function onSubmitSP(
      formData,
      selectedService,
      selectedTrait,
      selectedTraitCategory,
    ) {
      const formSave = { ...formData };
      if (selectedService?.value) {
        formSave.service = selectedService;
      } else if (serviceProviderData?.service) {
        formSave.service = serviceProviderData?.service;
      }

      if (selectedTraitCategory?.value) {
        formSave.traitcategory = selectedTraitCategory;
      }

      if (selectedTrait?.value) {
        formSave.trait = selectedTrait;
      }

      if (selectedTraitsList.length > 0) {
        formSave.selectedTraitsList = selectedTraitsList;
      }

      dispatch({
        type: 'SET_SERVICE_PROVIDER_DATA',
        payload: formSave,
      });

      const { serviceprovider, service, servicetype, purpose } = formSave;
      /**
       * This is for updating the service provider data whenever the user
       * selected new ones. Then this will update the whole edit data
       * for the review tab and for whenever the user will go back
       * to the Service tab. But all will only be persisted if
       * the user submits the Edit request.
       */
      const updatedToEditRowData = { ...toEditRowData };

      updatedToEditRowData['serviceprovider'] = serviceprovider;
      updatedToEditRowData['service'] = service;
      updatedToEditRowData['servicetype'] = servicetype;
      updatedToEditRowData['purpose'] = purpose;

      dispatch({
        type: SET_TO_EDIT_ROW_DATA,
        payload: updatedToEditRowData,
      });

      handleChange(null, value + 1, { purpose });
    }

    async function backReset() {
      handleChange(null, value - 1);
    }

    const formComponents = [
      {
        sizes: [12, 12, 12, 12, 12],
        component: 'Select',
        name: 'servicetype',
        options: serviceTypes,
        inputProps: {
          label: 'Service Type',
          styles: customStyles,
          disabled: serviceTypeDisabled,
        },
        defaultValue:
          mode === 'CREATE'
            ? (serviceProviderData?.servicetype ?? { label: '', value: null })
            : toEditRowData?.servicetype
              ? {
                  value:
                    toEditRowData?.servicetype?.id ||
                    toEditRowData?.servicetype?.value,
                  label:
                    toEditRowData?.servicetype?.name ||
                    toEditRowData?.servicetype?.label,
                }
              : {
                  value: '',
                  label: '',
                },
        helper: {
          title:
            'Type of services, such as Genotyping (G), Phytosanitary (P), seed quality (G), pathology including MLN screening',
          placement: 'right',
          arrow: true,
        },
        rules: { required: 'Service Type is required. Please select one.' },
      },
      {
        sizes: [12, 12, 12, 12, 12],
        component: 'Select',
        name: 'serviceprovider',
        options: serviceProviders,
        inputProps: {
          label: 'Service Laboratory',
          disabled: serviceProviderDisabled,
          styles: customStyles,
        },
        defaultValue:
          mode === 'CREATE'
            ? serviceProviderData?.serviceprovider
            : {
                value:
                  toEditRowData.serviceprovider.id ||
                  toEditRowData.serviceprovider.value,
                label:
                  toEditRowData.serviceprovider.name ||
                  toEditRowData.serviceprovider.label,
              },

        helper: {
          title:
            "Internal service unit that triage, and/or process clients' service requests.",
          placement: 'right',
          arrow: true,
        },
        rules: {
          required: 'Service Laboratory is required. Please select one.',
        },
      },
      {
        sizes: [12, 12, 12, 12, 12],
        component: 'Select',
        name: 'purpose',
        options: purposes,
        inputProps: {
          label: 'Purpose',
          disabled: mode === 'CREATE' ? purposesDisabled : false,
          styles: customStyles,
        },
        defaultValue:
          mode === 'CREATE'
            ? serviceProviderData?.purpose
            : {
                value: toEditRowData.purpose.id || toEditRowData.purpose.value,
                label: toEditRowData.purpose.name || toEditRowData.purpose.label,
              },
        helper: {
          title:
            'High level business needs that the service to address. For genotyping, values include MAS, QC, GS/GWAS, germplasm characterization',
          placement: 'right',
          arrow: true,
        },
        rules: { required: 'Purpose is required. Please select one.' },
      },
    ];

    const serviceProviderDefinition = (props) => {
      const { getValues, setValue } = props;

      if (typeof setValue === 'function') {
        formComponents.forEach((fc) => {
          for (let key in fc) {
            if (fc[key] === 'serviceprovider') {
              //changes dynamic display based on length of service provider
              if (userProfileServiceProviders.length === 1) {
                //if there's only one
                if (!serviceProviderData.serviceprovider) {
                  //if there's no service provider previously selected
                  setServiceProvidersValue(serviceProviders[0], setValue); // set the service provider value
                  if (!serviceTypes?.length)
                    updateServiceTypes(serviceProviders[0], setValue); //since service provider's value changed, update service types, make sure it updates only once.
                } else if (purposesDisabled) {
                  updatePurposesBySP(serviceProviderData.serviceprovider, setValue);
                }
              } else if (userProfileServiceProviders.length > 1) {
                fc['onChange'] = () =>
                  updateServiceTypesByValue(getValues, setValue);
              } else {
                fc['onChange'] = () => updatePurposesByValue(getValues, setValue);
              }
            } else if (fc[key] === 'purpose') {
              fc['onChange'] = () => {
                //trigger null reset on the markerPanel on the ONCHANGE only. not on the reset.
                dispatch({
                  type: 'SET_MARKER_PANEL',
                  payload: null,
                });
                getMarkerPanelsByPurpose(getValues);
                resetServiceByValue(getValues, setValue);
              };
            } else if (fc[key] === 'servicetype') {
              if (!userProfileServiceProviders.length) {
                fc['onChange'] = () =>
                  updateServiceProvidersByValue(getValues, setValue);
              } else {
                fc['onChange'] = () =>
                  updatePurposesByServiceType(getValues('servicetype', setValue));
              }
            }
          }
        });
      }
      return {
        name: 'serviceProvider',
        components: formComponents,
      };
    };

    return (
      <Box
        id={`serviceProviderForm.testId`}
        data-testid={'serviceProviderForm.testId'}
        ref={ref}
      >
        <Box sx={{ mt: 2 }}>
          <Typography variant='body2'>
            <span>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Please provide all of the information below.'}
              />
            </span>
            <span>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={
                  'Note: Disabled fields will be enabled and can be modified by selecting a service type or service laboratory first. '
                }
              />
            </span>
          </Typography>
        </Box>
        <Grid sx={{ mt: 2, minWidth: '100%' }} container>
          <Grid item xs={12} sm={10}>
            <EbsForm
              definition={serviceProviderDefinition}
              onSubmit={(data) =>
                onSubmitSP(
                  data,
                  selectedService,
                  selectedTrait,
                  selectedTraitCategory,
                )
              }
            >
              <DialogActions>
                <Button onClick={() => goBackPath()} className={WHITE_BUTTON_CLASS}>
                  <Typography variant='body1'>
                    <FormattedMessage id={'some.id'} defaultMessage={'CANCEL'} />
                  </Typography>
                </Button>
                <Button className={PRIMARY_DENSE_BUTTON_CLASS} onClick={backReset}>
                  <Typography variant='body1'>
                    <FormattedMessage id={'some.id'} defaultMessage={'BACK'} />
                  </Typography>
                </Button>
                <Button
                  disabled={nextDisabled}
                  type='submit'
                  className={PRIMARY_DENSE_BUTTON_CLASS}
                >
                  <Typography variant='body1'>
                    <FormattedMessage id={'some.id'} defaultMessage={'NEXT'} />
                  </Typography>
                </Button>
              </DialogActions>
            </EbsForm>
          </Grid>

          <Grid item xs={12} sm={8} sx={{ mT: 3 }}>
            {selectedPurpose ? (
              <MarkerPanelsTable
                selectedPurpose={selectedPurpose}
                defaultService={defaultService}
                setDefaultService={setDefaultService}
                setSelectedTraitCategory={setSelectedTraitCategory}
                setSelectedTraitsList={setSelectedTraitsList}
                setSelectedService={setSelectedService}
                services={services}
                serviceDisabled={serviceDisabled}
                selectedTraitCategory={selectedTraitCategory}
                crop={crop}
              />
            ) : null}
          </Grid>
        </Grid>
      </Box>
    );
  },
);
// Type and required properties
ServiceProviderFormAtom.propTypes = {};

export default ServiceProviderFormAtom;
