import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Box, Divider, FormControl, Grid, MenuItem, Paper } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import { SMgraph } from 'utils/apollo/SMclient';
import { FIND_ASSAY, FIND_LOAD_TYPE_LIST } from 'utils/apollo/gql/BatchManager';
import { generateRandomControlsPerPlate } from 'helpers/batchManagerUtils';
import WellsContainer from 'components/molecules/WellsContainer';
import BlankControlModal from 'components/atoms/BlankControlModal';
import RandomControlModal from 'components/atoms/RandomControlModal';
import RequestsSummaryAtom from '../BatchManager/RequestsSummaryAtom';
import { Core } from '@ebs/styleguide';
import {
  SET_EDIT_PLATE_CONTROL,
  SET_LAST_SAMPLE_NUMBER,
  SET_LOADING_BUTTONS_IN_PLATE_LAYOUT,
  SET_SELECTED_PLATE_FILL_DIRECTION_EDIT,
} from 'stores/modules/RequestBatch';
const { Button, CircularProgress, TextField, Typography } = Core;

const styles = {
  focused: {
    '& fieldset': {
      borderColor: 'palette.primary.main',
    },
  },
  alignItemsToTheRight: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  label: {
    fontWeight: 600,
  },
  colorKey: {
    textTransform: 'none',
    color: 'black',
  },
  dialog: {
    position: 'absolute',
    right: 0,
    top: 140,
  },
  formControl: {
    minWidth: 145,
    marginTop: '15px',
  },
  samplesAssignedCircle: {
    background: '#F2C113',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  dartControlsCircle: {
    background: '#A0A0A0',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  intertekControlsCircle: {
    background: '#A0A0A0',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  textTransformNone: {
    textTransform: 'none',
    fontSize: 12,
  },
  userAssignedBlanksCircle: {
    background: '#1279F2',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  userAssignedPositiveControlsCircle: {
    background: '#FF2500',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
  },
  problemWithSampleCircle: {
    background: '#F2C113',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
    border: 'solid 4px #FF2500',
  },
  emptyOrNotDefinedCircle: {
    background: '#FFFFFF',
    width: '20px',
    height: '20px',
    borderRadius: '50%',
    border: 'solid 2px #B1B1B1',
  },
};

const BatchDetailsPlateLayoutTabAtom = React.forwardRef((props, ref) => {
  const { batchDetails, mode, requests, setError, setMessage, wellsGrid } = props;
  const dispatch = useDispatch();

  let controlList = ['Blank', 'Positive'];
  if (mode === 'EDIT') {
    controlList.push('Delete');
  }

  const {
    batchCodeObject,
    checkedRequestSelection,
    selectedPlateFillDirection,
    selectedPlateSize,
    storedTabs,
    storedLoadingButtonsInPlateLayout,
    storedCells,
    storedCellsLabel,
    storedCellsColorKey,
    storedCellsLabelWithDirection,
    selectedControlType,
    lastSampleNumber,
    totalSamplesInBatch,
    wellsId,
    samplesListForEdit,
    stepTwoTechnology: {
      technologyServiceProvider: { controlPlate },
      serviceProvider: { label: vendorCode },
    },
    isDisableFormPlate,
  } = useSelector(({ request_batch }) => {
    return request_batch;
  });

  const [tabs, setTabs] = useState(storedTabs);
  const [plateFillDirectionList, setPlateFillDirectionList] = useState([]);
  const [plateFillDirection, setPlateFillDirection] = useState(
    selectedPlateFillDirection,
  );
  const [plateSize, setPlateSize] = useState(selectedPlateSize);
  const [plateSizeList, setPlateSizeList] = useState([]);
  const [cells, setCells] = useState(storedCells);
  const [cellsLabel, setCellsLabel] = useState(storedCellsLabel);
  const [cellsColorKey, setCellsColorKey] = useState(storedCellsColorKey);
  const [numberOfRandomControlsPerPlate, setNumberOfRandomControlsPerPlate] =
    useState(1);
  const [
    numberOfRandomBlankControlsPerPlate,
    setNumberOfRandomBlankControlsPerPlate,
  ] = useState(1);
  const [randomControlError, setRandomControlError] = useState('');
  const [totalPlates, setTotalPlates] = useState(0);

  const setupArrayOfGridsForEdit = (wellsGrid) => {
    const cellsContainer = [];
    const cellsLabelContainer = [];
    const cellsColorKeyContainer = [];
    const wellsIDContainer = [];

    for (let i = 0; i < wellsGrid.length; i++) {
      const flattenedWellsObject = getFlattenedWellsObject(wellsGrid[i].wells);
      const flattenedWellsId = getFlattenedWellsObject(wellsGrid[i].wellsId);
      const cellsData = createCells(selectedPlateSize);
      cellsContainer.push(cellsData);
      cellsLabelContainer.push(flattenedWellsObject);
      const colorKeyData = getFlattenedColorKey(flattenedWellsObject);
      cellsColorKeyContainer.push(colorKeyData);
      wellsIDContainer.push(flattenedWellsId);
    }

    dispatch({
      type: 'SET_CELLS',
      payload: cellsContainer,
    });

    dispatch({
      type: 'SET_CELLS_LABEL',
      payload: cellsLabelContainer,
    });

    dispatch({
      type: 'SET_CELLS_COLORKEY',
      payload: cellsColorKeyContainer,
    });

    dispatch({
      type: 'SET_WELLS_ID',
      payload: wellsIDContainer,
    });

    dispatch({
      type: 'SET_IS_TABLE_READY',
      payload: true,
    });
  };

  useEffect(() => {
    const plateSizeList = ['96'];
    setPlateSizeList(plateSizeList);
    if (mode == 'EDIT')
      dispatch({
        type: SET_EDIT_PLATE_CONTROL,
        payload: {},
      });
  }, []);

  useEffect(() => {
    setPlateSize(selectedPlateSize);
  }, [selectedPlateSize]);

  useEffect(() => {
    setTabs(storedTabs);
  }, [storedTabs]);

  useEffect(() => {
    setCells(storedCells);
  }, [storedCells]);

  useEffect(() => {
    setCellsLabel(storedCellsLabel);
  }, [storedCellsLabel]);

  useEffect(() => {
    setCellsColorKey(storedCellsColorKey);
  }, [storedCellsColorKey]);

  useEffect(() => {
    if (plateFillDirectionList.length === 0) {
      findPlateFillDirectionList();
    }
  }, []);

  useEffect(() => {
    /**
     * Only set up for Edit && for when Cells has value so that it will
     * not be rerendered all the time.
     */

    if (mode === 'EDIT' && wellsGrid && cells.length === 0) {
      setupArrayOfGridsForEdit(wellsGrid);
    }
  }, []);

  const convertToWholeNumbers = (plateSize, vendorControlPositions) => {
    if (vendorControlPositions.includes(',')) {
      return vendorControlPositions.split(',').map((item) => {
        // e.g. G11
        const letter = item[0]; // 'G'
        const number = item.substring(1); // '11'
        const convertedLetterToInt = letter.charCodeAt(0) - 65; // e.g. 'G' to 6
        const rowSizeGivenPlateSize = plateSize === '96' ? 12 : 24;
        const column = +number - 1; // e.g. '11' to 10
        return convertedLetterToInt * rowSizeGivenPlateSize + column;
      });
    }
    return [];
  };

  async function findPlateFillDirectionList() {
    try {
      const response = await SMgraph.query({
        query: FIND_LOAD_TYPE_LIST,
        fetchPolicy: 'no-cache',
      });
      const { content } = response.data.findLoadTypeList;
      const CIMMYT_FORMAT_ID = '3';
      const removedCIMMYTFormat = content.filter(
        (direction) => direction.id !== CIMMYT_FORMAT_ID,
      );
      setPlateFillDirectionList(removedCIMMYTFormat);
      if (!selectedPlateFillDirection) setPlateFillDirection(removedCIMMYTFormat[0]);
      return content;
    } catch (error) {
      throw new Error(error);
    }
  }

  const setupWellsTabs = (_totalPlates) => {
    const loadingState = {
      ...storedLoadingButtonsInPlateLayout,
      loadingGeneratePlateLayout: true,
    };
    dispatch({
      type: SET_LOADING_BUTTONS_IN_PLATE_LAYOUT,
      payload: loadingState,
    });

    const { lastPlateNumber, lastSampleNumber } = batchCodeObject;

    dispatch({
      type: SET_LAST_SAMPLE_NUMBER,
      payload: lastSampleNumber,
    });

    const wellsTabs = [];
    for (let i = 0; i < _totalPlates; i++) {
      wellsTabs.push({
        id: i,
        batchCode: `P${i + lastPlateNumber + 1}`,
      });
    }
    dispatch({
      type: 'SET_STORED_TABS',
      payload: wellsTabs,
    });
  };

  function getZeroes(length) {
    switch (length) {
      case 1:
        return '00000';
      case 2:
        return '0000';
      case 3:
        return '000';
      case 4:
        return '00';
      case 5:
        return '0';
      default:
        return '';
    }
  }

  function getSampleName(counter) {
    counter++;
    const zeroes = getZeroes(counter.toString().length);
    const sampleName = `${zeroes}${counter}`;
    return sampleName;
  }

  const generateSamplePlates = (_totalPlates) => {
    const loadingState = {
      ...storedLoadingButtonsInPlateLayout,
      loadingGeneratePlateSamples: true,
    };
    dispatch({
      type: 'SET_LOADING_BUTTONS_IN_PLATE_LAYOUT',
      payload: loadingState,
    });

    const size = plateSize === '96' ? 96 : 384;
    const N = size === 96 ? 8 : 16;
    const M = size === 96 ? 12 : 24;
    let cellsLabelContainer = [];
    let counter = lastSampleNumber;
    let maxCount = counter + totalSamplesInBatch;
    for (let i = 0; i < _totalPlates; i++) {
      let data = [];
      data = [...storedCellsLabel[i]];
      if (plateFillDirection.name === 'Rows') {
        for (let i = 0; i < size; i++) {
          if (data[i] === 'Approved') {
            if (counter !== maxCount) {
              data[i] = getSampleName(counter);
              counter++;
            }
          } else continue;
        }
      } else {
        for (let i = 0; i < M; i++) {
          let downCounter = 0;
          for (let j = 0; j < N; j++) {
            if (data[i + downCounter] === 'Approved') {
              if (counter !== maxCount) {
                data[i + downCounter] = getSampleName(counter);
                downCounter += M;
                counter++;
              }
            } else {
              downCounter += M;
              continue;
            }
          }
        }
      }
      cellsLabelContainer.push(data);
    }
    dispatch({
      type: 'SET_CELLS_LABEL_WITH_DIRECTION',
      payload: cellsLabelContainer,
    });
    if (mode === 'EDIT')
      dispatch({
        type: SET_SELECTED_PLATE_FILL_DIRECTION_EDIT,
        payload: plateFillDirection,
      });
    else
      dispatch({
        type: 'SET_SELECTED_PLATE_FILL_DIRECTION',
        payload: plateFillDirection,
      });
  };

  const createCells = (_plateSize) => {
    const data =
      _plateSize === '96' ? Array(96).fill(false) : Array(384).fill(false);
    return data;
  };

  const createCellsLabel = (_plateSize, vendorControlPositions) => {
    const data =
      _plateSize === '96' ? Array(96).fill('Approved') : Array(384).fill('Approved');
    const _vendorControlPositions = convertToWholeNumbers(
      _plateSize,
      vendorControlPositions,
    );
    _vendorControlPositions.map((item) => {
      data[item] = vendorCode;
    });
    return data;
  };

  const createCellsColorKey = (_plateSize, vendorControlPositions) => {
    let data =
      _plateSize === '96'
        ? Array(96).fill('samplesAssignedCircle')
        : Array(384).fill('samplesAssignedCircle');

    const _vendorControlPositions = convertToWholeNumbers(
      _plateSize,
      vendorControlPositions,
    );
    _vendorControlPositions.map((item) => {
      if (vendorCode === 'DArT' || vendorCode === 'Agriplex') {
        data[item] = 'dartControlsCircle';
      } else if (vendorCode === 'Intertek') {
        if (_plateSize === '96') {
          data[item] = 'intertekControlsCircle';
        } else {
          data[item] = 'intertekControlsCircle';
        }
      }
    });

    return data;
  };

  const getFlattenedWellsObject = (wells) => {
    /**
     * This is for returning the single array containing all the n rows of a Wells Grid.
     * This format is used because of the current implementation of the
     * Wells Grid Logic.
     */
    let flattened = [];
    for (let i = 0; i < Object.keys(wells).length; i++) {
      const row = Object.keys(wells)[i];
      flattened = [...flattened, ...wells[row]];
    }

    return flattened;
  };

  const getFlattenedColorKey = (flattenedWellsObject) => {
    const flattenedColorKey = [];

    for (let i = 0; i < flattenedWellsObject.length; i++) {
      if (flattenedWellsObject[i] === null || flattenedWellsObject[i] === 'Blank') {
        flattenedColorKey.push('userAssignedBlanksCircle');
      } else if (flattenedWellsObject[i] === 'Positive') {
        flattenedColorKey.push('userAssignedPositiveControlsCircle');
      } else if (flattenedWellsObject[i] === 'Intertek') {
        flattenedColorKey.push('intertekControlsCircle');
      } else if (
        flattenedWellsObject[i] === 'DArT' ||
        flattenedWellsObject[i] === 'Agriplex'
      ) {
        flattenedColorKey.push('dartControlsCircle');
      } else if (flattenedWellsObject[i] === 'Random') {
        flattenedColorKey.push('randomlyAssignedRandomControlsCircle');
      } else {
        flattenedColorKey.push('samplesAssignedCircle');
      }
    }

    return flattenedColorKey;
  };

  const setupArrayOfGrids = async (_totalPlates, plateSize) => {
    const _vendorControlPositions = controlPlate;
    const cellsContainer = [];
    const cellsLabelContainer = [];
    const cellsColorKeyContainer = [];

    for (let i = 0; i < _totalPlates; i++) {
      const cellsData = createCells(plateSize);
      cellsContainer.push(cellsData);

      const cellsLabelData = createCellsLabel(plateSize, _vendorControlPositions);
      cellsLabelContainer.push(cellsLabelData);

      const cellsColorKeyData = createCellsColorKey(
        plateSize,
        _vendorControlPositions,
      );
      cellsColorKeyContainer.push(cellsColorKeyData);
    }

    dispatch({
      type: 'SET_CELLS',
      payload: cellsContainer,
    });

    dispatch({
      type: 'SET_CELLS_LABEL',
      payload: cellsLabelContainer,
    });

    dispatch({
      type: 'SET_CELLS_COLORKEY',
      payload: cellsColorKeyContainer,
    });
  };
  const handlePlateSizeChange = (event) => {
    handleGeneratePlateLayouts(event.target.value);
  };

  const handleControlTypeChange = (event) => {
    dispatch({
      type: 'SET_SELECTED_CONTROL_TYPE',
      payload: event.target.value,
    });

    if (event.target.value === 'Blank') {
      dispatch({
        type: 'SET_OPEN_BLANK_CONTROL_TYPE_MODAL',
        payload: true,
      });
    } else if (event.target.value === 'Random') {
      dispatch({
        type: 'SET_OPEN_RANDOM_CONTROL_TYPE_MODAL',
        payload: true,
      });
    }

    const samples = mode === 'EDIT' ? batchDetails.numMembers : totalSamplesInBatch;
    const _totalPlates = Math.ceil(samples / (+plateSize - 2));

    const cellsContainer = [];

    for (let i = 0; i < _totalPlates; i++) {
      const cellsData = createCells(selectedPlateSize);
      cellsContainer.push(cellsData);
    }

    dispatch({
      type: 'SET_CELLS',
      payload: cellsContainer,
    });
  };

  const handlePlateFillDirectionChange = (event) => {
    setPlateFillDirection(event.target.value);
  };

  const handleGeneratePlateLayouts = (plateSize) => {
    dispatch({
      type: 'SET_SELECTED_PLATE_SIZE',
      payload: plateSize,
    });

    setTimeout(() => {
      const _totalPlates = Math.ceil(totalSamplesInBatch / (+plateSize - 2));
      setTotalPlates(_totalPlates);

      setupWellsTabs(_totalPlates);
      setupArrayOfGrids(_totalPlates, plateSize);
    }, 100);

    setTimeout(() => {
      const loadingState = {
        ...storedLoadingButtonsInPlateLayout,
        loadingGeneratePlateLayout: false,
      };

      dispatch({
        type: 'SET_LOADING_BUTTONS_IN_PLATE_LAYOUT',
        payload: loadingState,
      });
    }, 300);
  };

  const handleGeneratePlateSample = () => {
    dispatch({
      type: 'SET_SELECTED_PLATE_FILL_DIRECTION',
      payload: plateFillDirection,
    });

    const totalPlates = Math.ceil(totalSamplesInBatch / (+plateSize - 2));

    generateSamplePlates(totalPlates);

    const cellsContainer = [];
    for (let i = 0; i < totalPlates; i++) {
      const cellsData = createCells(selectedPlateSize);
      cellsContainer.push(cellsData);
    }

    dispatch({
      type: 'SET_CELLS',
      payload: cellsContainer,
    });

    dispatch({
      type: 'SET_CURRENT_PLATE_INDEX',
      payload: 0,
    });

    setTimeout(() => {
      const loadingState = {
        ...storedLoadingButtonsInPlateLayout,
        loadingGeneratePlateSamples: false,
      };

      dispatch({
        type: 'SET_LOADING_BUTTONS_IN_PLATE_LAYOUT',
        payload: loadingState,
      });
    }, 300);
  };

  const setSelectedValueLabel = (selectedValue, list) => {
    /**
     * This is because Material UI do not much support
     * using Select on arrays of objects.
     *
     * Got the solution from: https://www.youtube.com/watch?v=FS2YOGc0Veo
     */
    if (!selectedValue) return null;
    for (let i = 0; i < list.length; i++) {
      if (list[i].id === selectedValue.id) {
        return i;
      }
    }
  };

  const handleClearPlates = () => {
    let loadingState = {
      ...storedLoadingButtonsInPlateLayout,
      loadingClearPlates: true,
    };
    dispatch({
      type: 'SET_LOADING_BUTTONS_IN_PLATE_LAYOUT',
      payload: loadingState,
    });

    dispatch({
      type: 'RESET_PLATES',
      payload: null,
    });

    setTimeout(() => {
      loadingState = {
        ...storedLoadingButtonsInPlateLayout,
        loadingClearPlates: false,
      };
      dispatch({
        type: 'SET_LOADING_BUTTONS_IN_PLATE_LAYOUT',
        payload: loadingState,
      });
    }, 300);
  };

  const updatePlate = (
    updatedCells,
    updatedCellsLabel,
    updatedCellsColorKey,
    index,
    sampleDetails,
  ) => {
    const updatedStoredCells = [...storedCells];
    updatedStoredCells[index] = updatedCells;

    const updatedStoredCellsLabel = [...storedCellsLabel];
    updatedStoredCellsLabel[index] = updatedCellsLabel;

    const updatedStoredCellsColorKey = [...storedCellsColorKey];
    updatedStoredCellsColorKey[index] = updatedCellsColorKey;

    dispatch({
      type: 'SET_CELLS',
      payload: updatedStoredCells,
    });

    dispatch({
      type: 'SET_CELLS_LABEL',
      payload: updatedStoredCellsLabel,
    });

    dispatch({
      type: 'SET_CELLS_COLORKEY',
      payload: updatedStoredCellsColorKey,
    });

    if (mode === 'EDIT') {
      const _samplesListForEdit = [...samplesListForEdit];
      _samplesListForEdit.push(sampleDetails);
      dispatch({
        type: 'SET_SAMPLES_LIST_FOR_EDIT',
        payload: _samplesListForEdit,
      });
    }
  };

  const handleAddRandomControls = (event) => {
    setNumberOfRandomControlsPerPlate(event.target.value);
  };

  const handleRandomBlankCells = (value) => {
    // setNumberOfRandomBlankControlsPerPlate(Math.abs(value));
  };

  const handleCancelBlankControl = () => {
    dispatch({
      type: 'SET_OPEN_BLANK_CONTROL_TYPE_MODAL',
      payload: false,
    });

    dispatch({
      type: 'SET_SELECTED_CONTROL_TYPE',
      payload: null,
    });
  };

  const handleCancelRandomControl = () => {
    dispatch({
      type: 'SET_OPEN_RANDOM_CONTROL_TYPE_MODAL',
      payload: false,
    });

    dispatch({
      type: 'SET_SELECTED_CONTROL_TYPE',
      payload: null,
    });
  };

  const applyRandomControl = (randomIndexes, index) => {
    const cellsLabelWithRandomControl = [...storedCellsLabel[index]];
    for (let i = 0; i < randomIndexes.length; i++) {
      cellsLabelWithRandomControl[randomIndexes[i]] = 'Random';
    }

    return cellsLabelWithRandomControl;
  };

  const handleGenerateRandomControl = async () => {
    // Getting Vendor Controls indexes for indicating exception on Randomness of Controls
    // TODO: change for the controlPlate on redux
    // const _vendorControlPositions = await findVendorControlsBasedOnSelectedAssay();
    const _vendorControlPositions = controlPlate;
    const vendorControls = convertToWholeNumbers(plateSize, _vendorControlPositions);

    const cellslabelWithRandomControls = [];
    const randomIndexesContainer = [];
    for (let i = 0; i < storedCellsLabel.length; i++) {
      let numberOfApprovedLabels = 0;
      storedCellsLabel[i].map((cell) => {
        if (cell === 'Approved') numberOfApprovedLabels += 1;
      });

      // Check first if valid Random Control Application
      if (numberOfRandomControlsPerPlate <= numberOfApprovedLabels) {
        setRandomControlError('');
        const randomIndexes = generateRandomControlsPerPlate(
          storedCellsLabel[i],
          numberOfRandomControlsPerPlate,
          vendorControls,
        );

        randomIndexesContainer.push(randomIndexes);
        const data = applyRandomControl(randomIndexes, i);
        cellslabelWithRandomControls.push(data);
      } else {
        setRandomControlError(
          'Number of Random Controls exceeds the available Approved cells.',
        );
        return;
      }
    }

    const cellsColorKeyWithRandomContainer = [];
    randomIndexesContainer.forEach((randomIndexes, index) => {
      let cellsColorKeyWithRandom = [...cellsColorKey[index]];
      randomIndexes.forEach((index) => {
        cellsColorKeyWithRandom[index] = 'randomlyAssignedRandomControlsCircle';
      });
      cellsColorKeyWithRandomContainer.push(cellsColorKeyWithRandom);
    });

    dispatch({
      type: 'SET_CELLS_LABEL',
      payload: cellslabelWithRandomControls,
    });

    dispatch({
      type: 'SET_CELLS_COLORKEY',
      payload: cellsColorKeyWithRandomContainer,
    });

    handleCancelRandomControl();
  };

  const applyBlankRandomControl = (randomIndexes, index) => {
    const cellsLabelWithBlankRandomControl = [...storedCellsLabel[index]];
    for (let i = 0; i < randomIndexes.length; i++) {
      cellsLabelWithBlankRandomControl[randomIndexes[i]] = 'Blank';
    }

    return cellsLabelWithBlankRandomControl;
  };

  const handleGenerateBlankRandomControl = async () => {
    // Getting Vendor Controls indexes for indicating exception on Randomness of Controls
    // const _vendorControlPositions = await findVendorControlsBasedOnSelectedAssay();
    const _vendorControlPositions = controlPlate;
    const vendorControls = convertToWholeNumbers(plateSize, _vendorControlPositions);

    const cellslabelWithRandomControls = [];
    const randomIndexesContainer = [];
    for (let i = 0; i < storedCellsLabel.length; i++) {
      let numberOfApprovedLabels = 0;
      storedCellsLabel[i].map((cell) => {
        if (cell === 'Approved') numberOfApprovedLabels += 1;
      });

      // Check first if valid Random Control Application
      if (numberOfRandomBlankControlsPerPlate <= numberOfApprovedLabels) {
        // setRandomControlError('');
        const randomIndexes = generateRandomControlsPerPlate(
          storedCellsLabel[i],
          numberOfRandomBlankControlsPerPlate,
          vendorControls,
        );

        randomIndexesContainer.push(randomIndexes);
        const data = applyBlankRandomControl(randomIndexes, i);
        cellslabelWithRandomControls.push(data);
      } else {
        // setRandomControlError(
        //   'Number of Random Controls exceeds the available Approved cells.',
        // );
        return;
      }
    }

    const cellsColorKeyWithRandomContainer = [];
    randomIndexesContainer.forEach((randomIndexes, index) => {
      let cellsColorKeyWithRandom = [...cellsColorKey[index]];
      randomIndexes.forEach((index) => {
        cellsColorKeyWithRandom[index] = 'userAssignedBlanksCircle';
      });
      cellsColorKeyWithRandomContainer.push(cellsColorKeyWithRandom);
    });

    dispatch({
      type: 'SET_CELLS_LABEL',
      payload: cellslabelWithRandomControls,
    });

    dispatch({
      type: 'SET_CELLS_COLORKEY',
      payload: cellsColorKeyWithRandomContainer,
    });

    handleCancelBlankControl();
  };

  const applyManualBlankControl = () => {
    dispatch({
      type: 'SET_OPEN_BLANK_CONTROL_TYPE_MODAL',
      payload: false,
    });
    dispatch({
      type: 'SET_SELECTED_CONTROL_TYPE',
      payload: 'Blank',
    });
  };

  return (
    <Box
      data-testid={'BatchDetailsPlateLayoutTabTestId'}
      ref={ref}
      style={{ padding: '4px 25px' }}
    >
      <Grid>
        <Grid item xs={12}>
          <Typography variant='h6'>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage id={'some.id'} defaultMessage={'Plate Layout'} />
            </Box>
          </Typography>
        </Grid>

        <br />

        <Grid item xs={12} xl={6}>
          <RequestsSummaryAtom
            batchDetails={batchDetails}
            checkedRequestSelection={checkedRequestSelection}
            mode={mode}
            requests={requests}
            totalSamplesInBatch={totalSamplesInBatch}
          />
        </Grid>
        <Grid item xs={12} style={{ marginTop: '40px' }}>
          <Divider />
          <Typography variant='h6' style={{ marginTop: '20px' }}>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage
                id={'some.id'}
                defaultMessage={'Select Plate information'}
              />
            </Box>
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginTop: '7px' }}>
          <Grid container spacing={2}>
            <Grid item xs={12} lg={3}>
              <Typography>
                <FormattedMessage id={'some.id'} defaultMessage={'Plate Size'} />
              </Typography>
              <FormControl sx={styles.formControl}>
                <TextField
                  size='small'
                  select
                  variant='outlined'
                  disabled={isDisableFormPlate ? true : mode === 'EDIT'}
                  value={plateSize || ''}
                  onChange={handlePlateSizeChange}
                  sx={styles.focused}
                  label={Boolean(plateSize) ? '' : 'Select Plate Size'}
                >
                  {plateSizeList.map((item, index) => {
                    return (
                      <MenuItem key={index} value={item}>
                        {item}
                      </MenuItem>
                    );
                  })}
                </TextField>
              </FormControl>
              <br />
            </Grid>
            <Grid item xs={12} lg={3}>
              <Typography>
                <FormattedMessage
                  id={'some.id'}
                  defaultMessage={'Internal Controls'}
                />
              </Typography>
              <FormControl sx={styles.formControl}>
                <TextField
                  size='small'
                  select
                  variant='outlined'
                  value={selectedControlType || ''}
                  onChange={handleControlTypeChange}
                  label={Boolean(selectedControlType) ? '' : 'Use a Control'}
                  disabled={isDisableFormPlate ? true : !selectedPlateSize}
                >
                  {controlList.map((item, index) => {
                    return (
                      <MenuItem
                        disabled={mode === 'EDIT' && item === 'Positive'}
                        key={index}
                        value={item}
                      >
                        {item} (select wells)
                      </MenuItem>
                    );
                  })}
                </TextField>
              </FormControl>
            </Grid>
            <Grid item xs={12} lg={3}>
              <Typography>
                <FormattedMessage
                  id={'some.id'}
                  defaultMessage={'Plate Fill Direction'}
                />
              </Typography>
              <FormControl sx={styles.formControl}>
                <TextField
                  size='small'
                  select
                  variant='outlined'
                  value={
                    plateFillDirectionList.length > 0
                      ? plateFillDirectionList[
                          setSelectedValueLabel(
                            plateFillDirection,
                            plateFillDirectionList,
                          )
                        ]
                      : ''
                  }
                  onChange={handlePlateFillDirectionChange}
                  label={Boolean(plateFillDirection) ? '' : 'Set Direction'}
                  disabled={
                    isDisableFormPlate ? true : !selectedPlateSize || mode === 'EDIT'
                  }
                >
                  {plateFillDirectionList.map((item, index) => {
                    return (
                      <MenuItem key={`${index}_${item.name}`} value={item}>
                        {item.name}
                      </MenuItem>
                    );
                  })}
                </TextField>
              </FormControl>

              <br />

              <Button
                variant='contained'
                style={{ textTransform: 'none', minWidth: '145px' }}
                className={
                  'bg-ebs-brand-default rounded-md text-white mt-5 hover:bg-ebs-brand-900'
                }
                onClick={handleGeneratePlateSample}
                disabled={
                  !plateFillDirection ||
                  storedLoadingButtonsInPlateLayout?.loadingGeneratePlateSamples ||
                  mode === 'EDIT'
                }
              >
                {storedLoadingButtonsInPlateLayout?.loadingGeneratePlateSamples ? (
                  <CircularProgress size={20} color='grey' />
                ) : (
                  'Generate Plate Samples'
                )}
              </Button>
            </Grid>

            <Grid item xs={12} lg={3} style={{ marginTop: '35px' }}>
              <Button
                variant='outlined'
                color='primary'
                style={{ textTransform: 'none' }}
                onClick={handleClearPlates}
                disabled={
                  !selectedPlateSize ||
                  storedLoadingButtonsInPlateLayout?.loadingClearPlates ||
                  mode === 'EDIT'
                }
              >
                {storedLoadingButtonsInPlateLayout?.loadingClearPlates ? (
                  <CircularProgress size={20} color='grey' />
                ) : (
                  'Clear Plates'
                )}
              </Button>
            </Grid>
          </Grid>
        </Grid>

        <br />

        <Grid item xs={12}>
          <Typography variant='h6' style={{ marginTop: '20px' }}>
            <Box sx={{ fontWeight: 500 }}>
              <FormattedMessage id={'some.id'} defaultMessage={'Plates'} />
            </Box>
          </Typography>
        </Grid>

        <Grid item xs={12} style={{ marginTop: '20px' }}>
          <Paper>
            <Grid container>
              <Grid item xs={12}>
                {tabs && (
                  <WellsContainer
                    mode={mode}
                    cells={cells}
                    cellsLabel={cellsLabel}
                    cellsColorKey={cellsColorKey}
                    cellsLabelWithDirection={storedCellsLabelWithDirection}
                    controlType={selectedControlType}
                    tabs={tabs}
                    totalNumberOfPlates={totalPlates}
                    updatePlate={updatePlate}
                    wellsGrid={wellsGrid}
                    wellsId={wellsId}
                  />
                )}
              </Grid>
            </Grid>
          </Paper>
        </Grid>

        <Grid item xs={12}>
          <BlankControlModal
            loading={false}
            onCancel={handleCancelBlankControl}
            applyManualBlankControl={applyManualBlankControl}
            numberOfRandomBlankControlsPerPlate={numberOfRandomBlankControlsPerPlate}
            handleRandomBlankCells={handleRandomBlankCells}
            handleGenerateBlankRandomControl={handleGenerateBlankRandomControl}
          />

          <RandomControlModal
            handleAddRandomControls={handleAddRandomControls}
            loading={false}
            numberOfRandomControlsPerPlate={numberOfRandomControlsPerPlate}
            onCancel={handleCancelRandomControl}
            onConfirm={handleGenerateRandomControl}
            validationError={randomControlError}
          />
        </Grid>
      </Grid>
    </Box>
  );
});
// Type and required properties
BatchDetailsPlateLayoutTabAtom.propTypes = {};

export default BatchDetailsPlateLayoutTabAtom;
