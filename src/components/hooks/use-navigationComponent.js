import { useLocation, useNavigation } from 'react-router-dom';

const useNavigationComponent = () => {
  const navigate = useNavigation();
  const location = useLocation();

  return {
    navigate,
    location,
  };
};

export default useNavigationComponent;
