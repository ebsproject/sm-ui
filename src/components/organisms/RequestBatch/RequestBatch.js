/*eslint-disable*/
import React, { useState } from 'react';
// @mui/material components
import { Button } from '@mui/material';
import Slide from '@mui/material/Slide';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import TextField from '@mui/material/TextField';

import { useSelector } from 'react-redux';

// @mui/icons-material
import Close from '@mui/icons-material/Close';
import BatchIcon from '@mui/icons-material/NoteAdd';

// core components
import styles from 'assets/jss/material-dashboard-pro-react/views/notificationsStyle.js';
import { FormattedMessage } from 'react-intl';
import { getBatchCode } from 'utils/other/SampleManager';
import { SMgraph, createBatchWithRequest } from 'utils/apollo/SMclient';
import { MODIFY_REQUEST } from 'utils/apollo/gql/RequestManager';
import { useHistory } from 'react-router-dom';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction='down' ref={ref} {...props} />;
});

export default function BatchRequestComponent(props) {
  const history = useHistory();

  const [classicModal, setClassicModal] = useState(false);
  const [description, setDescription] = useState('');
  const [enableButton, setDisableButton] = useState(false);
  const [batchName, setBatchName] = useState('');
  const { refresh, rowData, children, ...rest } = props;

  const { checkedRequestSelection } = useSelector(
    ({ request_batch }) => request_batch,
  );

  const createBatch = async () => {
    setDisableButton(true);
    let total = 0;
    checkedRequestSelection.forEach((element) => {
      total += parseInt(element.totalEntities, 10);
    });

    createBatchWithRequest(
      checkedRequestSelection,
      batchName,
      description,
      1,
      2,
      total,
      1,
      1,
    );

    checkedRequestSelection.forEach((element) => {
      updateRequests(element);
    });
    refresh();
    history.push('/sm/inspect');
    setClassicModal(false);
  };

  const updateRequests = async (element) => {
    const STATUS_ID_BATCH = 4;

    const response = await SMgraph.mutate({
      mutation: MODIFY_REQUEST,
      variables: {
        RequestTo: {
          id: element.id,
          status: { id: STATUS_ID_BATCH },
        },
      },
    });
  };

  return (
    <div>
      <Button
        style={{ marginLeft: '7px', marginRight: '7px' }}
        onClick={() => {
          getBatchCode(checkedRequestSelection).then((response) => {
            setBatchName(response);
          });

          // updateBatchName(requests, 1)
          setDescription('');
          setDisableButton(false);
          setClassicModal(true);
        }}
        variant='contained'
        color='secondary'
        disabled={checkedRequestSelection.length > 0 ? false : true}
      >
        <FormattedMessage id={'sm.btn.lbl.batch'} defaultMessage={'Create Batch'} />{' '}
        &nbsp; <BatchIcon />
      </Button>

      <Dialog
        open={classicModal}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => setClassicModal(false)}
        aria-labelledby='classic-modal-slide-title'
        aria-describedby='classic-modal-slide-description'
      >
        <DialogTitle id='classic-modal-slide-title'>
          <Button
            justIcon
            key='close'
            aria-label='Close'
            color='transparent'
            onClick={() => setClassicModal(false)}
          >
            <Close />
          </Button>
          <h4>
            <FormattedMessage
              id={'sm.btn.dialog.title'}
              defaultMessage={'Details batch request'}
            />
          </h4>
        </DialogTitle>
        <DialogContent id='classic-modal-slide-description'>
          <form noValidate autoComplete='off'>
            <div>
              <TextField
                id='outlined-read-only-input'
                label={
                  <FormattedMessage
                    id={'sm.btn.dialog.lbl.name'}
                    defaultMessage={'Batch name'}
                  />
                }
                value={batchName}
                margin='normal'
                InputProps={{
                  readOnly: true,
                }}
                variant='outlined'
              />
            </div>
            <div>
              <TextField
                id='outlined-multiline-static'
                label={
                  <FormattedMessage
                    id={'sm.btn.dialog.lbl.description'}
                    defaultMessage={'Add description'}
                  />
                }
                multiline
                rows='4'
                margin='normal'
                variant='outlined'
                value={description}
                onChange={(e) => setDescription(e.target.value)}
              />
            </div>
          </form>
        </DialogContent>
        <DialogActions>
          <Button onClick={createBatch} disabled={enableButton} color='success'>
            <FormattedMessage
              id={'sm.btn.dialog.btn.lbl.create'}
              defaultMessage={'Create Batch'}
            />
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
