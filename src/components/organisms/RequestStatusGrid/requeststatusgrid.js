import React from 'react';
import PropTypes from 'prop-types';
import LoadingGear from 'components/atoms/LoadingGear';
import GridHeaderAtom from 'components/atoms/GridHeader';
import RequestListContent from 'components/atoms/RequestListContent';
import { VERSION } from 'utils/config';
import { Core } from '@ebs/styleguide';
import useRequestGrid from './hooks/use-requestGrid';
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@mui/material';
const { Grid, Button } = Core;


const RequestStatusGridOrganism = React.forwardRef(({ userProfile }, ref) => {
  const {
    error,
    handleCloseDialogNew,
    message,
    messageNewDialog,
    openDialogNew,
    rowActions,
    setError,
    setMessage,
    toolbarActions,
  } = useRequestGrid({ userProfile });

  return (
    <div ref={ref} data-testid={'RequestStatusGridTestId'}>
      {error ? (
        <LoadingGear message={message} />
      ) : (
        <div>
          <Grid container spacing={2}>
            <Grid item xs={8}>
              <GridHeaderAtom
                title={'Request Manager'}
                description={
                  'This is the browser for the Request Manager. You may access the requests here.'
                }
              />
            </Grid>
          </Grid>
          <RequestListContent
            view='request-manager'
            toolbarActions={toolbarActions}
            rowActions={rowActions}
            setError={setError}
            setMessage={setMessage}
            userProfile={userProfile}
          />
          {openDialogNew && (
            <Dialog open={openDialogNew} onClose={handleCloseDialogNew}>
              <DialogTitle id='alert-dialog-title'>Warning</DialogTitle>
              <DialogContent>
                <DialogContentText id='alert-dialog-description'>
                  {messageNewDialog.join('<br />')}
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={handleCloseDialogNew} autoFocus>
                  Close
                </Button>
              </DialogActions>
            </Dialog>
          )}
        </div>
      )}
    </div>
  );
});
// Type and required properties
RequestStatusGridOrganism.propTypes = {
  userProfile: PropTypes.object.isRequired,
};

export default RequestStatusGridOrganism;
