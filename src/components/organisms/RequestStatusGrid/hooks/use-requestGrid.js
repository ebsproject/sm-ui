import { useState, forwardRef } from 'react';
import { useDispatch } from 'react-redux';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { Box, Dialog, DialogContent, Stack } from '@mui/material';
import { FormattedMessage } from 'react-intl';
import { validateUserProfileBeforeRedirect } from 'helpers/requestManagerUtils';
import { GETCONTEXT } from 'utils/config';
import { FileRest } from 'utils/axios/Fileclient';
import { Icons, Core } from '@ebs/styleguide';
import { RBAC } from '@ebs/layout';
import { TOKENID, GET_CORE_SYSTEM_CONTEXT } from 'utils/config';
import DeleteRowButtonRequest from '../DeleteRowButtonRequest';
const { GetApp, Edit, Visibility, Message } = Icons;
const { graphqlUri } = GET_CORE_SYSTEM_CONTEXT();
const { Button, IconButton } = Core;

const useRequestGrid = ({ userProfile }) => {
  const [error, setError] = useState(false);
  const [message, setMessage] = useState(false);
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const [dialogNew, setDialogNew] = useState({ open: false, message: [] });

  const handleCloseDialogNew = () => {
    setDialogNew({ open: false, message: [] });
  };

  const handleClickAddNewRequest = () => {
    const messageNew = [];
    if (userProfile.permissions.memberOf.programs.length === 0) {
      messageNew.push('Does not have a program to create a new request.');
    }
    if (userProfile.service_providers.length === 0) {
      messageNew.push('Does not have a service provider to create a new request.');
    }
    if (messageNew.length > 0) {
      setDialogNew({ open: true, message: messageNew });
    } else {
      validateUserProfileBeforeRedirect({
        userProfile,
        navigate,
        location,
        pathName: '/sm/requestmanager/create-request-service',
        dispatch,
      });
    }
  };

  const toolbarActions = () => {
    return (
      <>
        <RBAC
          allowedAction={'Create'}
          product='Request Manager'
          domain='Service Management'
        >
          <Button
            id='requestGrid.button.addNewRequest'
            onClick={handleClickAddNewRequest}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'Add New Request'} />
          </Button>
        </RBAC>
      </>
    );
  };

  const downloadFile = (row) => {
    let alertPayload;
    FileRest({
      url: `${GETCONTEXT().tenantId}/objects/?tags=requestId:` + row.id,
      method: 'GET',
    }).then((response) => {
      if (response.data.result.data.length > 0) {
        FileRest({
          url: `${GETCONTEXT().tenantId}/objects/${
            response.data.result.data[0].key
          }/download`,
          method: 'GET',
          responseType: 'blob',
        }).then((response) => {
          const type = response.headers['content-type'];
          const blob = new Blob([response.data], { type: type, encoding: 'UTF-8' });
          const fileName = response.headers['content-disposition']
            .match(/".*"/)[0]
            .replace(/"/g, '');
          const link = document.createElement('a');
          link.href = window.URL.createObjectURL(blob);
          link.download = fileName;
          link.click();
        });
      } else {
        alertPayload = {
          message: 'The request does not have genotyping results yet',
          translationId: 'servicetype.upload_warning',
          severity: 'warning',
        };
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: alertPayload,
        });
      }
    });
  };

  const rowActions = forwardRef(({ rowData, refresh }, ref) => {
    const [openModal, setOpenModal] = useState(false);
    const disabledDownload =
      rowData &&
      rowData.batch &&
      rowData.batch.resultFileId &&
      rowData.status &&
      parseInt(rowData.status.id) === 9
        ? false
        : true;

    return (
      <Stack direction='row' spacing={0}>
        <Box>
          <RBAC allowedAction={'View'}>
            <IconButton
              variant='contained'
              color='primary'
              component={Link}
              title='View'
              to={`/sm/view-request-service/${rowData.requestCode}`}
              state={{
                currentData: rowData,
                prevPath: location.pathname,
              }}
            >
              <Visibility />
            </IconButton>
          </RBAC>
        </Box>

        {(rowData.status?.id == 1 || rowData.status?.id == 3) && (
          <Box>
            <RBAC allowedAction={'Modify'}>
              <IconButton
                variant='contained'
                color='secondary'
                component={Link}
                title='Edit'
                to={`/sm/requestmanager/edit-request-service/${rowData.requestCode}`}
                state={{
                  currentData: rowData,
                }}
              >
                <Edit />
              </IconButton>
            </RBAC>
          </Box>
        )}
        <DeleteRowButtonRequest
          refresh={refresh}
          rowData={rowData}
          userProfile={userProfile}
        />
        <Box>
          <RBAC allowedAction={'Download Result file'}>
            <IconButton
              variant='contained'
              color='secondary'
              disabled={disabledDownload}
              title='Download Results'
              onClick={() => downloadFile(rowData)}
            >
              <GetApp />
            </IconButton>
          </RBAC>
        </Box>
        <Box>
          <IconButton
            variant='contained'
            color='secondary'
            onClick={() => setOpenModal(true)}
          >
            <Message />
          </IconButton>
        </Box>

        <Dialog open={openModal} onClose={() => setOpenModal(false)}>
          <DialogContent>
            <ebs-messaging
              endPoint={graphqlUri}
              authorization_token={TOKENID()}
              data={JSON.stringify({
                type: 'record',
                recordId: rowData.id,
                name: `Genotyping Service Manager`,
                entity: 'Genotyping request',
              })}
            ></ebs-messaging>
          </DialogContent>
        </Dialog>
      </Stack>
    );
  });
  return {
    error,
    handleCloseDialogNew,
    message,
    messageNewDialog: dialogNew.message,
    openDialogNew: dialogNew.open,
    rowActions,
    setError,
    setMessage,
    toolbarActions,
  };
};

export default useRequestGrid;
