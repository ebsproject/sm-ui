import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Box } from '@mui/material';
import { RBAC } from '@ebs/layout';
import DeleteRequestButtonMolecule from 'components/molecules/DeleteRequestButton';

const DeleteRowButtonRequest = forwardRef(
  ({ rowData, refresh, userProfile }, ref) => {
    const isShowDeleteByRole = rowData.status && parseInt(rowData.status.id) === 1;

    const isShowDeleteByOwner =
      rowData.status &&
      parseInt(rowData.status.id) === 1 &&
      rowData.requesterOwnerId === userProfile.dbId;

    return (
      <Box ref={ref}>
        {isShowDeleteByOwner && (
          <DeleteRequestButtonMolecule rowData={rowData} refresh={refresh} />
        )}
        {!isShowDeleteByOwner && isShowDeleteByRole && (
          <RBAC allowedAction={'Delete'}>
            <DeleteRequestButtonMolecule rowData={rowData} refresh={refresh} />
          </RBAC>
        )}
      </Box>
    );
  },
);

export default DeleteRowButtonRequest;
