import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Box, Grid } from '@mui/material';
import GridHeaderAtom from 'components/atoms/GridHeader';
import { useDispatch, useSelector } from 'react-redux';
import LoadingGear from 'components/atoms/LoadingGear';
import BatchListContent from 'components/atoms/BatchListContent';
import { Core } from '@ebs/styleguide';
import { SET_BATCH_CODE_OBJECT } from 'stores/modules/RequestBatch';
const { LinearProgress } = Core;

const BatchStatusGridOrganism = React.forwardRef((_, ref) => {
  const [error, setError] = useState(false);
  const [message, setMessage] = useState(false);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const { isDatabaseUpdated } = useSelector(({ RequestManager }) => RequestManager);

  useEffect(() => {
    dispatch({
      type: 'RESET_VALUES',
      payload: null,
    });

    dispatch({
      type: 'SET_ORIGINAL_VAILABLE_MARKER_PANELS',
      payload: [],
    });

    dispatch({
      type: 'SET_AVAILABLE_MARKER_PANELS',
      payload: [],
    });

    dispatch({
      type: 'SET_SELECTED_MARKER_PANELS',
      payload: [],
    });

    dispatch({
      type: 'SET_BATCH_NAME',
      payload: null,
    });

    dispatch({
      type: SET_BATCH_CODE_OBJECT,
      payload: null,
    });

    dispatch({
      type: 'SET_CHECKED_REQUEST_SELECTION',
      payload: [],
    });
  }, []);

  function handleFetch() {
    /**
     * Calling the fetch function only if there's a database
     * change. This is so the user won't wait to long
     * for viewing Batch if there's no change in
     * the Batch List
     */
    if (isDatabaseUpdated) {
      fetch();
    }
  }

  /*
    @dataSelection: Array object with data rows selected.
    @refresh: Function to refresh Grid data.
    */
  const toolbarActions = () => {};

  return (
    <Box ref={ref} data-testid={'BatchStatusGridTestId'}>
      {error ? (
        <LoadingGear message={message} />
      ) : (
        <div>
          {loading ? (
            <LinearProgress />
          ) : (
            <div>
              <Grid container spacing={2}>
                <Grid item xs={8}>
                  <GridHeaderAtom
                    title={'Batch Manager'}
                    description={
                      'This is the browser for the Batch Manager. You may access the list of batches here.'
                    }
                  />
                </Grid>
              </Grid>
              <BatchListContent
                view='batch-manager'
                toolbarActions={toolbarActions}
                setError={setError}
                setMessage={setMessage}
                setLoading={setLoading}
              />
            </div>
          )}
        </div>
      )}
    </Box>
  );
});
// Type and required properties
BatchStatusGridOrganism.propTypes = {};

export default BatchStatusGridOrganism;
