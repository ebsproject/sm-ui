import React, { useState, useEffect, useContext } from 'react';
import { Link as ReactRouterLink, useNavigate } from 'react-router-dom';
import PropTypes from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { Backdrop, Box, Breadcrumbs, Grid, Link, Paper } from '@mui/material';
import { ArrowBackIos } from '@mui/icons-material';
import {
  PRIMARY_BUTTON_CLASS,
  WHITE_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import { useDispatch, useSelector } from 'react-redux';
import { GETCONTEXT } from 'utils/config';
import { userContext } from '@ebs/layout';
import { useWorkflow } from '@ebs/components';
import {
  FIND_BATCH,
  FIND_PLATE_LIST,
  FIND_REQUEST,
  FIND_SAMPLE_DETAIL_LIST,
  MODIFY_BATCH,
  MODIFY_SAMPLE_DETAIL,
} from 'utils/apollo/gql/BatchManager';
import { createBatchAsyncronous } from 'utils/other/SampleManager';
import { SMrest } from 'utils/axios/SMclient';
import { SMgraph } from 'utils/apollo/SMclient';
import RequestsTab from 'components/atoms/BatchManagerRequestsTab';
import ServiceAndTechnologyTab from 'components/atoms/BatchManagerServiceAndTechnologyTab';
import PlateLayoutTab from 'components/atoms/BatchDetailsPlateLayoutTab';
import RequestsTabForEdit from 'pages/BatchManager/pages/ViewBatch/RequestsTab';
import LoadingGear from 'components/atoms/LoadingGear';
import AlertNotification from 'components/atoms/AlertNotification';
import { fetchBatchCode } from 'utils/axios/api';
import {
  RESET_TECHNOLOGY,
  RESET_VALUES,
  SET_BATCH_CODE_OBJECT,
  SET_BATCH_NAME,
  SET_STEP_TWO_TECHNOLOGY,
} from 'stores/modules/RequestBatch';
import { convertFixedMarkers } from 'utils/apollo/apis/serviceTechnologyApi';
import TabPanelAtom from 'components/atoms/forms/tabs/TabPanelAtom';
import { Core } from '@ebs/styleguide';
import TabTitleAtom from 'components/atoms/forms/tabs/TabTitleAtom';
const { Typography, CircularProgress, LinearProgress, Button, Tabs } = Core;

const styles = {
  backdrop: {
    zIndex: 'zIndex.drawer + 1',
    color: '#fff',
  },
  navButtons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  tabNumber: {
    padding: '3px 8px',
    backgroundColor: 'palette.primary.main',
    borderRadius: '50px',
    color: 'white',
    fontWeight: '600',
    fontSize: '14px',
    marginRight: '5px',
  },
  tabPanelContainer: {
    flexGrow: 1,
    width: '100%',
    height: '100%',
    backgroundColor: 'white !important',
  },
};

const BatchManagerFormOrganism = React.forwardRef(({ mode, toEditBatch }, ref) => {
  const dispatch = useDispatch();
  const { tenantId } = GETCONTEXT('sm');
  const navigate = useNavigate();
  const { sendNotification } = useWorkflow();
  const [error, setError] = useState(false);
  const [message, setMessage] = useState(false);
  const [warning, setWarning] = useState(null);
  const [openBackdrop, setOpenBackdrop] = useState(false);
  const [vendorList, setVendorList] = useState([]);
  const [batchDetails, setBatchDetails] = useState(null);
  const [wellsGrid, setWellsGrid] = useState([]);
  const [requests, setRequests] = useState([]);
  const [loading, setLoading] = useState(true);

  const { userProfile } = useContext(userContext);

  const {
    batchCode,
    batchCodeObject,
    checkedRequestSelection,
    totalSamplesInBatch,
    selectedPlateFillDirection,
    selectedPlateSize,
    storedCellsLabel,
    samplesListForEdit,
    stepTwoTechnology,
  } = useSelector(({ request_batch }) => request_batch);

  useEffect(() => {
    if (mode === 'CREATE') {
      if (checkedRequestSelection.length === 0) {
        setError(true);
        setMessage(
          `No selected Request(s) found. Please select first in the Genotyping Service Manager.`,
        );
      }

      if (batchCodeObject === null || batchCodeObject === undefined) {
        dispatch({ type: RESET_TECHNOLOGY, payload: {} });
        findBatchCodeUnderUserProfile();
      }
    }

    const sum = checkedRequestSelection.reduce(
      (n, { totalEntities }) => n + totalEntities,
      0,
    );

    dispatch({
      type: 'SET_TOTAL_SAMPLES_IN_BATCH',
      payload: sum,
    });
    setLoading(false);

    if (warning) {
      let message = '';
      if (stepTwoTechnology.technologyServiceProvider === null) {
        message = 'Cannot proceed. No Service Provider and Technology was selected.';
      }

      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: message,
          translationId: 'some.id',
          severity: 'warning',
        },
      });
      setWarning(false); // for resetting the warning state
    }
  }, [checkedRequestSelection, warning]);

  useEffect(() => {
    if (batchCodeObject) {
      dispatch({
        type: SET_BATCH_NAME,
        payload: batchCodeObject.codeBatchNew,
      });
    }
  }, [batchCodeObject]);

  useEffect(() => {
    if (vendorList.length === 0) setVendorListData();
  }, []);

  useEffect(() => {
    if (batchDetails) {
      const buildWellsGridArray = () => {
        if (batchDetails && Object.entries(batchDetails).length > 0) {
          const plateFillDirection =
            batchDetails.loadtype.name === 'Columns'
              ? { id: '1', name: 'Columns' }
              : { id: '2', name: 'Rows' };
          dispatch({
            type: 'SET_SELECTED_PLATE_SIZE',
            payload: batchDetails.plateSize.toString(),
          });
          dispatch({
            type: 'SET_SELECTED_PLATE_FILL_DIRECTION',
            payload: plateFillDirection,
          });
          const builtWellsGrid = [];
          const numberOfColumns = setNumberOfColumns(batchDetails.plateSize);
          const plates = batchDetails.plates;
          for (const plate of plates) {
            let wellsGridObject = setWellsGridObject(numberOfColumns);
            let wellsGridIDs = setWellsGridObject(numberOfColumns);
            const vendorControlsPositions = getVendorControlPositions({
              controlPlate: batchDetails.technologyServiceProvider.controlPlate,
            });
            plate.sampleDetails.map((cell) => {
              wellsGridObject[cell.row][cell.column - 1] = cell.controltype
                ? cell.controltype.name
                : cell.sampleNumber;

              wellsGridIDs[cell.row][cell.column - 1] = cell.id;
            });

            vendorControlsPositions.map((pos) => {
              wellsGridObject[pos.row][pos.column - 1] = batchDetails.vendor.name;
            });
            const wellsGridRow = {
              plateNumber: batchDetails.name + '_P' + plate.plateId,
              wells: wellsGridObject,
              wellsId: wellsGridIDs,
              sortKey: `${plate.plateId}`.slice(1),
            };
            builtWellsGrid.push(wellsGridRow);
          }
          setWellsGrid(builtWellsGrid);
          dispatch({
            type: 'SET_STORED_TABS',
            payload: getTabsForEdit(builtWellsGrid, batchDetails),
          });
        }
      };

      buildWellsGridArray();
    }
  }, [batchDetails]);

  useEffect(() => {
    const findBatch = async () => {
      try {
        setLoading(true);
        const response = await SMgraph.query({
          query: FIND_BATCH,
          variables: {
            id: toEditBatch.id,
          },
          fetchPolicy: 'no-cache',
        });

        const plateSize = 96; //TODO: remove the number 96 and put he plateSize
        const plateListResponse = [];
        const plateResponse = await SMgraph.query({
          query: FIND_PLATE_LIST,
          variables: {
            filters: [
              { col: 'batchId', mod: 'EQ', val: response.data.findBatch.id },
            ],
            page: { number: 1, size: 10 },
          },
          fetchPolicy: 'no-cache',
        });
        const plateListByFor = [];
        for (let i = 2; i < plateResponse.data.findPlateList.totalPages; i++) {
          plateListByFor.push(i);
        }
        plateListResponse.push(...plateResponse.data.findPlateList.content);

        await Promise.all(
          plateListByFor.map(async (number) => {
            const plateResponsePage = await SMgraph.query({
              query: FIND_PLATE_LIST,
              variables: {
                filters: [
                  { col: 'batchId', mod: 'EQ', val: response.data.findBatch.id },
                ],
                page: { number, size: 50 },
              },
              fetchPolicy: 'no-cache',
            });
            plateListResponse.push(...plateResponsePage.data.findPlateList.content);
          }),
        );
        const plates = [];
        await Promise.all(
          plateListResponse.map(async (plate) => {
            const sampleDetailResponse = await SMgraph.query({
              query: FIND_SAMPLE_DETAIL_LIST,
              variables: {
                filters: [
                  { col: 'batch.id', mod: 'EQ', val: response.data.findBatch.id },
                  { col: 'plateId', mod: 'EQ', val: plate.plateId },
                ],
                page: { number: 1, size: plateSize },
              },
              fetchPolicy: 'no-cache',
            });

            plates.push({
              plateId: plate.plateId,
              sampleDetails: sampleDetailResponse.data.findSampleDetailList.content,
            });
          }),
        );
        const findBatch = {
          ...response.data.findBatch,
          plates,
          plateSize,
        };
        setBatchDetails(findBatch);
        setSNTDataForEdit(findBatch);
        const { technology, vendor, batchMarkerGroups, technologyServiceProvider } =
          findBatch;
        convertFixedMarkers({ technology, vendor, batchMarkerGroups }).then(
          (result) => {
            dispatch({
              type: SET_STEP_TWO_TECHNOLOGY,
              payload: {
                listBatchMarkerGroup: result,
                serviceProvider: {
                  id: vendor.id,
                  label: vendor.name,
                },
                technology: {
                  id: technology.id,
                  label: technology.name,
                },
                technologyServiceProvider: {
                  id: technologyServiceProvider.id,
                  controlPlate: technologyServiceProvider.controlPlate,
                  reportId: technologyServiceProvider.reportId,
                },
                isViewOnly: true,
              },
            });
          },
        );
        getRequestsByBatchId(findBatch.id);
      } catch (error) {
        const message = error?.response?.data?.metadata.status[0].messageType;
        if (message) {
          setMessage(message);
        }
        setError(true);
        throw new Error(error);
      }
    };

    if (mode === 'EDIT') findBatch();
  }, [toEditBatch]);

  const getTabsForEdit = (wellsGrid, _batchDetails) => {
    const platesTabList = [];
    for (let i = 0; i < wellsGrid.length; i++) {
      platesTabList.push({
        id: i,
        batchCode: `${wellsGrid[i].plateNumber}`,
      });
    }

    return platesTabList;
  };

  function setNumberOfColumns(plateSize) {
    switch (plateSize) {
      case 96:
        return 12;
      case 384:
        return 16;
    }
  }

  function setWellsGridObject(numberOfColumns) {
    const ROWS_OF_8 = {
      A: Array(numberOfColumns).fill(null),
      B: Array(numberOfColumns).fill(null),
      C: Array(numberOfColumns).fill(null),
      D: Array(numberOfColumns).fill(null),
      E: Array(numberOfColumns).fill(null),
      F: Array(numberOfColumns).fill(null),
      G: Array(numberOfColumns).fill(null),
      H: Array(numberOfColumns).fill(null),
    };

    switch (numberOfColumns) {
      case 12:
        return ROWS_OF_8;
    }
  }

  function getVendorControlPositions({ controlPlate }) {
    if (controlPlate.includes(',')) {
      return controlPlate
        .split(',')
        .map((item) => ({ row: item[0], column: item.substring(1) }));
    }
    return [];
  }

  async function getRequestsByBatchId(batchId) {
    const getRequestsByBatchIdResponse = await SMrest.get(
      `/batch/list-request-byBatchID?batchId=${batchId}`,
    );
    const requestIds = getRequestsByBatchIdResponse.data.result.data;

    buildRequestList(requestIds);
  }

  async function buildRequestList(requestIds) {
    let requests = [];
    if (requestIds.length) {
      for (let i = 0; i < requestIds.length; i++) {
        const requestBody = await SMgraph.query({
          query: FIND_REQUEST,
          variables: {
            id: requestIds[i].id,
          },
          fetchPolicy: 'no-cache',
        });
        requests.push(requestBody.data.findRequest);
      }
    }

    setRequests(requests);
    setLoading(false);
  }

  const setSNTDataForEdit = (data) => {
    const { vendor, technologyplatform, assayclass, markergroup } = data;

    dispatch({
      type: 'SET_SELECTED_VENDOR',
      payload: vendor,
    });

    dispatch({
      type: 'SET_SELECTED_TECHNOLOGY',
      payload: technologyplatform,
    });

    dispatch({
      type: 'SET_SELECTED_ASSAY',
      payload: assayclass,
    });

    dispatch({
      type: 'SET_SELECTED_MARKER_PANELS',
      payload: markergroup ? [markergroup] : [],
    });
  };

  const [tabs, setTabs] = useState([
    { id: 0, index: 1, name: 'Requests' },
    { id: 1, index: 2, name: 'Service & Technology' },
    { id: 2, index: 3, name: 'Plate layout' },
  ]);

  const [value, setValue] = useState(0);

  async function findBatchCodeUnderUserProfile() {
    try {
      /**
       * The Service Provider that will be used is the Service Provider under the
       * user profile. The Selected Requests should have the same Service
       * Provider as the one that is in the User Profile. Else, things wont work.
       */
      const _userProfile = userProfile;
      const { roles } = _userProfile?.permissions.memberOf;
      const isAdminOrDataManager =
        roles.includes('Admin') || roles.includes('Data Manager');

      const { service_providers } = _userProfile;
      const { serviceprovider } = checkedRequestSelection[0];

      /**
       * Admin/Data Manager can see all requests even though not under their Service
       * Provider in their profiles. So the batch creation logic is to do the same:
       * to remove the constraint regarding Service Provider by just using the
       * selected request(s) for creating a batch
       */
      let serviceProviderIDToBeUsed;
      if (isAdminOrDataManager) {
        serviceProviderIDToBeUsed = serviceprovider;
      } else {
        serviceProviderIDToBeUsed = service_providers.find(
          (item) => item.id === +serviceprovider.id,
        );
      }
      const codeBatchNew = await fetchBatchCode({
        serviceProviderId: checkedRequestSelection[0].serviceprovider.id,
        programId: [
          ...new Set(checkedRequestSelection.map((select) => select.program.id)),
        ].join(','),
        purposeId: [
          ...new Set(checkedRequestSelection.map((select) => select.purpose.id)),
        ].join(','),
        firstRequestId:
          checkedRequestSelection && checkedRequestSelection.length > 0
            ? checkedRequestSelection[0].id
            : 0,
      });
      const lastPlateNumber = 0;
      const lastBatchNumber = parseInt(codeBatchNew.split('_').pop()) - 1;

      dispatch({
        type: SET_BATCH_CODE_OBJECT,
        payload: {
          lastPlateNumber,
          lastBatchNumber,
          codeBatchNew,
          lastSampleNumber: 0,
          code: null,
        },
      });
    } catch (error) {
      console.error(error);
    }
  }

  const handleChange = (event, newValue) => {
    const VALID_TAB_INDEX = [0, 1, 2];
    const REQUESTS_TAB_INDEX = 0;
    const S_N_T_TAB_INDEX = 1;
    const PLATE_LAYOUT_TAB_INDEX = 2;

    // This is to always reset the Selected Control Type ever time there's a tab change
    dispatch({
      type: 'SET_SELECTED_CONTROL_TYPE',
      payload: null,
    });

    if (!VALID_TAB_INDEX.includes(newValue)) return;

    if (value === REQUESTS_TAB_INDEX && newValue === PLATE_LAYOUT_TAB_INDEX) {
      if (stepTwoTechnology.technologyServiceProvider) {
        setValue(newValue);
      } else {
        setWarning(true);
        return;
      }
    } else if (value === S_N_T_TAB_INDEX && newValue === PLATE_LAYOUT_TAB_INDEX) {
      if (stepTwoTechnology.technologyServiceProvider) {
        setValue(newValue);
      } else {
        setWarning(true);
        return;
      }
    }

    setValue(newValue);
  };

  const getControlId = (label) => {
    switch (label) {
      case 'Blank':
        return '1';
      case 'Random':
        return '2';
      case 'Positive':
        return '3';

      default:
        return;
    }
  };

  const generateControlSamples = (plate) => {
    const CONTROL_LIST = ['Blank', 'Random', 'Positive'];
    const M = selectedPlateSize === '96' ? 12 : 24;

    const controlSamples = [];
    let row = 1;
    let column = 1;
    let i = 0;
    while (i !== +selectedPlateSize) {
      column = 1;
      for (let j = i; j < i + M; j++) {
        if (CONTROL_LIST.includes(plate[j])) {
          const obj = {
            controlId: getControlId(plate[j]),
            row: row,
            column: column,
          };
          controlSamples.push(obj);
        }
        column++;
      }

      i += M;
      row++;
    }

    return controlSamples;
  };

  const getPlates = (cellsLabel) => {
    const plates = [];
    const { lastPlateNumber } = batchCodeObject;
    for (let i = 0; i < cellsLabel.length; i++) {
      const controlSamples = generateControlSamples(cellsLabel[i]);

      const plate = {
        plateId: i + lastPlateNumber + 1,
        controlSamples: controlSamples,
      };
      plates.push(plate);
    }

    return plates;
  };

  function submitMessagingAPI(record) {
    try {
      const BATCH_MANAGER = 4;
      sendNotification({
        recordId: record,
        jobWorkflowId: BATCH_MANAGER,
        otherParameters: {},
      });
    } catch (error) {
      throw new Error(error);
    }
  }

  const handleCreateBatch = async () => {
    try {
      setOpenBackdrop(true);
      const requestId = checkedRequestSelection.map((element) => element.id);
      // TODO: How select the service laboratory from deferent request
      await createBatchAsyncronous({
        batchName: batchCode,
        tenantID: tenantId,
        total: totalSamplesInBatch,
        cropID:
          [
            ...new Set(
              checkedRequestSelection
                .filter((requestSelected) => requestSelected.crop)
                .map((requestSelected) => requestSelected.crop.id),
            ),
          ].length === 1
            ? checkedRequestSelection[0].crop.id
            : 0,
        requestMap: requestId,
        description: batchCode,
        plates: getPlates(storedCellsLabel),
        selectedPlateFillDirection: selectedPlateFillDirection.id,
        serviceproviderID: checkedRequestSelection[0].serviceprovider.id,
        technologyID: stepTwoTechnology.technology.id,
        vendorID: stepTwoTechnology.serviceProvider.id,
        reportId: stepTwoTechnology.technologyServiceProvider.reportId,
        listBatchMarkerGroup: stepTwoTechnology.listBatchMarkerGroup.map(
          (listBatch) => ({ ...listBatch }),
        ),
        vendorControls: stepTwoTechnology.technologyServiceProvider.controlPlate,
      }).catch((error) => console.error(error));
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message:
            `Please wait, your batch: ${batchCode} is in the process of creation. Once created, you can see the batch in Batch Manager’s browser.` +
            (totalSamplesInBatch > 10000
              ? `\n As Batch Code contains more than 10K samples it may take a bit longer`
              : ``),
          translationId: 'some.id',
          severity: 'success',
        },
      });
      setTimeout(() => {
        navigate('/sm/batchmanager/view');
        dispatch({
          type: RESET_VALUES,
          payload: null,
        });
      }, 9000);
    } catch (error) {
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: 'Something went wrong.',
          translationId: 'some.id',
          severity: 'error',
        },
      });
      setTimeout(() => {
        navigate('/sm/batchmanager/view');
      }, 3000);
      console.error(error);
    }
  };

  const editMarkerGroup = async () => {
    await SMgraph.mutate({
      mutation: MODIFY_BATCH,
      variables: {
        BatchTo: {
          id: batchDetails.id,
          numMembers: batchDetails.numMembers,
          numContainers: batchDetails.numContainers,
        },
      },
    });
  };

  const modifySampleDetail = async (sampleNumber, sampleId, plateName) => {
    SMgraph.mutate({
      mutation: MODIFY_SAMPLE_DETAIL,
      variables: {
        SampleDetailTo: {
          sampleNumber: 0,
          id: sampleId,
          dataId: 0,
          entityId: 0,
          plateName: plateName,
          controltype: {
            id: 1, // SM-940 Only Blank is allowed at the moment
          },
          batch: {
            id: batchDetails.id,
          },
        },
      },
    })
      .then((response) => {
        if (response.data.modifySampleDetail) {
          const {
            sampleCode,
            column,
            row,
            request: { id: idRequest },
            batch: { name },
          } = response.data.modifySampleDetail;
          sendNotification({
            jobWorkflowId: 8,
            recordId: idRequest,
            otherParameters: {
              custom_data: {
                batch_code: sampleCode,
                well_position: `${row}${column}`,
                sample_code: name,
              },
            },
          });
        }
      })
      .catch((error) => console.error(error));
  };

  const editSamples = () => {
    for (let i = 0; i < samplesListForEdit.length; i++) {
      const { id, plateName, sampleNumber } = samplesListForEdit[i];
      modifySampleDetail(sampleNumber, id, plateName);
    }
  };

  const handleEditBatch = () => {
    try {
      editMarkerGroup();
      editSamples();

      submitMessagingAPI(batchDetails.id);
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: `Successfully edited ${batchDetails?.name}.`,
          translationId: 'some.id',
          severity: 'success',
        },
      });

      navigate('/sm/batchmanager/view');
    } catch (error) {
      dispatch({
        type: 'ALERT_MESSAGE',
        payload: {
          message: `Something went wrong.`,
          translationId: 'some.id',
          severity: 'error',
        },
      });
      navigate('/sm/batchmanager/view');
      throw new Error(error);
    }
  };

  const setVendorListData = () => {
    setVendorList(null);
  };

  const handleCloseBackdrop = () => {
    setOpenBackdrop(false);
  };

  return (
    <div data-testid={'BatchManagerFormTestId'} ref={ref}>
      {error ? (
        <LoadingGear message={message} />
      ) : (
        <Grid container>
          <AlertNotification />
          <Backdrop open={openBackdrop} onClick={handleCloseBackdrop}>
            <CircularProgress color='inherit' />
          </Backdrop>
          <Grid item xs={8}>
            <Breadcrumbs separator='>' aria-label='breadcrumb'>
              <Link color='inherit' href='/'>
                <Typography variant='subtitle1'>
                  <Box fontWeight={500}>
                    <FormattedMessage
                      id={'some.id'}
                      defaultMessage={'Batch manager'}
                    />
                  </Box>
                </Typography>
              </Link>
              <Typography variant='subtitle1'>
                <Box fontWeight={500}>
                  <FormattedMessage
                    id={'some.id'}
                    defaultMessage={'Batch details'}
                  />
                </Box>
              </Typography>
            </Breadcrumbs>
          </Grid>

          {checkedRequestSelection ? (
            <>
              <Grid container>
                <Grid item xs={8}>
                  <Typography variant='h5'>
                    <Box fontWeight={600}>
                      {mode === 'CREATE' ? (
                        <span id='span_title_on_create'>
                          <FormattedMessage
                            id={'some.id'}
                            defaultMessage={'Create Batch'}
                          />
                        </span>
                      ) : (
                        <span>
                          <FormattedMessage
                            id={'some.id'}
                            defaultMessage={'Edit Batch'}
                          />
                        </span>
                      )}
                      &nbsp;
                      {mode === 'CREATE' ? batchCode : toEditBatch.name}
                      &nbsp;
                      <span>
                        <FormattedMessage
                          id={'some.id'}
                          defaultMessage={'details'}
                        />
                      </span>
                    </Box>
                  </Typography>
                </Grid>
                <Grid item xs={4}>
                  <div>
                    {value !== 0 && (
                      <Button
                        style={{ marginLeft: '15px' }}
                        className={WHITE_BUTTON_CLASS}
                        variant='contained'
                        onClick={() => handleChange(null, value - 1)}
                      >
                        Back
                      </Button>
                    )}

                    {value === 2 ? (
                      <Button
                        style={{ marginLeft: '15px' }}
                        className={PRIMARY_BUTTON_CLASS}
                        variant='contained'
                        onClick={
                          mode === 'CREATE' ? handleCreateBatch : handleEditBatch
                        }
                        disabled={mode === 'CREATE' && !selectedPlateFillDirection}
                      >
                        Submit
                      </Button>
                    ) : (
                      <Button
                        style={{ marginLeft: '15px' }}
                        variant='contained'
                        className={PRIMARY_BUTTON_CLASS}
                        onClick={() => handleChange(null, value + 1)}
                      >
                        Next
                      </Button>
                    )}
                  </div>
                </Grid>
              </Grid>
              <Grid container>
                {loading ? (
                  <LinearProgress style={{ width: '100%' }} />
                ) : (
                  <>
                    <Grid item xs={12}>
                      <Paper square>
                        <Tabs
                          value={value}
                          onChange={handleChange}
                          indicatorColor='primary'
                        >
                          {tabs &&
                            tabs.map(({ id, name, index }) => (
                              <TabTitleAtom
                                id={id}
                                key={`tabTitle.${id}`}
                                label={name}
                                tabCount={index}
                              />
                            ))}
                        </Tabs>
                      </Paper>
                      <br />
                    </Grid>

                    <div>
                      <TabPanelAtom
                        id={`batchManagerForm.tab.request`}
                        index={0}
                        value={value}
                      >
                        {mode === 'CREATE' ? (
                          <RequestsTab />
                        ) : (
                          <RequestsTabForEdit
                            batchDetails={batchDetails}
                            currentData={toEditBatch}
                            requests={requests}
                          />
                        )}
                      </TabPanelAtom>
                      <TabPanelAtom
                        id={`batchManagerForm.tab.service`}
                        index={1}
                        value={value}
                      >
                        <ServiceAndTechnologyTab
                          batchDetails={batchDetails}
                          mode={mode}
                          requests={requests}
                          vendorList={vendorList}
                        />
                      </TabPanelAtom>
                      <TabPanelAtom
                        id={`batchManagerForm.tab.plate`}
                        index={2}
                        value={value}
                      >
                        <PlateLayoutTab
                          batchDetails={batchDetails}
                          mode={mode}
                          requests={requests}
                          setError={setError}
                          setMessage={setMessage}
                          wellsGrid={wellsGrid}
                        />
                      </TabPanelAtom>
                    </div>
                  </>
                )}
              </Grid>
            </>
          ) : (
            <>
              <br />
              <br />
              <Grid item xs={12}>
                <Typography variant='h5'>
                  No Approved Requests selected yet.
                </Typography>
              </Grid>

              <br />
              <br />

              <Grid item xs={12}>
                <Button
                  startIcon={<ArrowBackIos />}
                  component={ReactRouterLink}
                  variant='contained'
                  color='primary'
                  to={{
                    pathname: '/sm/review',
                  }}
                >
                  <FormattedMessage
                    id={'some.id'}
                    defaultMessage={'Back to Genotyping Service Manager'}
                  />
                </Button>
              </Grid>
            </>
          )}
        </Grid>
      )}
    </div>
  );
});
// Type and required properties
BatchManagerFormOrganism.propTypes = {};

export default BatchManagerFormOrganism;
