import {
  IconButton,
  Button,
  Dialog,
  DialogTitle,
  Typography,
  DialogContent,
  Paper,
  Tabs,
  Tab,
} from "@mui/material";
import { Edit } from "@mui/icons-material";
import React, { forwardRef, useState } from "react";
import { FormattedMessage } from "react-intl";
import LoadingGear from "components/atoms/LoadingGear";
import ServiceTypeInfoTab from "components/atoms/ServiceType/ServiceTypeInfoTab";
import { SMgraph } from "utils/apollo/SMclient";
import {
  CREATE_SERVICE_TYPE,
  MODIFY_SERVICE_TYPE,
} from "utils/apollo/gql/RequestManager";

function formattedTab(label, id) {
  return (
    <Typography>
      <FormattedMessage id={id} defaultMessage={label} />
    </Typography>
  );
}
function TabPanel(props) {
  const { children, value, index } = props;

  return (
    <div role="tabpanel" hidden={value !== index}>
      {value === index && <>{children}</>}
    </div>
  );
}

const ServiceTypeContent = forwardRef((props, ref) => {
  const { children, currentData, mode, ...rest } = props;
  const [value, setValue] = useState(null);
  const [open, setOpen] = useState(false);
  const [error, setError] = useState(false);
  const [message, setMessage] = useState(false);

  const handleChange = (event, value) => {
    setValue(value);
  };
  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };
  async function onSubmit(formData) {
    if (mode === "CREATE") {
      SMgraph.mutate({
        mutation: CREATE_SERVICE_TYPE,
        variables: {
          ServiceTypeTo: {
            id: 0,
            code: formData.code,
            name: formData.name,
            tenant: formData.tenant,
            description: formData.description,
          },
        },
        fetchPolicy: "no-cache",
      });
    } else {
      SMgraph.mutate({
        mutation: MODIFY_SERVICE_TYPE,
        variables: {
          ServiceTypeTo: {
            id: currentData.id,
            code: formData.code,
            name: formData.name,
            tenant: formData.tenant,
            description: formData.description,
          },
        },
      });
    }

    //history.push("/service-type");
    window.location.reload();
  }

  return (
    <div ref={ref}>
      {mode === "CREATE" ? (
        <Button
          variant="contained"
          color="secondary"
          onClick={() => {
            handleOpen();
          }}
        >
          <FormattedMessage
            id={"some.id"}
            defaultMessage={"Add Service Type"}
          />
        </Button>
      ) : (
        <IconButton
          title="View"
          color="primary"
          onClick={() => {
            handleOpen();
          }}
        >
          <Edit />
        </IconButton>
      )}

      <Dialog fullWidth={true} maxWidth="lg" open={open} onClose={handleClose}>
        {error ? (
          <LoadingGear message={message} />
        ) : (
          <>
            <DialogTitle id="responsive-dialog-title">
              <Typography variant="h6">
                <FormattedMessage
                  id={mode === "CREATE" ? "some.create.id" : "some.edit.id"}
                  defaultMessage={
                    mode === "CREATE"
                      ? "Create Service Type"
                      : `Edit Service Type: Name ${currentData.name}`
                  }
                />
              </Typography>
            </DialogTitle>
            <DialogContent>
              <Paper square>
                <Tabs value={0} onChange={handleChange}>
                  <Tab
                    label={formattedTab("INFORMATION", "some.id")}
                  />
                </Tabs>
              </Paper>
              <TabPanel value={0} index={0}>
                <br />
                <ServiceTypeInfoTab
                  currentData={currentData}
                  mode={mode}
                  value={value}
                  onSubmit={onSubmit}
                  handleClose={handleClose}
                />
              </TabPanel>
            </DialogContent>
          </>
        )}
      </Dialog>
    </div>
  );
});

export default ServiceTypeContent;
