import {
  Paper,
  Button,
  Tab,
  Tabs,
  Typography,
  Dialog,
  DialogTitle,
  DialogContent,
  IconButton,
} from '@mui/material';
import React, { forwardRef, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { SMgraph } from 'utils/apollo/SMclient';
import {
  MODIFY_SERVICE_PROVIDER,
  CREATE_SERVICE_PROVIDER,
} from 'utils/apollo/gql/RequestManager';
import { useNavigate } from 'react-router-dom';
import { Edit } from '@mui/icons-material';

function formattedTab(classes, tabCount, label, id) {
  return (
    <Typography>
      {/*<span className={classes.tabNumber}>{tabCount}</span>*/}
      <FormattedMessage id={id} defaultMessage={label} />
    </Typography>
  );
}

function TabPanel(props) {
  const { children, value, index } = props;

  return (
    <div role='tabpanel' hidden={value !== index}>
      {value === index && <>{children}</>}
    </div>
  );
}

const ServiceProviderContent = forwardRef((props, ref) => {
  const navigate = useNavigate();
  const { children, currentData, mode, ...rest } = props;
  const [value, setValue] = useState(null);
  const [open, setOpen] = useState(false);

  const handleChange = (event, value) => {
    setValue(value);
  };

  const handleOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  async function onSubmit(formData) {
    if (mode === 'CREATE') {
      SMgraph.mutate({
        mutation: CREATE_SERVICE_PROVIDER,
        variables: {
          ServiceProviderTo: {
            id: 0,
            code: formData.code,
            name: formData.name,
            tenant: formData.tenant,
            crop: { id: formData.crop.value },
          },
        },
        fetchPolicy: 'no-cache',
      });
    } else {
      SMgraph.mutate({
        mutation: MODIFY_SERVICE_PROVIDER,
        variables: {
          ServiceProviderTo: {
            id: currentData.id,
            code: formData.code,
            name: formData.name,
            tenant: formData.tenant,
          },
        },
      });
    }

    navigate('/service-provider');
    window.location.reload();
  }

  return (
    <div ref={ref}>
      {mode === 'CREATE' ? (
        <Button
          variant='contained'
          color='secondary'
          onClick={() => {
            handleOpen();
          }}
        >
          <FormattedMessage
            id={'some.id'}
            defaultMessage={'Add Service Laboratory'}
          />
        </Button>
      ) : (
        <IconButton
          title='View'
          color='primary'
          onClick={() => {
            handleOpen();
          }}
        >
          <Edit />
        </IconButton>
      )}

      <Dialog fullWidth={true} maxWidth='lg' open={open} onClose={handleClose}>
        <DialogTitle id='responsive-dialog-title'>
          <Typography variant='h6'>
            <FormattedMessage
              id={mode === 'CREATE' ? 'some.create.id' : 'some.edit.id'}
              defaultMessage={
                mode === 'CREATE'
                  ? 'Create Service Laboratory'
                  : `Edit Service Laboratory: Name ${currentData.name}`
              }
            />
          </Typography>
        </DialogTitle>

        <DialogContent>
          <Paper square>
            <Tabs value={0} onChange={handleChange}>
              <Tab
                label={formattedTab('', '0', 'INFO', 'some.id')}
                //{...a11yProps(0)}
              />
            </Tabs>
          </Paper>
          <TabPanel value={0} index={0}>
            <br />
            {/* <ServiceProviderInfoTab
              currentData={currentData}
              mode={mode}
              value={value}
              onSubmit={onSubmit}
              handleClose={handleClose}
            /> */}
          </TabPanel>
        </DialogContent>
      </Dialog>
    </div>
    //</Container>
  );
});

export default ServiceProviderContent;
