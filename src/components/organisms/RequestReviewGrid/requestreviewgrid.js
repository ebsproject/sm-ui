import React, { useState, useEffect, forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Link, useNavigate, useLocation } from 'react-router-dom';
import { Box, Dialog, DialogContent, Stack, Tooltip } from '@mui/material';
import { Edit, Visibility } from '@mui/icons-material';
import { FormattedMessage } from 'react-intl';
import AcceptRequestButton from 'components/molecules/AcceptButton';
import RejectRequestButton from 'components/molecules/RejectButton';
import { RBAC } from '@ebs/layout';
import { useDispatch, useSelector } from 'react-redux';
import { validateUserProfileBeforeRedirect } from 'helpers/requestManagerUtils';
import SelectRequestRow from 'components/atoms/SelectRequestRow';
import LoadingGear from 'components/atoms/LoadingGear';
import AlertNotification from 'components/atoms/AlertNotification';
import GridHeaderAtom from 'components/atoms/GridHeader';
import DeleteRequest from 'components/molecules/DeleteRequestButton';
import RequestListContent from 'components/atoms/RequestListContent';
import { TOKENID, GET_CORE_SYSTEM_CONTEXT } from 'utils/config';
import '@ebs/messaging';
import { Core } from '@ebs/styleguide';
const { Button, IconButton } = Core;
import MessageIcon from '@mui/icons-material/Message';
import { SET_BATCH_CODE_OBJECT } from 'stores/modules/RequestBatch';
const { graphqlUri } = GET_CORE_SYSTEM_CONTEXT();

//MAIN FUNCTION
/*
 @param props: component properties
 @param ref: reference made by React.forward
*/
const RequestReviewGridOrganism = React.forwardRef(({ userProfile }, ref) => {
  const [error, setError] = useState(false);
  const [message, setMessage] = useState(false);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  const {
    checkedRequestSelection,
    selectedAssay,
    selectedTechnology,
    selectedVendor,
    storeOriginalAvailableMarkerPanels,
    storeAvailableMarkerPanels,
    storeSelectedMarkerPanels,
    batchCode,
    batchCodeObject,
  } = useSelector(({ request_batch }) => request_batch);

  if (selectedVendor || selectedTechnology || selectedAssay) {
    dispatch({
      type: 'RESET_VALUES',
      payload: null,
    });
  }

  if (storeOriginalAvailableMarkerPanels.length) {
    dispatch({
      type: 'SET_ORIGINAL_VAILABLE_MARKER_PANELS',
      payload: [],
    });
  }

  if (storeAvailableMarkerPanels.length) {
    dispatch({
      type: 'SET_AVAILABLE_MARKER_PANELS',
      payload: [],
    });
  }

  if (storeSelectedMarkerPanels.length) {
    dispatch({
      type: 'SET_SELECTED_MARKER_PANELS',
      payload: [],
    });
  }

  if (batchCode) {
    dispatch({
      type: 'SET_BATCH_NAME',
      payload: null,
    });
  }

  if (batchCodeObject) {
    dispatch({
      type: SET_BATCH_CODE_OBJECT,
      payload: null,
    });
  }

  useEffect(() => {
    /**
     * This is to make sure to clear first any data in the selected requests
     * every mount of this component.
     */
    if (checkedRequestSelection.length > 0) {
      dispatch({
        type: 'SET_CHECKED_REQUEST_SELECTION',
        payload: [],
      });
    }
  }, []);

  const toolbarActions = (dataSelection, refresh) => {
    return (
      <Stack direction={'row'}>
        <RBAC allowedAction={'Create'}>
          <Button
            component={Link}
            to={`/sm/batchmanager/create-batch`}
            disabled={!Boolean(checkedRequestSelection.length)}
          >
            <FormattedMessage id={'some.id'} defaultMessage={'Create Batch'} />
          </Button>
        </RBAC>
        <RBAC allowedAction={'Create'}>
          <Button
            onClick={() =>
              validateUserProfileBeforeRedirect({
                userProfile,
                navigate,
                location,
                pathName: '/sm/requestmanager/create-request-service',
                dispatch,
              })
            }
          >
            <FormattedMessage id={'some.id'} defaultMessage={'Add New Request'} />
          </Button>
        </RBAC>
      </Stack>
    );
  };

  const rowActions = forwardRef(({ rowData, refresh }, ref) => {
    const [openModal, setOpenModal] = useState(false);
    return (
      <Stack direction={'row'}>
        <Box>
          <RBAC allowedAction={'Create'}>
            <SelectRequestRow rowData={rowData} />
          </RBAC>
        </Box>
        <Box>
          <RBAC allowedAction={'View'}>
            <Tooltip title='View' arrow>
              <>
                <IconButton
                  color='primary'
                  component={Link}
                  title='View'
                  to={`/sm/view-request-service/${rowData.requestCode}`}
                  state={{
                    currentData: rowData,
                    prevPath: location.pathname,
                  }}
                >
                  <Visibility />
                </IconButton>
              </>
            </Tooltip>
          </RBAC>
        </Box>
        {(rowData.status?.id == 1 ||
          rowData.status?.id == 3 ||
          rowData.status?.id == 6) && (
          <Box>
            <RBAC allowedAction={'Modify'}>
              <IconButton
                component={Link}
                title='Edit'
                color='primary'
                to={`/sm/requestmanager/edit-request-service/${rowData.requestCode}`}
                state={{
                  currentData: rowData,
                  previousPath: location.pathname,
                }}
              >
                <Edit />
              </IconButton>
            </RBAC>
          </Box>
        )}
        {(rowData.status?.id == 1 ||
          rowData.status?.id == 3 ||
          rowData.status?.id == 6) && (
          <>
            <Box>
              <RBAC allowedAction={'Accept'}>
                <AcceptRequestButton
                  rowData={rowData}
                  refresh={refresh}
                  idStatus={2}
                />
              </RBAC>
            </Box>
            {(rowData.status?.id == 1 || rowData.status?.id == 6) && (
              <Box>
                <RBAC allowedAction={'Reject'}>
                  <RejectRequestButton rowData={rowData} refresh={refresh} />
                </RBAC>
              </Box>
            )}
          </>
        )}
        {rowData.status?.id == 1 && (
          <Box>
            <RBAC allowedAction={'Delete'}>
              <DeleteRequest rowData={rowData} refresh={refresh} />
            </RBAC>
          </Box>
        )}
        <Box>
          <IconButton
            variant='contained'
            color='secondary'
            onClick={() => setOpenModal(true)}
          >
            <MessageIcon />
          </IconButton>
        </Box>
        <Dialog open={openModal} onClose={() => setOpenModal(false)}>
          <DialogContent>
            <ebs-messaging
              endPoint={graphqlUri}
              authorization_token={TOKENID()}
              data={JSON.stringify({
                type: 'record',
                recordId: rowData.id,
                name: `Genotyping Service Manager`,
                entity: 'Genotyping request',
              })}
            ></ebs-messaging>
          </DialogContent>
        </Dialog>
      </Stack>
    );
  });

  return (
    <Box ref={ref} data-testid={'RequestReviewGridId'}>
      {error ? (
        <LoadingGear message={message} />
      ) : (
        <>
          <GridHeaderAtom
            title={'Genotyping Service Manager'}
            description={'This is the browser for the Genotyping Service Manager.'}
          />
          <AlertNotification />

          <RequestListContent
            view='genotyping-service-manager'
            toolbarActions={toolbarActions}
            rowActions={rowActions}
            setError={setError}
            setMessage={setMessage}
            userProfile={userProfile}
          />
        </>
      )}
    </Box>
  );
});
// Type and required properties
RequestReviewGridOrganism.propTypes = {
  userProfile: PropTypes.object.isRequired,
};

export default RequestReviewGridOrganism;
