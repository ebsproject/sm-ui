import React, { forwardRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Core } from '@ebs/styleguide';
import {
  InputMolecule,
  SelectServiceProviderMolecule,
} from 'components/molecules/SettingFormComponents';
import { useServiceTypeFormOrganism } from './hooks/useServiceTypeFormOrganism';
const { Box, Button, Grid, Typography, CircularProgress } = Core;

const ServiceTypeOrganism = forwardRef(({ onClose, idEntity, mode }, ref) => {
  const {
    control,
    errorCreate,
    errorFetch,
    handleCancel,
    handleSave,
    handleSubmit,
    isLoadingMutation,
    isModeView,
    isSuccess,
  } = useServiceTypeFormOrganism({ onClose, idEntity, mode });

  if (errorFetch) return <Box>{errorFetch}</Box>;
  return (
    <Box
      noValidate
      autoComplete='off'
      onSubmit={handleSubmit(handleSave)}
      component='form'
      sx={{ mr: 2, ml: 2 }}
      ref={ref}
    >
      <Grid
        container
        direction='column'
        justifyContent='space-evenly'
        alignItems='stretch'
        spacing={4}
      >
        <Grid item>
          <InputMolecule
            control={control}
            id='name'
            label='Name'
            type='text'
            isRequired
            disabled={isModeView}
          />
        </Grid>
        <Grid item>
          <InputMolecule
            control={control}
            id='code'
            label='Code'
            type='text'
            isRequired
            disabled={isModeView}
          />
        </Grid>
        <Grid item>
          <InputMolecule
            control={control}
            id='description'
            label='Description'
            type='text'
            multiline
            rows={4}
            disabled={isModeView}
          />
        </Grid>
        <Grid item>
          <SelectServiceProviderMolecule
            control={control}
            errorData='Can not get service provider data'
            id='serviceProviderIds'
            isRequired
            label='Service Provider'
            multiple
            disabled={isModeView}
          />
        </Grid>
      </Grid>
      <Box sx={{ mt: 5 }} component='div'>
        <Grid
          container
          direction='row'
          justifyContent='space-between'
          alignItems='stretch'
        >
          <Grid item>
            <Button
              color='primary'
              className='hover:bg-ebs-green-900'
              onClick={handleCancel}
            >
              Cancel
            </Button>
          </Grid>
          {errorCreate ? (
            <Typography>{errorCreate}</Typography>
          ) : isLoadingMutation ? (
            <CircularProgress />
          ) : (
            <Button
              autoFocus
              color='inherit'
              className='text-white bg-ebs-green-default hover:bg-ebs-green-900'
              disabled={isLoadingMutation || isSuccess}
              type='submit'
            >
              Save
            </Button>
          )}
        </Grid>
      </Box>
    </Box>
  );
});

ServiceTypeOrganism.propTypes = {
  idEntity: PropTypes.number.isRequired,
  mode: PropTypes.oneOf(['add', 'edit', 'view']).isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ServiceTypeOrganism;
