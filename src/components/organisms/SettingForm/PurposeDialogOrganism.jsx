import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Core } from '@ebs/styleguide';
import { PurposeFormOrganism } from '.';
const { Dialog, DialogContent, DialogTitle, Divider, Grid, Typography } = Core;

const PurposeDialogOrganism = forwardRef(
  ({ onClose, idEntity, isOpen, mode }, ref) => {
    const title =
      mode === 'add'
        ? `Create a new Purpose`
        : mode === 'edit'
        ? `Update Purpose`
        : mode === 'view'
        ? `View Purpose`
        : '';
    return (
      <Dialog
        fullWidth={true}
        keepMounted
        maxWidth='lg'
        onClose={onClose}
        open={isOpen}
        ref={ref}
      >
        <DialogTitle>
          <Grid
            alignItems='stretch'
            container
            direction='row'
            justifyContent='space-between'
          >
            <Grid item>
              <Typography sx={{ ml: 2, flex: 1 }} variant='h6' component='div'>
                {title}
              </Typography>
            </Grid>
          </Grid>
        </DialogTitle>
        <Divider />
        <DialogContent>
          {mode === 'none' ? null : (
            <PurposeFormOrganism idEntity={idEntity} mode={mode} onClose={onClose} />
          )}
        </DialogContent>
      </Dialog>
    );
  },
);

PurposeDialogOrganism.propTypes = {
  idEntity: PropTypes.number.isRequired,
  isOpen: PropTypes.bool.isRequired,
  mode: PropTypes.oneOf(['none', 'add', 'edit', 'view']).isRequired,
  onClose: PropTypes.func.isRequired,
};

export default PurposeDialogOrganism;
