import React from 'react';
import PropTypes from 'prop-types';
import { Core } from '@ebs/styleguide';
import { ServiceTypeFormOrganism } from '.';
const { Dialog, DialogContent, DialogTitle, Divider, Grid, Typography } = Core;

const ServiceTypeDialogOrganism = ({ onClose, idEntity, isOpen, mode }) => {
  const title =
    mode === 'add'
      ? `Create a new Service Type`
      : mode === 'edit'
      ? `Update Service Type`
      : mode === 'view'
      ? `View Service Type`
      : '';
  return (
    <Dialog
      maxWidth='lg'
      fullWidth={true}
      open={isOpen}
      keepMounted
      onClose={onClose}
    >
      <DialogTitle>
        <Grid
          container
          direction='row'
          justifyContent='space-between'
          alignItems='stretch'
        >
          <Grid item>
            <Typography sx={{ ml: 2, flex: 1 }} variant='h6' component='div'>
              {title}
            </Typography>
          </Grid>
        </Grid>
      </DialogTitle>
      <Divider />
      <DialogContent>
        {mode === 'none' ? null : (
          <ServiceTypeFormOrganism
            onClose={onClose}
            idEntity={idEntity}
            mode={mode}
          />
        )}
      </DialogContent>
    </Dialog>
  );
};

ServiceTypeDialogOrganism.propTypes = {
  idEntity: PropTypes.number.isRequired,
  isOpen: PropTypes.bool.isRequired,
  mode: PropTypes.oneOf(['none', 'add', 'edit', 'view']).isRequired,
  onClose: PropTypes.func.isRequired,
};

export default ServiceTypeDialogOrganism;
