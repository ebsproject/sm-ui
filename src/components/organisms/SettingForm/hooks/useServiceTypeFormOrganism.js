import { useForm } from 'react-hook-form';
import { z } from 'zod';
import { zodResolver } from '@hookform/resolvers/zod';
import { useMutation, useQuery } from '@tanstack/react-query';
import { findServiceTypeById, saveServiceType } from 'utils/apollo/apis';
import { useEffect } from 'react';

const useServiceTypeFormOrganism = ({ onClose, idEntity, mode }) => {
  const disabledFetch = mode !== 'add';
  const isModeView = mode === 'view' ? true : false;

  const registerSchema = z.object({
    code: z.string().nonempty('Code is require'),
    name: z.string().nonempty('Name is require'),
    description: z.string().optional(),
    serviceProviderIds: z
      .object({
        id: z.string().nonempty(),
        label: z.string().nonempty(),
      })
      .array()
      .min(1, 'Please add some service provider'),
    oldServiceProviderIds: z.string().array().optional(),
  });

  const { control, handleSubmit, reset, setValue } = useForm({
    defaultValues: {
      code: '',
      description: '',
      name: '',
      serviceProviderIds: [],
      oldServiceProviderIds: [],
    },
    resolver: zodResolver(registerSchema),
  });

  const {
    data: serviceType,
    error: errorFetch,
    isLoading,
  } = useQuery({
    queryKey: ['serviceType', { id: idEntity }],
    queryFn: () => findServiceTypeById({ id: idEntity }),
    enabled: disabledFetch,
  });

  useEffect(() => {
    const isMounted = true;
    if (
      !isLoading &&
      !errorFetch &&
      isMounted &&
      !isSuccess &&
      serviceType.id !== 0
    ) {
      setValue('code', serviceType.code);
      setValue('name', serviceType.name);
      setValue(
        'description',
        serviceType.description ? serviceType.description : '',
      );
      setValue(
        'serviceProviderIds',
        serviceType.serviceproviders
          ? serviceType.serviceproviders.map((item) => ({
              id: item.id,
              label: item.name,
            }))
          : [],
      );
      setValue(
        'oldServiceProviderIds',
        serviceType.serviceproviders
          ? serviceType.serviceproviders.map((item) => item.id)
          : [],
      );
    }
    return () => {
      return { isMounted: false };
    };
  }, [idEntity, errorFetch, isLoading, serviceType, isSuccess]);

  const {
    error: errorCreate,
    isLoading: isLoadingMutation,
    isSuccess,
    mutate,
    reset: resetMutate,
  } = useMutation({
    mutationFn: saveServiceType,
  });

  useEffect(() => {
    const isMounted = true;
    if (isSuccess && !errorCreate && isMounted) {
      reset();
      resetMutate();
      onClose({ success: true });
    }
    return () => {
      isMounted: false;
    };
  }, [isSuccess, errorCreate]);

  const handleSave = ({ serviceProviderIds, ...data }) => {
    mutate({
      ...data,
      id: idEntity,
      serviceProviderIds: serviceProviderIds.map((item) => item.id),
    });
  };

  const handleCancel = () => {
    reset();
    onClose({ success: false });
  };
  return {
    handleCancel,
    handleSave,
    errorCreate,
    isLoadingMutation,
    isSuccess,
    control,
    handleSubmit,
    errorFetch,
    isModeView,
  };
};

export { useServiceTypeFormOrganism };
