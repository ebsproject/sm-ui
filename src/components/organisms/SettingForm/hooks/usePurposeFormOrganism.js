import { zodResolver } from '@hookform/resolvers/zod';
import { useMutation, useQuery } from '@tanstack/react-query';
import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { findPurposeById, savePurpose } from 'utils/apollo/apis';
import { z } from 'zod';

const usePurposeFormOrganism = ({ onClose, idEntity, mode }) => {
  const disabledFetch = mode !== 'add';
  const isModeView = mode === 'view' ? true : false;
  const registerSchema = z
    .object({
      code: z.string().min(1, 'Code is require'),
      description: z.string().optional(),
      markerPanelIds: z
        .object({ id: z.string(), label: z.string() })
        .array()
        .optional(),
      name: z.string().min(2, 'Name is require'),
      oldMarkerPanelIds: z.string().array().optional(),
      oldTraitCategoryIds: z.string().array().optional(),
      serviceTypeId: z.object({
        id: z.string(),
        label: z.string(),
      }),
      traitCategoryIds: z
        .object({ id: z.string(), label: z.string() })
        .array()
        .optional(),
      crop: z.object({ id: z.string(), label: z.string() }).optional(),
    })
    .superRefine((data, ctx) => {
      if (data.markerPanelIds.length === 0 && data.traitCategoryIds.length === 0) {
        ctx.addIssue({
          code: z.ZodIssueCode.invalid_arguments,
          path: ['markerPanelIds'],
          message: 'Please add some marker groups or trait categories',
        });
        ctx.addIssue({
          code: z.ZodIssueCode.invalid_arguments,
          path: ['traitCategoryIds'],
          message: 'Please add some trait categories or marker groups',
        });
      }
      if (data.markerPanelIds.length > 0 && data.traitCategoryIds.length > 0) {
        ctx.addIssue({
          code: z.ZodIssueCode.invalid_arguments,
          path: ['markerPanelIds'],
          message:
            'Cannot select marker groups and trait categories at the same time',
        });
        ctx.addIssue({
          code: z.ZodIssueCode.invalid_arguments,
          path: ['traitCategoryIds'],
          message:
            'Cannot select trait categories and marker groups at the same time',
        });
      }
    });

  const { control, handleSubmit, reset, setValue, watch } = useForm({
    defaultValues: {
      code: '',
      description: '',
      markerPanelIds: [],
      name: '',
      oldMarkerPanelIds: [],
      oldTraitCategoryIds: [],
      serviceTypeId: null,
      traitCategoryIds: [],
      crop: { id: '0', label: 'all' },
    },
    resolver: zodResolver(registerSchema),
  });

  const idCrop = watch('crop') ? watch('crop').id : 0;

  const handleClearMarkerPanel = () => {
    setValue('markerPanelIds', []);
  };

  const {
    data: purpose,
    error: errorFetch,
    isLoading,
  } = useQuery({
    queryKey: ['purpose', { id: idEntity }],
    queryFn: () => findPurposeById({ id: idEntity }),
    enabled: disabledFetch,
  });

  useEffect(() => {
    const isMounted = true;
    if (!isLoading && !errorFetch && isMounted && !isSuccess && purpose.id !== 0) {
      setValue('code', purpose.code);
      setValue('name', purpose.name);
      setValue('description', purpose.description ? purpose.description : '');
      setValue('serviceTypeId', {
        id: purpose.servicetype.id,
        label: purpose.servicetype.name,
      });
      setValue(
        'markerPanelIds',
        purpose.markerPanels
          ? purpose.markerPanels.map((item) => ({ id: item.id, label: item.name }))
          : [],
      );
      if (
        purpose.markerPanels &&
        purpose.markerPanels[0] &&
        purpose.markerPanels[0].crop
      ) {
        const crop = purpose.markerPanels[0].crop;
        setValue('crop', { id: crop.id, label: crop.name });
      }
      setValue(
        'traitCategoryIds',
        purpose.traitCategories
          ? purpose.traitCategories.map((item) => ({
              id: item.id,
              label: item.name,
            }))
          : [],
      );
      setValue(
        'oldMarkerPanelIds',
        purpose.markerPanels ? purpose.markerPanels.map((item) => item.id) : [],
      );
      setValue(
        'oldTraitCategoryIds',
        purpose.traitCategories
          ? purpose.traitCategories.map((item) => item.id)
          : [],
      );
    }
    return () => {
      isMounted: false;
    };
  }, [idEntity, isLoading, purpose]);

  const {
    error: errorCreate,
    isLoading: isLoadingMutation,
    isSuccess,
    mutate,
    reset: resetMutate,
  } = useMutation({
    mutationFn: savePurpose,
    onSuccess: (data, variables) => {},
  });

  useEffect(() => {
    const isMounted = true;
    if (isSuccess && !errorCreate && isMounted) {
      reset();
      resetMutate();
      onClose({ success: true });
    }
    return () => {
      isMounted: false;
    };
  }, [isSuccess, errorCreate]);

  const handleSave = ({
    serviceTypeId,
    markerPanelIds,
    traitCategoryIds,
    ...data
  }) => {
    mutate({
      ...data,
      id: idEntity,
      serviceTypeId: serviceTypeId.id,
      markerPanelIds: markerPanelIds.map((item) => item.id),
      traitCategoryIds: traitCategoryIds.map((item) => item.id),
    });
  };

  const handleCancel = () => {
    reset();
    onClose({ success: false });
  };

  return {
    control,
    errorCreate,
    errorFetch,
    handleCancel,
    handleClearMarkerPanel,
    handleSave,
    handleSubmit,
    idCrop,
    isLoadingMutation,
    isModeView,
    isSuccess,
  };
};

export { usePurposeFormOrganism };
