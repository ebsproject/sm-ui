import React, { forwardRef } from 'react';
import PropTypes from 'prop-types';
import { Box, Button, CircularProgress, Grid, Typography } from '@mui/material';

const ActionButtonFormOrganism = forwardRef(
  ({ handleCancel, errorCreate, isLoadingMutation, isSuccess, disabled }, ref) => {
    return (
      <Box sx={{ mt: 5 }} component='div'>
        <Grid
          container
          direction='row'
          justifyContent='space-between'
          alignItems='stretch'
        >
          <Grid item>
            <Button
              color='primary'
              className='hover:bg-ebs-green-900'
              onClick={handleCancel}
            >
              Cancel
            </Button>
          </Grid>
          {errorCreate ? (
            <Typography>{errorCreate}</Typography>
          ) : isLoadingMutation ? (
            <CircularProgress />
          ) : (
            <Button
              autoFocus
              color='secondary'
              className='text-white bg-ebs-green-default hover:bg-ebs-green-900'
              disabled={disabled || isLoadingMutation || isSuccess}
              type='submit'
              variant='contained'
            >
              Save
            </Button>
          )}
        </Grid>
      </Box>
    );
  },
);

ActionButtonFormOrganism.propTypes = {};

export default ActionButtonFormOrganism;
