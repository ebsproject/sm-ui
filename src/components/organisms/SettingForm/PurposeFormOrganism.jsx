import React, { forwardRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Core } from '@ebs/styleguide';
import {
  InputMolecule,
  SelectMarkerGroupMolecule,
  SelectServiceTypeMolecule,
  SelectTraitCategoryMolecule,
} from 'components/molecules/SettingFormComponents';
import { usePurposeFormOrganism } from './hooks/usePurposeFormOrganism';
import ActionButtonFormOrganism from './ActionButtonFormOrganism';
import SelectCropMolecule from 'components/molecules/SettingFormComponents/SelectCropMolecule';
const { Box, Grid } = Core;

const PurposeFormOrganism = forwardRef(({ onClose, idEntity, mode }, ref) => {
  const {
    control,
    errorCreate,
    errorFetch,
    handleCancel,
    handleClearMarkerPanel,
    handleSave,
    handleSubmit,
    idCrop,
    isLoadingMutation,
    isModeView,
    isSuccess,
    markerPanelIds,
  } = usePurposeFormOrganism({ onClose, idEntity, mode });

  if (errorFetch) return <Box>{errorFetch}</Box>;
  return (
    <Box
      autoComplete='off'
      component='form'
      noValidate
      onSubmit={handleSubmit(handleSave)}
      ref={ref}
      sx={{ mr: 2, ml: 2 }}
    >
      <Grid
        container
        direction='column'
        justifyContent='space-evenly'
        alignItems='stretch'
        spacing={4}
      >
        <Grid item>
          <InputMolecule
            control={control}
            id='name'
            label='Name'
            type='text'
            isRequired
            disabled={isModeView}
          />
        </Grid>
        <Grid item>
          <InputMolecule
            control={control}
            id='code'
            label='Code'
            type='text'
            isRequired
            disabled={isModeView}
          />
        </Grid>
        <Grid item>
          <InputMolecule
            control={control}
            id='description'
            label='Description'
            type='text'
            multiline
            rows={4}
            disabled={isModeView}
          />
        </Grid>
        <Grid item>
          <SelectServiceTypeMolecule
            control={control}
            disabled={isModeView}
            errorData='Error to load service type'
            id='serviceTypeId'
            isRequired
            label='Service Type'
          />
        </Grid>
        <Grid item>
          <SelectCropMolecule
            control={control}
            disabled={isModeView}
            errorData='Error to try to get the crops'
            id='crop'
            label='Crop for filter Marker Group (Not store on database)'
            callbackChange={handleClearMarkerPanel}
          />
        </Grid>
        <Grid item>
          <SelectMarkerGroupMolecule
            control={control}
            disabled={isModeView}
            errorData='Error to load marker group'
            id='markerPanelIds'
            label='Marker Group'
            multiple
            idCrop={idCrop}
          />
        </Grid>
        <Grid item>
          <SelectTraitCategoryMolecule
            control={control}
            disabled={isModeView}
            errorData='Error to load trait category'
            id='traitCategoryIds'
            label='Trait Category'
            multiple
          />
        </Grid>
      </Grid>
      <ActionButtonFormOrganism
        disabled={isModeView}
        errorCreate={errorCreate}
        handleCancel={handleCancel}
        isLoadingMutation={isLoadingMutation}
        isSuccess={isSuccess}
      />
    </Box>
  );
});

PurposeFormOrganism.propTypes = {
  idEntity: PropTypes.number.isRequired,
  mode: PropTypes.oneOf(['add', 'edit', 'view']).isRequired,
  onClose: PropTypes.func.isRequired,
};

export default PurposeFormOrganism;
