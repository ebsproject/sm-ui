import React, { useState, forwardRef } from 'react'
import { Box, Typography, IconButton, Button } from '@mui/material'
// import EbsGrid from 'ebs-grid-lib'
import { findServiceProviderList } from 'helpers/graphQLUtils'
import GridHeaderAtom from 'components/atoms/GridHeader'
import ViewServicePrivider from 'components/molecules/ViewServiceProviderListButton'
import DeleteServiceProvider from 'components/molecules/DeleteServiceProvider'
import ServiceProviderContent from 'components/organisms/ServiceProviderContents'

const ServiceProviderListGrid = forwardRef((props, ref) => {
    const [data, setData] = useState(null)

    const [page, setPage] = useState(1)
    const [totalPages, setTotalPages] = useState(null)
    const [error, setError] = useState(false)
    const [message, setMessage] = useState(false)
    
  const columns = [
    {
      Header: 'Id',
      accessor: 'id',
      hidden: true,
    },
    {
      Header: 'Code',
      accessor: 'code',
      filter: true,
    },
    {
      Header: 'Name',
      accessor: 'name',
      filter: true,
    },
    {
      Header: 'Service Type',
      accessor: 'servicetypes',
      filter: true,
    },
  ]

  const notSetLabel = (
    <Typography component="span" color="primary">
      <Box fontStyle="italic">(not set)</Box>
    </Typography>
  )

  function tableValue(value) {
    if (value === '' || value === null) return notSetLabel
    return value
  }

  const toolbarActions = (selection, refresh) => {
    return (
      <>
          <ServiceProviderContent currentData={selection} mode="CREATE" refresh={refresh}/>
      </>
    )
  }

  async function fetch(page, pageSize, columnsToFilter, value) {
    try {
      
      var { data, currentPage, totalpages } = await findServiceProviderList(
        page,
        pageSize,
        columnsToFilter,
        value
      )
      
      setData(data)
      setPage(currentPage)
      setTotalPages(totalpages)
    } catch (error) {
      const message = error?.response?.data?.metadata.status[0].messageType
      if (message) {
        setMessage(message)
      }
      setError(true)
      console.error(error)
    }
  }

  const rowActions = (rowData, refresh) => {
  return (
    <div>
        <div style={{display: 'flex'}}>
          <ViewServicePrivider rowData={rowData} refresh={refresh} />
          
            
          <ServiceProviderContent currentData={rowData} mode="EDIT" refresh={refresh}/>
            
          
          <DeleteServiceProvider rowData={rowData} refresh={refresh} />
        </div>

        
      </div>
    )
  }

    return (
        <div
            ref={ref}
        >
            <GridHeaderAtom
                title={'Service Provider List Grid'}
                description={'This is the browser for inspecting the batches.'}
            />
            {/* <EbsGrid 
                toolbar = {true}
                columns={columns}
                toolbaractions={toolbarActions}
                rowactions={rowActions}
                fetch={fetch}
                data={data}
                page={page}
                totalPages={totalPages}
                globalfilter
                pagination
            /> */}
        </div>
    )
})

export default ServiceProviderListGrid
