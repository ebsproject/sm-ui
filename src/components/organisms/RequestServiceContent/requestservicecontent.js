import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { RBAC, userContext } from '@ebs/layout';
import { FormattedMessage } from 'react-intl';
import { SMrest } from 'utils/axios/SMclient';
import { SMgraph } from 'utils/apollo/SMclient';
import { coreSystemContext } from 'utils/apollo/CSclient';
import { getUserById, getRequestCodeAPI } from 'utils/axios/CSclient';
import { updatePreviousRequestStatus } from 'helpers/graphQLUtils';
import {
  PRIMARY_BUTTON_CLASS,
  WHITE_BUTTON_CLASS,
} from 'utils/other/tailwindCSSClasses';
import {
  CREATE_REQUEST_STATUS,
  CREATE_REQUEST_TRAIT,
  CREATE_REQUEST,
  MODIFY_REQUEST,
  MODIFY_VALUE,
  CREATE_VALUE,
  FIND_REQUEST,
  FIND_TAB_LIST,
  FIND_CROP,
  FIND_PROGRAM,
  FIND_SERVICE_PROVIDER,
  FIND_REQUESTTRAIT_BY_ID,
  DELETE_REQUESTTRAIT_BY_ID,
} from 'utils/apollo/gql/RequestManager';
import ServieProviderDialog from 'components/atoms/RadioButtonDialog';
import BasicTab from 'components/atoms/CreateRequestServiceTabPanels/Basic/BasicTab';
import ServiceProviderTab from 'components/atoms/CreateRequestServiceTabPanels/Basic/ServiceProviderTab';
import EntryListTab from 'components/atoms/CreateRequestServiceTabPanels/EntryList/EntryListTab';
import ReviewTab from 'components/atoms/CreateRequestServiceTabPanels/Review/ReviewTab';
import GridHeaderAtom from 'components/atoms/GridHeader';
import LoadingGear from 'components/atoms/LoadingGear';
import { goToRequestManagerGrid } from 'helpers/requestManagerUtils';
import { formatDate } from 'utils/other/generalFunctions';
import {
  B4Rrest,
  getPersonID,
  getFullInformationPerson,
} from 'utils/axios/B4Rclient';
import { b4rInstance } from 'utils/axios';
import { useDispatch, useSelector } from 'react-redux';
import { useQuery } from '@apollo/client';
import AlertNotification from 'components/atoms/AlertNotification';
import { useNavigate } from 'react-router-dom';
import { useWorkflow } from '@ebs/components';
import { GETCONTEXT, GETUSERPROFILE } from 'utils/config';
import { Box, Grid, Paper, Stack, Toolbar } from '@mui/material';
import { checkForLocalStorageMissingFields } from 'helpers/requestManagerUtils';
import TabPanelAtom from 'components/atoms/forms/tabs/TabPanelAtom';
import { Core } from '@ebs/styleguide';
import TabTitleAtom from 'components/atoms/forms/tabs/TabTitleAtom';
import { SET_TO_EDIT_ROW_DATA } from 'stores/modules/RequestManager';
const { Button, Typography, Tabs } = Core;

const RequestServiceContentOrganism = React.forwardRef(
  ({ currentData, previousPath }, ref) => {
    const { userProfile } = useContext(userContext);
    const {
      basicTabData,
      cropId,
      cropName,
      customFields,
      germplasmOwnerEmail,
      markerPanelList,
      mode,
      numberOfSamplesPerItem,
      programId,
      programName,
      requestCode,
      requester,
      requesterEmail,
      selectedEntryList,
      serviceProviderData,
      serviceProviderId,
      tissueTypeId,
      tissueTypeName,
      toEditRowData,
      userId,
      userProfileStorage,
    } = useSelector(({ RequestManager }) => RequestManager);

    const [error, setError] = useState(false);
    const [message, setMessage] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { sendNotification } = useWorkflow();
    const [value, setValue] = useState(0);
    const [open, setOpen] = useState(false);
    const [firstLoading, setFirstLoading] = useState(true);
    const [isSubmit, setIsSubmit] = useState(false);

    const requestCodeLabel = toEditRowData?.requestCode || '';

    const { loading, data, findTabListError } = useQuery(FIND_TAB_LIST);

    const [tabs, setTabs] = useState([]);

    const load_request = async ({
      currentData,
      data,
      toEditRowData,
      cropId,
      userId,
      selectedEntryList,
    }) => {
      if (currentData && toEditRowData === null) {
        dispatch({
          type: SET_TO_EDIT_ROW_DATA,
          payload: currentData,
        });

        dispatch({
          type: 'SET_NUMBER_OF_SAMPLES_PER_ITEM',
          payload: currentData.samplesperItem,
        });
      }

      if (data) {
        setTabs(data.findTabList.content);
      }

      if (findTabListError) {
        setMessage(
          `There was a problem retrieving the findTabList graphQL call from the SM-API.`,
        );
        setError(true);
        console.error(findTabListError);
      }
      if (mode === 'CREATE' && userId && selectedEntryList === null) {
        let user_profile = userProfileStorage;
        if (
          user_profile.service_providers.length > 1 &&
          (serviceProviderId === null || cropName === null)
        )
          setOpen(true);
        else if (user_profile.service_providers.length == 1) {
          dispatch({
            type: 'SET_SERVICE_LABORATORY_ID',
            payload: user_profile.service_providers[0].id, //Default
          });
          await setEBSForm(user_profile.service_providers[0].id);
        } else {
          dispatch({
            type: 'SET_SERVICE_LABORATORY_ID',
            payload: 1, //Default
          });

          await setEBSForm(user_profile.service_providers[0].id);
        }
        await getUserPrograms(userId);
      } else if (mode === 'EDIT' && toEditRowData && selectedEntryList === null) {
        let user_profile = userProfileStorage;
        //check of serviceprovider is valid for edit
        if (user_profile?.service_providers?.length > 0) {
          var foundValue = user_profile.service_providers.filter(
            (obj) => obj.id == toEditRowData?.serviceprovider?.id,
          );
          if (foundValue.length > 0) {
          } else {
            setMessage(
              `You do not belong to the Service Provider of the request you are trying to edit. Request's Service Provider Id: ${toEditRowData.serviceprovider.id}`,
            );
            setError(true);
          }
        }
        //check of program is valid for edit
        if (user_profile?.permissions?.memberOf?.programs > 0) {
          let userprograms = user_profile.permissions.memberOf.programs;
          var foundValue = userprograms.filter(
            (obj) => obj.id == toEditRowData.program.id,
          );
          if (foundValue.length > 0) {
          } else {
            setMessage(
              `You do not belong to the Program of the request you are trying to edit. Request's Program Id: ${toEditRowData.program.id}`,
            );
            setError(true);
          }
        }

        let SPid = toEditRowData?.serviceprovider?.id;
        await setEBSForm(SPid);
        if (toEditRowData.tissuetype) {
          dispatch({
            type: 'SET_TISSUE_TYPE_ID',
            payload: toEditRowData.tissuetype.id,
          });
          dispatch({
            type: 'SET_TISSUE_TYPE_NAME',
            payload: toEditRowData.tissuetype.name,
          });
        }
        await getUserPrograms(toEditRowData.requesterOwnerId);
      }

      if (toEditRowData?.requesterEmail) {
        dispatch({
          type: 'SET_REQUESTER_EMAIL', // unused since there's no requesterEmail key
          payload: toEditRowData.requesterEmail,
        });
      }

      if (toEditRowData?.requester) {
        dispatch({
          type: 'SET_REQUESTER', // unused since there's no requester key
          payload: toEditRowData.requester,
        });
      }

      if (mode === 'EDIT' && basicTabData === null && toEditRowData) {
        let SPid = toEditRowData?.serviceprovider?.id;
        await setEBSForm(SPid);
        getBasicTabData();
        getServiceTabData();
        getCustomFields();
        getProvider();
      }

      if (selectedEntryList) {
        if (selectedEntryList.creatorDbId)
          await setGermplasmOwner(selectedEntryList.creatorDbId);
        else await setGermplasmOwner(selectedEntryList.germplasmOwnerId);
      }
      if (mode === 'EDIT' && markerPanelList.length !== 0 && firstLoading) {
        dispatch({
          type: 'SET_MARKER_PANEL_LIST',
          payload: [],
        });
        setFirstLoading(false);
      }
    };

    useEffect(() => {
      load_request({
        currentData,
        data,
        toEditRowData,
        cropId,
        userId,
        selectedEntryList,
      });
    }, [currentData, data, toEditRowData, cropId, userId, selectedEntryList]);

    // Populate tissue if is empty
    useEffect(() => {
      if (selectedEntryList && mode === 'CREATE') {
        dispatch({
          type: 'SET_TISSUE_TYPE_ID',
          payload: selectedEntryList.type === 'plot' ? 2 : 1,
        });
        dispatch({
          type: 'SET_TISSUE_TYPE_NAME',
          payload: selectedEntryList.type === 'plot' ? 'Leaf' : 'Seed',
        });
        dispatch({
          type: 'SET_NUMBER_OF_SAMPLES_PER_ITEM',
          payload: 1,
        });
      }
    }, [selectedEntryList]);

    useEffect(() => {
      async function getListName(listId) {
        try {
          let _selectedEntryList = {
            listId: currentData.listId,
            requestCode: currentData.requestCode,
            germplasmOwnerId: currentData.germplasmOwnerId,
          };
          const response = await b4rInstance.get(`/lists/${listId}`);
          if (response) {
            const listName = response.data.result.data[0].name;
            _selectedEntryList['name'] = listName;
            _selectedEntryList['memberCount'] =
              toEditRowData.totalEntities && toEditRowData.samplesperItem
                ? toEditRowData.totalEntities / toEditRowData.samplesperItem
                : 0;
            return _selectedEntryList;
          }
        } catch (error) {
          console.error(error);
        }
      }

      if (mode === 'EDIT' && selectedEntryList === null) {
        getListName(currentData.listId).then((response) => {
          dispatch({
            type: 'SET_SELECTED_ENTRY_LIST',
            payload: response,
          });
        });
      }
    }, [mode, selectedEntryList]);

    if (!requester && !requesterEmail) {
      let _userProfile = { ...userProfile };
      let missing = checkForLocalStorageMissingFields(_userProfile, dispatch);
      if (missing.length > 0) {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: `It's not possible to continue with any operation in the system, you do not have a valid user profile please contact to your administrator. Missing Information: ${missing}`,
            translationId: 'some.id',
            severity: 'warning',
          },
        });
      } else {
        let email = _userProfile.account;
        getUserLoggedIn(_userProfile.externalId, email);
      }
    }
    async function setEBSForm(spId) {
      if (spId == null) {
        spId = 1;
      }
      SMgraph.query({
        query: FIND_SERVICE_PROVIDER,
        variables: {
          id: spId,
        },
        fetchPolicy: 'no-cache',
      })
        .then(({ data }) => {
          let ebsForms = data.findServiceProvider.ebsForms;
          if (ebsForms.length && ebsForms[0].id < 3) {
            dispatch({
              type: 'SET_EBS_FORM_ID',
              payload: ebsForms[0].id,
            });
          } else {
            dispatch({
              type: 'SET_EBS_FORM_ID',
              payload: 1,
            });
          }
        })
        .catch((error) => {
          const message = error?.response?.data?.metadata.status[0].messageType;
          if (message) {
            setMessage(message);
          } else {
            setMessage(
              `There was a problem retrieving the findServiceProvider by ID: ${spId} graphQL call from the SM-API.`,
            );
          }
          setError(true);
          console.error(error);
        });
    }

    async function setEntryUser() {
      /**
       * This for getting the email of the requester
       * Update: which as of February 2022, is changed to whoever is loggedIn, not the entryList owner
       */

      try {
        let id = mode === 'CREATE' ? userId : toEditRowData.requesterOwnerId;
        const userInfo = await getUserById(id);
        if (userInfo.data) {
          dispatch({
            type: 'SET_ENTRY_USER_EMAIL',
            payload: userInfo.data.account,
          });

          dispatch({
            type: 'SET_ENTRY_USER_NAME',
            payload: userInfo.data.full_name,
          });

          dispatch({
            type: 'SET_REQUESTER_EMAIL',
            payload: userInfo.data.account,
          });

          dispatch({
            type: 'SET_REQUESTER',
            payload: userInfo.data.full_name,
          });

          return userInfo.data;
        }
      } catch (error) {
        console.error(error);
      }
    }

    async function setGermplasmOwner(id) {
      /**
       * This for getting the email of the germplasmOwner
       */

      try {
        const response = await getFullInformationPerson(id);
        if (response.data.result.data.length) {
          dispatch({
            type: 'SET_GERMPLASM_OWNER_EMAIL',
            payload: response.data.result.data[0].email,
          });

          dispatch({
            type: 'SET_GERMPLASM_OWNER',
            payload: response.data.result.data[0].personName,
          });
          return response.data.result.data[0];
        }
      } catch (error) {
        console.error(error);
      }
    }

    async function getAdminUserName() {
      try {
        const response = await getUserById(toEditRowData?.adminContactId);
        return response;
      } catch (error) {
        return null;
      }
    }

    async function getBasicTabData() {
      let data = {};
      for (const value of toEditRowData?.values) {
        data[value.field.name] = value.value;
      }
      const adminContact = await getAdminUserName();
      data['adminContact'] = adminContact;
      const entryUser = await setEntryUser();
      data['requester'] = entryUser.full_name;
      data['requesterEmail'] = entryUser.account;
      data.tissueType = 'Leaf';
      dispatch({
        type: 'SET_BASIC_TAB_DATA',
        payload: data,
      });
    }

    function getProvider() {
      /**
       * This is to retrieve the provider given the Request upon Edit Request.
       * Since in the Request body there's a value prop that contains the
       * custom forms, we can check if it include ICC field or not. We
       * can then determine if the provider is CIMMYT or IRRI.
       *
       * This is so when the user edits a Request and immediately goes to
       * the Review Tab, there will be no missing fields that comes
       * from the provider.
       *
       * ICC field        - CIMMYT provider
       * OCS Number field - IRRI provider
       */

      let cimmytProvider = false;
      for (const value of toEditRowData.values) {
        for (const key in value) {
          if (key === 'field') {
            if (value[key].name === 'idICC') {
              cimmytProvider = true;
              break;
            }
          }
        }
      }

      dispatch({
        type: 'SET_PROVIDER',
        payload: cimmytProvider ? 'CIMMYT' : 'IRRI',
      });
    }

    function getServiceTabData() {
      let data = {};
      data['purpose'] = {
        label: toEditRowData?.purpose.name,
        value: toEditRowData?.purposeId || toEditRowData?.purpose.id,
      };
      data['service'] = toEditRowData?.service
        ? {
            label: toEditRowData?.service.name,
            value: toEditRowData?.serviceId || toEditRowData?.service.id,
          }
        : null;
      data['servicetype'] = {
        label: toEditRowData?.servicetype.name,
        value: toEditRowData?.servicetypeId || toEditRowData?.servicetype.id,
      };
      data['serviceprovider'] = {
        label: toEditRowData?.serviceprovider.name,
        value: toEditRowData?.serviceproviderId || toEditRowData?.serviceprovider.id,
      };

      dispatch({
        type: 'SET_SERVICE_PROVIDER_DATA',
        payload: data,
      });
    }

    function getCustomFields() {
      dispatch({
        type: 'SET_CUSTOM_FIELDS',
        payload: toEditRowData.values,
      });
    }

    const handleChange = (event, value, data) => {
      /**
       * `event` parameter is used for detecting if the user clicks on the tab or if
       * the user used the BACK and NEXT buttons to navigate through the tabs.
       *
       * If the user clicks on a tab and the values of that tab is not yet filled
       * up, it will prompt a warning that it cannot. But if the user already
       * filled up the tabs and the user clicks on it, the tab panel can
       * now be displayed.
       */
      const ENTRY_TAB = 0;
      const BASIC_TAB = 1;
      const SERVICE_TAB = 2;
      const REVIEW_TAB = 3;

      const basicTabHasValues =
        mode === 'CREATE'
          ? basicTabData && Object.entries(basicTabData).length
          : toEditRowData;

      const serviceTabHasValues =
        mode === 'CREATE'
          ? serviceProviderData && Object.entries(serviceProviderData).length
          : toEditRowData;
      if (event) {
        if (value === BASIC_TAB && basicTabHasValues) {
          setValue(value);
        } else if (value === SERVICE_TAB && serviceTabHasValues) {
          setValue(value);
        } else if (value === ENTRY_TAB) {
          setValue(value);
        } else if (
          value === REVIEW_TAB &&
          basicTabHasValues &&
          serviceTabHasValues
        ) {
          setValue(value);
        } else
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: `The tab you're trying to access does not have values yet. Please fill it up first.`,
              translationId: 'some.id',
              severity: 'warning',
            },
          });
      } else {
        if (value === REVIEW_TAB) {
          getRequestCode(data);
        }
        setValue(value);
      }
    };

    async function getRequestCode({ purpose }) {
      const { data, error, loading } = await getRequestCodeAPI({
        purpose,
      });
      dispatch({
        type: 'SET_REQUEST_CODE',
        payload: data,
      });
    }

    function storeCustomFields(basicTabData) {
      const reformattedArray = customFields.map((obj) => {
        const objNew = { ...obj };
        Object.entries(basicTabData).forEach(([key, value]) => {
          if (obj.component.name === key) {
            objNew.value = value;
          }
        });
        return objNew;
      });
      dispatch({
        type: 'SET_CUSTOM_FIELDS',
        payload: reformattedArray,
      });
    }

    async function onSubmitCimmyt(formData, numberOfSamplesPerItem) {
      const formSave = formData;
      formSave.requester = requester;
      formSave.requesterEmail = requesterEmail;
      formSave.crop = cropName;
      formSave.program = programName;
      let tissueType = {
        value: tissueTypeId,
        label: tissueTypeName,
      };
      formSave.tissueType = tissueType;
      formSave.submitted = true;
      dispatch({
        type: 'SET_BASIC_TAB_DATA',
        payload: formSave,
      });
      dispatch({
        type: 'SET_CIMMYT_DATA',
        payload: formSave,
      });
      dispatch({
        type: 'SET_IRRI_DATA',
        payload: formSave,
      });
      dispatch({
        type: 'SET_NUMBER_OF_SAMPLES_PER_ITEM',
        payload: numberOfSamplesPerItem,
      });
      storeCustomFields(formSave);
      handleChange(null, value + 1);
    }

    async function getEntryUserInfo() {
      const id = mode === 'CREATE' ? userId : toEditRowData.requesterOwnerId;
      await setEntryUser(); //should no longer be current requester but is now the person logged in

      handleChange(null, value + 1);
    }

    async function getUserLoggedIn(id, email) {
      /**
       * This for getting the creatorDbId of the logged in user using
       * the email. creatorDbId then will be used to get the lists
       * under the logged in user.
       */
      const response = await getPersonID(email);
      if (response.data.result.data[0].personDbId) {
        dispatch({
          type: 'SET_ENTRY_USER_EMAIL',
          payload: response.data.result.data[0].email,
        });
        dispatch({
          type: 'SET_ENTRY_USER_NAME',
          payload: response.data.result.data[0].personName,
        });

        dispatch({
          type: 'SET_REQUESTER_EMAIL',
          payload: response.data.result.data[0].email,
        });

        dispatch({
          type: 'SET_REQUESTER',
          payload: response.data.result.data[0].personName,
        });

        dispatch({
          type: 'SET_USER_ID',
          payload: GETUSERPROFILE.dbId,
        });
      }
    }

    async function getUserPrograms(id) {
      try {
        if (GETUSERPROFILE) {
          if (mode === 'CREATE') {
            const arrProgram = GETUSERPROFILE.permissions.memberOf.units
              ?.filter((program) => program.type === 'Program')
              .map((program) => {
                return {
                  value: program.id,
                  label: program.name,
                  type: program.type,
                  crops: program.crops,
                };
              });
            dispatch({
              type: 'SET_PROGRAM_FULL_DETAIL',
              payload: {
                programId: arrProgram[0].value,
                programName: arrProgram[0].label,
                cropId: arrProgram[0].crops[0].id,
              },
            });

            await getCropName(arrProgram[0].crops[0].id);
          } else if (mode === 'EDIT') {
            const response = await coreSystemContext.query({
              query: FIND_PROGRAM,
              variables: {
                id: toEditRowData.program.id,
              },
              fetchPolicy: 'no-cache',
            });

            const { id, name } = response.data.findProgram;
            dispatch({
              type: 'SET_PROGRAM_ID',
              payload: id,
            });
            dispatch({
              type: 'SET_PROGRAM_NAME',
              payload: name,
            });

            dispatch({
              type: 'SET_CROP_ID',
              payload: toEditRowData.crop.id,
            });

            await getCropName(toEditRowData.crop.id);
          }

          if (GETUSERPROFILE.permissions.memberOf.units.length > 0) {
            const arrProgram = GETUSERPROFILE.permissions.memberOf.units
              ?.map((program) => {
                return {
                  value: program.id,
                  label: program.name,
                  type: program.type,
                  crops: program.crops,
                };
              })
              .filter((program) => program.type === 'Program');
            dispatch({
              type: 'SET_PROGRAM_LIST',
              payload: arrProgram,
            });
          }
        } else {
          let message = `The Core System API call ${link} did not return any program information for your profile. Please contact the cb team`;

          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: message,
              translationId: 'some.id',
              severity: 'error',
            },
          });
        }
      } catch (error) {
        console.error(error);
      }
    }

    async function getCropName(id) {
      try {
        const response = await coreSystemContext.query({
          query: FIND_CROP,
          variables: {
            id: id,
          },
          fetchPolicy: 'no-cache',
        });

        dispatch({
          type: 'SET_CROP_NAME',
          payload: response.data.findCrop?.name || 'Default',
        });
      } catch (error) {
        console.error(error);
      }
    }

    async function createValue(requestId) {
      customFields.forEach((element, index, array) => {
        submitCustomValue(requestId, element.id, element.value);
      });
    }

    async function submitCustomValue(requestId, id, value) {
      SMgraph.mutate({
        mutation: CREATE_VALUE,
        variables: {
          ValueTo: {
            id: 0,
            value: value,
            tenant: GETCONTEXT().tenantId, //CIMMYT
            field: {
              id: id,
            },
            request: {
              id: requestId,
            },
          },
        },
      })
        .then(() => {
          return true;
        })
        .catch(() => {
          return false;
        });
    }

    async function storeMemberList(listDbId, entityId, requestId) {
      try {
        const response = await B4Rrest.get(`/lists/${listDbId}/members`);
        if (
          response.data.result.data[0].entityDbId == 3 ||
          response.data.result.data[0].entityDbId == 17
        ) {
          const member = await SMrest.get(
            `/request/createRequestListMemberById?listID=${listDbId}&entityTypeId=${response.data.result.data[0].entityDbId}&requestId=${requestId}`,
            {
              headers: {
                'Access-Control-Allow-Origin': '*',
              },
            },
          );
        }
      } catch (error) {
        throw new Error(error.message);
      }
    }

    async function submitCustomValues(requestId) {
      if (createValue(requestId)) {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: 'Request created!',
            translationId: 'request.created',
            severity: 'success',
          },
        });

        /**
         * This is for not using the cached data but instead the fetch
         * function in the Request Manager to get the updated
         * changes in the Request List.
         */
        dispatch({
          type: 'SET_IS_DATABASE_UPDATED',
          payload: true,
        });

        navigate(previousPath);
      }
    }

    async function submitMessagingAPI(record, message, jobWorkflowId) {
      sendNotification({
        recordId: parseInt(record),
        jobWorkflowId: jobWorkflowId,
        otherParameters: {},
      });
    }

    async function createRequestTrait(requestId, requestTraitId, markerGroupId) {
      SMgraph.mutate({
        mutation: CREATE_REQUEST_TRAIT,
        variables: {
          RequestTraitTo: {
            id: 0,
            traitId: requestTraitId,
            markerGroupId: markerGroupId,
            request: {
              id: requestId,
            },
          },
        },
      })
        .then(() => {})
        .catch(() => {
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: 'Request failed!',
              translationId: 'request.failed',
              severity: 'error',
            },
          });
        });
    }

    async function modifyStatusRequest(requestId) {
      try {
        const STATUS_ID = 6;
        await SMgraph.mutate({
          mutation: MODIFY_REQUEST,
          variables: {
            RequestTo: {
              id: requestId,
              status: { id: STATUS_ID },
            },
          },
        });
        if (updatePreviousRequestStatus(requestId)) {
          await SMgraph.mutate({
            mutation: CREATE_REQUEST_STATUS,
            variables: {
              RequestStatusTo: {
                id: 0,
                tenant: 1,
                request: {
                  id: requestId,
                },
                status: {
                  id: STATUS_ID,
                },
              },
            },
          });
        }
      } catch (error) {
        dispatch({
          type: 'ALERT_MESSAGE',
          payload: {
            message: 'Modifying a Request failed!',
            translationId: 'some.id',
            severity: 'error',
          },
        });
        setOpen(false);
        throw new Error(error);
      }
    }

    async function deleteRequestTrait(requestId, traitId, markerPanelId) {
      SMgraph.mutate({
        mutation: FIND_REQUESTTRAIT_BY_ID,
        variables: {
          traitId: traitId,
          markerGroupId: markerPanelId,
          requestId: requestId,
        },
      })
        .then(({ data }) => {
          if (data?.findRequestTraitList?.content[0]?.id) {
            deleteRequestTraitId(data.findRequestTraitList.content[0].id);
          }
        })
        .catch(() => {
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: 'FIND_REQUESTTRAIT_BY_ID failed!',
              translationId: 'request.failed',
              severity: 'error',
            },
          });
        });
    }

    async function deleteRequestTraitId(requestTraitId) {
      let idRequestTrait = parseInt(requestTraitId);
      SMgraph.mutate({
        mutation: DELETE_REQUESTTRAIT_BY_ID,
        variables: {
          idRequestTrait: idRequestTrait,
        },
      })
        .then(({ data }) => {})
        .catch(() => {
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: 'DELETE_REQUESTTRAIT_BY_ID failed!',
              translationId: 'request.failed',
              severity: 'error',
            },
          });
        });
    }

    async function createRequestStatus(requestId) {
      SMgraph.mutate({
        mutation: CREATE_REQUEST_STATUS,
        variables: {
          RequestStatusTo: {
            id: 1,
            tenant: 1,
            comments: 'New',
            request: {
              id: requestId,
            },
            status: {
              id: 1,
            },
          },
        },
      })
        .then(() => {})
        .catch(() => {
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: 'Request failed!',
              translationId: 'request.failed',
              severity: 'error',
            },
          });
        });
    }

    async function onSubmitGeneral() {
      setIsSubmit(true);
      let serviceValue;
      let requestVars = {
        id: 0,
        requesterOwnerId: GETUSERPROFILE.dbId,
        germplasmOwnerId: selectedEntryList.creatorDbId,
        requestCode: requestCode,
        adminContactId: basicTabData?.adminContact?.value,
        listId: selectedEntryList.listDbId,
        totalEntities: Boolean(+numberOfSamplesPerItem)
          ? +numberOfSamplesPerItem * selectedEntryList.memberCount
          : selectedEntryList.memberCount,
        tenant: GETUSERPROFILE.subscription.tenants[0].id, //CIMMYT
        idCrop: cropId,
        idProgram: programId, //BW Wheat = 102, //default for SM=1
        purpose: { id: Number(serviceProviderData.purpose.value) },
        serviceprovider: {
          id: Number(serviceProviderData.serviceprovider.value),
        },
        status: { id: 1 },
        servicetype: {
          id: Number(serviceProviderData.servicetype?.value),
        },
        tissuetype: { id: tissueTypeId },
        form: {
          id: 1,
        },
        samplesperItem: numberOfSamplesPerItem || 0,
      };

      if (
        markerPanelList &&
        markerPanelList.filter((markerPanel) => markerPanel.checked).length > 0
      ) {
        requestVars.idMarkerGroup = markerPanelList
          .filter((markerPanel) => markerPanel.checked)
          .map((markerPanel) => markerPanel.id);
      }

      if (serviceProviderData?.service?.value) {
        serviceValue = { id: serviceProviderData?.service.value };
        requestVars.service = serviceValue;
      }

      SMgraph.mutate({
        mutation: CREATE_REQUEST,
        variables: {
          RequestTo: requestVars,
        },
      })
        .then((response) => {
          let record = response.data.createRequest.id;
          let message =
            'The user ' + requester + ' submitted a request to be approved by you';

          submitMessagingAPI(record, message, 1);
          createRequestStatus(response.data.createRequest.id);
          submitCustomValues(response.data.createRequest.id);
          if (serviceProviderData?.selectedTraitsList?.length > 0) {
            serviceProviderData.selectedTraitsList.forEach(
              (element, index, array) => {
                createRequestTrait(
                  response.data.createRequest.id,
                  element?.trait?.id,
                  element?.markerpanel?.id,
                );
              },
            );
          }

          if (selectedEntryList.entityId == 3 || selectedEntryList.entityId == 17) {
            storeMemberList(
              selectedEntryList.listDbId,
              selectedEntryList.entityId,
              response.data.createRequest.id,
            );
          }
        })
        .catch((error) => {
          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: 'Request failed!',
              translationId: 'request.failed',
              severity: 'error',
            },
          });
          throw new Error(error);
        });
    }

    function setFieldValue(fieldId) {
      let label = null;
      customFields.forEach((element) => {
        if (fieldId == element.id) {
          label = element.value;
        }
      });
      return label;
    }

    async function modifyValues() {
      try {
        const request = await SMgraph.query({
          query: FIND_REQUEST,
          variables: {
            id: toEditRowData.id,
          },
          fetchPolicy: 'no-cache',
        });

        if (request) {
          const customValuesForEdit = request.data.findRequest.values;
          if (customValuesForEdit.length > 0) {
            customValuesForEdit.forEach(async (customValue) => {
              if (setFieldValue(customValue.field.id) !== null) {
                await SMgraph.mutate({
                  mutation: MODIFY_VALUE,
                  variables: {
                    ValueTo: {
                      id: customValue.id,
                      value: setFieldValue(customValue.field.id),
                    },
                  },
                });
              }
            });
          } else {
            if (customValuesForEdit) submitCustomValues(toEditRowData.id);
          }
          dispatch({
            type: 'SET_IS_DATABASE_UPDATED',
            payload: true,
          });

          submitMessagingAPI(toEditRowData.id, '', 9);

          dispatch({
            type: 'ALERT_MESSAGE',
            payload: {
              message: 'Request edited!',
              translationId: 'request.edited',
              severity: 'success',
            },
          });

          navigate(previousPath);
        }
      } catch (error) {
        console.error(error);
      }
    }

    async function modifyRequest() {
      let requestVars = {
        id: toEditRowData.id,
        adminContactId: basicTabData.adminContact?.value || null,
        listId: selectedEntryList.listDbId,
        totalEntities: toEditRowData.totalEntities,
        germplasmOwnerId: toEditRowData.germplasmOwnerId,
        //requesterOwnerId: userId,
        idCrop: cropId,
        idProgram: programId,
        servicetype: { id: Number(serviceProviderData.servicetype.value) },
        serviceprovider: {
          id: Number(serviceProviderData.serviceprovider.value),
        },
        purpose: {
          id: Number(serviceProviderData.purpose.value),
        },
        tissuetype: { id: tissueTypeId },
        samplesperItem: numberOfSamplesPerItem || 0,
      };

      if (
        markerPanelList &&
        markerPanelList.filter((markerPanel) => markerPanel.checked).length > 0
      ) {
        requestVars.idMarkerGroup = markerPanelList
          .filter((markerPanel) => markerPanel.checked)
          .map((markerPanel) => markerPanel.id);
      }

      if (serviceProviderData?.service?.value) {
        requestVars.service = { id: serviceProviderData?.service.value };
      }
      SMgraph.mutate({
        mutation: MODIFY_REQUEST,
        variables: {
          RequestTo: requestVars,
        },
      })
        .then(() => {
          modifyValues();
          if (toEditRowData?.traitCustom?.length > 0) {
            if (serviceProviderData?.selectedTraitsList?.length > 0) {
              serviceProviderData.selectedTraitsList.forEach(
                (spItem, index, array) => {
                  const found = toEditRowData.traitCustom.some(
                    (editItem) =>
                      editItem.id + editItem.markerpanel?.id ===
                      spItem?.id + spItem.markerpanel?.id,
                  );
                  if (!found) {
                    createRequestTrait(
                      toEditRowData.id,
                      spItem.id,
                      spItem.markerpanel?.id,
                    );
                  }
                },
              );

              //Add new Items and Delete items not in the new ServiceProvider Data.
              //for each trait in the already saved traits list in the request
              toEditRowData.traitCustom.forEach((editItem, index, array) => {
                //check if it's no longer in the current selected
                const found = serviceProviderData.selectedTraitsList.some(
                  (spItem) =>
                    spItem?.id + spItem.markerpanel?.id ===
                    editItem?.id + editItem.markerpanel?.id,
                );
                if (!found) {
                  //if not in the current selected, then we need to delete it
                  deleteRequestTrait(
                    toEditRowData.id,
                    editItem.id,
                    editItem.markerpanel.id,
                  );
                }
              });
            } else {
              //delete all because nothing has been selected now.
              toEditRowData.traitCustom.forEach((element, index, array) => {
                deleteRequestTrait(
                  toEditRowData.id,
                  element?.id,
                  element.markerpanel.id,
                );
              });
            }
          } else if (serviceProviderData?.selectedTraitsList?.length > 0) {
            //there are no existing lists so just add if there are any newly added

            serviceProviderData.selectedTraitsList.forEach(
              (element, index, array) => {
                createRequestTrait(toEditRowData.id, element?.trait?.id);
              },
            );
          }
          modifyStatusRequest(toEditRowData.id);
        })
        .catch((error) => console.error(error));
    }

    async function editRequest() {
      setIsSubmit(true);
      try {
        await modifyRequest();
      } catch (error) {
        console.error(error);
        setIsSubmit(false);
      }
    }
    const goBackPath = () => {
      navigate(previousPath);
    };

    const disableSubmitButton = isSubmit
      ? true
      : selectedEntryList
        ? requestCode == null
          ? true
          : false
        : true;

    return (
      <div data-testid={'CreateRequestServiceTestId'} component='main' ref={ref}>
        {error ? (
          <LoadingGear message={message} />
        ) : (
          <>
            <GridHeaderAtom
              title={
                mode === 'CREATE'
                  ? 'Create Request Service'
                  : `Edit Request Service: Request Code ${requestCodeLabel}`
              }
              description={' '}
            />
            <AlertNotification />
            <Grid container>
              <Grid item xs={12} md={12}>
                {/* TODO: create a new jsx to get a function to enable or disable the tab on every state */}
                <Paper square>
                  <Tabs value={value}>
                    {tabs &&
                      tabs.map(({ id, name, index }) => (
                        <TabTitleAtom
                          id={id}
                          key={`tabTitle.${id}`}
                          label={name}
                          tabCount={index}
                        />
                      ))}
                  </Tabs>
                </Paper>
              </Grid>
              <br />
            </Grid>
            <Grid item xs={12} md={12}>
              <ServieProviderDialog
                open={open}
                setOpen={setOpen}
                setError={setError}
                setEBSForm={setEBSForm}
                setMessage={setMessage}
              />
              {value === 3 && (
                <Stack
                  direction='row'
                  justifyContent='flex-end'
                  alignItems='flex-start'
                  spacing={2}
                >
                  <Toolbar>
                    <Box pr={2}>
                      <Button
                        className={WHITE_BUTTON_CLASS}
                        onClick={() => navigate(previousPath)}
                        // onClick={() => goToRequestManagerGrid(navigate, previousPath)}
                      >
                        <Typography variant='body1'>
                          <FormattedMessage
                            id={'some.id'}
                            defaultMessage={'CANCEL'}
                          />
                        </Typography>
                      </Button>
                    </Box>
                    <Box pr={2}>
                      <Button
                        className={PRIMARY_BUTTON_CLASS}
                        variant='contained'
                        onClick={() => handleChange(null, value - 1)}
                      >
                        <Typography variant='body1'>
                          <FormattedMessage id={'some.id'} defaultMessage={'BACK'} />
                        </Typography>
                      </Button>
                      <Button
                        className={PRIMARY_BUTTON_CLASS}
                        disabled={disableSubmitButton}
                        variant='contained'
                        onClick={mode === 'CREATE' ? onSubmitGeneral : editRequest}
                      >
                        <Typography variant='body1'>
                          <FormattedMessage
                            id={'some.id'}
                            defaultMessage={'SUBMIT'}
                          />
                        </Typography>
                      </Button>
                    </Box>
                  </Toolbar>
                </Stack>
              )}
              {value === 0 && (
                <Stack
                  direction='row'
                  justifyContent='flex-end'
                  alignItems='flex-start'
                  spacing={2}
                >
                  <Toolbar>
                    <Box pr={2}>
                      <Button
                        onClick={() =>
                          goToRequestManagerGrid(navigate, previousPath)
                        }
                        className={WHITE_BUTTON_CLASS}
                      >
                        <Typography variant='body1'>
                          <FormattedMessage
                            id={'some.id'}
                            defaultMessage={'CANCEL'}
                          />
                        </Typography>
                      </Button>
                    </Box>
                    {mode === 'CREATE' ? (
                      <RBAC allowedAction={'Create'}>
                        <Button
                          disabled={
                            !Boolean(
                              selectedEntryList && cropName && germplasmOwnerEmail,
                            )
                          }
                          className={PRIMARY_BUTTON_CLASS}
                          onClick={getEntryUserInfo}
                        >
                          <Typography variant='body1'>
                            <FormattedMessage
                              id={'some.id'}
                              defaultMessage={'NEXT'}
                            />
                          </Typography>
                        </Button>
                      </RBAC>
                    ) : (
                      <RBAC allowedAction={'Modify'}>
                        <Button
                          disabled={
                            !Boolean(
                              selectedEntryList &&
                                cropName &&
                                programName &&
                                germplasmOwnerEmail,
                            )
                          }
                          className={PRIMARY_BUTTON_CLASS}
                          onClick={getEntryUserInfo}
                        >
                          <Typography variant='body1'>
                            <FormattedMessage
                              id={'some.id'}
                              defaultMessage={'NEXT'}
                            />
                          </Typography>
                        </Button>
                      </RBAC>
                    )}
                  </Toolbar>
                </Stack>
              )}
            </Grid>
            <TabPanelAtom id='entryTab' value={value} index={0}>
              <EntryListTab
                setError={setError}
                setMessage={setMessage}
                currentData={currentData}
                goBackPath={goBackPath}
              />
            </TabPanelAtom>
            <TabPanelAtom id='basicTab' value={value} index={1}>
              <BasicTab
                setError={setError}
                setMessage={setMessage}
                value={value}
                handleChange={handleChange}
                onSubmitCimmyt={onSubmitCimmyt}
                goBackPath={goBackPath}
              />
            </TabPanelAtom>
            <TabPanelAtom id={'serviceTab'} value={value} index={2}>
              <ServiceProviderTab
                setError={setError}
                setMessage={setMessage}
                value={value}
                handleChange={handleChange}
                crop={{ id: cropId, name: cropName }}
                goBackPath={goBackPath}
              />
            </TabPanelAtom>
            <TabPanelAtom id={'reviewTab'} value={value} index={3}>
              <ReviewTab />
            </TabPanelAtom>
          </>
        )}
      </div>
    );
  },
);
// Type and required properties
RequestServiceContentOrganism.propTypes = {};

export default RequestServiceContentOrganism;
