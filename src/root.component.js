import React, { useState } from 'react';
import { Provider } from 'react-redux';
import { ApolloProvider } from '@apollo/client';
import { BrowserRouter, Outlet, Route, Routes } from 'react-router-dom';
import { IntlProvider } from 'react-intl';
import { SMgraph } from 'utils/apollo/SMclient';
import { PersistGate } from 'redux-persist/integration/react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import store, { persist } from 'stores/store';
import RequestManager from 'pages/RequestManager';
import CreateRequestService from 'pages/RequestManager/pages/CreateRequestService';
import EditRequestService from 'pages/RequestManager/pages/EditRequestService';
import ViewRequestService from 'pages/RequestManager/pages/ViewRequestService';
import ViewBatch from 'pages/BatchManager/pages/ViewBatch';
import CreateBatch from 'pages/BatchManager/pages/CreateBatch';
import EditBatch from 'pages/BatchManager/pages/EditBatch';
import BatchManagerView from 'pages/BatchManager/pages/BatchManagerView';
import RequestReview from 'pages/RequestReview';
import ServicesProviderListView from 'pages/ServiceProviderList';
import UnderConstruction from 'pages/UnderConstructionView';
import MainSettingPage from 'pages/SettingPage';
import { routes } from 'utils/config';
import { Box, ThemeProvider } from '@mui/material';
import VersionAtom from 'components/atoms/Version/VersionAtom';
import { useThemeContext } from '@ebs/styleguide';

export default function App() {
  const [locale, setLocale] = useState('en');
  const [messages, setMessages] = useState(null);
  const queryClient = new QueryClient();
  const { theme } = useThemeContext();

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persist}>
        <ApolloProvider client={SMgraph}>
          <QueryClientProvider client={queryClient}>
            <IntlProvider locale={locale} messages={messages} defaultLocale='en'>
              <ThemeProvider theme={theme}>
                <BrowserRouter>
                  <Routes>
                    <Route
                      path={routes.BASE_PATH}
                      element={
                        <Box>
                          <VersionAtom />
                          <Outlet />
                        </Box>
                      }
                    >
                      <Route
                        path={routes.REQUEST_MANAGER}
                        element={<RequestManager />}
                      />
                      <Route
                        path={routes.CREATE_REQUEST_SERVICE}
                        element={<CreateRequestService />}
                      />
                      <Route path={routes.VENDOR} element={<RequestManager />} />
                      <Route path={routes.CREATE_BATCH} element={<CreateBatch />} />
                      <Route path={routes.EDIT_BATCH} element={<EditBatch />} />
                      <Route
                        path={routes.BATCH_MANAGER_VIEW}
                        element={<BatchManagerView />}
                      />
                      <Route path={routes.VIEW_BATCH} element={<ViewBatch />} />
                      <Route path={routes.REVIEW} element={<RequestReview />} />
                      <Route
                        path={routes.VIEW_SERVICE}
                        element={<ViewRequestService />}
                      />
                      <Route
                        path={routes.EDIT_REQUEST_SERVICE}
                        element={<EditRequestService />}
                      />
                      <Route
                        path={routes.SERVICE_PROVIDER}
                        element={<ServicesProviderListView />}
                      />
                      <Route
                        path={routes.UNDER_CONSTRUCTION}
                        element={<UnderConstruction />}
                      />
                      <Route
                        path={routes.CATALOGUES}
                        element={<MainSettingPage />}
                      />
                    </Route>
                  </Routes>
                </BrowserRouter>
              </ThemeProvider>
            </IntlProvider>
          </QueryClientProvider>
        </ApolloProvider>
      </PersistGate>
    </Provider>
  );
}
