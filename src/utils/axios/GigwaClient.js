import axios from 'axios';
import {TOKENID,  GIGWA_CONTEXT } from 'utils/config';


const { GigwaContext } = GIGWA_CONTEXT;


/**
 * Creates an AxiosInstance, pointing to gigwa, with or without an existing token.
 * @param {string} token
 * @returns AxiosInstance
 */
export const GigwaRest = (token) => {
  let axiosHeaders = {};
  if (token) {
    axiosHeaders = {
      headers: {
        authorization: `Bearer ${token}`,
      },
    };
  }
  return axios.create({
    // baseURL: 'sgContext',
    // baseURL: 'https://gigwa-dev.ebsproject.org/gigwa',
    baseURL: GigwaContext,
    ...axiosHeaders,
  });
};

/**
 * Generate a token. The obtained token then needs to be passed along with every request.
 * @TODO stop using the default admin account / get from env var?  generate token somewhere else?
 *
 * @returns {string} Auth token for provided user credentials.
 */
export const getToken = async () => {
  
  try {
    const response = await GigwaRest().post('/rest/gigwa/generateToken', {
      password: 'nimda',
      username: 'gigwadmin',
    });

    return response.data.token;
  } catch (err) {
    console.error(err.message);
    throw new Error(err.message);
  }
};

/**
 *
 * @param {string} token The token given when the upload was accepted.
 * @returns ProgressIndicator {
aborted	boolean,
complete	boolean,
currentStepNumber	integer($int32),
currentStepProgress	integer($int64),
error	string,
notificationEmail	string,
processId	string,
progressDescription	string,
stepCount	integer($int32)
}
 */
export const getProgress = async (token) => {
  try {
    const response = await GigwaRest(token).get(`/rest/gigwa/progress`);
    return response.data;
  } catch (err) {
    console.error(err.message);
    throw new Error(err.message);
  }
};

export const GigWarestClient = axios.create({
  baseURL: GIGWA_CONTEXT,
  headers: {
    authorization: TOKENID()
      ? `Bearer ${TOKENID()}`
      : 'Bearer eyJ4NXQiOiJaalJtWVRNd05USmpPV1U1TW1Jek1qZ3pOREkzWTJJeU1tSXlZMkV6TWpkaFpqVmlNamMwWmciLCJraWQiOiJaalJtWVRNd05USmpPV1U1TW1Jek1qZ3pOREkzWTJJeU1tSXlZMkV6TWpkaFpqVmlNamMwWmdfUlMyNTYiLCJhbGciOiJSUzI1NiJ9.eyJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC91c2VybmFtZSI6InhLNW04dUxjbWtsR3Nqb3J1aWRzelhzNnpMaHFEX1YyTTF1SUtyNl9Vd3MiLCJzdWIiOiJ4SzVtOHVMY21rbEdzam9ydWlkc3pYczZ6TGhxRF9WMk0xdUlLcjZfVXdzIiwiaHR0cDpcL1wvd3NvMi5vcmdcL2NsYWltc1wvcm9sZSI6WyIyMzdjMGM3YS1lYmU0LTRlYjYtYWM1OC1kNTc2ZmQ2MmM3YTkiLCIwYmRhNTczYi1lMTc4LTQ4MjQtOTVkMi03NDcwYjFiMzRlYjQiLCI5NGQ0MWZlMi00ZTcwLTQ3OTAtYTIzOC01YzVjMTRiMjc4MWMiLCI3ZDFhODczOS1jOWE5LTQ2MDYtODg3Ny1kYzkyZWYwNmVhNjgiLCJlMzBkOWUxMS1kNWJiLTQ1YjMtYTExOC02Mjg2ZDQ4N2RmYmYiLCIwNjY2MzYxYi1iMjk4LTRjMjMtYjBhMi00MGIyZDE1YWYyMjciLCJjNTMyNTNiZS0xYjY4LTQ0MjEtYmMxYi1jM2M5YWNlYTdhN2MiLCIxMDgwNjE1My0zZWNhLTRjODgtYjU2Yi04YzdmYzMwZDZmMGMiLCJiMDE3ZDExYi1lNDI5LTRiOTMtOWY3NC1jZWQ1MTcwMGE5NWQiLCJiY2RmOWNjMC04MDA1LTQ3NmYtYmIwNS03MWM5NmYwMzgxYTYiLCI0ZjZiODcwZS0xYjY2LTQ0YzgtYWMxYy1mMGUzNWYyNGQyODAiXSwiaXNzIjoiaHR0cHM6XC9cL2Vicy5jaW1teXQub3JnOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJhdWQiOiJUN0JWdnFVb0hUZjR2aEJVSjlkVk45emZLR1lhIiwibmJmIjoxNTkxODU1MzY0LCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9mdWxsbmFtZSI6WyJCUklPTkVTIFBFUkVZUkEiLCIgRXJuZXN0byBKb3NlIChDSU1NWVQpIl0sImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL2Rpc3BsYXlOYW1lIjoiRS5CUklPTkVTQENJTU1ZVC5PUkciLCJhenAiOiJUN0JWdnFVb0hUZjR2aEJVSjlkVk45emZLR1lhIiwic2NvcGUiOiJvcGVuaWQiLCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9lbWFpbGFkZHJlc3MiOiJFLkJSSU9ORVNAY2ltbXl0Lm9ubWljcm9zb2Z0LmNvbSIsImV4cCI6MTU5MTg1ODk2NCwiaWF0IjoxNTkxODU1MzY0LCJqdGkiOiI0NDBlMzc2OC05NTlhLTRiODgtOWYyYS04Y2NhNjMxNWVhYmUifQ.gy7fE1Wf8GFQix9KnmCFI-3IIIYn1Wiq6GeIAca7gZnn5SCH3OeNpRYdVIih3Xs0t0EJkW4YuD5LTIICMNywpVKh93FfoYVIhbw1ghDcnBmcv__VwKlIuX7CTOLjzpID3PqYfIzkEeaxdaVx5zFutcWcKzzBGpUR8FypTYvCMnpWa9RZKhUvGvhAG_KC4HsnHVDkaE1HVmuR1_fOsCJg8E2JUxVFKnBe8uF40m_wyT_MeKRQvCF-2OKqVmsbF9Qi9hH-Juf7_X2WiKfbcAj-5p85KDxocUEGK04coVU2kIb0-886G5My-4IiqqpjxDGgprg4GescWkXxYaBTootCbQ',
      'Content-Type': 'multipart/form-data',
  },
});


