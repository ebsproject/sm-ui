import axios from 'axios';
import { TOKENID, GET_SM_CONTEXT } from 'utils/config';

/**
 * Note: tokens expire. since SM UI does not really have a login system,
 * this token needs to be constantly updated by getting a new token.
 * But when this gets integrated with the Core System, the token
 * will be then handled by its auth system and the
 * will now have a value.
 */

const { sgContext } = GET_SM_CONTEXT('cb');

export const B4Rrest = axios.create({
  baseURL: sgContext,
  headers: {
    authorization: TOKENID()
      ? `Bearer ${TOKENID()}`
      : 'Bearer eyJ4NXQiOiJNRE00TnpZM05HWTBOV0ppTkdNMk1qSmtNams0TlRNME1EUmtNelkwT1RVeU56aGlNakl5WkRKaVpUQm1NbVJpTTJGbFlqSTFZbVl6WW1JelpEVmhNdyIsImtpZCI6Ik1ETTROelkzTkdZME5XSmlOR00yTWpKa01qazROVE0wTURSa016WTBPVFV5TnpoaU1qSXlaREppWlRCbU1tUmlNMkZsWWpJMVltWXpZbUl6WkRWaE13X1JTMjU2IiwiYWxnIjoiUlMyNTYifQ.eyJhdF9oYXNoIjoiMl9nUV9OVEk4a2o3ZVlHVlpPMmM5USIsImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL3VzZXJuYW1lIjoiMTE3MjE1NzM4ODM2NDYzNTg0NDUyIiwic3ViIjoiMTE3MjE1NzM4ODM2NDYzNTg0NDUyQGNhcmJvbi5zdXBlciIsImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL3Bob3RvdXJsIjoiaHR0cHM6XC9cL2xoMy5nb29nbGV1c2VyY29udGVudC5jb21cL2EtXC9BT2gxNEdqUnlFa2xFbHNieDFabGdaTjZMdWEwdzlRMGEtSTNIWkplNTdiQ0F3PXM5Ni1jIiwiYW1yIjpbIlNBTUxTU09BdXRoZW50aWNhdG9yIl0sImlzcyI6Imh0dHBzOlwvXC9zZy5lYnNwcm9qZWN0Lm9yZzo5NDQzXC9vYXV0aDJcL3Rva2VuIiwiaHR0cDpcL1wvd3NvMi5vcmdcL2NsYWltc1wvZ2l2ZW5uYW1lIjoiRG9taW5pYyIsImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL2lkZW50aXR5XC9lbWFpbFZlcmlmaWVkIjoidHJ1ZSIsImF1ZCI6IktFMko0eXBpOTRpYWJicFRFR1lFbmw4S04xUWEiLCJjX2hhc2giOiI5cnNFOFRoWUVtVEJhdmM3cU5KdU1BIiwibmJmIjoxNjI2ODQ0MjYzLCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9sb2NhbCI6ImVuIiwiaHR0cDpcL1wvd3NvMi5vcmdcL2NsYWltc1wvZnVsbG5hbWUiOiJEb21pbmljIExhcGl0YW4iLCJhenAiOiJLRTJKNHlwaTk0aWFiYnBURUdZRW5sOEtOMVFhIiwiaHR0cDpcL1wvd3NvMi5vcmdcL2NsYWltc1wvZW1haWxhZGRyZXNzIjoiZG9tbGFwaXRhbkBnbWFpbC5jb20iLCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9sYXN0bmFtZSI6IkxhcGl0YW4iLCJleHAiOjE2MjY4NDc4NjMsImlhdCI6MTYyNjg0NDI2M30.o4Kw7AvQvJW09p26fY-4VtVbnZZGOd_P7u9S7kVJQ_fxhbj-jRzphn1IQz-68Ja2dn17uiHUMUPC8piZdTwDyX2ZhEpwTB4rsgCQAxBmwANuN35d3wmoR3PN7UlmNVsd85WKg4KL1yonKCAkAd4Cfic3mnMcwL1PlXJfEjawC5bdRtD-C_Jx4rAgnZimCJPiz99mxNecYqh7KQNCgax9a148j84i-yjOW0lrW0xKvnZJN0KXfkLzVR0CDlCCUTM1z--QICfefGx4UtPAsSOVZ2XvrjecD3Qt51_VMrS_5K0g9YpiRohCGoxvzDYRbdJ20sx0DeufnDL8KmS7_jmR2g',
  },
});


export async function getPersonID(email) {
  try {
    const byEmail = {
      fields:
        'person.email | person.id AS personDbId | person.username | person.first_name AS firstName | person.last_name AS lastName | person.person_name AS personName',
      email: `equals ${email}`,
    };
    const response = await B4Rrest.post('/persons-search', byEmail);
    if (response.data.result.data[0].personDbId) {
      return response
    }
  } catch (error) {
    throw new Error(error);
  }
}

export async function getRequesterOwnerInfo(requesterOwnerId) {
  try {
    const body = {
      fields:
        'person.id AS personDbId | person.email | person.username | person.first_name AS firstName | person.last_name AS lastName | person.person_name AS personName | person.person_type AS personType | person.person_status AS personStatus | person.person_role_id AS personRoleId | person.is_active AS isActive | person.person_document AS personDocument | person.creation_timestamp AS creationTimestamp | person.creator_id AS creatorDbId | creator.person_name AS creator | person.modification_timestamp AS modificationTimestamp | person.modifier_id AS modifierDbId | modifier.person_name AS modifier',
      personDbId: `equals ${requesterOwnerId}`,
    };
    const response = await B4Rrest.post(`/persons-search`, body);
    return response;
  } catch (error) {
    throw new Error(error);
  }
}


export async function getFullInformationPerson(requesterOwnerId) {
  try {
    const body = {
      fields:
        'person.email | person.username | person.first_name AS firstName | person.last_name AS lastName | person.person_name AS personName | person.email AS personEmail | person.creator_id AS creatorDbId | person.id AS personDbId',
      personDbId: `equals ${requesterOwnerId}`,
    };
    const response = await B4Rrest.post(`/persons-search`, body);
    return response;
  } catch (error) {
    throw new Error(error);
  }
}