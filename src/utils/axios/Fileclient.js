import axios from 'axios';
import { TOKENID, GIGWA_CONTEXT } from 'utils/config';

export const FileRest = axios.create({
  baseURL: GIGWA_CONTEXT,
  //   timeout: 1000,
  headers: {
    authorization: `Bearer ${TOKENID()}`
  },
});
