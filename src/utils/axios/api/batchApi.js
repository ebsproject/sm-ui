import { CSrest } from '../CSclient';

const fetchBatchCode = async ({ serviceProviderId, purposeId, programId, firstRequestId }) => {
  const response = await CSrest.post('/sequence/next/2', {
    3: programId,
    6: purposeId,
    21: serviceProviderId,
    40: firstRequestId,
  });
  return response.data;
};

export { fetchBatchCode };
