import {
  getContext,
  getAuthState,
  getTokenId,
  getCoreSystemContext,
  getDomainContext,
  getUserProfile,
} from '@ebs/layout';
import packages from '../../package.json';
export const VERSION = packages.version;
export const GETCONTEXT = getContext;
export const GETAUTHSTATE = getAuthState;
export const GET_CORE_SYSTEM_CONTEXT = getCoreSystemContext;
export const TOKENID = getTokenId;
export const GET_SM_CONTEXT = getDomainContext;
export const GIGWA_CONTEXT = getCoreSystemContext().fileAPIUrl;
export const MARKER_DB_CONTEXT = getCoreSystemContext().markerDbUrl;
const { printoutUri } = getCoreSystemContext();
const token = getTokenId();
export { printoutUri, token };

// export const MARKER_DB_CONTEXT = 'http://localhost:8081/';
export const GETUSERPROFILE = getUserProfile();
export const GET_SM_URL =
  process.env.NODE_ENV === 'development' && process.env.REACT_APP_SM_URL
    ? process.env.REACT_APP_SM_URL
    : GET_SM_CONTEXT('sm').sgContext;

const routes = {
  BASE_PATH: '/sm',
  REQUEST_MANAGER: 'requestmanager',
  CREATE_REQUEST_SERVICE: 'requestmanager/create-request-service',
  EDIT_REQUEST_SERVICE: 'requestmanager/edit-request-service/:id',
  VIEW_SERVICE: 'view-request-service/:id',
  VENDOR: 'vendor',
  REVIEW: 'review',
  CREATE_BATCH: 'batchmanager/create-batch',
  EDIT_BATCH: 'batchmanager/edit-batch/:batchName',
  BATCH_MANAGER_VIEW: 'batchmanager/view',
  VIEW_BATCH: 'batchmanager/view/:batchName',
  SERVICE_PROVIDER: 'service-provider',
  SERVICE_TYPE: 'service-type',
  UNDER_CONSTRUCTION: 'under-construction',
  CATALOGUES: 'settings',
};

export { routes };
