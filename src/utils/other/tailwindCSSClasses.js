export const PRIMARY_BUTTON_CLASS =
  'bg-ebs-brand-default rounded-md text-white p-3 m-2 hover:bg-ebs-brand-900';
export const PRIMARY_DENSE_BUTTON_CLASS =
  'bg-ebs-brand-default rounded-md text-white p-5 m-5 hover:bg-ebs-brand-900';
export const PRIMARY_DENSE_BUTTON_BATCH_CLASS =
    'rounded-md text-white p-5 m-5 hover:bg-ebs-brand-900';
export const WHITE_BUTTON_CLASS = 'rounded-md p-3 m-2';
