import { CSrest } from 'utils/axios/CSclient';

const idWorkflow = localStorage.getItem('workflowId')
  ? localStorage.getItem('workflowId')
  : 4;
const idNode = localStorage.getItem('workflowNodeId')
  ? localStorage.getItem('id_idNode')
  : 6;
const instanceId = localStorage.getItem('instanceId')
  ? localStorage.getItem('instanceId')
  : 0;

export const getRequestWorkflow = () => {
  return new Promise((resolve, reject) => {
    CSrest.get('/tenant/1/workflow/' + idWorkflow)
      .then((response) => {
        const count = response.data.metadata.pagination.totalCount;
        if (count > 0) {
          const data = response.data.result.data;
          const dataAdapter = [];
          data.map((element) => {
            if (
              parseInt(instanceId, 10) === 0 ||
              parseInt(element.workflowinstance_id, 10) === parseInt(instanceId, 10)
            )
              if (
                element.status === 'New' ||
                element.status === 'Rejected' ||
                element.status === 'Approved' ||
                element.status === 'Create Design'
              ) {
                dataAdapter.push(getTransformationData(element));
              }
          });
          resolve(dataAdapter);
        }
        //   }
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  });
};

const getTransformationData = (element) => {
  return {
    id: element.id,
    status: element.status,
    Request_Code: element.request_code,
    Requester: element.requester,
    Crop: element.Crop,
    Program: element.Program,
    ICC_Code: element.charge_account,
    Purpose: element.Purpose,
    Service: element.Service,
    Service_Provider: element.ServiceProvider,
    Total: element.Total,
    Tissue_Type: element.TissueType,
    Submition_Date: element.submition_date,
    Complete_by: element.CompletedBy,
    RM_Listid: element.Listid_Id,
    ServiceType: element.ServiceType,
    Crop_ID: element.Crop_ID,
    ProgramCode_Id: element.Program_Id,
    PurposeCode_Id: element.Purpose_Id,
    ServiceCode_Id: element.Service_Id,
    TissueType_Id: element.TissueType_Id,
    ServiceProvider_Id: element.ServiceProvider_Id,
    workflowinstance_id: element.workflowinstance_id,
  };
};

export const getRequestWorkflowByID = (arrID) => {
  return new Promise((resolve, reject) => {
    CSrest.get('/tenant/1/workflow/' + idWorkflow)
      .then((response) => {
        const count = response.data.metadata.pagination.totalCount;
        let dataAdapter = {};
        if (count > 0) {
          const data = response.data.result.data;
          const dataAdapter = [];
          arrID.forEach((elementID) => {
            data.map((element) => {
              if (element.id === elementID) {
                dataAdapter.push(getTransformationData(element));
              }
            });
          });
          resolve(dataAdapter);
        }
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  });
};
