import { SMrest } from 'utils/axios/SMclient';

import PropTypes from 'prop-types';

export const getRequestListBybatchID = (idBatch) => {
  return new Promise((resolve, reject) => {
    SMrest.get('/batch/list-request-byBatchID?batchId=' + idBatch)
      .then((response) => {
        resolve(response.data);
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  });
};

export const getLaboratoryReport = (row) => {
  return new Promise((resolve, reject) => {
    SMrest({
      url: '/laboratory-report/createLaboratoryReport',
      method: 'POST',
      data: {
        idBatch: row.id,
        idBatchCode: 0,
        idControl: 0,
        idPlateDirection: 0,
        idCollection: 0,
      },
      responseType: 'blob',
    }).then((response) => {
      const type = response.headers['content-type'];
      const blob = new Blob([response.data], { type: type, encoding: 'UTF-8' });
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = row.Name + '.xlsx';
      link.click();
      resolve(response);
    });
  });
};

export const saveDesignBatchByID = (
  idBatch,
  idPlateDirection,
  idControlVendor,
  idcollection,
  plates,
  idBatchCode,
) => {
  return new Promise((resolve, reject) => {
    SMrest({
      url: '/batch/createSamples',
      method: 'POST',
      data: {
        idBatch: idBatch,
        idTenant: 1, // TODO: use the tenant id from the user profile
        idBatchCode: idBatchCode,
        idVendorControl: idControlVendor,
        idPlateDirection: idPlateDirection,
        idCollection: idcollection,
        plates: plates,
      },
    })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error.response.data.metadata.status[0]);
      });
  });
};

export const getDesignListBatch = (idBatch) => {
  return new Promise((resolve, reject) => {
    SMrest.get('/batch/list-byBatchIDToDesign?batchId=' + idBatch)
      .then((response) => {
        const data = response.data.result.data;
        const result = data.map((element) => {
          return {
            id: element.id,
            Name: element.name,
            Total: element.nummembers,
            Objective: element.objective,
            num_containers: element.numcontainers,
            last_plate_number: element.lastPlate,
            first_Plate: element.fistPlate,
            updateDate: element.updateDate,
            updateBy: element.updateBy,
          };
        });
        resolve(result);
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  });
};

export const getBatchList = (idBatch) => {
  return new Promise((resolve, reject) => {
    SMrest.get('/batch/list-byBatchIDToDesign?batchId=' + idBatch)
      .then((response) => {
        resolve(response.data);
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  });
};

export const downloadBatch = (userProfile, row) => {
  return new Promise((resolve, reject) => {
    //workAround because there's a space in the json object key "full name"
    for (var key in userProfile) {
      if (key.includes('name')) {
        userProfile.fullName = userProfile[key];
      }
    }

    SMrest({
      url: '/vendor-report/createIntertekReport',
      method: 'POST',
      data: {
        batchId: row.id,
        shipmentId: 0,
        vendorId: row.vendorId,
        fullName: userProfile.fullName,
        email: userProfile.account,
        organization: userProfile.subscription.tenants[0].organization,
        crop: userProfile.subscription.tenants[0].crop_name,
      },
      responseType: 'blob',
    }).then((response) => {
      const type = response.headers['content-type'];
      const blob = new Blob([response.data], { type: type, encoding: 'UTF-8' });
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      const match = /filename="([^"]+)"/.exec(
        response.headers['content-disposition'],
      );
      if (match && match[1]) {
        link.download = match[1];
      } else {
        link.download = 'batch_' + row.batchId + '_report.xls';
      }

      link.click();
      resolve(response);
    });
  });
};

export const getBatchCode = (requests, batchCodeObject) => {
  const serviceCode = requests.map((request) => {
    if (request.service === null) return;
    else return request.service.id;
  });

  const serviceCodeWithNoUndefined = serviceCode.filter(
    (item) => item !== undefined,
  );

  return new Promise((resolve, reject) => {
    SMrest({
      url: 'batch/createBatchCode',
      method: 'POST',
      data: {
        servicecode: serviceCodeWithNoUndefined,
        program: [requests[0].program.id],
        purpose: requests[0].purpose.id,
        batchID: batchCodeObject?.id,
      },
    })
      .then((response) => {
        resolve(response.data.result);
      })
      .catch((error) => {
        reject(error);
      });
  });
};

const createBatchAsyncronous = ({
  batchName,
  tenantID,
  total,
  cropID,
  requestMap,
  description,
  plates,
  selectedPlateFillDirection,
  serviceproviderID,
  technologyID,
  vendorID,
  reportId,
  listBatchMarkerGroup,
  vendorControls,
}) => {
  return new Promise((resolve, reject) => {
    SMrest({
      url: '/batch/createSamplesTask',
      method: 'POST',
      data: {
        batch: {
          cropID,
          description,
          name: batchName,
          serviceprovider: serviceproviderID, //Update to new varaible
          technologyplataform: technologyID, //Update to new varaible
          tenantID,
          total,
          vendorId: vendorID,
          reportId, //update from new varaible
          listBatchMarkerGroup, //Update from new varaible
        },
        arrRequestID: requestMap,
        idBatch: 0,
        idTenant: tenantID,
        idPlateDirection: selectedPlateFillDirection,
        idCollection: 1,
        vendorControls, // Update with new variable
        plates,
      },
    })
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error.response.data.metadata.status[0]);
      });
  });
};

createBatchAsyncronous.prototype = {
  batchName: PropTypes.string.isRequired,
  tenantID: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  batchCodeID: PropTypes.number.isRequired,
  cropID: PropTypes.number.isRequired,
  requestMap: PropTypes.array.isRequired,
  serviceproviderID: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
  vendorID: PropTypes.number.isRequired,
  technologyID: PropTypes.number.isRequired,
  assayID: PropTypes.number.isRequired,
  markerPanelID: PropTypes.number,
  plates: PropTypes.array.isRequired,
  vendorControlID: PropTypes.number.isRequired,
  selectedPlateFillDirection: PropTypes.number.isRequired,
};
export { createBatchAsyncronous };
export const createRequestListMember = (batchId, requestId) => {
  return new Promise((resolve, reject) => {
    SMrest.get(
      '/batch/createRequestListMemberById?batchId=' +
        batchId +
        '&requestId=' +
        requestId,
    ).then((response) => {
      resolve(response);
    });
  }).catch((err) => {
    throw new Error(err.message);
  });
};

export const createRequestListMemberFromRequest = (
  listId,
  entityTypeId,
  requestId,
) => {
  return new Promise((resolve, reject) => {
    SMrest.get(
      '/request/createRequestListMemberById?listID=' +
        listId +
        '&entityTypeId=' +
        entityTypeId +
        '&requestId=' +
        requestId,
    ).then((response) => {
      resolve(response);
    });
  }).catch((err) => {
    throw new Error(err.message);
  });
};
