import React from 'react'
import { FormControl, MenuItem, Select, OutlinedInput } from '@mui/material'
import { useQuery } from '@tanstack/react-query';
import { findStatusList } from 'utils/apollo/SMclient';
export const formatDate = (date, type) => {
  let dateFormatted = null;
  let month = null;
  let day = null;
  let hours = null;
  let minutes = null;
  let seconds = null;
  // extract months
  if (date.getMonth() + 1 < 10) {
    month = `0${date.getMonth() + 1}`;
  } else {
    month = `${date.getMonth() + 1}`;
  }
  // extract days
  if (date.getDate() < 10) {
    day = `0${date.getDate()}`;
  } else {
    day = `${date.getDate()}`;
  }
  // extract hours
  if (date.getHours() < 10) {
    hours = `0${date.getHours()}`;
  } else {
    hours = `${date.getHours()}`;
  }
  // extract minutes
  if (date.getMinutes() < 10) {
    minutes = `0${date.getMinutes()}`;
  } else {
    minutes = `${date.getMinutes()}`;
  }
  // extract seconds
  if (date.getSeconds() < 10) {
    seconds = `0${date.getSeconds()}`;
  } else {
    seconds = `${date.getSeconds()}`;
  }
  // return timestamp or a date
  type
    ? (dateFormatted = `${date.getFullYear()}-${month}-${day} ${hours}:${minutes}:${seconds}`)
    : (dateFormatted = `${date.getFullYear()}-${month}-${day}`);
  return dateFormatted;
};

// Return a value for DropDown component and a null for DatePicker component
export const extractCFcodeValue = (customField) => {
  return customField.value ? customField.value : null;
};

// Return a label for DropDown component and a date formatted for DatePicker component
export const extractCFtextValue = (customField) => {
  let cfTextValue = null;
  if (typeof customField === 'string') {
    cfTextValue = customField;
  } else {
    cfTextValue = customField.label
      ? customField.label
      : formatDate(customField, true);
  }
  return cfTextValue;
};

// Extract custom fields values of a Request already created
export const extractCFValues = (cfValues) => {
  // Save an object with each CFValue
  let CFValues = {};
  cfValues.map((cf) => {
    CFValues[cf.workflownodecf.name] =
      cf.codevalue > 0 ? cf.codevalue : cf.textvalue;
  });
  return CFValues;
};

// Custom Filter for EBS_GRID_LIBRARY
export function SelectColumnFilter({
  column: { filterValue, setFilter, preFilteredRows, id },
}) {

  const {
    data: options,
    error: errorFetch,
    isLoading,
  } = useQuery({
    queryKey: ['statusRequestAll'],
    queryFn: findStatusList,
  });

  // Render a multi-select box\
  if (isLoading) {
    return <div>... </div>;
  } else if (errorFetch) {
    return <div>Error ...</div>;
  } else {

  return (
    <FormControl variant="outlined">
      <Select
        value={filterValue || ''}
        onChange={(e) => {
          setFilter(e.target.value)
        }}
        input={<OutlinedInput />}
        displayEmpty
      >
        <MenuItem value="">All</MenuItem>
        {options.map((option) => (
          <MenuItem key={option.id} value={option.id}>
            {option.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}    
}

/**
 * @deprecated This method only removed unsupported sort remove when all components integrate on single database.
 */
const getSortOrDefault = (sort) => {
  if (sort.length) {
    switch (sort[0].col) {
      case 'requester':
      case 'programName':
      case 'cropName':
      case 'traitCustomString':
      case 'markerGroup':
      case 'updatedByUser':
      case 'createByUser':
      case 'firstPlateName':
      case 'lastPlateName':
      case 'vendor':
        return { col: 'id', mod: 'DES' };
        break;
      default:
        return { ...sort[0] };
        break;
    }
  } else return { col: 'id', mod: 'DES' };
};

export { getSortOrDefault };
