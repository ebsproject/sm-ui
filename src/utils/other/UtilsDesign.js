
export const calculatePlates = (total, size, platePrefix, num_container, lastsample, idDesnsity) =>{
    let calculo = Math.floor(total/size);
    let remainder = total % size;
    let _lastsample =parseInt(lastsample, 10);
    let _totalControlXplate =  getSizeControls(idDesnsity);
    let _totalSampleXPlate = size - _totalControlXplate;
    let  arrayPlate = []
    if (remainder > 0 )
      calculo = calculo + 1  
      for (var index= 0; index < calculo; index++ ){
        if ((index+1) === calculo)
        arrayPlate.push({"id":index, "plate":platePrefix+"-P"+(num_container+index+1), 
        "startIndex":_lastsample+1, "sampleQuantity":remainder })
        else
        arrayPlate.push({"id":index, "plate":platePrefix+"-P"+(num_container+index+1), 
        "startIndex":_lastsample+1, "sampleQuantity":_totalSampleXPlate })
        _lastsample= _lastsample+ _totalSampleXPlate;
        remainder = remainder+_totalControlXplate;
      }
      return arrayPlate;
  }
  const vendor= 'Vendor';
  const getSizeControls = (idDesnsity) =>{
    let density = parseInt(idDesnsity, 10)
      switch(density){
          case 1:{
              return 2;
          }
          case 2:
              return 2;
          default: 
            return 0;
      }
  } 

 export const columns = [
    { id: 'col0', label: '', minWidth: 60 },
    { id: 'col1', label: '1', minWidth: 170 },
    { id: 'col2', label: '2', minWidth: 170 },
    { id: 'col3', label: '3', minWidth: 170 },
    { id: 'col4', label: '4', minWidth: 170 },
    { id: 'col5', label: '5', minWidth: 170 },
    { id: 'col6', label: '6', minWidth: 170 },
    { id: 'col7', label: '7', minWidth: 170 },
    { id: 'col8', label: '8', minWidth: 170 },
    { id: 'col9', label: '9', minWidth: 170 },
    { id: 'col10', label: '10', minWidth: 170 },
    { id: 'col11', label: '11', minWidth: 170 },
    { id: 'col12', label: '12', minWidth: 170 },
  ];

  function createData(col0, col1, col2, col3, col4, col5, col6, col7
    , col8, col9, col10, col11, col12) {
    
    return {col0,  col1, col2, col3, col4, col5, col6, col7
      , col8, col9, col10, col11, col12};
  }

  const zeroPad = (num, places) => String(num).padStart(places, '0');

  const setLetterColumn = (indexRow) =>{
    switch (indexRow){
        case 0:
            return'A';
        case 1:
            return'B';
        case 2:
            return'C';
        case 3:
            return'D';
        case 4:
            return'E';
        case 5:
            return'F';
        case 6:
            return'G';
        case 7:
            return'H';
        }
  }

  export const createGebericDataMatrix = (plateName, idPlate, idDesnsity, initalIndex,  maxSize) =>{
    let density = parseInt(idDesnsity, 10)
    if (density ===3 )
        density =2;
    let places =5;
    let plateformat = plateName + "-"
    let nextID=1; 
    if (idPlate === 1)
        nextID =9;
    let maxZiseCounting =0;
    let  arrayPlate = []
      for (var indexRow= 0; indexRow < 8; indexRow++ ){
         let col0="", col1="", col2="", col3="", col4="", col5="", col6="", 
         col7="", col8="" ,col9="", col10="", col11="", col12="";
         for (var indexColumn= 0; indexColumn <= 12; indexColumn++ ){ 
            switch(indexColumn){
                case 0 :
                   col0=setLetterColumn(indexRow)
                case 1 :
                    col1=maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places)
                    break
                case 2 :
                    col2=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
                case 3 :
                    col3=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
                case 4 :
                    col4=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
                case 5 :
                    col5=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
                case 6 :
                     col6=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                        break
                case 7 :
                    col7=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
                case 8 :
                    col8=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                        break
                case 9 :
                    col9=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
                case 10 :
                     col10=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
                case 11 :
                    if ( density === 1 && indexRow === 7){
                        col11= vendor;
                        maxZiseCounting--;
                        initalIndex--;
                    }else
                    col11=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
                case 12 :
                    if (( density === 1 && indexRow === 7 ) || (density === 2 && (indexRow === 7  || indexRow === 6 ))){
                        col12= vendor;
                        maxZiseCounting--;
                        initalIndex--;
                    }else
                    col12=maxZiseCounting===maxSize ?"":plateformat+zeroPad(initalIndex,places)
                    break
            }
            if (indexColumn !== 0)
            initalIndex ++;
            
            if (indexColumn !== 0)
            if (maxZiseCounting < maxSize)
                maxZiseCounting = maxZiseCounting + 1;
        }
        arrayPlate.push(createData(col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12)) 
      }
      return arrayPlate;
  }

  export const createDataBaseColumns = (plateName, idPlate, idDesnsity, startIndex, quantitySample) =>{
      let _idPlate =parseInt(idPlate, 10)
        if (_idPlate === 2){
            return createGebericDataMatrix (plateName, idPlate, idDesnsity, startIndex,  quantitySample)
        }
        if (_idPlate === 1){
          return  setValuetoPlate96ByColumns(plateName, idPlate, idDesnsity, startIndex,  quantitySample)
        }
  } 

  const setValuetoPlate96ByColumns =(plateName, idPlate, idDesnsity, initalIndex,  maxSize) =>{
      
      let density = parseInt(idDesnsity, 10)
      if (density ===3 )
      density =2;

    let places =5;
    let plateformat = plateName + "-"
    let maxZiseCounting =0;
    let  arrayPlate = createTemplatePlate96();
      for (var indexColumn= 0; indexColumn <=12 ; indexColumn++ ){
        
         for (var indexRow= 0; indexRow < 8; indexRow++ ){ 
                    switch(indexColumn){
                        case 0:
                            break
                        case 1:
                            arrayPlate[indexRow].col1= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 2:
                            arrayPlate[indexRow].col2=maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 3:
                            arrayPlate[indexRow].col3= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 4:
                            arrayPlate[indexRow].col4=maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 5:
                            arrayPlate[indexRow].col5= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 6:
                            arrayPlate[indexRow].col6= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 7:
                            arrayPlate[indexRow].col7= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 8:
                            arrayPlate[indexRow].col8= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 9:
                            arrayPlate[indexRow].col9= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 10:
                            arrayPlate[indexRow].col10= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 11:

                            if ( density === 1 && indexRow === 7){
                                arrayPlate[indexRow].col11= vendor;
                                maxZiseCounting--;
                                initalIndex--;
                            }
                            else
                                arrayPlate[indexRow].col11= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);
                        break;
                        case 12:
                            
                            if (( density === 1 && indexRow === 7 ) || (density === 2 && (indexRow === 7  || indexRow === 6 ))){
                                arrayPlate[indexRow].col12= vendor;
                                maxZiseCounting--;
                                initalIndex--;
                            }else{
                                arrayPlate[indexRow].col12= maxZiseCounting===maxSize ?"": plateformat+zeroPad(initalIndex,places);    
                            }
                            
                            break;
                    }
                    if (indexColumn !== 0)
                    if (maxZiseCounting < maxSize){
                        maxZiseCounting = maxZiseCounting + 1; 
                        initalIndex ++; 
                    }
            }
        }
        return arrayPlate;
  }

  const createTemplatePlate96 = () =>{
    let  arrayPlate = []
      for (var indexRow= 0; indexRow < 8; indexRow++ ){
         let col0="", col1="", col2="", col3="", col4="", col5="", col6="", 
         col7="", col8="" ,col9="", col10="", col11="", col12="";
         for (var indexColumn= 0; indexColumn <= 12; indexColumn++ ){ 
            switch(indexColumn){
                case 0 :
                   col0=setLetterColumn(indexRow)
                case 1 :
                    col1=""
                    break
                case 2 :
                    col2=""
                    break
                case 3 :
                    col3=""
                    break
                case 4 :
                    col4=""
                    break
                case 5 :
                    col5=""
                    break
                case 6 :
                     col6=""
                        break
                case 7 :
                    col7=""
                    break
                case 8 :
                    col8=""
                        break
                case 9 :
                    col9=""
                    break
                case 10 :
                     col10=""
                    break
                case 11 :
                    col11=""
                    break
                case 12 :
                    col12=""
                    break
            }
        }
        arrayPlate.push(createData(col0, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12)) 
      }
      return arrayPlate;
  }


  