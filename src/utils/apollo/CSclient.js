import { ApolloClient, InMemoryCache, gql } from '@apollo/client';
// get the authentication token from local storage if it exists
import { TOKENID, GET_CORE_SYSTEM_CONTEXT } from 'utils/config';

const token =
  TOKENID() ||
  'eyJ4NXQiOiJaalJtWVRNd05USmpPV1U1TW1Jek1qZ3pOREkzWTJJeU1tSXlZMkV6TWpkaFpqVmlNamMwWmciLCJraWQiOiJaalJtWVRNd05USmpPV1U1TW1Jek1qZ3pOREkzWTJJeU1tSXlZMkV6TWpkaFpqVmlNamMwWmdfUlMyNTYiLCJhbGciOiJSUzI1NiJ9.eyJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC91c2VybmFtZSI6InhLNW04dUxjbWtsR3Nqb3J1aWRzelhzNnpMaHFEX1YyTTF1SUtyNl9Vd3MiLCJzdWIiOiJ4SzVtOHVMY21rbEdzam9ydWlkc3pYczZ6TGhxRF9WMk0xdUlLcjZfVXdzIiwiaHR0cDpcL1wvd3NvMi5vcmdcL2NsYWltc1wvcm9sZSI6WyIyMzdjMGM3YS1lYmU0LTRlYjYtYWM1OC1kNTc2ZmQ2MmM3YTkiLCIwYmRhNTczYi1lMTc4LTQ4MjQtOTVkMi03NDcwYjFiMzRlYjQiLCI5NGQ0MWZlMi00ZTcwLTQ3OTAtYTIzOC01YzVjMTRiMjc4MWMiLCI3ZDFhODczOS1jOWE5LTQ2MDYtODg3Ny1kYzkyZWYwNmVhNjgiLCJlMzBkOWUxMS1kNWJiLTQ1YjMtYTExOC02Mjg2ZDQ4N2RmYmYiLCIwNjY2MzYxYi1iMjk4LTRjMjMtYjBhMi00MGIyZDE1YWYyMjciLCJjNTMyNTNiZS0xYjY4LTQ0MjEtYmMxYi1jM2M5YWNlYTdhN2MiLCIxMDgwNjE1My0zZWNhLTRjODgtYjU2Yi04YzdmYzMwZDZmMGMiLCJiMDE3ZDExYi1lNDI5LTRiOTMtOWY3NC1jZWQ1MTcwMGE5NWQiLCJiY2RmOWNjMC04MDA1LTQ3NmYtYmIwNS03MWM5NmYwMzgxYTYiLCI0ZjZiODcwZS0xYjY2LTQ0YzgtYWMxYy1mMGUzNWYyNGQyODAiXSwiaXNzIjoiaHR0cHM6XC9cL2Vicy5jaW1teXQub3JnOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJhdWQiOiJUN0JWdnFVb0hUZjR2aEJVSjlkVk45emZLR1lhIiwibmJmIjoxNTkxODU1MzY0LCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9mdWxsbmFtZSI6WyJCUklPTkVTIFBFUkVZUkEiLCIgRXJuZXN0byBKb3NlIChDSU1NWVQpIl0sImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL2Rpc3BsYXlOYW1lIjoiRS5CUklPTkVTQENJTU1ZVC5PUkciLCJhenAiOiJUN0JWdnFVb0hUZjR2aEJVSjlkVk45emZLR1lhIiwic2NvcGUiOiJvcGVuaWQiLCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9lbWFpbGFkZHJlc3MiOiJFLkJSSU9ORVNAY2ltbXl0Lm9ubWljcm9zb2Z0LmNvbSIsImV4cCI6MTU5MTg1ODk2NCwiaWF0IjoxNTkxODU1MzY0LCJqdGkiOiI0NDBlMzc2OC05NTlhLTRiODgtOWYyYS04Y2NhNjMxNWVhYmUifQ.gy7fE1Wf8GFQix9KnmCFI-3IIIYn1Wiq6GeIAca7gZnn5SCH3OeNpRYdVIih3Xs0t0EJkW4YuD5LTIICMNywpVKh93FfoYVIhbw1ghDcnBmcv__VwKlIuX7CTOLjzpID3PqYfIzkEeaxdaVx5zFutcWcKzzBGpUR8FypTYvCMnpWa9RZKhUvGvhAG_KC4HsnHVDkaE1HVmuR1_fOsCJg8E2JUxVFKnBe8uF40m_wyT_MeKRQvCF-2OKqVmsbF9Qi9hH-Juf7_X2WiKfbcAj-5p85KDxocUEGK04coVU2kIb0-886G5My-4IiqqpjxDGgprg4GescWkXxYaBTootCbQ';

let today = new Date();
let dd = String(today.getDate()).padStart(2, '0');
let mm = String(today.getMonth() + 1).padStart(2, '0');
let yyyy = today.getFullYear();
today = yyyy + '-' + mm + '-' + dd;
const { graphqlUri } = GET_CORE_SYSTEM_CONTEXT();

export const CSgraph = new ApolloClient({
  cache: new InMemoryCache({
    addTypename: false,
  }),
  uri: process.env.REACT_APP_API_GRAPHQL_TENANT,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    authorization: `Bearer ${token}`,
  },
});

export const coreSystemContext = new ApolloClient({
  cache: new InMemoryCache({
    addTypename: false,
  }),
  uri: graphqlUri,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    authorization: `Bearer ${token}`,
  },
});

const getWorkFlowEventID = (idWorflowInstance, idWorwflowNode) => {
  return new Promise((resolve, reject) => {
    const Query_WorkFlowEvent = gql`
      query FindWorkflowEvent(
        $workflowinstanceID: String!
        $workflownodeID: String!
      ) {
        findWorkflowEventList(
          filters: [
            { mod: EQ, col: "workflowinstance.id", val: $workflowinstanceID }
            { mod: EQ, col: "workflownode.id", val: $workflownodeID }
          ]
        ) {
          content {
            completed
            id
            tenant
            description
          }
        }
      }
    `;
    CSgraph.query({
      variables: {
        workflowinstanceID: idWorflowInstance,
        workflownodeID: idWorwflowNode,
      },
      query: Query_WorkFlowEvent,
    })
      .then((response) => {
        response.data.findWorkflowEventList.content.forEach((element) => {
          const Mutation_Status = gql`
            mutation ModifyWorkflowEvent($id: ID!, $completed: Date) {
              modifyWorkflowEvent(
                WorkflowEventTo: { id: $id, tenant: 1, completed: $completed }
              ) {
                id
              }
            }
          `;
          CSgraph.mutate({
            variables: { id: element.id, completed: today },
            mutation: Mutation_Status,
          })
            .then((response) => {
              resolve(response);
            })
            .catch((err) => {
              console.error(err.message);
              reject(err.message);
            });
        });
      })
      .catch((err) => {
        console.error(err.message);
        throw new Error(err.message);
      });
  });
};

export const updateStatusRequest = (row, idStatusUpdate, workflowNodeId) => {
  getWorkFlowEventID(row.workflowinstance_id, 15).then((response) => {
    const CREATE_WORKFLOW_EVENT = gql`
      mutation CreateWorkflow(
        $tenant_id: Int
        $workflowNodeId: ID!
        $workflowInstanceId: ID!
        $workflowStageId: ID!
      ) {
        createWorkflowEvent(
          WorkflowEventTo: {
            id: 0
            tenant: $tenant_id
            description: "Genotyping Event - Request"
            workflownode: { id: $workflowNodeId }
            workflowinstance: { id: $workflowInstanceId }
            workflowstage: { id: $workflowStageId }
          }
        ) {
          id
        }
      }
    `;

    CSgraph.mutate({
      variables: {
        tenant_id: 1,
        workflowNodeId: workflowNodeId,
        workflowInstanceId: row.id,
        workflowStageId: idStatusUpdate,
      },
      mutation: CREATE_WORKFLOW_EVENT,
    }).catch((err) => {
      console.error(err.message);
      throw new Error(err.message);
    });
  });
};
