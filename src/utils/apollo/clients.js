import { ApolloClient, InMemoryCache } from '@apollo/client';
import { GET_SM_URL, MARKER_DB_CONTEXT, TOKENID } from 'utils/config';

const token = TOKENID();

const MarkerGraphApolloClient = new ApolloClient({
  uri: `${MARKER_DB_CONTEXT}/graphql`,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    authorization: `Bearer ${token}`,
  },
  cache: new InMemoryCache({
    addTypename: false,
  }),
});

const ServiceManagerApolloClient = new ApolloClient({
  uri: `${GET_SM_URL}graphql`,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    authorization: `Bearer ${token}`,
  },
  cache: new InMemoryCache({
    addTypename: false,
  }),
});

export { ServiceManagerApolloClient, MarkerGraphApolloClient };
