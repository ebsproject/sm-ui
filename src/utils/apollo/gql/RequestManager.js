import { gql } from '@apollo/client';

// QUERIES FIND BY ID

export const FIND_CROP = gql`
  query findCrop($id: ID!) {
    findCrop(id: $id) {
      id
      name
    }
  }
`;

export const FIND_PROGRAM = gql`
  query findProgram($id: ID!) {
    findProgram(id: $id) {
      id
      name
    }
  }
`;

export const FIND_MARKER_GROUP = gql`
  query findMarkerGroup($id: ID!) {
    findMarkerGroup(id: $id) {
      id
      name
      assayclass {
        id
        name
      }
      markers {
        id
        name
        description
        sourcePlatformType
        chromosome
        sequence
        sequenceStartositionCsVs1
        sequenceEndPositionCdVs1
        similarSequenceChr96
        hybridEvaluation
        famAllele
        hexAllele
        primerGcAlleleX
        primerGcAlleleY
        primerGcCommon
        blastScore
        blastExpect
      }
    }
  }
`;

export const FIND_PURPOSE = gql`
  query findPurpose($id: ID!) {
    findPurpose(id: $id) {
      id
      name
      servicetype {
        id
        name
        serviceproviders {
          id
          name
        }
      }
      markerPanels {
        id
        name
        vendor {
          id
          name
        }
        crop {
          id
          name
        }
      }
      traitCategories {
        id
        name
      }
    }
  }
`;

// QUERIES FIND LIST
export const FIND_PROGRAM_LIST = gql`
  query {
    findProgramList {
      content {
        id
        name
      }
    }
  }
`;

export const FIND_REQUEST = gql`
  query findRequest($id: ID!) {
    findRequest(id: $id) {
      values {
        id
        value
        field {
          id
          label
          name
        }
      }
    }
  }
`;

export const FIND_SERVICE_PROVIDER = gql`
  query findServiceProvider($id: ID!) {
    findServiceProvider(id: $id) {
      id
      name
      code
      ebsForms {
        id
        name
      }
    }
  }
`;

// Find All Request List
export const FIND_ALL_REQUEST_LIST = gql`
  query findRequestList($filters: [FilterInput]) {
    findRequestList(page: { number: 1, size: 100 }, filters: $filters) {
      content {
        id
        requesterOwnerId
        serviceprovider {
          id
          name
        }
        crop {
          id
          name
        }
      }
    }
  }
`;

// Find All Request List
export const FIND_REQUEST_LIST_BY_ENTRY = gql`
  query findRequestList($filters: [FilterInput]) {
    findRequestList(page: { number: 1, size: 100 }, filters: $filters) {
      content {
        id
        listId
        requestCode
        requesterOwnerId
        status {
          name
        }
      }
    }
  }
`;

// Find Request List
export const FIND_REQUEST_LIST = gql`
  query findRequestList(
    $page: PageInput
    $sort: SortInput
    $filters: [FilterInput]
    $disjunctionFilters: Boolean = false
  ) {
    findRequestList(
      page: $page
      sort: $sort
      filters: $filters
      disjunctionFilters: $disjunctionFilters
    ) {
      totalPages
      totalElements
      content {
        id
        requesterOwnerId
        germplasmOwnerId
        user {
          contact {
            person {
              givenName
              familyName
              fullName
            }
          }
        }
        requestCode
        submitionDate
        adminContactId
        completedBy
        samplesperItem
        note
        listId
        totalEntities
        tenant
        requeststatuss {
          id
          comments
        }
        crop {
          id
          name
          code
        }
        program {
          id
          name
          code
        }
        form {
          name
          description
          id
          formtype {
            name
            id
          }
          fields {
            name
            label
            tooltip
            defaultValue
            isRequired
            order
            isBase
            datatype {
              name
            }
            tab {
              index
              name
            }
          }
        }
        status {
          id
          name
          color
        }
        purpose {
          id
          name
          markerPanels {
            id
            name
          }
        }
        traitCustom {
          id
          name
        }
        serviceprovider {
          id
          name
        }
        service {
          id
          name
        }
        tissuetype {
          id
          name
        }
        servicetype {
          id
          name
        }

        values {
          id
          field {
            label
            id
            name
            datatype {
              name
            }
          }
          value
        }
        batch {
          id
          name
          startDate
          resultFileId
        }
        traitCustom {
          name
          id
          traitCategory {
            id
            name
          }
          markergroups {
            id
            name
            vendor {
              id
              name
            }
          }
        }
        markerPanel {
          id
          name
          vendor {
            id
            name
          }
        }
        updatedByUser {
          userName
          contact {
            person {
              familyName
              givenName
              fullName
            }
          }
        }
        updatedOn
        createdOn
      }
    }
  }
`;
// Find Crop List
export const FIND_CROP_LIST = gql`
  query {
    findCropList {
      content {
        cropName
        id
      }
    }
  }
`;

export const FIND_ENTRY_LIST_LIST = gql`
  query {
    findPlantEntryListList {
      content {
        id
        listName
        experiementName
        occurrenceCode
        siteName
        fieldName
        experimentYear
        experimentSeason
        expEntryNumber
        plotCode
        germplasmCode
        pedigree
        seedGeneration
        seedCode
        materialType
        x
        y
        materialClass
        plantNumber
      }
    }
  }
`;

export const FIND_TAB_LIST = gql`
  query {
    findTabList(sort: { col: "index", mod: ASC }) {
      content {
        id
        index
        name
      }
    }
  }
`;

// Find Tyssue Type List
export const FIND_TYSSUE_TYPE_LIST = gql`
  query {
    findTissueTypeList {
      content {
        id
        name
        description
      }
    }
  }
`;

// Find Service Provider List
export const FIND_SERVICE_PROVIDER_LIST = gql`
  query {
    findServiceProviderList(page: { number: 1, size: 100 }) {
      content {
        id
        name
      }
    }
  }
`;

//Find Fields List

export const FIND_BASE_FIELDS = gql`
  query findBaseFields {
    findFieldList(
      page: { number: 1, size: 10 }
      sort: { col: "order", mod: ASC }
      filters: [
        { mod: EQ, col: "isBase", val: "true" }
        { mod: EQ, col: "group.id", val: "2" }
      ]
    ) {
      content {
        name
        order
        isBase
        component {
          name
          componenttype {
            name
          }
          checkprops {
            key
            value
          }
          size
          label
          helpers {
            title
            placement
            arrow
          }
          rule
        }
        isRequired
      }
    }
  }
`;
export const FIND_EBS_FORM_BY_SERVICE_PROVIDER_ID = gql`
  query findEbsFormByServiceProviderId($id: String!) {
    findEbsFormList(
      page: { number: 1, size: 10 }
      sort: { col: "order", mod: ASC }
      filters: [{ mod: EQ, col: "serviceProviders.id", val: $id }]
    ) {
      content {
        id
        name
        order
        isBase
        component {
          name
          componenttype {
            name
          }
          checkprops {
            key
            value
          }
          size
          label
          helpers {
            title
            placement
            arrow
          }
          rule
        }
        isRequired
      }
    }
  }
`;

export const FIND_CUSTOM_FIELDS_BY_EBSFORM_ID = gql`
  query findCustomFieldsByEbsFormId($id: String!) {
    findFieldList(
      page: { number: 1, size: 10 }
      sort: { col: "order", mod: ASC }
      filters: [{ mod: EQ, col: "form.id", val: $id }]
    ) {
      content {
        id
        name
        order
        isBase
        component {
          name
          componenttype {
            name
          }
          checkprops {
            key
            value
          }
          size
          label
          helpers {
            title
            placement
            arrow
          }
          rule
        }
        isRequired
      }
    }
  }
`;

// Find Service Type List
export const FIND_SERVICE_TYPE_LIST = gql`
  query {
    findServiceTypeList {
      content {
        id
        name
        code
        description
        tenant
      }
    }
  }
`;

export const FIND_SERVICE_PROVIDER_LIST_BY_SERVICE_TYPE_ID = gql`
  query findServiceType($id: ID!) {
    findServiceType(id: $id) {
      serviceproviders {
        id
        name
      }
    }
  }
`;

export const FIND_SERVICE_TYPE_BY_SERVICE_PROVIDER_ID = gql`
  query findServiceProvider($id: ID!) {
    findServiceProvider(id: $id) {
      servicetypes {
        id
        name
      }
    }
  }
`;

export const FIND_PURPOSES_BY_SERVICE_TYPE_ID = gql`
  query findPurposeList($sort: SortInput, $filters: [FilterInput]) {
    findPurposeList(sort: $sort, filters: $filters, page: { size: 100, number: 1 }) {
      content {
        id
        name
      }
    }
  }
`;

export const FIND_SERVICE_BY_PURPOSE_ID = gql`
  query findPurpose($id: ID!) {
    findPurpose(id: $id) {
      services {
        id
        name
      }
    }
  }
`;

// Find Purpose List
export const FIND_PURPOSE_LIST = gql`
  query {
    findPurposeList {
      content {
        id
        name
      }
    }
  }
`;
// Find Service List
export const FIND_SERVICE_LIST = gql`
  query {
    findServiceList {
      content {
        id
        name
      }
    }
  }
`;

export const FIND_REQUEST_STATUS_LIST = gql`
  query findRequestStatusList($sort: SortInput, $filters: [FilterInput]) {
    findRequestStatusList(sort: $sort, filters: $filters) {
      content {
        id
        comments
        completed
        request {
          id
        }
        status {
          id
          description
        }
      }
    }
  }
`;

// MUTATIONS CREATE
// Create Request
export const CREATE_REQUEST = gql`
  mutation createRequest($RequestTo: RequestInput!) {
    createRequest(RequestTo: $RequestTo) {
      id
    }
  }
`;
// Create Request
export const CREATE_REQUEST_STATUS = gql`
  mutation createRequestStatus($RequestStatusTo: RequestStatusInput!) {
    createRequestStatus(RequestStatusTo: $RequestStatusTo) {
      id
    }
  }
`;

// Create Request Trait
export const CREATE_REQUEST_TRAIT = gql`
  mutation createRequestTrait($RequestTraitTo: RequestTraitInput!) {
    createRequestTrait(RequestTraitTo: $RequestTraitTo) {
      id
    }
  }
`;

// Create Request Value fields
export const CREATE_VALUE = gql`
  mutation createValue($ValueTo: ValueInput!) {
    createValue(ValueTo: $ValueTo) {
      id
    }
  }
`;

// MUTATION MODIFY
// Modify Request
export const MODIFY_REQUEST = gql`
  mutation modifyRequest($RequestTo: RequestInput!) {
    modifyRequest(RequestTo: $RequestTo) {
      id
    }
  }
`;

export const MODIFY_REQUEST_STATUS = gql`
  mutation modifyRequestStatus($RequestStatusTo: RequestStatusInput!) {
    modifyRequestStatus(RequestStatusTo: $RequestStatusTo) {
      id
    }
  }
`;

// MUTATION MODIFY
export const MODIFY_VALUE = gql`
  mutation modifyValue($ValueTo: ValueInput!) {
    modifyValue(ValueTo: $ValueTo) {
      id
      value
    }
  }
`;

export const MODIFY_BATCH_CODE = gql`
  mutation modifyBatchCode($BatchCodeTo: BatchCodeInput!) {
    modifyBatchCode(BatchCodeTo: $BatchCodeTo) {
      id
      lastPlateNumber
      lastSampleNumber
      tenant
      lastBatchNumber
    }
  }
`;

// MUTATION DELETE
// Delete Request
export const DELETE_REQUEST = gql`
  mutation deleteRequest($id: Int!) {
    deleteRequest(idrequest: $id)
  }
`;
// MUTATIONS CREATE
// Create Service Provider
export const CREATE_SERVICE_PROVIDER = gql`
  mutation createServiceProvider($ServiceProviderTo: ServiceProviderInput!) {
    createServiceProvider(ServiceProviderTo: $ServiceProviderTo) {
      id
    }
  }
`;

// MUTATION MODIFY
// Modify Service Provider
export const MODIFY_SERVICE_PROVIDER = gql`
  mutation modifyServiceProvider($ServiceProviderTo: ServiceProviderInput!) {
    modifyServiceProvider(ServiceProviderTo: $ServiceProviderTo) {
      id
    }
  }
`;
// MUTATION DELETE
// Delete Service Provider
export const DELETE_SERVICE_PROVIDER = gql`
  mutation deleteServiceProvider($id: Int!) {
    deleteServiceProvider(idserviceprovider: $id)
  }
`;
// MUTATIONS CREATE
// Create Service Type
export const CREATE_SERVICE_TYPE = gql`
  mutation createServiceType($ServiceTypeTo: ServiceTypeInput!) {
    createServiceType(ServiceTypeTo: $ServiceTypeTo) {
      id
    }
  }
`;
// MUTATION DELETE
// Delete Service Type
export const DELETE_SERVICE_TYPE = gql`
  mutation deleteServiceType($id: Int!) {
    deleteServiceType(idservicetype: $id)
  }
`;
// MUTATION MODIFY
// Modify Service Type
export const MODIFY_SERVICE_TYPE = gql`
  mutation modifyServiceType($ServiceTypeTo: ServiceTypeInput!) {
    modifyServiceType(ServiceTypeTo: $ServiceTypeTo) {
      id
    }
  }
`;

export const FIND_BATCH_LIST = gql`
  query findBatchList($page: Int!, $size: Int!, $filters: [FilterInput]) {
    findBatchList(
      page: { number: $page, size: $size }
      filters: $filters
      sort: { col: "id", mod: DES }
    ) {
      totalPages
      content {
        id
        name
        objective
        numMembers
        numContainers
        numControls
        batchcode {
          name
          code
          lastPlateNumber
          lastSampleNumber
        }
      }
    }
  }
`;

export const DELETE_REQUESTTRAIT_BY_ID = gql`
  mutation deleteRequestTrait($idRequestTrait: Int!) {
    deleteRequestTrait(idRequestTrait: $idRequestTrait)
  }
`;

export const FIND_REQUESTTRAIT_BY_ID = gql`
  query findRequestTraitList(
    $traitId: String!
    $requestId: String!
    $markerGroupId: String
  ) {
    findRequestTraitList(
      page: { number: 1, size: 10 }
      filters: [
        { mod: EQ, col: "traitId", val: $traitId }
        { mod: EQ, col: "request.id", val: $requestId }
        { mod: EQ, col: "markerGroupId", val: $markerGroupId }
      ]
    ) {
      content {
        id
        traitId
        markerGroupId
        request {
          id
        }
      }
    }
  }
`;
