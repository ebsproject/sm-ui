import { gql } from '@apollo/client';

// Find Batch List
export const FIND_BATCH_LIST = gql`
  query findBatchList($page: PageInput, $sort: SortInput, $filters: [FilterInput]) {
    findBatchList(page: $page, sort: $sort, filters: $filters) {
      totalPages
      totalElements
      content {
        id
        resultFileId
        name
        objective
        numMembers
        numContainers
        numControls
        fistPlate
        lastPlate
        createdBy
        updatedBy
        objective
        vendorId
        createByUser {
          id
          userName
          contact {
            person {
              familyName
              givenName
              fullName
            }
          }
        }
        updatedByUser {
          userName
          contact {
            person {
              familyName
              givenName
              fullName
            }
          }
        }
        createdOn
        updatedOn
        serviceprovider {
          name
          servicetypes {
            name
          }
        }
        requests {
          id
          status {
            id
            name
            color
            description
          }
        }
        vendor {
          name
        }
      }
    }
  }
`;

export const FIND_VENDOR_LIST = gql`
  query {
    findVendorList {
      content {
        id
        code
        technologyplatforms {
          id
          name
          assayclasss {
            id
            name
          }
        }
      }
    }
  }
`;

export const FIND_TECHNOLOGY_PLATFORM_LIST = gql`
  query {
    findTechnologyPlatformList {
      content {
        id
        name
        vendor {
          id
          code
        }
      }
    }
  }
`;

export const FIND_ASSAY_CLASS_LIST = gql`
  query {
    findAssayclassList {
      content {
        id
        name
        technologyplatforms {
          id
          name
        }
      }
    }
  }
`;

export const FIND_LOAD_TYPE_LIST = gql`
  query {
    findLoadTypeList {
      content {
        id
        name
      }
    }
  }
`;

export const FIND_MARKER_GROUP_LIST = gql`
  query {
    findMarkerGroupList {
      content {
        id
        name
        assayclass {
          id
          name
        }
        markers {
          id
          name
          description
          sourcePlatformType
          chromosome
          sequence
          sequenceStartositionCsVs1
          sequenceEndPositionCdVs1
          similarSequenceChr96
          hybridEvaluation
          famAllele
          hexAllele
          primerGcAlleleX
          primerGcAlleleY
          primerGcCommon
          blastScore
          blastExpect
        }
      }
    }
  }
`;

export const FIND_BATCH_CODE_LIST = gql`
  query {
    findBatchCodeList {
      content {
        id
        serviceprovider {
          id
          name
        }
        name
        code
        lastPlateNumber
        lastBatchNumber
        lastPlateNumber
        tenant
        lastSampleNumber
      }
    }
  }
`;

export const FIND_BATCH_CODE_LIST_FILTER = gql`
  query findBatchCodeList($filters: [FilterInput]) {
    findBatchCodeList(filters: $filters) {
      content {
        id
        serviceprovider {
          id
          name
        }
        name
        code
        lastPlateNumber
        lastBatchNumber
        lastPlateNumber
        tenant
        lastSampleNumber
      }
    }
  }
`;

export const FIND_BATCH = gql`
  query findBatch($id: ID!) {
    findBatch(id: $id) {
      id
      name
      numMembers
      numContainers
      vendorId
      vendor {
        id
        name
      }
      technology {
        id
        description
        name
      }
      technologyPlatformId
      technologyServiceProvider {
        id
        controlPlate
        reportId
      }
      batchMarkerGroups {
        isFixed
        id
        markerGroupID
        markerGroup {
          id
          name
          vendor {
            id
            name
          }
        }
        technologyServiceProviderId
        batchMarkerGroupCustoms {
          id
          assayId
          assay {
            id
            name
          }
          markerId
          marker {
            id
            name
          }
        }
      }
      loadtype {
        name
      }
    }
  }
`;

export const FIND_REQUEST = gql`
  query findRequest($id: ID!) {
    findRequest(id: $id) {
      id
      adminContactId
      batch {
        id
        name
        startDate
        resultFileId
      }
      completedBy
      crop {
        id
        name
        code
      }
      form {
        name
        description
        id
        formtype {
          name
          id
        }
        fields {
          name
          label
          tooltip
          defaultValue
          isRequired
          order
          isBase
          datatype {
            name
          }
          tab {
            index
            name
          }
        }
      }
      germplasmOwnerId
      listId
      markerPanel {
        id
        name
        vendor {
          id
          name
        }
      }
      note
      program {
        id
        name
        code
      }
      purpose {
        id
        name
      }
      requestCode
      requesterOwnerId
      requeststatuss {
        id
        comments
      }
      samplesperItem
      service {
        id
        name
      }
      serviceprovider {
        id
        name
      }
      servicetype {
        id
        name
      }
      status {
        id
        name
      }
      submitionDate
      tenant
      tissuetype {
        id
        name
      }
      traitCustom {
        id
        name
        markergroups {
          id
          name
        }
        traitCategory {
          id
          name
        }
      }
      totalEntities
      user {
        contact {
          person {
            givenName
            familyName
          }
        }
      }
      values {
        id
        value
        field {
          datatype {
            name
          }
          id
          label
          name
        }
      }
    }
  }
`;

export const FIND_ASSAY = gql`
  query findAssayclass($id: ID!) {
    findAssayclass(id: $id) {
      id
      name
      technologyplatforms {
        vendorcontrols {
          position
        }
      }
    }
  }
`;

export const MODIFY_BATCH = gql`
  mutation modifyBatch($BatchTo: BatchInput!) {
    modifyBatch(BatchTo: $BatchTo) {
      id
    }
  }
`;

export const MODIFY_SAMPLE_DETAIL = gql`
  mutation modifySampleDetail($SampleDetailTo: SampleDetailInput!) {
    modifySampleDetail(SampleDetailTo: $SampleDetailTo) {
      id
      sampleCode
      column
      row
      request {
        id
      }
      batch {
        name
      }
    }
  }
`;

export const DELETE_BATCH = gql`
  mutation deleteBatch($id: Int!) {
    deleteBatch(idbatch: $id)
  }
`;

export const FIND_BATCH_KEYFILE = gql`
  query findBatch($id: ID!) {
    findBatch(id: $id) {
      resultFileId
    }
  }
`;

const FIND_PLATE_LIST = gql`
  query findPlateList($filters: [FilterInput], $page: PageInput) {
    findPlateList(filters: $filters, page: $page) {
      totalPages
      content {
        plateId
      }
    }
  }
`;

const FIND_SAMPLE_DETAIL_LIST = gql`
  query findSampleDetailList($filters: [FilterInput], $page: PageInput) {
    findSampleDetailList(filters: $filters, page: $page) {
      content {
        sampleNumber
        row
        column
        plateName
        plateId
        controltype {
          name
        }
        id
      }
    }
  }
`;

export { FIND_PLATE_LIST, FIND_SAMPLE_DETAIL_LIST };
