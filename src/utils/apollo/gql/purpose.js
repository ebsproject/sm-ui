import { gql } from "@apollo/client";
import { MARKER_DB_CONTEXT } from "utils/config";
import { SMgraph } from "../SMclient";

const FETCH_PURPOSE = gql`
query findPurposeList($page: PageInput, $sort: SortInput, $filters: [FilterInput]) {
  findPurposeList(page: $page, sort: $sort, filters: $filters){
      content{
          id
          code
          description
          name
          markerPanels{
              id
              name
              vendor{
                  id
                  name
              }
          }
          services{
              id
              code
              description
              name
          }
          servicetype{
              id
              name
              code
              description
          }
      }
  }
}
`;

const fetchPurpose = async ({ page, sort, filters }) => {
    const result = await SMgraph.query({
        variables: { PageInput: page, SortInput: sort, FilterInput: filters },
        query: FETCH_PURPOSE,
    });
    return {
        pages: data.findContactList.totalPages,
        elements: data.findContactList.totalElements,
        data: data.findContactList.content,
        data: result.data.findPurposeList.content
    }
        ;
};


export { fetchPurpose }