import gql from 'graphql-tag'

// QUERIES
export const FIND_COMPONENT = gql`
  query findComponent($id: ID!) {
    findComponent(id: $id) {
      name
      id
      modules {
        id
        name
        workflownodes {
          name
          workflow {
            id
            description
          }
        }
      }
    }
  }
`

export const FIND_CROP = gql`
  query findCrop($id: ID!) {
    findCrop(id: $id) {
      cropname
      programs {
        id
        programname
      }
      serviceproviders {
        id
        name
        servicetypes {
          id
          name
          purposes {
            id
            name
            services {
              id
              name
            }
          }
        }
      }
    }
  }
`
