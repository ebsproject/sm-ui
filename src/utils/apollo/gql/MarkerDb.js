import { gql } from '@apollo/client';

export const FIND_MARKER_LIST = gql`
  query findMarkerList($filters: [FilterInput]) {
    findMarkerList(filters: $filters) {
      content {
        id
        alleledefinitions {
          allele {
            name
            comments
          }
          call
        }
        assays {
          name
        }
        category
        markergroups {
          id
          name
        }
        markerpositions {
          chromosome {
            name
          }
          position
          referencegenome {
            name
          }
        }
        name
        source
        target
      }
    }
  }
`;

export const FIND_TRAITCATEGORY_BY_ID = gql`
  query findTraitCategory($id: ID!) {
    findTraitCategory(id: $id) {
      id
      name
      traits {
        id
        name
        crop {
          id
          name
        }
        markergroups {
          id
          name
          crop {
            id
            name
          }
          vendor {
            id
            name
          }
        }
      }
    }
  }
`;

export const FIND_TRAIT_WITH_MARKER_INFO_BY_ID = gql`
  query findTrait($id: ID!) {
    findTrait(id: $id) {
      id
      name
      markergroups {
        id
        name
        markers {
          id
          alleledefinitions {
            allele {
              name
              comments
            }
            call
          }
          assays {
            name
          }
          category
          markergroups {
            id
            name
          }
          markerpositions {
            chromosome {
              name
            }
            position
            referencegenome {
              name
            }
          }
          name
          source
          target
        }
      }
    }
  }
`;
