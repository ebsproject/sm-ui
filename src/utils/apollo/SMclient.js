import { ApolloClient, InMemoryCache, gql } from '@apollo/client';
import { getRequestWorkflowByID } from 'utils/other/Workflow';
import {
  getRequestListBybatchID,
  createRequestListMember,
} from 'utils/other/SampleManager';
import { useDispatch } from 'react-redux';
import { MODIFY_REQUEST } from 'utils/apollo/gql/RequestManager';
import { TOKENID, GET_SM_URL, MARKER_DB_CONTEXT } from 'utils/config';
import { constant } from 'lodash';

// get the authentication token from local storage if it exists
const token =
  TOKENID() ||
  'eyJ4NXQiOiJaalJtWVRNd05USmpPV1U1TW1Jek1qZ3pOREkzWTJJeU1tSXlZMkV6TWpkaFpqVmlNamMwWmciLCJraWQiOiJaalJtWVRNd05USmpPV1U1TW1Jek1qZ3pOREkzWTJJeU1tSXlZMkV6TWpkaFpqVmlNamMwWmdfUlMyNTYiLCJhbGciOiJSUzI1NiJ9.eyJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC91c2VybmFtZSI6InhLNW04dUxjbWtsR3Nqb3J1aWRzelhzNnpMaHFEX1YyTTF1SUtyNl9Vd3MiLCJzdWIiOiJ4SzVtOHVMY21rbEdzam9ydWlkc3pYczZ6TGhxRF9WMk0xdUlLcjZfVXdzIiwiaHR0cDpcL1wvd3NvMi5vcmdcL2NsYWltc1wvcm9sZSI6WyIyMzdjMGM3YS1lYmU0LTRlYjYtYWM1OC1kNTc2ZmQ2MmM3YTkiLCIwYmRhNTczYi1lMTc4LTQ4MjQtOTVkMi03NDcwYjFiMzRlYjQiLCI5NGQ0MWZlMi00ZTcwLTQ3OTAtYTIzOC01YzVjMTRiMjc4MWMiLCI3ZDFhODczOS1jOWE5LTQ2MDYtODg3Ny1kYzkyZWYwNmVhNjgiLCJlMzBkOWUxMS1kNWJiLTQ1YjMtYTExOC02Mjg2ZDQ4N2RmYmYiLCIwNjY2MzYxYi1iMjk4LTRjMjMtYjBhMi00MGIyZDE1YWYyMjciLCJjNTMyNTNiZS0xYjY4LTQ0MjEtYmMxYi1jM2M5YWNlYTdhN2MiLCIxMDgwNjE1My0zZWNhLTRjODgtYjU2Yi04YzdmYzMwZDZmMGMiLCJiMDE3ZDExYi1lNDI5LTRiOTMtOWY3NC1jZWQ1MTcwMGE5NWQiLCJiY2RmOWNjMC04MDA1LTQ3NmYtYmIwNS03MWM5NmYwMzgxYTYiLCI0ZjZiODcwZS0xYjY2LTQ0YzgtYWMxYy1mMGUzNWYyNGQyODAiXSwiaXNzIjoiaHR0cHM6XC9cL2Vicy5jaW1teXQub3JnOjk0NDNcL29hdXRoMlwvdG9rZW4iLCJhdWQiOiJUN0JWdnFVb0hUZjR2aEJVSjlkVk45emZLR1lhIiwibmJmIjoxNTkxODU1MzY0LCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9mdWxsbmFtZSI6WyJCUklPTkVTIFBFUkVZUkEiLCIgRXJuZXN0byBKb3NlIChDSU1NWVQpIl0sImh0dHA6XC9cL3dzbzIub3JnXC9jbGFpbXNcL2Rpc3BsYXlOYW1lIjoiRS5CUklPTkVTQENJTU1ZVC5PUkciLCJhenAiOiJUN0JWdnFVb0hUZjR2aEJVSjlkVk45emZLR1lhIiwic2NvcGUiOiJvcGVuaWQiLCJodHRwOlwvXC93c28yLm9yZ1wvY2xhaW1zXC9lbWFpbGFkZHJlc3MiOiJFLkJSSU9ORVNAY2ltbXl0Lm9ubWljcm9zb2Z0LmNvbSIsImV4cCI6MTU5MTg1ODk2NCwiaWF0IjoxNTkxODU1MzY0LCJqdGkiOiI0NDBlMzc2OC05NTlhLTRiODgtOWYyYS04Y2NhNjMxNWVhYmUifQ.gy7fE1Wf8GFQix9KnmCFI-3IIIYn1Wiq6GeIAca7gZnn5SCH3OeNpRYdVIih3Xs0t0EJkW4YuD5LTIICMNywpVKh93FfoYVIhbw1ghDcnBmcv__VwKlIuX7CTOLjzpID3PqYfIzkEeaxdaVx5zFutcWcKzzBGpUR8FypTYvCMnpWa9RZKhUvGvhAG_KC4HsnHVDkaE1HVmuR1_fOsCJg8E2JUxVFKnBe8uF40m_wyT_MeKRQvCF-2OKqVmsbF9Qi9hH-Juf7_X2WiKfbcAj-5p85KDxocUEGK04coVU2kIb0-886G5My-4IiqqpjxDGgprg4GescWkXxYaBTootCbQ';

let today = new Date();
let dd = String(today.getDate()).padStart(2, '0');
let mm = String(today.getMonth() + 1).padStart(2, '0');
let yyyy = today.getFullYear();
today = yyyy + '-' + mm + '-' + dd;

export const SMgraph = new ApolloClient({
  cache: new InMemoryCache({
    addTypename: false,
  }),
  uri: `${GET_SM_URL}graphql`,
  // uri: "http://localhost:8091/graphql",
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    authorization: `Bearer ${token}`,
  },
});

export const MarkerDbGraph = new ApolloClient({
  cache: new InMemoryCache({
    addTypename: false,
  }),
  uri: `${MARKER_DB_CONTEXT}/graphql`,
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*',
    authorization: `Bearer ${token}`,
  },
});

const createRequestListMemberwithBatch = (batchID, requestMap, tenantID) => {
  return new Promise((resolve, reject) => {
    const requestId = [];
    requestMap.forEach((element) => {
      requestId.push(element.id);
       });
      createRequestListMember(batchID, requestId).then((response) => {
        resolve(response);
      });
   // });
  });
};

export const handleCreateRequestListMemberwithBatch = async (
  batchID,
  requestMap,
  tenantID,
) => {
  const response = await createRequestListMemberwithBatch(
    parseInt(batchID, 10),
    requestMap,
    tenantID,
  );

  if (response.data.metadata.status[0].message === 'success') return true;
};

export const createBatch = (
  batchName,
  tenantID,
  total,
  batchCodeID,
  cropID,
  requestMap,
  serviceproviderID,
  description,
  vendorID,
  technologyID,
  assayID,
  markerPanelID,
) => {
  return new Promise((resolve) => {
    const CREATE_BATCH = gql`
      mutation createBatch(
        $tenant_id: Int
        $name: String
        $total: Int
        $batchCodeID: ID!
        $cropID: Int
        $serviceprovider: ID!
        $description: String
        $vendor: ID!
        $technologyplatform: ID!
        $assayclass: ID!
        $markergroup: ID!
      ) {
        createBatch(
          BatchTo: {
            id: 0
            name: $name
            objective: $description
            numMembers: $total
            tenant: $tenant_id
            batchcode: { id: $batchCodeID }
            idCrop: $cropID
            inDesign: false
            vendor: { id: $vendor }
            technologyplatform: { id: $technologyplatform }
            serviceprovider: { id: $serviceprovider }
            assayclass: { id: $assayclass }
            markergroup: { id: $markergroup }
          }
        ) {
          id
        }
      }
    `;

    const CREATE_BATCH_WITHOUT_MARKERGROUP = gql`
      mutation createBatch(
        $tenant_id: Int
        $name: String
        $total: Int
        $batchCodeID: ID!
        $cropID: Int
        $serviceprovider: ID!
        $description: String
        $vendor: ID!
        $technologyplatform: ID!
        $assayclass: ID!
      ) {
        createBatch(
          BatchTo: {
            id: 0
            name: $name
            objective: $description
            numMembers: $total
            tenant: $tenant_id
            batchcode: { id: $batchCodeID }
            idCrop: $cropID
            inDesign: false
            vendor: { id: $vendor }
            technologyplatform: { id: $technologyplatform }
            serviceprovider: { id: $serviceprovider }
            assayclass: { id: $assayclass }
          }
        ) {
          id
        }
      }
    `;

    SMgraph.mutate({
      variables: {
        tenant_id: tenantID,
        name: batchName,
        total: total,
        batchCodeID: batchCodeID,
        cropID: cropID,
        description: description,
        serviceprovider: serviceproviderID,
        vendor: vendorID,
        technologyplatform: technologyID,
        assayclass: assayID,
        markergroup: markerPanelID,
      },
      mutation: markerPanelID ? CREATE_BATCH : CREATE_BATCH_WITHOUT_MARKERGROUP,
    })
      .then((response) => {
        resolve(response.data.createBatch.id);
      })
      .catch((err) => {
        console.error(err.message);
        throw new Error(err.message);
      });
  });
};

const createBachCode = (
  batchName,
  description,
  tenantID,
  serviceproviderID,
  total,
  cropID,
  requestMap,
  idBatchCode,
  lastBatchNumber,
  lastPlateNumber,
  lastSampleNumber,
) => {
  return new Promise((resolve, reject) => {
    const MODIFY_BATCH_CODE = gql`
      mutation modifyBatchCode(
        $idBatchCode: ID!
        $lastBatchNumber: Int
        $lastPlateNumber: Int
        $lastSampleNumber: Int
      ) {
        modifyBatchCode(
          BatchCodeTo: {
            id: $idBatchCode
            lastBatchNumber: $lastBatchNumber
            lastPlateNumber: $lastPlateNumber
            lastSampleNumber: $lastSampleNumber
          }
        ) {
          id
        }
      }
    `;
    SMgraph.mutate({
      variables: {
        idBatchCode: idBatchCode,
        lastBatchNumber: lastBatchNumber,
        lastPlateNumber: lastPlateNumber,
        lastSampleNumber: lastSampleNumber,
      },
      mutation: MODIFY_BATCH_CODE,
    })
      .then((response) => {
        createBatch(
          batchName,
          tenantID,
          total,
          parseInt(response.data.modifyBatchCode.id, 10),
          cropID,
          requestMap,
          description,
        );
        resolve(response.data.modifyBatchCode.id);
      })
      .catch((err) => {
        reject(err.message);
      });
  });
};

export const createBatchWithRequest = (
  requestMap,
  batchName,
  description,
  tenantID,
  serviceproviderID,
  total,
  cropID,
  idBAtchCode,
) => {
  let lastBatchNumber = 0;
  let lastPlateNumber = 0;
  let lastSampleNumber = 0;
  new Promise(() => {
    getBatchCodeById(idBAtchCode).then((response) => {
      lastBatchNumber = response.lastBatchNumber + 1;
      lastPlateNumber = response.lastPlateNumber;
      lastSampleNumber = response.lastSampleNumber;
      createBachCode(
        batchName,
        description,
        tenantID,
        serviceproviderID,
        total,
        cropID,
        requestMap,
        idBAtchCode,
        lastBatchNumber,
        lastPlateNumber,
        lastSampleNumber,
      );
    });
  });
};

export const getbatchCodeById = (idBatch) => {
  return new Promise((resolve) => {
    const Query_FINDBATCHBYID = gql`
      query findBatchCode($idBatch: ID!) {
        findBatchCode(id: $idBatch) {
          id
          name
          code
          lastBatchNumber
          lastPlateNumber
          lastSampleNumber
        }
      }
    `;
    SMgraph.query({
      variables: { idBatch: idBatch },
      query: Query_FINDBATCHBYID,
    }).then((response) => {
      resolve(response.data.findBatchCode);
    });
  });
};

export const getBatchList = (idServiceProvider, isDesign) => {
  return new Promise((resolve, reject) => {
    const Query_FINDBATCHLIST = gql`
      query findBatchList($isDesign: String!) {
        findBatchList(
          sort: { col: "id", mod: DES }
          filters: [{ mod: EQ, col: "indesign", val: $isDesign }]
        ) {
          content {
            id
            name
            objective
            nummembers
            numcontainers
            numcontrols
            batchcode {
              name
              code
              lastplate_number
              lastsample_number
            }
          }
        }
      }
    `;
    SMgraph.query({
      variables: { isDesign: isDesign },
      query: Query_FINDBATCHLIST,
    })
      .then((response) => {
        const data = response.data.findBatchList.content;
        const result = data.map((element) => {
          return {
            id: element.id,
            Name: element.name,
            total: element.nummembers,
            Objective: element.objective,
            num_containers: element.numcontainers,
            num_controls: element.numcontrols,
            last_plate_number: element.batchcode.lastplate_number,
            last_sample_number: element.batchcode.lastsample_number,
            //"crop": element.crop.crop_name,
            //"Tissue":element.tissuetype.name,
          };
        });
        resolve(result);
      })
      .catch((error) => {
        console.error(error.message);
        throw new Error(error.message);
      });
  });
};

export const getBatchByID = (idBatch) => {
  return new Promise((resolve) => {
    getRequestListBybatchID(idBatch).then((response) => {
      resolve(response);
    });
  })
    .then((response) => {
      return new Promise((resolve, reject) => {
        getRequestWorkflowByID(response).then((response) => {
          resolve(response);
        });
      });
    })
    .catch((error) => {
      console.error(error.message);
      throw new Error(error.message);
    });
};

export const getBatchCodeById = (idBatchCode) => {
  return new Promise((resolve, reject) => {
    const Query_FINDBATCHCODEYID = gql`
      query findBatchCode($idBatchCode: ID!) {
        findBatchCode(id: $idBatchCode) {
          name
          lastBatchNumber
          code
          lastPlateNumber
          lastSampleNumber
          id
        }
      }
    `;
    SMgraph.query({
      variables: { idBatchCode: idBatchCode },
      query: Query_FINDBATCHCODEYID,
    })
      .then((response) => {
        resolve(response.data.findBatchCode);
      })
      .catch((error) => {
        console.error(error.message);
        reject(error.message);
      });
  });
};

export const updateListRequestByBatchByID = (idBatch, idStatus, isUpate) => {
  return new Promise((resolve) => {
    getRequestListBybatchID(idBatch.id).then((response) => {
      resolve(response);
    });
  })
    .then((response) => {
      return new Promise((resolve, reject) => {
        const STATUS_ID = idStatus;
        response.result.data.forEach((element) => {
          new Promise((resolve, reject) => {
            SMgraph.mutate({
              mutation: MODIFY_REQUEST,
              variables: {
                RequestTo: {
                  id: parseInt(element.id, 10),
                  status: { id: STATUS_ID },
                },
              },
            });
          });
        });

        if (isUpate === true) {
          updateBatchStatus(idBatch, true);
        } else {
          deleteLogical(idBatch);
        }
      });
    })
    .catch((error) => {
      console.error(error.message);
      throw new Error(error.message);
    });
};

export const updateBatchStatus = (idBatchCode, isDesign) => {
  return new Promise((reject) => {
    const UPDATE_BATCH_STATUS = gql`
      mutation modifyBatch($idBatchCode: ID!, $isDesign: Boolean, $total: Int) {
        modifyBatch(
          BatchTo: {
            id: $idBatchCode
            inDesign: $isDesign
            numMembers: $total
            tenant: 1
          }
        ) {
          id
        }
      }
    `;
    SMgraph.mutate({
      variables: {
        idBatchCode: idBatchCode.id,
        isDesign: isDesign,
        total: idBatchCode.total,
      },
      mutation: UPDATE_BATCH_STATUS,
    }).catch((err) => {
      reject(err.message);
    });
  });
};

export const deleteLogical = (idBatchCode) => {
  return new Promise((reject) => {
    const DELETE_BATCH_LOGICA = gql`
      mutation deleteBatch($idBatchCode: Int!) {
        deleteBatch(idbatch: $idBatchCode)
      }
    `;
    SMgraph.mutate({
      variables: { idBatchCode: idBatchCode.id },
      mutation: DELETE_BATCH_LOGICA,
    }).catch((err) => {
      reject(err.message);
    });
  });
};

export const findStatusList = async () => {
  const reponse = await SMgraph.query({
    variables: {},
    query: gql`
     query findSatusList {
      findStatusList{
        content{
            id
            name
        }
      }
    }
    `,
  });
  return reponse.data.findStatusList.content.map((item) => ({
    id: item.id,
    name: item.name,
  }));
}; 


