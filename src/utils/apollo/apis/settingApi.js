import { gql } from '@apollo/client';
import { MarkerGraphApolloClient, ServiceManagerApolloClient } from '../clients';

const addMarkerPanelsToPurpose = async ({ purposeId, markerPanels }) => {
  const response = await ServiceManagerApolloClient.mutate({
    variables: {
      purposeId,
      markerPanels,
    },
    mutation: gql`
      mutation addMarkerPanelsToPurpose($purposeId: Int!, $markerPanels: [Int!]!) {
        addMarkerPanelsToPurpose(
          purposeId: $purposeId
          markerPanels: $markerPanels
        ) {
          id
          markerPanels {
            id
            name
          }
        }
      }
    `,
  });
  return response.data.addMarkerPanelsToPurpose;
};

const removeMarkerPanelFromPurpose = async ({ purposeId, markerPanels }) => {
  const response = await ServiceManagerApolloClient.mutate({
    variables: {
      purposeId,
      markerPanels,
    },
    mutation: gql`
      mutation removeMarkerPanelsFromPurpose(
        $purposeId: Int!
        $markerPanels: [Int!]!
      ) {
        removeMarkerPanelsFromPurpose(
          purposeId: $purposeId
          markerPanels: $markerPanels
        ) {
          id
          markerPanels {
            id
            name
          }
        }
      }
    `,
  });
  return response.data.removeMarkerPanelsFromPurpose;
};

const addTraitCategoryToPurpose = async ({ purposeId, traitCategories }) => {
  const response = await ServiceManagerApolloClient.mutate({
    variables: {
      purposeId,
      traitCategories,
    },
    mutation: gql`
      mutation addTraitCategoriesToPurpose(
        $purposeId: Int!
        $traitCategories: [Int!]!
      ) {
        addTraitCategoriesToPurpose(
          purposeId: $purposeId
          traitCategories: $traitCategories
        ) {
          id
          traitCategories {
            id
            name
          }
        }
      }
    `,
  });
  return response.data.addTraitCategoriesToPurpose;
};

const removeTraitCategoryFromPurpose = async ({ purposeId, traitCategories }) => {
  const response = await ServiceManagerApolloClient.mutate({
    variables: {
      purposeId,
      traitCategories,
    },
    mutation: gql`
      mutation removeTraitCategoriesFromPurpose(
        $purposeId: Int!
        $traitCategories: [Int!]!
      ) {
        removeTraitCategoriesFromPurpose(
          purposeId: $purposeId
          traitCategories: $traitCategories
        ) {
          id
          traitCategories {
            id
            name
          }
        }
      }
    `,
  });
  return response.data.removeTraitCategoriesFromPurpose;
};

const addServiceProvidersToServiceType = async ({
  serviceTypeId,
  serviceProviders,
}) => {
  const response = await ServiceManagerApolloClient.mutate({
    variables: {
      serviceTypeId,
      serviceProviders,
    },
    mutation: gql`
      mutation addServiceProvidersToServiceType(
        $serviceTypeId: Int!
        $serviceProviders: [Int!]!
      ) {
        addServiceProvidersToServiceType(
          serviceTypeId: $serviceTypeId
          serviceProviders: $serviceProviders
        ) {
          id
          serviceproviders {
            id
            name
          }
        }
      }
    `,
  });
  return response.data.addServiceProvidersToServiceType;
};

const removeServiceProvidersFromServiceType = async ({
  serviceTypeId,
  serviceProviders,
}) => {
  const response = await ServiceManagerApolloClient.mutate({
    variables: {
      serviceTypeId,
      serviceProviders,
    },
    mutation: gql`
      mutation removeServiceProvidersFromServiceType(
        $serviceTypeId: Int!
        $serviceProviders: [Int!]!
      ) {
        removeServiceProvidersFromServiceType(
          serviceTypeId: $serviceTypeId
          serviceProviders: $serviceProviders
        ) {
          id
          serviceproviders {
            id
            name
          }
        }
      }
    `,
  });
  return response.data.removeServiceProvidersFromServiceType;
};

const fetchAllServiceType = async () => {
  const page = { number: 1, size: 100 };
  const response = await ServiceManagerApolloClient.query({
    variables: {
      page,
    },
    query: gql`
      query findServiceTypeList($page: PageInput) {
        findServiceTypeList(page: $page) {
          content {
            id
            name
            code
          }
        }
      }
    `,
  });
  return response.data.findServiceTypeList.content.map((item) => ({
    id: item.id,
    label: item.name,
  }));
};

const onlyFetchMarkerGroup = async ({ page, sort, filters }) => {
  const response = await MarkerGraphApolloClient.query({
    variables: {
      page,
      sort,
      filters,
    },
    query: gql`
      query findMarkerGroupList(
        $page: PageInput
        $sort: SortInput
        $filters: [FilterInput]
      ) {
        findMarkerGroupList(page: $page, sort: $sort, filters: $filters) {
          totalPages
          content {
            id
            name
          }
        }
      }
    `,
  });
  return {
    content: response.data.findMarkerGroupList.content,
    totalPages: response.data.findMarkerGroupList.totalPages,
  };
};

const fetchAllMarkerGroup = async ({ idCrop }) => {
  const result = [];
  const filters = [];
  const size = 100;
  const sort = { col: 'name', mod: 'ASC' };
  if (idCrop !== '0') filters.push({ col: 'crop.id', val: idCrop, mod: 'EQ' });
  const { content, totalPages } = await onlyFetchMarkerGroup({
    page: { number: 1, size },
    sort,
    filters,
  });
  content.forEach((item) =>
    result.push({
      id: item.id,
      label: item.name,
    }),
  );
  if (totalPages > 1) {
    for (let i = 2; i <= totalPages; i++) {
      const { content: contentPage } = await onlyFetchMarkerGroup({
        page: { number: i, size },
        sort,
        filters,
      });
      contentPage.forEach((item) =>
        result.push({
          id: item.id,
          label: item.name,
        }),
      );
    }
  }
  return result;
};

const fetchAllTraitCategory = async () => {
  const page = { number: 1, size: 100 };
  const response = await MarkerGraphApolloClient.query({
    variables: {
      page,
    },
    query: gql`
      query findTraitCategoryList($page: PageInput) {
        findTraitCategoryList(page: $page) {
          content {
            id
            name
          }
        }
      }
    `,
  });
  return response.data.findTraitCategoryList.content.map((item) => ({
    id: item.id,
    label: item.name,
  }));
};

const fetchAllServiceProvider = async () => {
  const page = { number: 1, size: 100 };
  const response = await ServiceManagerApolloClient.query({
    variables: { page },
    query: gql`
      query findServiceProviderList($page: PageInput) {
        findServiceProviderList(page: $page) {
          content {
            id
            name
            code
          }
        }
      }
    `,
  });
  return response.data.findServiceProviderList.content.map((item) => ({
    id: item.id,
    label: item.name,
  }));
};

const savePurpose = async ({
  code,
  description,
  id,
  markerPanelIds,
  name,
  oldMarkerPanelIds,
  oldTraitCategoryIds,
  serviceTypeId,
  traitCategoryIds,
}) => {
  if (id === 0) {
    const response = await ServiceManagerApolloClient.mutate({
      variables: {
        PurposeTo: {
          id,
          name,
          code,
          description,
          servicetype: {
            id: serviceTypeId,
          },
        },
      },
      mutation: gql`
        mutation createPurpose($PurposeTo: PurposeInput!) {
          createPurpose(PurposeTo: $PurposeTo) {
            id
            name
            code
            description
            servicetype {
              id
            }
            markerPanels {
              id
              name
            }
            traitCategories {
              id
              name
            }
          }
        }
      `,
    });
    if (markerPanelIds.length > 0) {
      const responseAddMarkerPanel = await addMarkerPanelsToPurpose({
        purposeId: response.data.createPurpose.id,
        markerPanels: markerPanelIds,
      });
      response.data.createPurpose.markerPanel = responseAddMarkerPanel.markerPanels;
    }
    if (traitCategoryIds.length > 0) {
      const responseAdd = await addTraitCategoryToPurpose({
        purposeId: response.data.createPurpose.id,
        traitCategories: traitCategoryIds,
      });
      response.data.createPurpose.traitCategories = responseAdd.traitCategories;
    }
    return response.data.createPurpose;
  } else {
    const response = await ServiceManagerApolloClient.mutate({
      variables: {
        PurposeTo: {
          id,
          name,
          code,
          description,
          servicetype: {
            id: serviceTypeId,
          },
        },
      },
      mutation: gql`
        mutation modifyPurpose($PurposeTo: PurposeInput!) {
          modifyPurpose(PurposeTo: $PurposeTo) {
            id
            name
            code
            description
            servicetype {
              id
            }
            markerPanels {
              id
              name
            }
            traitCategories {
              id
              name
            }
          }
        }
      `,
    });
    response.data.modifyPurpose.markerPanel = await overwriteMarkerPanelsToPurpose({
      markerPanelIds,
      oldMarkerPanelIds,
      id,
    });
    response.data.modifyPurpose.traitCategories =
      await overwriteTraitCategoriesToPurpose({
        traitCategoryIds,
        oldTraitCategoryIds,
        id,
      });

    return response.data.modifyPurpose;
  }
};

const overwriteMarkerPanelsToPurpose = async ({
  id,
  markerPanelIds,
  oldMarkerPanelIds,
}) => {
  let removed = null;
  if (oldMarkerPanelIds.length > 0) {
    const responseRemove = await removeMarkerPanelFromPurpose({
      purposeId: id,
      markerPanels: oldMarkerPanelIds,
    });
    removed = responseRemove.markerPanel;
  }
  if (removed == null && markerPanelIds.length > 0) {
    const responseAdd = await addMarkerPanelsToPurpose({
      purposeId: id,
      markerPanels: markerPanelIds,
    });
    return responseAdd.markerPanels;
  }
  return [];
};

const overwriteTraitCategoriesToPurpose = async ({
  traitCategoryIds,
  oldTraitCategoryIds,
  id,
}) => {
  let removed = null;
  if (oldTraitCategoryIds.length > 0) {
    const responseRemove = await removeTraitCategoryFromPurpose({
      purposeId: id,
      traitCategories: oldTraitCategoryIds,
    });
    removed = responseRemove.traitCategories;
  }
  if (removed == null && traitCategoryIds.length > 0) {
    const responseAdd = await addTraitCategoryToPurpose({
      purposeId: id,
      traitCategories: traitCategoryIds,
    });
    return responseAdd.traitCategories;
  }
  return [];
};

const saveServiceType = async ({
  id,
  name,
  code,
  description,
  serviceProviderIds,
  oldServiceProviderIds,
}) => {
  if (id === 0) {
    const response = await ServiceManagerApolloClient.mutate({
      variables: {
        ServiceTypeTo: {
          id,
          name,
          code,
          description,
        },
      },
      mutation: gql`
        mutation createServiceType($ServiceTypeTo: ServiceTypeInput!) {
          createServiceType(ServiceTypeTo: $ServiceTypeTo) {
            id
            code
          }
        }
      `,
    });
    const responseAdd = await addServiceProvidersToServiceType({
      serviceTypeId: response.data.createServiceType.id,
      serviceProviders: serviceProviderIds,
    });
    response.data.createServiceType.serviceproviders = responseAdd.serviceproviders;
    return response.data.createServiceType;
  } else {
    const response = await ServiceManagerApolloClient.mutate({
      variables: {
        ServiceTypeTo: {
          id,
          name,
          code,
          description,
        },
      },
      mutation: gql`
        mutation modifyServiceType($ServiceTypeTo: ServiceTypeInput!) {
          modifyServiceType(ServiceTypeTo: $ServiceTypeTo) {
            id
            name
          }
        }
      `,
    });
    response.data.modifyServiceType.serviceproviders =
      await overwriteServiceProvidersToServiceType({
        serviceProviderIds,
        oldServiceProviderIds,
        id,
      });
    return response.data.modifyServiceType;
  }
};

const overwriteServiceProvidersToServiceType = async ({
  serviceProviderIds,
  oldServiceProviderIds,
  id,
}) => {
  let removed = null;
  if (oldServiceProviderIds.length > 0) {
    const responseRemove = await removeServiceProvidersFromServiceType({
      serviceTypeId: id,
      serviceProviders: oldServiceProviderIds,
    });
    removed =
      responseRemove.serviceproviders === null
        ? null
        : responseRemove.serviceproviders.length === 0
        ? null
        : 'Error';
  }
  if (removed == null) {
    const responseAdd = await addServiceProvidersToServiceType({
      serviceTypeId: id,
      serviceProviders: serviceProviderIds,
    });
    return responseAdd.serviceproviders;
  }
  return [];
};

const findPurposeById = async ({ id }) => {
  const response = await ServiceManagerApolloClient.query({
    variables: { idPurpose: id },
    query: gql`
      query findPurpose($idPurpose: ID!) {
        findPurpose(id: $idPurpose) {
          id
          name
          code
          description
          servicetype {
            id
            name
          }
          markerPanels {
            id
            name
          }
          traitCategories {
            id
            name
          }
        }
      }
    `,
    fetchPolicy: 'no-cache',
  });
  const result = response.data.findPurpose;
  if (result.markerPanels) {
    result.markerPanels = await Promise.all(
      result.markerPanels
        .filter((markerPanel) => markerPanel)
        .map(async ({ id, name }) => {
          const response = await MarkerGraphApolloClient.query({
            variables: { id: id },
            query: gql`
              query findMarkerGroup($id: ID!) {
                findMarkerGroup(id: $id) {
                  crop {
                    id
                    name
                  }
                }
              }
            `,
            fetchPolicy: 'no-cache',
          });
          return {
            id,
            name,
            crop: response.data.findMarkerGroup.crop
              ? response.data.findMarkerGroup.crop
              : {},
          };
        }),
    );
  }
  return result;
};

const findServiceTypeById = async ({ id }) => {
  const response = await ServiceManagerApolloClient.query({
    variables: { idServiceType: id },
    query: gql`
      query findServiceType($idServiceType: ID!) {
        findServiceType(id: $idServiceType) {
          id
          name
          code
          description
          serviceproviders {
            id
            name
          }
        }
      }
    `,
    fetchPolicy: 'no-cache',
  });
  return response.data.findServiceType;
};

const fetchCropToSelect = async () => {
  const page = { number: 1, size: 100 };
  const response = await MarkerGraphApolloClient.query({
    query: gql`
      query findCropList($page: PageInput) {
        findCropList(page: $page) {
          content {
            id
            name
          }
        }
      }
    `,
    variables: {
      page: page,
    },
    fetchPolicy: 'no-cache',
  });
  const result = response.data.findCropList.content.map((item) => ({
    id: item.id,
    label: item.name,
  }));
  result.push({ id: '0', label: 'all' });
  result.sort((a, b) => (a.id > b.id ? 1 : b.id > a.id ? -1 : 0));
  return result;
};

const deletePurpose = async ({ id }) => {
  const response = await ServiceManagerApolloClient.mutate({
    variables: {
      id,
    },
    mutation: gql`
      mutation deletePurpose($id: Int!) {
        deletePurpose(idpurpose: $id)
      }
    `,
  });
  return response.data;
};

const deleteServiceType = async ({ id }) => {
  const response = await ServiceManagerApolloClient.mutate({
    variables: {
      id,
    },
    mutation: gql`
      mutation deleteServiceType($id: Int!) {
        deleteServiceType(idservicetype: $id)
      }
    `,
  });
  return response;
};

export {
  deletePurpose,
  deleteServiceType,
  fetchAllMarkerGroup,
  fetchAllServiceProvider,
  fetchAllServiceType,
  fetchAllTraitCategory,
  fetchCropToSelect,
  findPurposeById,
  findServiceTypeById,
  savePurpose,
  saveServiceType,
};
