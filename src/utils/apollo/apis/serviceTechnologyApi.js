import { MarkerGraphApolloClient } from '../clients';
import { MarkerDBComponents } from '@ebs/md';
const { fetchMarkerAndAssayByMarkerGroupAndTechnologyServiceProvider } =
  MarkerDBComponents;

const convertFixedMarkers = async ({ technology, vendor, batchMarkerGroups }) => {
  const response = await Promise.all(
    batchMarkerGroups.map(async (batch) => {
      if (batch.isFixed) {
        const result =
          await fetchMarkerAndAssayByMarkerGroupAndTechnologyServiceProvider({
            technologyId: technology.id,
            serviceProviderId: vendor.id,
            markerGroupId: batch.markerGroupID,
          });
        return {
          listBatchMarkerGroupCustom: result,
          markerGroupID: batch.markerGroupID,
        };
      } else {
        return {
          listBatchMarkerGroupCustom: batch.batchMarkerGroupCustoms,
          markerGroupID: batch.markerGroupID,
        };
      }
    }),
  );
  return response;
};

export { convertFixedMarkers };
