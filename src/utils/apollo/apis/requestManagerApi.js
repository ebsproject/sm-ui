import { gql } from '@apollo/client';
import { MarkerGraphApolloClient, ServiceManagerApolloClient } from '../clients';

const fetchEntryList = async ({ page, sort, filters }) => {
  const adjustedFilters = convertFilters(filters);
  const adjustedSort = sort.length > 0 ? sort[0] : null;

  const response = ServiceManagerApolloClient.query({
    query: gql`
      query findListList(
        $page: PageInput
        $sort: SortInput
        $filters: [FilterInput]
      ) {
        findListList(page: $page, sort: $sort, filters: $filters) {
          totalPages
          totalElements
          size
          number
          content {
            listDbId
            creatorDbId
            entityId
            memberCount
            requests {
              status {
                name
              }
              requestCode
            }
            abbrev
            name
            displayName
            type
            memberCount
            description
          }
        }
      }
    `,
    variables: {
      page: page,
      sort: adjustedSort,
      filters: [...adjustedFilters],
    },
    fetchPolicy: 'no-cache',
  });
  return response;
};

/**
 * Convert from EBS from filter to graphql filter
 */
function convertFilters(filters) {
  const adjustedFilters = filters.map((filter) => {
    if (filter.col != 'requestCode') {
      const adjustedFilter = { ...filter };
      // The keys for filtering: col, mod, val
      adjustedFilter['col'] = filter.col;
      adjustedFilter['mod'] = 'EQ';
      adjustedFilter['val'] = filter.val;
      // Removing unimportant keys
      delete adjustedFilter.id;
      delete adjustedFilter.value;
      return adjustedFilter;
    }
  });
  var removeNullFilters = adjustedFilters.filter(function (fn) {
    return fn != null;
  });
  return removeNullFilters;
}

const fetchAllEntryByEntryListId = async ({ page, sort, filters, listId }) => {
  const filtersDefault = {
    mod: 'EQ',
    col: 'listId',
    val: listId,
  };
  const adjustedFilters = [...convertFilters(filters)];
  const adjustedSort = sort.length > 0 ? sort[0] : null;
  adjustedFilters.push(filtersDefault);
  const response = await ServiceManagerApolloClient.query({
    query: gql`
      query findRequestList(
        $page: PageInput
        $sort: SortInput
        $filters: [FilterInput]
      ) {
        findRequestList(page: $page, sort: $sort, filters: $filters) {
          totalPages
          totalElements
          size
          number
          content {
            listId
            status {
              name
            }
            requestCode
            user {
              contact {
                person {
                  familyName
                  givenName
                }
              }
            }
            program {
              name
            }
            serviceprovider {
              name
            }
            servicetype {
              name
            }
            purpose {
              name
            }
            totalEntities
          }
        }
      }
    `,
    variables: {
      page: page,
      sort: adjustedSort,
      filters: adjustedFilters,
    },
    fetchPolicy: 'no-cache',
  });
  return response;
};

const findMarkerByMarkerGroup = async ({ page, sort, filters, markerGroupId }) => {
  const adjustedFilters = [...convertFilters(filters)];
  const adjustedSort = sort.length > 0 ? sort[0] : null;
  adjustedFilters.push({
    mod: 'EQ',
    col: 'markergroups.id',
    val: markerGroupId,
  });
  const response = await MarkerGraphApolloClient.query({
    query: gql`
      query findMarkerList(
        $page: PageInput
        $sort: SortInput
        $filters: [FilterInput]
      ) {
        findMarkerList(page: $page, sort: $sort, filters: $filters) {
          totalPages
          totalElements
          size
          number
          content {
            id
            alleledefinitions {
              allele {
                name
                comments
              }
              call
            }
            assays {
              name
            }
            category
            markergroups {
              id
              name
            }
            markerpositions {
              chromosome {
                name
              }
              position
              referencegenome {
                name
              }
            }
            name
            source
            target
          }
        }
      }
    `,
    variables: {
      page: page,
      sort: adjustedSort,
      filters: adjustedFilters,
    },
    fetchPolicy: 'no-cache',
  });

  const data = response.data.findMarkerList.content.map((item) => ({
    id: item.id,
    name: item.name,
    alleleDefinitions: item.alleledefinitions
      .map((allele) => `${allele.allele.name} - ${allele.allele.comments}`)
      .join(','),
    assays: item.assays.map((assay) => assay.name).join(','),
    category: item.category,
    markerPositions: item.markerpositions
      .map((position) => position.position)
      .join(','),
    referenceGenome: item.markerpositions
      .map((position) => position.referencegenome.name)
      .join(','),
    chromosome: item.markerpositions
      .map((position) => position.chromosome.name)
      .join(','),
    source: item.source,
    target: item.target,
  }));
  return {
    pages: response.data.findMarkerList.totalPages,
    elements: response.data.findMarkerList.totalElements,
    data,
  };
};

const searchPurposeByServiceTypeId = async ({
  serviceTypeId,
  defaultErrorMessage,
}) => {
  try {
    const response = await ServiceManagerApolloClient.query({
      query: gql`
        query findServiceType($id: ID!) {
          findServiceType(id: $id) {
            purposes {
              id
              name
            }
          }
        }
      `,
      variables: {
        id: serviceTypeId,
      },
      fetchPolicy: 'no-cache',
    });
    const data = response.data.findServiceType.purposes
      .map((purpose) => ({
        value: purpose.id,
        label: purpose.name,
      }))
      .sort((a, b) => (a.label > b.label ? 1 : b.label > a.label ? -1 : 0));
    return {
      data,
      isError: false,
      message: '',
    };
  } catch (error) {
    console.error('Error to fetch purpose', error);
    return {
      data: [],
      isError: true,
      message:
        error?.response?.data?.metadata.status[0].messageType || defaultErrorMessage,
    };
  }
};

export {
  fetchAllEntryByEntryListId,
  fetchEntryList,
  findMarkerByMarkerGroup,
  searchPurposeByServiceTypeId,
};
