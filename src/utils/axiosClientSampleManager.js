import { sampleMamagerInstance } from './axios';

export const getRequestListBybatchID = (idBatch) => {
  return new Promise((resolve, reject) => {
    sampleMamagerInstance
      .get('/batch/list-request-byBatchID?batchId=' + idBatch)
      .then((response) => {
        resolve(response.data);
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  });
};

export const getLaboratoryReport = (row) => {
  return new Promise((resolve, reject) => {
    sampleMamagerInstance({
      url: '/laboratory-report/createLaboratoryReport',
      method: 'POST',
      data: {
        idBatch: row.id,
        idBatchCode: 0,
        idControl: 0,
        idPlateDirection: 0,
        idCollection: 0,
      },
      responseType: 'blob',
    }).then((response) => {
      const type = response.headers['content-type'];
      const blob = new Blob([response.data], { type: type, encoding: 'UTF-8' });
      const link = document.createElement('a');
      link.href = window.URL.createObjectURL(blob);
      link.download = row.Name + '.xlsx';
      link.click();
      resolve(response);
    });
  });
};

export const saveDesignBatchByID = (
  idBatch,
  idPlateDirection,
  idControlVendor,
  idcollection,
) => {
  return new Promise((resolve, reject) => {
    sampleMamagerInstance({
      url: '/batch/createSamples',
      method: 'POST',
      data: {
        idBatch: idBatch,
        idBatchCode: 0,
        idControl: idControlVendor,
        idPlateDirection: idPlateDirection,
        idCollection: idcollection,
      },
    }).then((response) => {
      resolve(response);
    });
  });
};

export const getDesignListBatch = (idBatch) => {
  return new Promise((resolve, reject) => {
    sampleMamagerInstance
      .get('/batch/list-byBatchIDToDesign?batchId=' + idBatch)
      .then((response) => {
        const data = response.data.result.data;

        const result = data.map((element) => {
          return {
            id: element.id,
            Name: element.name,
            Total: element.nummembers,
            Objective: element.objective,
            num_containers: element.numcontainers,
            last_plate_number: element.lastPlate,
            first_Plate: element.fistPlate,
            updateDate: element.updateDate,
            updateBy: element.updateBy,
          };
        });
        resolve(result);
      })
      .catch((err) => {
        throw new Error(err.message);
      });
  });
};

export const getBatchCode = (requestMap, idBatchCode) => {
  let intServiceCode = 0;
  let intPurpose = 0;
  var ProgramMapa = new Map();

  requestMap.forEach((element) => {
    ProgramMapa.set(element.original.programId, element.original.programId);
    intPurpose = element.original.purposeId;
    intServiceCode = element.original.serviceId;
  });

  let keys = Array.from(ProgramMapa.keys());
  var URL = '';

  return new Promise((resolve, reject) => {
    sampleMamagerInstance({
      url: 'batch/createBatchCode',
      method: 'POST',
      data: {
        servicecode: intServiceCode,
        program: keys,
        purpose: intPurpose,
        batchID: 1,
      },
    }).then((response) => {
      resolve(response.data.result);
    });
  });
};

export const createRequestListMember = (batchId, requestId) => {
  return new Promise(() => {
    sampleMamagerInstance
      .get(
        '/batch/createRequestListMemberById?batchId=' +
          batchId +
          '&requestId=' +
          requestId,
      )
      .then((response) => {});
  }).catch((err) => {
    throw new Error(err.message);
  });
};
