import React from 'react';
import { Box, Typography } from '@mui/material';
import { SMgraph } from 'utils/apollo/SMclient';
import {
  FIND_REQUEST_LIST,
  FIND_SERVICE_PROVIDER_LIST,
  FIND_SERVICE_TYPE_LIST,
  FIND_REQUEST_STATUS_LIST,
  MODIFY_REQUEST_STATUS,
} from 'utils/apollo/gql/RequestManager';

import { formatDate } from 'utils/other/generalFunctions';

const notSetLabel = (
  <Typography component='span' color='primary'>
    <Box fontStyle='italic'> (not set) </Box>{' '}
  </Typography>
);

function tableValue(value) {
  /**
   * This function is to check if the value of the row from the rowData
   * is a string or null. This is because a request can be created
   * in which some of its values can be an empty string or a
   * null value.
   */
  if (value === '' || value === null) return notSetLabel;
  return value;
}
async function updateCompleted(requestStatusId) {
  try {
    await SMgraph.mutate({
      mutation: MODIFY_REQUEST_STATUS,
      variables: {
        RequestStatusTo: {
          id: requestStatusId,
          completed: formatDate(new Date(), false), // Date Format should be: YYYY-MM-DD
        },
      },
    });
  } catch (error) {
    throw new Error(error);
  }
}

export const findRequestList = async (page, pageSize, columnsToFilter, value) => {
  try {
    // Update the data of the Grid based on user interaction with the Grid
    let filters = [];
    // Getting all the filters before passing to the query
    if (columnsToFilter && columnsToFilter.length > 0) {
      columnsToFilter.map((_column) => {
        let column = '';
        let _mod = 'LK';
        switch (_column) {
          case 'crop':
            column = 'crop.cropName';
            break;
          case 'purpose':
            column = 'purpose.name';
            break;
          case 'serviceprovider':
            column = 'serviceprovider.name';
            break;
          case 'service':
            column = 'service.name';
            break;
          case 'servicetype':
            column = 'servicetype.name';
            break;
          case 'status':
            column = 'status.name';
            break;
          case 'program':
            column = 'program.programName';
            break;
          case 'totalEntities':
            _mod = 'EQ';
            column = _column;
            break;
          case 'submitionDate' || 'completedBy':
            _mod = 'EQ';
            column = _column;
            break;
          default:
            column = _column;
            break;
        }

        filters.push({
          mod: _mod,
          col: column,
          val: value,
        });
      });
    }

    const { data } = await SMgraph.query({
      query: FIND_REQUEST_LIST,
      variables: {
        page: page || 1,
        size: pageSize || 10,
        filters: filters,
        disjunctionFilters:
          columnsToFilter && columnsToFilter.length > 0 ? true : false,
      },
      fetchPolicy: 'no-cache',
    });
    let newData = [];
    if (data) {
      // Populating the values of the Grid
      data.findRequestList.content.map((row) => {
        newData.push({
          ...row,
          requesterOwnerId: tableValue(row.requesterOwnerId),
          requestCode: tableValue(row.requestCode),
          status: row.status.name,
          crop: tableValue(row.crop.cropName),
          cropId: tableValue(row.crop.id),
          purpose: tableValue(row.purpose.name),
          purposeId: tableValue(row.purpose.id),
          program: tableValue(row.program.programName),
          programId: tableValue(row.program.id),
          serviceprovider: tableValue(row.serviceprovider.name),
          serviceproviderId: row.serviceprovider.id,
          service: tableValue(row.service.name),
          serviceId: tableValue(row.service.id),
          servicetype: tableValue(row.servicetype.name),
          servicetypeId: tableValue(row.servicetype.id),
        });
      });
    }

    const requestListdata = {
      data: newData,
      currentPage: page,
      totalpages: data.findRequestList.totalPages,
    };

    return requestListdata;
  } catch (error) {
    throw new Error(error);
  }
};

export const findServiceProviderList = async (
  page,
  pageSize,
  columnsToFilter,
  value,
) => {
  try {
    let filters = [];
    if (columnsToFilter && columnsToFilter.length > 0) {
      columnsToFilter.map((_column) => {
        let column = '';
        let _mod = 'LK';
        switch (_column) {
          case 'name':
            column = 'name';
            break;
          case 'code':
            column = 'code';
            break;
          case 'servicetypes':
            column = 'servicetypes.name';
            break;
          default:
            column = _column;
            break;
        }

        filters.push({
          mod: _mod,
          col: column,
          val: value,
        });
      });
    }
    const { data } = await SMgraph.query({
      query: FIND_SERVICE_PROVIDER_LIST,
      variables: {
        page: page || 1,
        size: pageSize || 10,
        filters: filters,
        disjunctionFilters:
          columnsToFilter && columnsToFilter.length > 0 ? true : false,
      },
      fetchPolicy: 'no-cache',
    });

    let newData = [];
    if (data) {
      data.findServiceProviderList.content.map((row) => {
        newData.push({
          ...row,
          servicetypes: tableValue(row.servicetypes.map((val) => val.name)),
        });
      });
    }
    const serviceProviderListdata = {
      data: newData,
      currentPage: page,
      totalpages: data.findServiceProviderList.totalPages,
    };
    return serviceProviderListdata;
  } catch (error) {
    throw new Error(error);
  }
};

export const findServiceTypeList = async (
  page,
  pageSize,
  columnsToFilter,
  value,
) => {
  try {
    let filters = [];
    if (columnsToFilter && columnsToFilter.length > 0) {
      columnsToFilter.map((_column) => {
        let column = '';
        let _mod = 'LK';
        switch (_column) {
          case 'name':
            column = 'name';
            break;
          case 'code':
            column = 'code';
            break;
          case 'description':
            column = 'description';
            break;
          default:
            column = _column;
            break;
        }
        filters.push({
          mod: _mod,
          col: column,
          val: value,
        });
      });
    }

    const { data } = await SMgraph.query({
      query: FIND_SERVICE_TYPE_LIST,
      variables: {
        page: page || 1,
        size: pageSize || 10,
        filters: filters,
        disjunctionFilters:
          columnsToFilter && columnsToFilter.length > 0 ? true : false,
      },
      fetchPolicy: 'no-cache',
    });

    let newData = [];
    if (data) {
      data.findServiceTypeList.content.map((row) => {
        newData.push({
          ...row,
        });
      });
    }

    const serviceTypeListdata = {
      data: newData,
      currentPage: page,
      totalpages: data.findServiceTypeList.totalPages,
    };
    return serviceTypeListdata;
  } catch (error) {
    throw new Error(error);
  }
};

export const updatePreviousRequestStatus = async (requestId) => {
  try {
    /**
     * Note: previous here is the "to be" previous request status since there will be
     * a newly created Request Status after this function call.
     */

    const findRequestStatusListResponse = await SMgraph.query({
      query: FIND_REQUEST_STATUS_LIST,
      variables: {
        sort: { col: 'id', mod: 'DES' },
        filters: [{ col: 'request.id', mod: 'EQ', val: requestId }],
        disjunctionFilters: false,
      },
      fetchPolicy: 'no-cache',
    });

    const requestStatusList =
      findRequestStatusListResponse.data.findRequestStatusList.content;

    const previousRequestStatus = requestStatusList[0];
    updateCompleted(previousRequestStatus.id);

    return true;
  } catch (error) {
    throw new Error(error);
  }
};
