import { getProgramsById } from 'utils/axios/CSclient';

export const getAdminContactsByTeam = async (userId, setMessage) => {
  try {
    const programlist = await getProgramsById(userId);

    if (programlist) {
      let teamMembersWithAdminAsRoleCode = [];
      let firstMember = programlist.data.members;

      for (let i = 0; i < firstMember.length; i++) {
        let teamMember = firstMember[i];

        teamMembersWithAdminAsRoleCode.push({
          value: teamMember.userId,
          label: teamMember.name,
          email: teamMember.username,
          lastName: teamMember.name,
        });
      }
      return teamMembersWithAdminAsRoleCode;
    }
  } catch (error) {
    const message = error?.response?.data?.metadata.status[0].messageType;
    if (message) {
      setMessage(message);
    } else {
      setMessage(
        `There was a problem getting the list of Admin Contacts By Team from the CS-API.`,
      );
    }
    setError(true);
    return message;
  }
};

export const goToRequestManagerGrid = (navigate, previousPath) => {
  navigate(previousPath);
};

export const validateUserProfileBeforeRedirect = ({
  userProfile,
  navigate,
  location,
  pathName,
  dispatch,
}) => {
  let isMissing = checkForLocalStorageMissingFields(userProfile, dispatch);
  /** check required fields from the localStorage: **/
  if (!isMissing.length) {
    /** if nothing's missing, set user profile related variables **/
    dispatch({
      type: 'SET_USER_PROFILE_STORAGE',
      payload: userProfile,
    });

    navigate(pathName, { state: { previousPath: location.pathname } });
  }
};

export const validateUserProfileBeforeEdit = (
  userProfile,
  history,
  pathName,
  dispatch,
) => {
  let isMissing = checkForLocalStorageMissingFields(userProfile, dispatch);
  /** check required fields from the localStorage: **/
  if (!isMissing.length) {
    /** if nothing's missing, set user profile related variables **/
    dispatch({
      type: 'SET_USER_PROFILE_STORAGE',
      payload: userProfile,
    });

    history.push({
      pathname: pathName,
      state: {
        previousPath: history.location.pathname,
      },
    });
  }
};

export const checkForLocalStorageMissingFields = (userProfile, dispatch) => {
  let missingFields = '';
  let isMissing = false;

  /** check required fields from the localStorage: **/
  if (
    !userProfile.account ||
    userProfile.account == null ||
    userProfile.account.includes('not available')
  ) {
    missingFields = appendToString(missingFields, 'account/email');
    isMissing = true;
  }
  if (!userProfile.dbId || isNaN(userProfile.dbId)) {
    //check if does not exist or has other values like "not available"
    missingFields = appendToString(missingFields, 'dbId');
    isMissing = true;
  }
  if (!userProfile.externalId || isNaN(userProfile.externalId)) {
    missingFields = appendToString(missingFields, 'externalId');
    isMissing = true;
  }
  if (!userProfile.service_providers) {
    missingFields = appendToString(missingFields, 'service providers');
    isMissing = true;
  }
  if (missingFields.length > 0) {
    dispatch({
      type: 'ALERT_MESSAGE',
      payload: {
        message: `It's not possible to continue with any operation in the system, you do not have a valid user profile please contact to your administrator. Missing Information: ${missingFields}`,
        translationId: 'some.id',
        severity: 'warning',
      },
    });
  }
  return missingFields;
};

export const appendToString = (currentString, newString) => {
  if (currentString.length > 0) {
    currentString += ', ';
  }
  currentString += newString;

  return currentString;
};
