import { filter, orderBy } from 'lodash';

function distributeDataPerPage(data, page) {
  /**
   * Manual pagination since this is an array of objects.
   * The API returns pagination data but not about the
   * members per list.
   */
  let distributedData = [];
  const { number, size } = page;
  let i = 0;
  let dataSized = [];

  /**
   * Given page number and size selected per page, distribute the array of objects
   * into sets of objects, separated by each page. For example:
   *
   * [obj0, obj1, obj2, obj3, obj4, obj5, obj6]
   * size 3 per page, given an array with length of 7
   *
   * [
   *    [obj0, obj1, obj2], // page 1
   *    [obj3, obj4, obj5], // page 2
   *    [obj6,]             // page 3
   * ]
   *
   */
  if (!data?.length) return [];
  while (i < data?.length) {
    if (dataSized?.length < size) {
      dataSized.push(data[i]);

      if (dataSized?.length === size) {
        distributedData.push(dataSized);
        dataSized = [];
      } else if (i === data?.length - 1) {
        distributedData.push(dataSized);
      }

      i++;
    }
  }

  return distributedData[number - 1];
}

// TODO: delete this function
export function setData(members, page) {
  let pagesBasedOnNumberOfrequests = 1;
  const distributedData = distributeDataPerPage(members, page);
  let noOfelements = members?.length ? members.length : 0;
  if (noOfelements > 0)
    pagesBasedOnNumberOfrequests = Math.ceil(members?.length / page.size);

  return {
    pages: pagesBasedOnNumberOfrequests,
    elements: noOfelements,
    data: distributedData,
  };
}

// TODO: delete
export function filterMembers(members, filters) {
  /**
   * Given an array of filters, filter the members given a filter value.
   * For example:
   *
   * filter is: {
   *  col: 'designation',
   *  val: '14'
   * }
   *
   * return all member that have a 'designation' value that includes
   * the '14' substring.
   *
   * This is manually implemented because there's no API (correct me if I'm wrong)
   * for filtering the members inside a Germplasm.
   */
  

  const filteredData = filter(members, (o) => {
    for (const filter of filters) {
      if (filter.col.indexOf('.') !== -1) {
        /**
         * If the 'col' in the filter object has a period (e.g. service.name),
         * there's a need to parse the string to get the actual column, i.e. 'service'.
         *
         * After getting the actual column, use that for accessing the property
         * in the Request object that it's trying to filter given a 'val'.
         */
        const col = filter.col.split('.')[0];

        if (
          o[col].name
            .toString()
            .toLowerCase()
            .includes(filter.val.toString().toLowerCase()) === false
        )
          return false;
      } else {
        if (
          o[filter.col]
            .toString()
            .toLowerCase()
            .includes(filter.val.toString().toLowerCase()) === false
        ) {
          return false;
        }
      }
    }
    return true;
  });

  return filteredData;
}
// TODO: delete
export function sortData(data, sort) {
  let sortedData = { ...data };

  // for multiple sorting
  // 0 is the default sort value of {col: id, mod: DES }

  // TODO: create a condition for this 0 and 1. Sometimes it's in the 0 sometimes 1
  for (let i = 0; i < sort.length; i++) {
    let { col, mod } = sort[i];

    /**
     * This is because the parameter that lodash uses
     * is 'desc'. And the API on the other hand
     * uses 'DES'.
     */
    if (mod === 'DES') mod = 'desc';

    sortedData['data'] = orderBy(sortedData['data'], [col], [mod.toLowerCase()]);
  }

  return sortedData;
}
