function getRandomNumbersWithExclusion(lengthOfArray, indexesOfNotApproved, n) {
  // Got original algorithm from: https://stackoverflow.com/a/30627914/9352807
  // (but with a twist)

  /**
   * This is to generate an array of N random numbers where N is the number of
   * Random Controls per plate.
   *
   * The randomness condition here is that no random generated number
   * can repeat. And that the random generated number can be the
   * same as the indexes of those labels that are not 'Approved'
   * so as to not override the current label of the cell.
   */

  const randomNumbers = [];
  for (let i = 0; i < n; i++) {
    let rand = null; // an integer
    while (
      rand === null ||
      indexesOfNotApproved.includes(rand) ||
      randomNumbers.includes(rand)
    ) {
      rand = Math.round(Math.random() * (lengthOfArray - 1));
    }

    randomNumbers.push(rand);
  }

  return randomNumbers;
}

export const generateRandomControlsPerPlate = (
  plate,
  numberOfRandomControlsPerPlate,
  vendorControls,
) => {
  let indexesOfNotApproved = [];
  for (let i = 0; i < plate.length; i++) {
    if (plate[i] !== 'Approved' && !vendorControls.includes(i))
      indexesOfNotApproved.push(i);
  }

  // Indexes of cells that are not 'Approved'
  indexesOfNotApproved = [...indexesOfNotApproved, ...vendorControls];

  const randomNumbers = getRandomNumbersWithExclusion(
    plate.length,
    indexesOfNotApproved,
    numberOfRandomControlsPerPlate,
  );

  return randomNumbers;
};
