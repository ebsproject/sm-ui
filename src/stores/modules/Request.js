import { updateStatusRequest } from 'utils/apollo/CSclient';
import { getRequestWorkflow } from 'utils/other/Workflow';

export const initialState = {
  isLoading: false,
  data: [],
  selection: [],
  selectAll: false,
  error: null,
  mapSelection: new Map(),
};

export const START_REQUEST = 'Request/START_REQUEST';
export const LOAD_REQUEST = 'Request/LOAD_REQUEST';
export const REQUEST_SUCCESS = 'Request/REQUEST_SUCCESS';
export const ADD_SELECTION = 'Request/ADD_SELECTION';
export const ADD_SELECTION_ALL = 'Request/ADD_SELECTION_ALL';
export const VALIDATE_REQUEST = 'Request/VALIDATE_REQUEST';
export const INFORMATE_REQUEST = 'Request/INFORMATE_REQUEST';
export const REJECTED_REQUEST = 'Request/REJECTED_REQUEST';
export const BATCH_REQUEST = 'Request/BATCH_REQUEST';

export const startRequest = () => ({
  type: START_REQUEST,
});

export const loadRequest = (payload) => ({
  type: LOAD_REQUEST,
  payload,
});

export const requestSuccess = () => ({
  type: REQUEST_SUCCESS,
});

export const addToSelection = (payload) => ({
  type: ADD_SELECTION,
  payload,
});

export const addToSelectionAll = (payload, isSelectAll) => ({
  type: ADD_SELECTION_ALL,
  payload,
  isSelectAll,
});

export const validRequest = (row) => ({
  type: VALIDATE_REQUEST,
  row,
});

export const infoRequest = (row) => ({
  type: INFORMATE_REQUEST,
  row,
});

export const rejectRequest = (row) => ({
  type: REJECTED_REQUEST,
  row,
});

export const batchToRequest = (requests) => ({
  type: BATCH_REQUEST,
  requests,
});

export const getRequest = () => async (dispatch) => {
  dispatch(startRequest());
  try {
    var resolveValue = await getRequestWorkflow();
    dispatch(loadRequest(resolveValue));
    dispatch(requestSuccess());
  } catch (err) {
    throw new Error(err.message);
  }
};

export const addSelection = (select) => (dispatch) => {
  dispatch(addToSelection(select));
};

export const addSelectionAll = (selectAll, select) => (dispatch) => {
  dispatch(addToSelectionAll(select, selectAll));
};

export const validateRequest = (row) => async (dispatch) => {
  return new Promise((resolve, reject) => {
    updateStatusRequest(row, 18, 15);
    dispatch(validRequest(row));
  });
};

export const informateRequest = (row) => (dispatch) => {
  dispatch(infoRequest(row));
};

export const rejectedRequest = (row) => async (dispatch) => {
  return new Promise((resolve, reject) => {
    updateStatusRequest(row, 19, 15);
    dispatch(rejectRequest(row));
  });
};

export const nextStage = (requests) => async (dispatch) => {
  dispatch(batchToRequest(requests));
};

export default function RequestReducer(
  state = initialState,
  { type, payload, isSelectAll, row, requests },
) {
  switch (type) {
    case START_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case REQUEST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
      };
    case LOAD_REQUEST:
      return {
        ...state,
        data: payload,
        isLoading: false,
        error: null,
      };
    case ADD_SELECTION:
      return {
        ...state,
        selection: payload,
      };
    case ADD_SELECTION_ALL:
      return {
        ...state,
        selection: payload,
        selectAll: isSelectAll,
      };
    case VALIDATE_REQUEST:
      let newDataValid = [...state.data];
      const newRowValid = newDataValid.find((e) => row.id === e.id);
      newDataValid = newDataValid.filter((e) => row.id !== e.id);
      newRowValid['status'] = 'Approved';
      newDataValid.push(newRowValid);

      return {
        ...state,
        data: newDataValid,
      };

    case REJECTED_REQUEST:
      let newDataReject = [...state.data];
      const newRowReject = newDataReject.find((e) => row.id === e.id);
      newDataReject = newDataReject.filter((e) => row.id !== e.id);
      newRowReject['status'] = 'Rejected';
      newDataReject.push(newRowReject);
      return {
        ...state,
        data: newDataReject,
      };

    case BATCH_REQUEST:
      let newDataBatch = [...state.data];
      requests.forEach((request) => {
        newDataBatch = newDataBatch.filter(function (item) {
          return request.id !== item.id;
        });
      });

      return {
        ...state,
        data: newDataBatch,
        selection: [],
        mapSelection: new Map(),
      };
    default:
      return state;
  }
}
