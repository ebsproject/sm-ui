import { CSrest } from 'utils/axios/CSclient';
import { updateStatusRequest } from 'utils/apollo/CSclient';
import {
  createBatchWithRequest,
  getBatchList,
  getBatchCodeById,
  updateListRequestByBatchByID,
} from 'utils/apollo/SMclient';

export const initialState = {
  userProfile: null,
  batchName: '',
  isLoading: false,
  data: [],
  error: null,
  checkedRequestSelection: [],
  approvedRequests: [],
  totalSamplesInBatch: null,
  totalPlatesInBatch: null,
  selectedVendor: null,
  selectedTechnology: null,
  selectedAssay: null,
  selectedPlateSize: null,
  selectedBlanks: {},
  selectedPositiveControls: {},
  selectedCoordinates: [],
  batchCode: null,
  batchCodeObject: null,
  wellsGrid: null,
  cells: [],
  cellsLabelWithDirection: null,
  cellsLabel: [],
  cellsColorKey: [],
  passedPlateSize: null,
  selectedPlateFillDirection: null,
  index: 0,
  storeSelectedMarkerPanels: [],
  storeAvailableMarkerPanels: [],
  storeOriginalAvailableMarkerPanels: [],
  storedTabs: [],
  storedCells: [],
  storedCellsLabel: [],
  storedCellsColorKey: [],
  storedCellsLabelWithDirection: [],
  storedLoadingButtonsInPlateLayout: {
    loadingGeneratePlateLayout: false,
    loadingGeneratePlateSamples: false,
    loadingClearPlates: false,
  },
  selectedControlType: null,
  currentPlateIndex: 0,
  lastSampleNumber: null,
  storedRandomIndexes: [],
  wellsId: [],
  samplesListForEdit: [],
  openBlankControlTypeModal: false,
  openRandomControlTypeModal: false,
  stepTwoTechnology: {
    listBatchMarkerGroup: [],
    serviceProvider: null,
    technology: null,
    technologyServiceProvider: null,
    isViewOnly: false,
  },
  isDisableFormPlate: false,
};

export const BATCH_NAME = 'Request/BATCH_NAME';
export const START_BATCH = 'Batch/START_BATCH';
export const LOAD_BATCH = 'Batch/LOAD_BATCH';
export const BATCH_SUCCESS = 'Batch/BATCH_SUCCESS';
export const NEXT_STAGE = 'Batch/ACCEPT_NEXTSTAGE';
export const REJECTED_STAGE = 'Batch/REJECTED_STAGE';
export const SET_CHECKED_REQUEST_SELECTION = 'SET_CHECKED_REQUEST_SELECTION';
export const SET_APPROVED_REQUESTS = 'SET_APPROVED_REQUESTS';
export const SET_TOTAL_SAMPLES_IN_BATCH = 'SET_TOTAL_SAMPLES_IN_BATCH';
export const SET_TOTAL_PLATES_IN_BATCH = 'SET_TOTAL_PLATES_IN_BATCH';
export const SET_SELECTED_VENDOR = 'SET_SELECTED_VENDOR';
export const SET_SELECTED_TECHNOLOGY = 'SET_SELECTED_TECHNOLOGY';
export const SET_SELECTED_ASSAY = 'SET_SELECTED_ASSAY';
export const RESET_TECHNOLOGY = 'RESET_TECHNOLOGY';

export const SET_STORED_TABS = 'SET_STORED_TABS';
export const SET_LOADING_BUTTONS_IN_PLATE_LAYOUT =
  'SET_LOADING_BUTTONS_IN_PLATE_LAYOUT';
export const SET_SELECTED_CONTROL_TYPE = 'SET_SELECTED_CONTROL_TYPE';
export const SET_CURRENT_PLATE_INDEX = 'SET_CURRENT_PLATE_INDEX';
export const SET_LAST_SAMPLE_NUMBER = 'SET_LAST_SAMPLE_NUMBER';
export const SET_USER_PROFILE = 'SET_USER_PROFILE';
export const SET_BATCH_CODE_OBJECT = 'SET_BATCH_CODE_OBJECT';

export const RESET_VALUES = 'RESET_VALUES';
export const RESET_STATE_UPON_VENDOR_CHANGE = 'RESET_STATE_UPON_VENDOR_CHANGE';
export const RESET_PLATES = 'RESET_PLATES';
export const SET_SELECTED_BLANKS = 'SET_SELECTED_BLANKS';
export const SET_RANDOM_INDEXES = 'SET_RANDOM_INDEXES';

export const SET_SELECTED_COORDINATES = 'SET_SELECTED_COORDINATES';
export const SET_BATCH_NAME = 'SET_BATCH_NAME';
export const SET_WELLS_GRID = 'SET_WELLS_GRID';
export const SET_CELLS = 'SET_CELLS';
export const SET_CELLS_LABEL = 'SET_CELLS_LABEL';
export const SET_CELLS_COLORKEY = 'SET_CELLS_COLORKEY';
export const SET_WELLS_ID = 'SET_WELLS_ID';
export const SET_SELECTED_POSITIVE_CONTROLS = 'SET_SELECTED_POSITIVE_CONTROLS';
export const SET_SELECTED_PLATE_SIZE = 'SET_SELECTED_PLATE_SIZE';
export const SET_PASSED_PLATE_SIZE = 'SET_PASSED_PLATE_SIZE';
export const SET_SELECTED_PLATE_FILL_DIRECTION = 'SET_SELECTED_PLATE_FILL_DIRECTION';
export const SET_CELLS_LABEL_WITH_DIRECTION = 'SET_CELLS_LABEL_WITH_DIRECTION';
export const SET_TAB_INDEX = 'SET_TAB_INDEX';
export const SET_SELECTED_MARKER_PANELS = 'SET_SELECTED_MARKER_PANELS';
export const SET_AVAILABLE_MARKER_PANELS = 'SET_AVAILABLE_MARKER_PANELS';
export const SET_ORIGINAL_VAILABLE_MARKER_PANELS =
  'SET_ORIGINAL_VAILABLE_MARKER_PANELS';
export const SET_SAMPLES_LIST_FOR_EDIT = 'SET_SAMPLES_LIST_FOR_EDIT';

export const SET_OPEN_BLANK_CONTROL_TYPE_MODAL = 'SET_OPEN_BLANK_CONTROL_TYPE_MODAL';
export const SET_OPEN_RANDOM_CONTROL_TYPE_MODAL =
  'SET_OPEN_RANDOM_CONTROL_TYPE_MODAL';

export const SET_STEP_TWO_TECHNOLOGY = 'SET_STEP_TWO_TECHNOLOGY';

export const SET_SELECTED_PLATE_FILL_DIRECTION_EDIT =
  'SET_SELECTED_PLATE_FILL_DIRECTION_EDIT';

export const SET_EDIT_PLATE_CONTROL = 'SET_EDIT_PLATE_CONTROL';

export const nextStage = (row) => ({
  type: NEXT_STAGE,
  row,
});

export const rejectStage = (row) => ({
  type: REJECTED_STAGE,
  row,
});

export const setBatchName = (batchName) => ({
  type: BATCH_NAME,
  batchName,
});

export const startBatch = () => ({
  type: START_BATCH,
});

export const loadBatch = (payload) => ({
  type: LOAD_BATCH,
  payload,
});
export const batchSuccess = () => ({
  type: BATCH_SUCCESS,
});

export const nextStageBatch = (row) => async (dispatch) => {
  updateListRequestByBatchByID(row, 21, true);
  dispatch(nextStage(row));
};

export const rejectedSatge = (row) => async (dispatch) => {
  updateListRequestByBatchByID(row, 18, false);
  dispatch(rejectStage(row));
};
export const updateBatchName = (requestMap, idBatchCode) => async (dispatch) => {
  let intServiceCode = 0;
  let intPurpose = 0;
  let intArrProgram = [];
  var ProgramMapa = new Map();

  for (let [key, value] of requestMap) {
    ProgramMapa.set(value.ProgramCode_Id, value.ProgramCode_Id);
    intPurpose = value.PurposeCode_Id;
    intServiceCode = value.ServiceCode_Id;
  }
  let keys = Array.from(ProgramMapa.keys());
  var URL = '';
  URL = '/tenant/1/rules/3';
  await CSrest.post(URL, {
    servicecode: intServiceCode,
    program: keys,
    purpose: intPurpose,
  })
    .then((response) => {
      let code = response.data.code;
      let last_batch_number = 0;
      new Promise(() => {
        getBatchCodeById(1).then((response) => {
          last_batch_number = response.lastbatch_number + 1;
          dispatch(setBatchName(code + last_batch_number));
        });
      });
    })
    .catch((error) => {
      console.error('error... ', error);
      throw new Error(error.message);
    });
};

export const batchRequest =
  (requestMap, batchName, description, idBatchCode) => async (dispatch) => {
    let total = 0;
    for (let [key, value] of requestMap) {
      new Promise((resolve, reject) => {
        updateStatusRequest(value, 20, 16);
        total += parseInt(value.Total, 10);
      });
    }
    createBatchWithRequest(
      requestMap,
      batchName,
      description,
      1,
      2,
      total,
      1,
      idBatchCode,
    );
    //getBatchList(4, "false");
  };

export const getBatchListResult =
  (idServiceProvider) => async (dispatch, getState) => {
    dispatch(startBatch());
    try {
      var resolver = await getBatchList(idServiceProvider, 'false');
      dispatch(loadBatch(resolver));
      dispatch(batchSuccess());
    } catch (error) {
      throw new Error(error.message);
    }
  };

export default function RequestReducer(
  state = initialState,
  { type, payload, batchName, row },
) {
  switch (type) {
    case BATCH_NAME:
      return {
        ...state,
        batchName,
      };
    case START_BATCH:
      return {
        ...state,
        isLoading: true,
      };
    case BATCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
      };
    case LOAD_BATCH:
      return {
        ...state,
        data: payload,
        isLoading: false,
        error: null,
      };
    case NEXT_STAGE: {
      let newDataValid = [...state.data];
      newDataValid = newDataValid.filter((e) => row.id !== e.id);
      return {
        ...state,
        data: newDataValid,
        isDisableFormPlate: false,
      };
    }
    case REJECTED_STAGE: {
      let newDataReject = [...state.data];
      newDataReject = newDataReject.filter((e) => row.id !== e.id);
      return {
        ...state,
        data: newDataReject,
      };
    }

    case SET_USER_PROFILE:
      return {
        ...state,
        userProfile: payload,
      };

    case SET_BATCH_CODE_OBJECT:
      return {
        ...state,
        batchCodeObject: payload,
      };

    case SET_CHECKED_REQUEST_SELECTION:
      return {
        ...state,
        checkedRequestSelection: payload,
      };
    case SET_APPROVED_REQUESTS:
      return {
        ...state,
        approvedRequests: payload,
      };

    case SET_TOTAL_SAMPLES_IN_BATCH:
      return {
        ...state,
        totalSamplesInBatch: payload,
      };

    case SET_TOTAL_PLATES_IN_BATCH:
      return {
        ...state,
        totalPlatesInBatch: payload,
      };

    case SET_SELECTED_VENDOR:
      return {
        ...state,
        selectedVendor: payload,
      };

    case SET_SELECTED_TECHNOLOGY:
      return {
        ...state,
        selectedTechnology: payload,
      };

    case SET_SELECTED_ASSAY:
      return {
        ...state,
        selectedAssay: payload,
      };

    case SET_SELECTED_CONTROL_TYPE:
      return {
        ...state,
        selectedControlType: payload,
      };

    case SET_CURRENT_PLATE_INDEX:
      return {
        ...state,
        currentPlateIndex: payload,
      };

    case SET_STORED_TABS:
      return {
        ...state,
        storedTabs: payload,
      };

    case SET_LAST_SAMPLE_NUMBER:
      return {
        ...state,
        lastSampleNumber: payload,
      };

    case SET_LOADING_BUTTONS_IN_PLATE_LAYOUT:
      return {
        ...state,
        storedLoadingButtonsInPlateLayout: payload,
      };

    case SET_STEP_TWO_TECHNOLOGY:
      return {
        ...state,
        stepTwoTechnology: payload,
        isDisableFormPlate: false,
      };

    case RESET_TECHNOLOGY:
      return {
        ...state,
        stepTwoTechnology: initialState.stepTwoTechnology,
      };

    case RESET_VALUES:
      return {
        ...state,
        selectedVendor: null,
        selectedTechnology: null,
        selectedAssay: null,
        selectedPlateSize: null,
        storedCells: [],
        storedCellsLabel: [],
        storedCellsColorKey: [],
        storedCellsLabelWithDirection: [],
        storedTabs: [],
        storedLoadingButtonsInPlateLayout: {
          loadingGeneratePlateLayout: false,
          loadingGeneratePlateSamples: false,
          loadingClearPlates: false,
        },
        selectedControlType: null,
        currentPlateIndex: 0,
        selectedPlateFillDirection: null,
        lastSampleNumber: null,
        storedRandomIndexes: [],
        batchCodeObject: null,
        wellsId: [],
        samplesListForEdit: [],
        openBlankControlTypeModal: false,
        openRandomControlTypeModal: false,
        stepTwoTechnology: initialState.stepTwoTechnology,
        isDisableFormPlate: false,
      };

    case RESET_STATE_UPON_VENDOR_CHANGE:
      return {
        ...state,
        generatePlateLayouts: null,
        storedCells: [],
        storedCellsLabel: [],
        storedCellsColorKey: [],
        storedCellsLabelWithDirection: [],
        selectedPlateSize: null,
        selectedPlateFillDirection: null,
        currentPlateIndex: 0,
        storedTabs: [],
        storeSelectedMarkerPanels: [],
        storeAvailableMarkerPanels: [],
        storeOriginalAvailableMarkerPanels: [],
        selectedControlType: null,
        lastSampleNumber: null,
        storedRandomIndexes: [],
        wellsId: [],
        samplesListForEdit: [],
        isDisableFormPlate: false,
      };

    case RESET_PLATES:
      return {
        ...state,
        selectedPlateSize: null,
        storedTabs: [],
        storedCells: [],
        storedCellsLabel: [],
        storedCellsColorKey: [],
        storedCellsLabelWithDirection: [],
        selectedPlateFillDirection: null,
        selectedControlType: null,
        currentPlateIndex: 0,
        lastSampleNumber: null,
        storedRandomIndexes: [],
        wellsId: [],
        samplesListForEdit: [],
        openBlankControlTypeModal: false,
        openRandomControlTypeModal: false,
        isDisableFormPlate: false,
      };

    case SET_SELECTED_PLATE_SIZE:
      return {
        ...state,
        selectedPlateSize: payload,
      };

    case SET_SELECTED_BLANKS:
      return {
        ...state,
        selectedBlanks: payload,
        selectedPositiveControls: null,
      };

    // case SET_GENERAL_PLATE_LAYOUTS:
    //   return {
    //     ...state,
    //     generatePlateLayouts: payload,
    //   };

    case SET_SELECTED_COORDINATES:
      return {
        ...state,
        selectedCoordinates: payload,
      };

    case SET_BATCH_NAME:
      return {
        ...state,
        batchCode: payload,
      };

    case SET_WELLS_GRID:
      return {
        ...state,
        wellsGrid: payload,
      };

    case SET_CELLS:
      return {
        ...state,
        storedCells: payload,
      };

    case SET_CELLS_LABEL:
      return {
        ...state,
        storedCellsLabel: payload,
      };

    case SET_SELECTED_POSITIVE_CONTROLS:
      return {
        ...state,
        selectedBlanks: null,
        selectedPositiveControls: payload,
      };

    case SET_CELLS_COLORKEY:
      return {
        ...state,
        storedCellsColorKey: payload,
      };

    case SET_WELLS_ID:
      return {
        ...state,
        wellsId: payload,
      };

    case SET_PASSED_PLATE_SIZE:
      return {
        ...state,
        passedPlateSize: payload,
      };

    case SET_SELECTED_PLATE_FILL_DIRECTION:
      return {
        ...state,
        selectedPlateFillDirection: payload,
        isDisableFormPlate: true,
      };

    case SET_EDIT_PLATE_CONTROL:
      return {
        ...state,
        isDisableFormPlate: false,
      };

    case SET_SELECTED_PLATE_FILL_DIRECTION_EDIT:
      return {
        ...state,
        selectedPlateFillDirection: payload,
        isDisableFormPlate: false,
      };

    case SET_CELLS_LABEL_WITH_DIRECTION:
      return {
        ...state,
        storedCellsLabelWithDirection: payload,
      };

    case SET_TAB_INDEX:
      return {
        ...state,
        index: payload,
      };

    case SET_RANDOM_INDEXES:
      return {
        ...state,
        storedRandomIndexes: payload,
      };

    case SET_SELECTED_MARKER_PANELS:
      return {
        ...state,
        storeSelectedMarkerPanels: payload,
      };

    case SET_AVAILABLE_MARKER_PANELS:
      return {
        ...state,
        storeAvailableMarkerPanels: payload,
      };

    case SET_ORIGINAL_VAILABLE_MARKER_PANELS:
      return {
        ...state,
        storeOriginalAvailableMarkerPanels: payload,
      };

    case SET_SAMPLES_LIST_FOR_EDIT:
      return {
        ...state,
        samplesListForEdit: payload,
      };

    case SET_OPEN_BLANK_CONTROL_TYPE_MODAL:
      return {
        ...state,
        openBlankControlTypeModal: payload,
      };

    case SET_OPEN_RANDOM_CONTROL_TYPE_MODAL:
      return {
        ...state,
        openRandomControlTypeModal: payload,
      };

    default:
      return state;
  }
}
