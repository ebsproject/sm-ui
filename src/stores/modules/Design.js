import { getBatchList } from 'utils/other/SampleManager';
import {
  getLaboratoryReport,
  saveDesignBatchByID,
  getDesignListBatch,
} from 'utils/other/SampleManager';
/*
 Here goes initial state and properties to be changed
 */
export const initialState = {
  isLoading: false,
  data: [],
  error: null,
};
/*
 Action types
 */
export const START_DESIGN = 'Design/START_DESIGN';
export const LOAD_DESIGN = 'Design/LOAD_DESIGN';
export const DESIGN_SUCCESS = 'Design/DESIGN_SUCCESS';
export const CREATE_PLATES = 'Design/CREATE_PLATES';
export const CREATING_DESIGN_SUCESS = 'Design/CREATING_DESIGN_SUCESS';
export const CREATE_REPORT_DESIGN_SUCESS = 'Design/CREATE_REPORT_DESIGN_SUCESS';
/*
 Arrow function for change state
 */
export const startDesign = () => ({
  type: START_DESIGN,
});

export const loadDesign = (payload) => ({
  type: LOAD_DESIGN,
  payload,
});
export const designSuccess = () => ({
  type: DESIGN_SUCCESS,
});

export const createPlates = (row) => ({
  type: CREATE_PLATES,
  row,
});

export const savingDesignSucess = () => ({
  type: CREATING_DESIGN_SUCESS,
});
export const getBatchListResult =
  (idServiceProvider) => async (dispatch, getState) => {
    dispatch(startDesign());
    try {
      var resolver = await getDesignListBatch(0);
      dispatch(loadDesign(resolver));
      dispatch(designSuccess());
    } catch (error) {
      throw new Error(error.message);
    }
  };

export const createLaboratoryReport = (row) => async (dispatch) => {
  dispatch(startDesign());
  await getLaboratoryReport(row);
  dispatch(savingDesignSucess());
};

export const saveDesignBatch =
  (idBatch, idPlateDirection, idControlVendor, idcollection) => async (dispatch) => {
    dispatch(startDesign());
    var resolve = await saveDesignBatchByID(
      idBatch,
      idPlateDirection,
      idControlVendor,
      idcollection,
    );
    dispatch(createPlates(resolve.data));
    var resolver = await getDesignListBatch(0);
    dispatch(loadDesign(resolver));
    dispatch(savingDesignSucess());
  };

/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload, row }) {
  switch (type) {
    case START_DESIGN:
      return {
        ...state,
        isLoading: true,
      };
    case DESIGN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
      };
    case LOAD_DESIGN:
      return {
        ...state,
        data: payload,
        isLoading: false,
        error: null,
      };
    case CREATE_PLATES: {
      let newData = [...state.data];
      const newRow = newData.find((e) => row.id === e.id);

      newData = newData.filter((e) => row.id !== e.id);
      newRow['lastPlate'] = row.lastPlate;
      newRow['fistPlate'] = row.fistPlate;
      newRow['numcontainers'] = row.numcontainers;
      let mapDesign = newData.map((u) => (u.id !== row.id ? u : newRow));
      let arrayDesign = Array.from(mapDesign.keys());
      return {
        ...state,
        data: arrayDesign,
      };
    }
    case CREATING_DESIGN_SUCESS:
      return {
        ...state,
        isLoading: false,
        error: null,
      };

    default:
      return state;
  }
}
