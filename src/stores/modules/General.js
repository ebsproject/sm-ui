// THIS MODULE WILL MANAGEMENT ALL GENERAL ACTIONS LIKE NOTIFICATIONS AND GENERAL MESSAGE DIALOGS
/*
 Initial state and properties
 */
export const initialState = {
  // Notifications message state
  open: false,
  message: null,
  translationId: null,
  severity: null,
  strongMessage: null,
};
/*
 Action types
 */
export const ALERT_MESSAGE = 'ALERT_MESSAGE';
export const CLOSE_ALERT = 'CLOSE_ALERT';
/*
 Reducer to describe how the state changed
 */
export default function Reducer(state = initialState, { type, payload }) {
  switch (type) {
    case ALERT_MESSAGE:
      return {
        ...state,
        open: true,
        message: payload.message,
        translationId: payload.translationId,
        severity: payload.severity,
        strongMessage: payload.strongMessage,
      };
    case CLOSE_ALERT:
      return {
        ...state,
        open: false,
      };
    default:
      return state;
  }
}
