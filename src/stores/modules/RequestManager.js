export const initialState = {
  entryListTabData: [],
  requestList: [],
  programList: [],
  isDatabaseUpdated: false,
  selectedEntryList: null,
  toEditRowData: null,
  mode: null,
  requestCode: null,
  totalNumberOfEntries: null,
  userId: null,
  userProfileStorage: null,
  serviceProviderId: null,
  ebsFormId: null,
  markerPanel: null,
  markerPanelList: [],

  // Basic Tab values
  tissueTypeId: null,
  cropId: null,
  cropName: null,
  programId: null,
  programName: null,
  cimmyt: null,
  irri: null,
  provider: null,
  requesterEmail: null,
  requester: null,
  germplasmOwnerEmail: null,
  germplasmOwner: null,
  entryUserName: null,
  entryUserEmail: null,
  basicTabData: null,
  adminContactId: null,
  customFields: [],
  numberOfSamplesPerItem: null,

  // Service tab values
  serviceProviderData: {},
};

// Action Types
export const SET_ADMIN_CONTACT_ID = 'SET_ADMIN_CONTACT_ID';
export const SET_BASIC_TAB_DATA = 'SET_BASIC_TAB_DATA';
export const SET_CIMMYT_DATA = 'SET_CIMMYT_DATA';
export const SET_CROP_ID = 'SET_CROP_ID';
export const SET_CROP_NAME = 'SET_CROP_NAME';
export const SET_CUSTOM_FIELDS = 'SET_CUSTOM_FIELDS';
export const SET_ENTRY_LIST_TAB_DATA = 'SET_ENTRY_LIST_TAB_DATA';
export const SET_EBS_FORM_ID = 'SET_EBS_FORM_ID';
export const SET_IRRI_DATA = 'SET_IRRI_DATA';
export const SET_IS_DATABASE_UPDATED = 'SET_IS_DATABASE_UPDATED';
export const SET_MODE = 'SET_MODE';
export const SET_MARKER_PANEL = 'SET_MARKER_PANEL';
export const SET_MARKER_PANEL_LIST = 'SET_MARKER_PANEL_LIST';
export const SET_PROGRAM_ID = 'SET_PROGRAM_ID';
export const SET_PROGRAM_NAME = 'SET_PROGRAM_NAME';
export const SET_PROGRAM_LIST = 'SET_PROGRAM_LIST';
export const SET_PROVIDER = 'SET_PROVIDER';
export const SET_REQUEST_CODE = 'SET_REQUEST_CODE';
export const SET_REQUEST_LIST = 'SET_REQUEST_LIST';
export const SET_ENTRY_USER_EMAIL = 'SET_ENTRY_USER_EMAIL';
export const SET_ENTRY_USER_NAME = 'SET_ENTRY_USER_NAME';
export const SET_REQUEST_VALUES = 'SET_REQUEST_VALUES';
export const SET_REQUESTER = 'SET_REQUESTER';
export const SET_REQUESTER_EMAIL = 'SET_REQUESTER_EMAIL';
export const SET_GERMPLASM_OWNER = 'SET_GERMPLASM_OWNER';
export const SET_GERMPLASM_OWNER_EMAIL = 'SET_GERMPLASM_OWNER_EMAIL';
export const SET_SELECTED_ENTRY_LIST = 'SET_SELECTED_ENTRY_LIST';
export const SET_SERVICE_PROVIDER_DATA = 'SET_SERVICE_PROVIDER_DATA';
export const SET_SERVICE_LABORATORY_ID = 'SET_SERVICE_LABORATORY_ID';
export const SET_TISSUE_TYPE_ID = 'SET_TISSUE_TYPE_ID';
export const SET_TISSUE_TYPE_NAME = 'SET_TISSUE_TYPE_NAME';
export const SET_TO_EDIT_ROW_DATA = 'SET_TO_EDIT_ROW_DATA';
export const SET_TO_EDIT_ROW_DATA_REFORMATED = 'SET_TO_EDIT_ROW_DATA_REFORMATED';
export const SET_TOTAL_NUMBER_OF_ENTRIES = 'SET_TOTAL_NUMBER_OF_ENTRIES';
export const SET_USER_ID = 'SET_USER_ID';
export const SET_USER_PROFILE_STORAGE = 'SET_USER_PROFILE_STORAGE';
export const SET_PROGRAM_FULL_DETAIL = 'SET_PROGRAM_FULL_DETAIL';
export const SET_NUMBER_OF_SAMPLES_PER_ITEM = 'SET_NUMBER_OF_SAMPLES_PER_ITEM';

export default function RequestManagerReducer(
  state = initialState,
  { type, payload },
) {
  switch (type) {
    case SET_REQUEST_LIST:
      return {
        ...state,
        requestList: payload,
      };

    case SET_EBS_FORM_ID:
      return {
        ...state,
        ebsFormId: payload,
      };

    case SET_IS_DATABASE_UPDATED:
      return {
        ...state,
        isDatabaseUpdated: payload,
      };

    case SET_ENTRY_LIST_TAB_DATA:
      return {
        ...state,
        entryListTabData: payload,
      };

    case SET_SELECTED_ENTRY_LIST:
      return {
        ...state,
        selectedEntryList: payload,
      };

    case SET_TO_EDIT_ROW_DATA:
      return {
        ...state,
        toEditRowData: payload,
      };

    case SET_TO_EDIT_ROW_DATA_REFORMATED:
      return {
        ...state,
        toEditRowData: { ...state.toEditRowData, reformatedCustomFields: payload },
      };

    case SET_MODE:
      return {
        ...state,
        mode: payload,
      };

    case SET_CROP_ID:
      return {
        ...state,
        cropId: payload,
      };

    case SET_TISSUE_TYPE_ID:
      return {
        ...state,
        tissueTypeId: payload,
      };

    case SET_TISSUE_TYPE_NAME:
      return {
        ...state,
        tissueTypeName: payload,
      };
    case SET_CROP_NAME:
      return {
        ...state,
        cropName: payload,
      };

    case SET_PROGRAM_ID:
      return {
        ...state,
        programId: payload,
      };

    case SET_PROGRAM_NAME:
      return {
        ...state,
        programName: payload,
      };

    case SET_PROGRAM_LIST:
      return {
        ...state,
        programList: payload,
      };

    case SET_CIMMYT_DATA:
      return {
        ...state,
        cimmyt: payload,
      };

    case SET_IRRI_DATA:
      return {
        ...state,
        irri: payload,
      };

    case SET_REQUESTER_EMAIL:
      return {
        ...state,
        requesterEmail: payload,
      };

    case SET_CUSTOM_FIELDS:
      return {
        ...state,
        customFields: payload,
      };
    case SET_MARKER_PANEL:
      return {
        ...state,
        markerPanel: payload,
      };
    case SET_MARKER_PANEL_LIST:
      return {
        ...state,
        markerPanelList: payload,
      };
    case SET_PROVIDER:
      return {
        ...state,
        provider: payload,
      };

    case SET_REQUESTER:
      return {
        ...state,
        requester: payload,
      };

    case SET_GERMPLASM_OWNER:
      return {
        ...state,
        germplasmOwner: payload,
      };

    case SET_GERMPLASM_OWNER_EMAIL:
      return {
        ...state,
        germplasmOwnerEmail: payload,
      };

    case SET_REQUEST_CODE:
      return {
        ...state,
        requestCode: payload,
      };

    case SET_ENTRY_USER_NAME:
      return {
        ...state,
        entryUserName: payload,
      };

    case SET_ENTRY_USER_EMAIL:
      return {
        ...state,
        entryUserEmail: payload,
      };

    case SET_BASIC_TAB_DATA:
      return {
        ...state,
        basicTabData: payload,
      };

    case SET_SERVICE_PROVIDER_DATA:
      return {
        ...state,
        serviceProviderData: payload,
      };

    case SET_SERVICE_LABORATORY_ID:
      return {
        ...state,
        serviceProviderId: payload,
      };

    case SET_TOTAL_NUMBER_OF_ENTRIES:
      return {
        ...state,
        totalNumberOfEntries: payload,
      };

    case SET_ADMIN_CONTACT_ID:
      return {
        ...state,
        adminContactId: payload,
      };

    case SET_NUMBER_OF_SAMPLES_PER_ITEM:
      return {
        ...state,
        numberOfSamplesPerItem: payload,
      };

    case SET_PROGRAM_FULL_DETAIL:
      return {
        ...state,
        programId: payload.programId,
        programName: payload.programName,
        cropId: payload.cropId,
      };

    case SET_REQUEST_VALUES:
      return {
        ...state,
        selectedEntryList: payload,
        toEditRowData: payload,
        mode: payload,
        requestCode: payload,
        totalNumberOfEntries: payload,
        cropName: payload,
        programName: payload,
        cimmyt: payload,
        irri: payload,
        provider: payload,
        basicTabData: payload,
        customFields: payload,
        serviceProviderData: {},
      };

    case SET_USER_ID:
      return {
        ...state,
        userId: payload,
      };

    case SET_USER_PROFILE_STORAGE:
      return {
        ...state,
        userProfileStorage: payload,
      };

    default:
      return state;
  }
}
