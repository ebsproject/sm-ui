import { combineReducers } from 'redux'

import user from './modules/UserState'
import request from './modules/Request'
import request_batch from './modules/RequestBatch'
import design from './modules/Design'
import general from './modules/General'
// import batch from 'components/BatchTable/BatchState'
import catalogs from './modules/Catalogs'
import RequestManager from './modules/RequestManager'

export default combineReducers({
  user,
  catalogs,
  request,
  request_batch,
  design,
  general,
  RequestManager,
})
